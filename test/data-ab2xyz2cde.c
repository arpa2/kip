/* make two different paths A -> X -> A and B -> Y -> B and require A, B, X and Y all differ
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#define DEBUG 1
#undef NDEBUG

#include <assert.h>

#include <stdlib.h>
#include <stdio.h>

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <krb5.h>

#include <arpa2/kip.h>

#include <errno.h>
#include <com_err.h>


void print_block (char *descr, uint8_t *blk, uint32_t blklen, bool skip) {
	int i;
	printf ("%s =", descr);
	for (i=0; i<blklen; i++) {
		printf (" %02x", blk [i]);
		if (skip && (i > 5) && (i < blklen - 5)) {
			printf (" ...");
			i = blklen - 5;
		}
	}
	printf (" -> #%d\n", blklen);
}


int main (int argc, char *argv []) {
	assert (argc > 0);
	//
	// Open a context for KIP
	kipt_ctx kip;
	assert (kip_init ());
	assert (kipctx_open (&kip));
	//
	// Construct initial data
	uint8_t A_in [256];
	uint8_t B_in [256];
	uint32_t A_len = sizeof (A_in);
	uint32_t B_len = sizeof (B_in);
	printf ("A_len = %d, B_len = %d\n", A_len, B_len);
	assert (kipdata_random (kip, A_len, A_in));
	assert (kipdata_random (kip, B_len, B_in));
	print_block ("A_in", A_in, A_len, true);
	print_block ("B_in", B_in, B_len, true);
	//
	// Create two keys, K and L -- and a third, M
	kipt_keyid K_id;
	kipt_keyid L_id;
	kipt_keyid M_id;
	assert (kipkey_generate (kip, 17 /* aes128-cts-hmac-sha1-96 */, 11, &K_id));
	assert (kipkey_generate (kip, 17 /* aes128-cts-hmac-sha1-96 */, 12, &L_id));
	assert (kipkey_generate (kip, 18 /* aes256-cts-hmac-sha1-96 */, 13, &M_id));
	//
	// Find the desired length for encrypting A--[K]-->X and B--[L]-->Y
	// as well as the different B--[M]-->Z
	// we could actually make many more combinations here...
	uint32_t X_len;
	uint32_t Y_len;
	uint32_t Z_len;
	assert ((kipdata_up (kip, K_id, NULL, A_len, NULL, 0, &X_len) == false) && (errno == KIPERR_OUTPUT_SIZE));
	assert ((kipdata_up (kip, L_id, NULL, B_len, NULL, 0, &Y_len) == false) && (errno == KIPERR_OUTPUT_SIZE));
	assert ((kipdata_up (kip, M_id, NULL, B_len, NULL, 0, &Z_len) == false) && (errno == KIPERR_OUTPUT_SIZE));
	printf ("X_len = %d, Y_len = %d, Z_len = %d\n", X_len, Y_len, Z_len);
	assert (X_len == Y_len);
	assert (Y_len <= Z_len);
	//
	// Allocate memory (on the stack) for X and Y -- and for Z
	uint8_t X_mud [X_len];
	uint8_t Y_mud [Y_len];
	uint8_t Z_mud [Z_len];
	assert (kipdata_up (kip, K_id, A_in, A_len, X_mud, X_len, &X_len));
	assert (kipdata_up (kip, L_id, B_in, B_len, Y_mud, Y_len, &Y_len));
	assert (kipdata_up (kip, M_id, B_in, B_len, Z_mud, Z_len, &Z_len));
	assert (sizeof (X_mud) >= X_len);
	assert (sizeof (Y_mud) >= Y_len);
	assert (sizeof (Z_mud) >= Z_len);
	//
	// Find the desired length for decrypting X--[K]-->C and Y--[L]-->D
	// as well as the different Z--[M]-->E
	uint32_t C_len;
	uint32_t D_len;
	uint32_t E_len;
	assert ((kipdata_down (kip, K_id, X_mud, X_len, NULL, 0, &C_len) == false) && (errno == KIPERR_OUTPUT_SIZE));
	assert ((kipdata_down (kip, L_id, Y_mud, Y_len, NULL, 0, &D_len) == false) && (errno == KIPERR_OUTPUT_SIZE));
	assert ((kipdata_down (kip, M_id, Z_mud, Z_len, NULL, 0, &E_len) == false) && (errno == KIPERR_OUTPUT_SIZE));
	printf ("C_len = %d, D_len = %d, E_len = %d\n", C_len, D_len, E_len);
	assert (C_len == D_len);
	assert (D_len <= E_len);
	//
	// Allocate memory (on the stack) for C and D -- and for E
	uint8_t C_out [C_len];
	uint8_t D_out [D_len];
	uint8_t E_out [E_len];
	assert (kipdata_down (kip, K_id, X_mud, X_len, C_out, C_len, &C_len));
	assert (kipdata_down (kip, L_id, Y_mud, Y_len, D_out, D_len, &D_len));
	assert (kipdata_down (kip, M_id, Z_mud, Z_len, E_out, E_len, &E_len));
	assert (sizeof (C_out) >= C_len);
	assert (sizeof (D_out) >= D_len);
	assert (sizeof (E_out) >= E_len);
	assert (C_len == A_len);
	assert (D_len == B_len);
	assert (E_len == B_len);
	//
	// TODO: Consider measuring and printing bits different
	//
	// Check whether strings are same/different as required
	assert (A_len == C_len);
	assert (B_len == D_len);
	assert (B_len == E_len);
	assert (memcmp (A_in, C_out, C_len) == 0);
	assert (memcmp (B_in, D_out, D_len) == 0);
	assert (memcmp (B_in, E_out, E_len) == 0);
	assert (memcmp (C_out, D_out, C_len) != 0);
	//
	// TODO: Consider using memmem() for short/long comparison
	//
	// Nothing failed, so return success
	printf ("All operations were successful...\n");
	print_block ("A_in ", A_in , A_len, true);
	print_block ("C_out", C_out, C_len, true);
	print_block ("X_mud", X_mud, X_len, true);
	printf ("...and...\n");
	print_block ("B_in ", B_in , B_len, true);
	print_block ("D_out", D_out, D_len, true);
	print_block ("E_out", E_out, E_len, true);
	print_block ("Y_mud", Y_mud, Y_len, true);
	print_block ("Z_mud", Z_mud, Z_len, true);
	printf ("...where A_in --[K]--> X_mud --[~K]--> C_out\n         B_in --[L]--> Y_mud --[~L]--> D_out\n         B_in --[M]--> Z_mud --[~M]--> E_out\n");
	kipctx_close (kip);
	kip_fini ();
	exit (0);
}
