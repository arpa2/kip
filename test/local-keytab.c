/* Context-to-Context transport of keys and data, locally.
 *
 * Data can always travel free, so that part is trivial.  But how
 * do we move keys?  The answer is, we always need a common key
 * to derive our work from.  And for that, we need to move keys!
 *
 * What we need is the simplest possible extension relative to the
 * kipkey_tomap() and kipkey_frommap() calls, namely a way to
 * retrieve literal keys or keys that are encrypted to a key in a
 * context (that is provided literally, if it is to be networked).
 * This is the test for that minimal extension.
 *
 * The work used here is bound to extend until it is a KIP Service,
 * with everything it encompasses (TLS, SASL, and so on).  But this
 * one test will also work in the simplest of environments.  It
 * uses an "internal" API call that loads keys from a keytab into
 * the context, just like a KIP Service will one day do.  This is
 * what the service-keytab.c program does.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#define DEBUG 1
#undef NDEBUG

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <errno.h>
#include <com_err.h>

#include <krb5.h>

#include <arpa2/kip.h>



void print_block (char *descr, uint8_t *blk, uint32_t blklen, bool skip) {
	int i;
	printf ("%s =", descr);
	for (i=0; i<blklen; i++) {
		printf (" %02x", blk [i]);
		if (skip && (i > 5) && (i < blklen - 5)) {
			printf (" ...");
			i = blklen - 5;
		}
	}
	printf (" -> #%d\n", blklen);
}


int main (int argc, char *argv []) {
	//
	// Process the parameter into a keytab
	if (argc != 2) {
		printf ("Usage: %s FILE:name.keytab\n", argv [0]);
		exit (1);
	}
	char *ktname = argv [1];
	//
	// Open a first context for KIP, representing the sender
	kipt_ctx kip1;
	assert (kip_init ());
	assert (kipctx_open (&kip1));
	//
	// Load the master key into the first context
	kipt_keyid master1;
	assert (kipkey_fromkeytab (kip1, ktname,
			NULL, NULL, "unicorn.demo.arpa2.org",
			2019, ENCTYPE_AES256_CTS_HMAC_SHA384_192,
			&master1));
	printf ("Master key identity in sender: %lx\n", master1);
	//
	// Generate random data A_in and B_in
	uint8_t A_in [256];
	uint8_t B_in [256];
	uint32_t A_len = sizeof (A_in);
	uint32_t B_len = sizeof (B_in);
	printf ("A_len = %d, B_len = %d\n", A_len, B_len);
	assert (kipdata_random (kip1, A_len, A_in));
	assert (kipdata_random (kip1, B_len, B_in));
	print_block ("A_in", A_in, A_len, true);
	print_block ("B_in", B_in, B_len, true);
	//
	// Generate a key K_in
	kipt_keyid K_in;
	assert (kipkey_generate (kip1, 18 /* aes256-cts-hmac-sha1-96 */, 13, &K_in));
	//
	// Encrypt K_in under the master@1 key
	uint32_t Y_keymudlen;
	kipt_keyid K_mapids [1] = { master1 };
	assert ((kipkey_tomap (kip1, K_in, 1, K_mapids, NULL,     0,           &Y_keymudlen) == false) && (errno = KIPERR_OUTPUT_SIZE));
	uint8_t  Y_keymud [Y_keymudlen];
	assert ( kipkey_tomap (kip1, K_in, 1, K_mapids, Y_keymud, Y_keymudlen, &Y_keymudlen));
	//
	// Encrypt A_in-->X under master@1, B_in-->Z under K_in
	uint32_t X_mudlen;
	uint32_t Z_mudlen;
	assert ((kipdata_up (kip1, master1, NULL, A_len, NULL,  0,     &X_mudlen) == false) && (errno == KIPERR_OUTPUT_SIZE));
	assert ((kipdata_up (kip1, K_in,    NULL, B_len, NULL,  0,     &Z_mudlen) == false) && (errno == KIPERR_OUTPUT_SIZE));
	uint8_t X_mud [X_mudlen];
	uint8_t Z_mud [Z_mudlen];
	assert ( kipdata_up (kip1, master1, A_in, A_len, X_mud, X_mudlen, &X_mudlen));
	assert ( kipdata_up (kip1, K_in,    B_in, B_len, Z_mud, Z_mudlen, &Z_mudlen));
	//
	// Switch contexts
	printf ("Data and keys have been encrypted as shown below:\n");
	print_block ("X", X_mud,       X_mudlen, true);
	print_block ("Y", Y_keymud, Y_keymudlen, true);
	print_block ("Z", Z_mud,       Z_mudlen, true);
	printf ("Collected data in X, Y and Z -- destroying context 1 (sender)\n");
	kipctx_close (kip1);
	//
	// Start again, open the new context for the receiver
	kipt_ctx kip2;
	assert (kipctx_open (&kip2));
	//
	// Load the master key into the first context
	kipt_keyid master2;
	assert (kipkey_fromkeytab (kip2, ktname,
			NULL, NULL, "unicorn.demo.arpa2.org",
			2019, ENCTYPE_AES256_CTS_HMAC_SHA384_192,
			&master2));
	krb5_kvno master2_keynr = (krb5_kvno) master2;  /* Trim to wire size */
	printf ("Master key identity in receiver: %lx (should be the same as in the server)\n", master2);
	printf ("Master key numer as received: %x (should be the same as in the server)\n", master2_keynr);
	//TODO// How dependent are we on static key identities?  At least use key numbers!
	//
	// Reconstruct key K_out from Y_keymud
	kipt_keyid K_out;
	assert (kipkey_frommap (kip2, Y_keymud, Y_keymudlen, &K_out));
	//
	// Decrypt X_mud to A_out under master2_keynr, and Z_mud to B_out under K_out
	uint8_t A_out [X_mudlen];
	uint8_t B_out [Z_mudlen];
	uint32_t A_outlen = X_mudlen;
	uint32_t B_outlen = Z_mudlen;
	assert (kipdata_down (kip2, master2_keynr, X_mud, X_mudlen, A_out, A_outlen, &A_outlen));
	assert (kipdata_down (kip2, K_out,         Z_mud, Z_mudlen, B_out, B_outlen, &B_outlen));
	print_block ("A_out", A_out, A_outlen, true);
	print_block ("B_out", B_out, B_outlen, true);
	//
	// Now compare if A_in equals A_out, and if B_in equals B_out
	assert (A_len == A_outlen);
	assert (B_len == B_outlen);
	assert (memcmp (A_in, A_out, A_len) == 0);
	assert (memcmp (B_in, B_out, B_len) == 0);
	//
	// Close down
	kipctx_close (kip2);
	kip_fini ();
	//
	// Conclude
	printf ("Tested equal outcomes for encrypted context-to-context transfers:\n");
	printf (" - simple data:  A_in --[ M@1 ]--> X@1 --> X@2 --[ M@2 ]--> A_out\n");
	printf (" - session key:  K_in --[ M@1 ]--> Y@1 --> X@2 --[ M@2 ]--> K_out\n");
	printf (" - keying data:  B_in --[ K1  ]--> Z@1 --> Z@2 --[ K2  ]--> B_Out\n");
}
