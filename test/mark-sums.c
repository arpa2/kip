/* Fork checksums, and assure the same result.
 *
 * Start a checksum, insert A, then fork and insert B
 * in both.  One should be able to validate the other.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#define DEBUG 1
#undef NDEBUG

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>

#include <errno.h>
#include <com_err.h>

#include <arpa2/kip.h>



int main (int argc, char *argv []) {
	assert (argc > 0);
	//
	// Create context
	kipt_ctx kip;
	assert (kip_init ());
	assert (kipctx_open (&kip));
	//
	// Create two random sequences, A and B
	uint8_t A [256];
	uint8_t B [256];
	uint32_t Alen = sizeof (A);
	uint32_t Blen = sizeof (B);
	kipdata_random (kip, Alen, A);
	kipdata_random (kip, Blen, B);
	//
	// Introduce one key K
	kipt_keyid K;
	assert (kipkey_generate (kip, 17 /* aes128-cts-hmac-sha1-96 */, 1, &K));
	//
	// Introduce checksums Q, R, S
	kipt_sumid Q;
	kipt_sumid R;
	kipt_sumid S;
	assert (kipsum_start (kip, K, &Q));
	assert (kipsum_start (kip, K, &R));
	assert (kipsum_start (kip, K, &S));
	//
	// Append value A to all three checksums
	assert (kipsum_append (kip, Q, A, Alen));
	assert (kipsum_append (kip, R, A, Alen));
	assert (kipsum_append (kip, S, A, Alen));
	//
	// Insert different marks in Q en R, none in S
	assert (kipsum_mark (kip, Q, "Q en Q"));
	assert (kipsum_mark (kip, R, "Rock and Roll"));
	//
	// Append value B to all three checksums
	assert (kipsum_append (kip, Q, B, Blen));
	assert (kipsum_append (kip, R, B, Blen));
	assert (kipsum_append (kip, S, B, Blen));
	//
	// Produce the checksums for Q, R and S
	uint8_t Qsum [1024];
	uint8_t Rsum [1024];
	uint8_t Ssum [1024];
	uint32_t Qsumsz = sizeof (Qsum);
	uint32_t Rsumsz = sizeof (Rsum);
	uint32_t Ssumsz = sizeof (Ssum);
	uint32_t Qsumlen;
	uint32_t Rsumlen;
	uint32_t Ssumlen;
	assert (kipsum_sign (kip, Q, Qsumsz, &Qsumlen, Qsum));
	assert (kipsum_sign (kip, R, Rsumsz, &Rsumlen, Rsum));
	assert (kipsum_sign (kip, S, Ssumsz, &Ssumlen, Ssum));
	//
	// The signatures should not cross-check
	assert ( kipsum_verify (kip, Q, Qsumlen, Qsum));
	assert (!kipsum_verify (kip, Q, Rsumlen, Rsum));
	assert (!kipsum_verify (kip, Q, Ssumlen, Ssum));
	assert (!kipsum_verify (kip, R, Qsumlen, Qsum));
	assert ( kipsum_verify (kip, R, Rsumlen, Rsum));
	assert (!kipsum_verify (kip, R, Ssumlen, Ssum));
	assert (!kipsum_verify (kip, S, Qsumlen, Qsum));
	assert (!kipsum_verify (kip, S, Rsumlen, Rsum));
	assert ( kipsum_verify (kip, S, Ssumlen, Ssum));
	//
	// Close off
	kipctx_close (kip);
	kip_fini ();
	exit (0);
}
