#!/usr/bin/env python3
#
# Quick SASL for Python -- PLAIN mechanism test
#
# From: Rick van Rein <rick@openfortress.nl>


mechname = 'PLAIN'
print ('\n### MECHANISM %s' % (mechname,))


import quicksasl


cli = quicksasl.QuickSASL ('http', client=True )
srv = quicksasl.QuickSASL ('http', client=False)

print ('\n### CLIENT STEP')

cli.set_mech (mechname)
cli.set_client_realm ('arpa2.org')
cli.set_clientuser_login ('demo')
cli.set_clientuser_acl ('demo+ali')

(c2s,mech)  = cli.step_client (s2c=None, extra=None)

print ('C: mech=%r, c2s=%r' % (mech,c2s))
print ('C.state := %d' % (cli.get_state (),))
assert cli.get_state () != -1

print ('\n### SERVER STEP')

srv.set_mech (mechname)

srv.set_client_realm ('arpa2.org')
srv.set_clientuser_login ('demo')

(s2c,extra) = srv.step_server (c2s)

print ('S: s2c=%r, extra=%r' % (s2c,extra))
print ('S.state := %d' % (srv.get_state (),))
assert srv.get_state () != -1

assert cli.get_state () == 1
assert srv.get_state () == 1
