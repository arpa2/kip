/* keymixing.c -- Tests of the KIP key mixing operation.
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#define DEBUG 1
#undef NDEBUG

#include <stdlib.h>
#include <assert.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <krb5.h>

#include <errno.h>
#include <com_err.h>

#include <arpa2/kip.h>


void print_block (char *descr, uint8_t *blk, uint32_t blklen, bool skip) {
	int i;
	printf ("%s =", descr);
	for (i=0; i<blklen; i++) {
		printf (" %02x", blk [i]);
		if (skip && (i > 5) && (i < blklen - 5)) {
			printf (" ...");
			i = blklen - 5;
		}
	}
	printf (" -> #%d\n", blklen);
}


int main (int argc, char **argv) {
	//
	// Sanity checks on the commandline args
	assert (argc > 0);
	//
	// Open a context for KIP
	kipt_ctx kip;
	assert (kip_init ());
	assert (kipctx_open (&kip));
	//
	// Create three salts, X, Y and Z
	uint8_t X_in [256];
	uint8_t Y_in [256];
	uint8_t Z_in [512];
	uint32_t X_len = sizeof (X_in);
	uint32_t Y_len = sizeof (Y_in);
	uint32_t Z_len = sizeof (Z_in);
	printf ("X_len = %d, Y_len = %d, Z_len = %d\n", X_len, Y_len, Z_len);
	assert (kipdata_random (kip, X_len, X_in));
	assert (kipdata_random (kip, Y_len, Y_in));
	assert (kipdata_random (kip, Z_len, Z_in));
	print_block ("X_in", X_in, X_len, true);
	print_block ("Y_in", Y_in, Y_len, true);
	print_block ("Z_in", Z_in, Z_len, true);
	//
	// Create three keys, K, L and M
	kipt_keyid K_id;
	kipt_keyid L_id;
	kipt_keyid M_id;
	assert (kipkey_generate (kip, 17 /* aes128-cts-hmac-sha1-96    */, 11, &K_id));
	assert (kipkey_generate (kip, 17 /* aes128-cts-hmac-sha1-96    */, 12, &L_id));
	assert (kipkey_generate (kip, 18 /* aes256-cts-hmac-sha256-128 */, 13, &M_id));
	//
	// Prepare for key mixing: K/A, K/A+L/B, K/A+M/C, M/C+K/A, K/A+L/B+M/C
	const kipt_keyid keys_K [] = { K_id };
	const kipt_keyid keys_KL [] = { K_id, L_id };
	const kipt_keyid keys_KM [] = { K_id, M_id };
	const kipt_keyid keys_MK [] = { M_id, K_id };
	const kipt_keyid keys_KLM [] = { K_id, L_id, M_id };
	const uint8_t *salts_X [] = { X_in };
	const uint8_t *salts_XY [] = { X_in, Y_in };
	const uint8_t *salts_XZ [] = { X_in, Z_in };
	const uint8_t *salts_ZX [] = { Z_in, X_in };
	const uint8_t *salts_XYZ [] = { X_in, Y_in, Z_in };
	const uint32_t saltlens_X [] = { X_len };
	const uint32_t saltlens_XY [] = { X_len, Y_len };
	const uint32_t saltlens_XZ [] = { X_len, Z_len };
	const uint32_t saltlens_ZX [] = { Z_len, X_len };
	const uint32_t saltlens_XYZ [] = { X_len, Y_len, Z_len };
	//
	// Produce keys for all mixtures
	kipt_keyid K_id_1;
	kipt_keyid KL_id_1;
	kipt_keyid KM_id_1;
	kipt_keyid MK_id_1;
	kipt_keyid KLM_id_1;
	assert (kipkey_mixer (kip, 1, keys_K,   salts_X,   saltlens_X,   100, &K_id_1  ));
	assert (kipkey_mixer (kip, 2, keys_KL,  salts_XY,  saltlens_XY,  101, &KL_id_1 ));
	assert (kipkey_mixer (kip, 2, keys_KM,  salts_XZ,  saltlens_XZ,  102, &KM_id_1 ));
	assert (kipkey_mixer (kip, 2, keys_MK,  salts_ZX,  saltlens_ZX,  103, &MK_id_1 ));
	assert (kipkey_mixer (kip, 3, keys_KLM, salts_XYZ, saltlens_XYZ, 104, &KLM_id_1));
	//
	// Repeat the key production for all mixtures
	kipt_keyid K_id_2;
	kipt_keyid KL_id_2;
	kipt_keyid KM_id_2;
	kipt_keyid MK_id_2;
	kipt_keyid KLM_id_2;
	assert (kipkey_mixer (kip, 1, keys_K,   salts_X,   saltlens_X,   200, &K_id_2  ));
	assert (kipkey_mixer (kip, 2, keys_KL,  salts_XY,  saltlens_XY,  201, &KL_id_2 ));
	assert (kipkey_mixer (kip, 2, keys_KM,  salts_XZ,  saltlens_XZ,  202, &KM_id_2 ));
	assert (kipkey_mixer (kip, 2, keys_MK,  salts_ZX,  saltlens_ZX,  203, &MK_id_2 ));
	assert (kipkey_mixer (kip, 3, keys_KLM, salts_XYZ, saltlens_XYZ, 204, &KLM_id_2));
	//
	// Construct initial data
	uint8_t A_in [256];
	uint8_t B_in [256];
	uint32_t A_len = sizeof (A_in);
	uint32_t B_len = sizeof (B_in);
	printf ("A_len = %d, B_len = %d\n", A_len, B_len);
	krb5_data a;
	krb5_data b;
	a.data = (char*)A_in;
	b.data = (char*)B_in;
	a.length = A_len;
	b.length = B_len;
	assert (kipdata_random (kip, A_len, A_in));
	assert (kipdata_random (kip, B_len, B_in));
	//
	// Encrypt A and B with K_id_1, KL_id_1 and so on.
	// We shall be lazy, and offer a lot of space for the output
	uint8_t A_K   [512]; uint32_t A_K_len;
	uint8_t B_K   [512]; uint32_t B_K_len;
	uint8_t A_KL  [512]; uint32_t A_KL_len;
	uint8_t B_KL  [512]; uint32_t B_KL_len;
	uint8_t A_KM  [512]; uint32_t A_KM_len;
	uint8_t B_KM  [512]; uint32_t B_KM_len;
	uint8_t A_MK  [512]; uint32_t A_MK_len;
	uint8_t B_MK  [512]; uint32_t B_MK_len;
	uint8_t A_KLM [512]; uint32_t A_KLM_len;
	uint8_t B_KLM [512]; uint32_t B_KLM_len;
	assert (kipdata_up (kip, K_id_1  , A_in, A_len, A_K  , 512, &A_K_len  ));
	assert (kipdata_up (kip, K_id_1  , B_in, B_len, B_K  , 512, &B_K_len  ));
	assert (kipdata_up (kip, KL_id_1 , A_in, A_len, A_KL , 512, &A_KL_len ));
	assert (kipdata_up (kip, KL_id_1 , B_in, B_len, B_KL , 512, &B_KL_len ));
	assert (kipdata_up (kip, KM_id_1 , A_in, A_len, A_KM , 512, &A_KM_len ));
	assert (kipdata_up (kip, KM_id_1 , B_in, B_len, B_KM , 512, &B_KM_len ));
	assert (kipdata_up (kip, MK_id_1 , A_in, A_len, A_MK , 512, &A_MK_len ));
	assert (kipdata_up (kip, MK_id_1 , B_in, B_len, B_MK , 512, &B_MK_len ));
	assert (kipdata_up (kip, KLM_id_1, A_in, A_len, A_KLM, 512, &A_KLM_len));
	assert (kipdata_up (kip, KLM_id_1, B_in, B_len, B_KLM, 512, &B_KLM_len));
	assert (A_K_len == B_K_len);
	assert (A_K_len == A_KL_len);
	assert (A_K_len == A_KM_len);
	assert (A_K_len == A_MK_len);
	assert (A_K_len == A_KLM_len);
	assert (B_K_len == B_KL_len);
	assert (B_K_len == B_KM_len);
	assert (B_K_len == B_MK_len);
	assert (B_K_len == B_KLM_len);
	// TODO: We might check that all the binary output is different (quadratic)
	//
	// Decrypt back to A and B with the opposite keys in _2 variant.
	// We shall assume that the proper size returns (else some checks will trigger)
	uint8_t reA_K   [512]; uint32_t reA_K_len;
	uint8_t reB_K   [512]; uint32_t reB_K_len;
	uint8_t reA_KL  [512]; uint32_t reA_KL_len;
	uint8_t reB_KL  [512]; uint32_t reB_KL_len;
	uint8_t reA_KM  [512]; uint32_t reA_KM_len;
	uint8_t reB_KM  [512]; uint32_t reB_KM_len;
	uint8_t reA_MK  [512]; uint32_t reA_MK_len;
	uint8_t reB_MK  [512]; uint32_t reB_MK_len;
	uint8_t reA_KLM [512]; uint32_t reA_KLM_len;
	uint8_t reB_KLM [512]; uint32_t reB_KLM_len;
	assert (kipdata_down (kip, K_id_2  , A_K  , A_K_len  , reA_K  , 512, &reA_K_len));
	assert (kipdata_down (kip, K_id_2  , B_K  , B_K_len  , reB_K  , 512, &reB_K_len));
	assert (kipdata_down (kip, KL_id_2 , A_KL , A_KL_len , reA_KL , 512, &reA_KL_len));
	assert (kipdata_down (kip, KL_id_2 , B_KL , B_KL_len , reB_KL , 512, &reB_KL_len));
	assert (kipdata_down (kip, KM_id_2 , A_KM , A_KM_len , reA_KM , 512, &reA_KM_len));
	assert (kipdata_down (kip, KM_id_2 , B_KM , B_KM_len , reB_KM , 512, &reB_KM_len));
	assert (kipdata_down (kip, MK_id_2 , A_MK , A_MK_len , reA_MK , 512, &reA_MK_len));
	assert (kipdata_down (kip, MK_id_2 , B_MK , B_MK_len , reB_MK , 512, &reB_MK_len));
	assert (kipdata_down (kip, KLM_id_2, A_KLM, A_KLM_len, reA_KLM, 512, &reA_KLM_len));
	assert (kipdata_down (kip, KLM_id_2, B_KLM, B_KLM_len, reB_KLM, 512, &reB_KLM_len));
	assert (reA_K_len == A_len);
	assert (reB_K_len == B_len);
	assert (reA_K_len == reA_KL_len);
	assert (reA_K_len == reA_KM_len);
	assert (reA_K_len == reA_MK_len);
	assert (reA_K_len == reA_KLM_len);
	assert (reB_K_len == reB_KL_len);
	assert (reB_K_len == reB_KM_len);
	assert (reB_K_len == reB_MK_len);
	assert (reB_K_len == reB_KLM_len);
	//
	// Finally... the output should be the same as the original!
	assert (memcmp (A_in, reA_K  , A_len) == 0);
	assert (memcmp (B_in, reB_K  , B_len) == 0);
	assert (memcmp (A_in, reA_KL , A_len) == 0);
	assert (memcmp (B_in, reB_KL , B_len) == 0);
	assert (memcmp (A_in, reA_KM , A_len) == 0);
	assert (memcmp (B_in, reB_KM , B_len) == 0);
	assert (memcmp (A_in, reA_MK , A_len) == 0);
	assert (memcmp (B_in, reB_MK , B_len) == 0);
	assert (memcmp (A_in, reA_KLM, A_len) == 0);
	assert (memcmp (B_in, reB_KLM, B_len) == 0);
	//
	// Cleanup the context
	kipctx_close (kip);
	kip_fini ();
	//
	// We are happy, nothing went wrong!
	printf ("Mixing of keys in a variety of paths -- perfect results -- repeatable key material!\n");
	exit (0);
}
