/* Test key-based signatures.
 *
 * We create pairs of the same keys, which then pass through different
 * actions... or the same.  For instance, encryption in one path and
 * decryption in another should have the same result.  Passing over no
 * data should not matter either.  But passing over different data, or
 * data in different order, should croak and carp.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#define DEBUG 1
#undef NDEBUG

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>

#include <errno.h>
#include <com_err.h>

#include <arpa2/kip.h>



/* We use a simple command language:
 *   'A' and 'B' are for kip_up()
 *   'a' and 'b' are for kip_down()
 */
typedef char pathcommand_t;

struct pathtest {
	bool expected;
	char *K_path;
	char *L_path;
	char *testname;
};


struct pathtest all_tests [] = {
	{ true,  "",   "",   "No action on either side" },
	{ false, "A",  "",   "One side kip_up(A), the other does nothing" },
	{ true,  "A",  "A",  "Both sides kip_up(A)" },
	{ false, "A",  "B",  "One side kip_up(A), the other kip_up(B)" },
	{ true,  "A",  "a",  "One side kip_up(A), the other kip_down() to find A" },
	{ true,  "AB", "AB", "Both sides kip_up(A) then kip_up(B)" },
	{ false, "AB", "BA", "Both sides kip_up(A) and kip_up(B), but different order" },
	{ true,  "AB", "ab", "One side kip_up(A) then kip_up(B), the other kip_down() to find A then B" },
	// End marker has ( K_path==NULL ) || ( L_path==NULL )
	{ false, NULL, NULL }
};


uint8_t  keymud [512];
uint32_t keymudlen;

uint8_t  A [256],    B [256];
uint32_t Alen,       Blen;
uint8_t  Amud [512], Bmud [512];
uint32_t Amudlen,    Bmudlen;


void apply_commands (kipt_ctx kip, kipt_keyid keyid, char *path) {
	pathcommand_t *cmdpath = (pathcommand_t *) path;
	uint8_t dump [1024];
	uint32_t dumplen;
	while (*cmdpath) {
		switch (*cmdpath++) {
		case 'A':
			kipdata_up   (kip, keyid, A,    Alen,    dump, sizeof (dump), &dumplen);
			break;
		case 'B':
			kipdata_up   (kip, keyid, B,    Blen,    dump, sizeof (dump), &dumplen);
			break;
		case 'a':
			kipdata_down (kip, keyid, Amud, Amudlen, dump, sizeof (dump), &dumplen);
			break;
		case 'b':
			kipdata_down (kip, keyid, Bmud, Bmudlen, dump, sizeof (dump), &dumplen);
			break;
		default:
			fprintf (stderr, "Unknown path command '%c'\n", cmdpath [-1]);
			exit (1);
		}
	}
}


void run_test (kipt_ctx kip, struct pathtest *test) {
	kipt_keyid K;
	kipt_keyid L;
	uint8_t sig1 [512];
	uint8_t sig2 [512];
	uint32_t sig1len;
	uint32_t sig2len;
	//
	// Mention the test
	printf ("Test to run: %s (expected to %s)\n",
				test->testname,
				test->expected ? "match" : "differ");
	//
	// Restart keys, run paths, compute signatures
	assert (kipkey_frommap (kip,  keymud,  keymudlen, &K));
	assert (kipkey_frommap (kip,  keymud,  keymudlen, &L));
	apply_commands (kip, K, test->K_path);
	apply_commands (kip, L, test->L_path);
	assert (kipsum_sign (kip, K, sizeof (sig1), &sig1len, sig1));
	assert (kipsum_sign (kip, L, sizeof (sig2), &sig2len, sig2));
	//
	// Restart keys, run paths
	assert (kipkey_frommap (kip,  keymud,  keymudlen, &K));
	assert (kipkey_frommap (kip,  keymud,  keymudlen, &L));
	apply_commands (kip, K, test->K_path);
	apply_commands (kip, L, test->L_path);
	//
	// Now crossover!  The same keys, different instances; swap signatures
	//
	// Check for validity or checksum failure, as expected
	bool success1 = kipsum_verify (kip, K, sig2len, sig2);
	int  errno1   = errno;
	bool success2 = kipsum_verify (kip, L, sig1len, sig1);
	int  errno2   = errno;
	bool bad = false;
	if (test->expected) {
		if (!success1) {
			com_err ("side.one", errno1, "Unexpected difference (%d)", errno1);
		}
		if (!success2) {
			com_err ("side.two", errno2, "Unexpected difference (%d)", errno2);
		}
		bad = (!success1) || (!success2);
	} else {
		if (success1) {
			printf ("side.one: Unexpected match\n");
		} else if (errno1 != KIPERR_CHECKSUM_INVALID) {
			com_err ("side.one", errno1, "Unexpected error (%d)", errno1);
		}
		if (success2) {
			printf ("side.two: Unexpected match\n");
		} else if (errno2 != KIPERR_CHECKSUM_INVALID) {
			com_err ("side.two", errno2, "Unexpected error (%d)", errno2);
		}
		bad = (success1 || success2 || (errno1 != KIPERR_CHECKSUM_INVALID) || (errno2 != KIPERR_CHECKSUM_INVALID));
	}
	if (bad) {
		exit (1);
	}
}


int main (int argc, char *argv []) {
	assert (argc > 0);
	//
	// Create context
	kipt_ctx kip;
	assert (kip_init ());
	assert (kipctx_open (&kip));
	//
	// Create two random sequences, A and B
	Alen = sizeof (A);
	Blen = sizeof (B);
	kipdata_random (kip, Alen, A);
	kipdata_random (kip, Blen, B);
	//
	// Introduce one key KL (to be known as both K and L later on)
	kipt_keyid KL;
	assert (kipkey_generate (kip, 17 /* aes128-cts-hmac-sha1-96 */, 1, &KL));
	//
	// Determine K-encrypted values for Amud and Bmud
	assert (kipdata_up (kip, KL, A, Alen, Amud, sizeof (Amud), &Amudlen));
	assert (kipdata_up (kip, KL, B, Blen, Bmud, sizeof (Bmud), &Bmudlen));
	//
	// Pack K and L for many-times-over reuse
	assert (kipkey_tomap (kip, KL, 1, &KL, keymud, sizeof (keymud), &keymudlen));
	//
	// Run the tests, which block rudely on the first sign of trouble
	struct pathtest *test = all_tests;
	while ((test->K_path != NULL) && (test->L_path != NULL)) {
		run_test (kip, test);
		test++;
	}
	//
	// Close off
	kipctx_close (kip);
	kip_fini ();
	exit (0);
}

