/* have two salts and two keys; they should all yield different usermud,
 * but repeating the same operation on the same data should yield the same.
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#define DEBUG 1
#undef NDEBUG

#include <assert.h>

#include <stdlib.h>
#include <stdio.h>

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <krb5.h>

#include <arpa2/kip.h>

#include <errno.h>
#include <com_err.h>


void print_block (char *descr, uint8_t *blk, uint32_t blklen, bool skip) {
	int i;
	printf ("%s =", descr);
	for (i=0; i<blklen; i++) {
		printf (" %02x", blk [i]);
		if (skip && (i > 5) && (i < blklen - 5)) {
			printf (" ...");
			i = blklen - 5;
		}
	}
	printf (" -> #%d\n", blklen);
}


int main (int argc, char *argv []) {
	assert (argc > 0);
	//
	// Open a context for KIP
	kipt_ctx kip;
	assert (kip_init ());
	assert (kipctx_open (&kip));
	//
	// Construct initial data
	uint8_t A_in [256];
	uint8_t B_in [256];
	uint32_t A_len = sizeof (A_in);
	uint32_t B_len = sizeof (B_in);
	printf ("A_len = %d, B_len = %d\n", A_len, B_len);
	assert (kipdata_random (kip, A_len, A_in));
	assert (kipdata_random (kip, B_len, B_in));
	print_block ("A_in", A_in, A_len, true);
	print_block ("B_in", B_in, B_len, true);
	//
	// Create two keys, K and L
	kipt_keyid K_id;
	kipt_keyid L_id;
	assert (kipkey_generate (kip, 17 /* aes128-cts-hmac-sha1-96 */, 11, &K_id));
	assert (kipkey_generate (kip, 17 /* aes128-cts-hmac-sha1-96 */, 12, &L_id));
	//
	// Pass A and B through K and L (2x2 combinations)
	uint8_t A_via_K_1 [A_len];
	uint8_t A_via_L_1 [A_len];
	uint8_t B_via_K_1 [B_len];
	uint8_t B_via_L_1 [B_len];
	assert (kipdata_usermud (kip, K_id, A_in, A_len, A_via_K_1, A_len+1) == false);
	assert (kipdata_usermud (kip, K_id, A_in, A_len, A_via_K_1, A_len));
	assert (kipdata_usermud (kip, L_id, A_in, A_len, A_via_L_1, A_len));
	assert (kipdata_usermud (kip, K_id, B_in, B_len, B_via_K_1, B_len));
	assert (kipdata_usermud (kip, L_id, B_in, B_len, B_via_L_1, B_len));
	//
	// Ensure that all results are different
	assert (memcmp (A_in, A_via_K_1, A_len) != 0);
	assert (memcmp (A_in, A_via_L_1, A_len) != 0);
	assert (memcmp (B_in, B_via_K_1, B_len) != 0);
	assert (memcmp (B_in, B_via_L_1, B_len) != 0);
	//
	// Repeat this and hope to find the same
	uint8_t A_via_K_2 [A_len];
	uint8_t A_via_L_2 [A_len];
	uint8_t B_via_K_2 [B_len];
	uint8_t B_via_L_2 [B_len];
	assert (kipdata_usermud (kip, K_id, A_in, A_len, A_via_K_2, A_len+1) == false);
	assert (kipdata_usermud (kip, K_id, A_in, A_len, A_via_K_2, A_len));
	assert (kipdata_usermud (kip, L_id, A_in, A_len, A_via_L_2, A_len));
	assert (kipdata_usermud (kip, K_id, B_in, B_len, B_via_K_2, B_len));
	assert (kipdata_usermud (kip, L_id, B_in, B_len, B_via_L_2, B_len));
	//
	// Again, ensure that all results are different
	assert (memcmp (A_in, A_via_K_2, A_len) != 0);
	assert (memcmp (A_in, A_via_L_2, A_len) != 0);
	assert (memcmp (B_in, B_via_K_2, B_len) != 0);
	assert (memcmp (B_in, B_via_L_2, B_len) != 0);
	//
	// Now require that the first and second pass delivered the same
	assert (memcmp (A_via_K_1, A_via_K_2, A_len) == 0);
	assert (memcmp (A_via_L_1, A_via_L_2, A_len) == 0);
	assert (memcmp (B_via_K_1, B_via_K_2, B_len) == 0);
	assert (memcmp (B_via_L_1, B_via_L_2, B_len) == 0);
	//
	// Nothing failed, so return success
	printf ("All operations were successful...\n");
	print_block ("A_in ", A_in , A_len, true);
	print_block ("A_via_K", A_via_K_1, A_len, true);
	print_block ("A_via_L", A_via_L_1, A_len, true);
	printf ("...and...\n");
	print_block ("B_in ", B_in , B_len, true);
	print_block ("B_via_K", B_via_K_1, B_len, true);
	print_block ("B_via_L", B_via_L_1, B_len, true);
	printf ("...are all different, but a second run gave the same results.\n");
	kipctx_close (kip);
	kip_fini ();
	exit (0);
}
