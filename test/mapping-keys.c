/* keymapping.c -- Tests of the KIP tomap/frommap operations.
 *
 * Key maps allow a key to be retrieved by another key.  This can be
 * used to approach one key over multiple paths.  It works like an OR
 * condition on who may access the key (and what it can decrypt).
 * To also have an AND, multiple mapping keys can be required.  This
 * means that all positive logic expressions can be expressed (no NOT).
 *
 * Key mapping is basically encryption on keys.  This means that keys
 * can be extracted from a mapping, or stored in one.
 *
 * Key mappings are like a local version of contacting a KIP Service;
 * there is no network interaction, but somehow magically a new key
 * might pop up and be added to the context.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#define DEBUG 1
#undef NDEBUG

#include <stdlib.h>
#include <assert.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <krb5.h>

#include <errno.h>
#include <com_err.h>

#include <arpa2/kip.h>


void print_block (char *descr, uint8_t *blk, uint32_t blklen, bool skip) {
	int i;
	printf ("%s =", descr);
	for (i=0; i<blklen; i++) {
		printf (" %02x", blk [i]);
		if (skip && (i > 5) && (i < blklen - 5)) {
			printf (" ...");
			i = blklen - 5;
		}
	}
	printf (" -> #%d\n", blklen);
}


int main (int argc, char **argv) {
	//
	// Sanity checks on the commandline args
	assert (argc > 0);
	//
	// Open a context for KIP
	kipt_ctx kip;
	assert (kip_init ());
	assert (kipctx_open (&kip));
	//
	// Create three keys, K, L and M
	kipt_keyid K_id;
	kipt_keyid L_id;
	kipt_keyid M_id;
	assert (kipkey_generate (kip, 17 /* aes128-cts-hmac-sha1-96    */, 11, &K_id));
	assert (kipkey_generate (kip, 17 /* aes128-cts-hmac-sha1-96    */, 12, &L_id));
	assert (kipkey_generate (kip, 18 /* aes256-cts-hmac-sha256-128 */, 13, &M_id));
	//
	// Prepare to conceal K via L        to form K_mapL
	//     and to conceal K via M        to form K_mapM
	//     and to conceal K via L then M to form K_mapLM
	//     and to conceal K via M then L to form K_mapML
	kipt_keyid  L_mapids [1] = { L_id };
	kipt_keyid  M_mapids [1] = { M_id };
	kipt_keyid LM_mapids [2] = { L_id, M_id };
	kipt_keyid ML_mapids [2] = { M_id, L_id };
	//
	// Determine the lengths of the concealed keys (or: of the key mappings)
	uint32_t  L_keymudlen;
	uint32_t  M_keymudlen;
	uint32_t LM_keymudlen;
	uint32_t ML_keymudlen;
	assert ((kipkey_tomap (kip, K_id, 1,  L_mapids, NULL, 0,  &L_keymudlen) == false) && (errno = KIPERR_OUTPUT_SIZE));
	assert ((kipkey_tomap (kip, K_id, 1,  M_mapids, NULL, 0,  &M_keymudlen) == false) && (errno = KIPERR_OUTPUT_SIZE));
	assert ((kipkey_tomap (kip, K_id, 2, LM_mapids, NULL, 0, &LM_keymudlen) == false) && (errno = KIPERR_OUTPUT_SIZE));
	assert ((kipkey_tomap (kip, K_id, 2, ML_mapids, NULL, 0, &ML_keymudlen) == false) && (errno = KIPERR_OUTPUT_SIZE));
	assert ( L_keymudlen ==  M_keymudlen);
	assert ( L_keymudlen <= ML_keymudlen);
	assert (LM_keymudlen == ML_keymudlen);
	//
	// Now actually hide the keys in key mappings
	uint8_t  L_keymud [ L_keymudlen];
	uint8_t  M_keymud [ M_keymudlen];
	uint8_t LM_keymud [LM_keymudlen];
	uint8_t ML_keymud [ML_keymudlen];
	assert (kipkey_tomap (kip, K_id, 1,  L_mapids,  L_keymud,  L_keymudlen,  &L_keymudlen));
	assert (kipkey_tomap (kip, K_id, 1,  M_mapids,  M_keymud,  M_keymudlen,  &M_keymudlen));
	assert (kipkey_tomap (kip, K_id, 2, LM_mapids, LM_keymud, LM_keymudlen, &LM_keymudlen));
	assert (kipkey_tomap (kip, K_id, 2, ML_mapids, ML_keymud, ML_keymudlen, &ML_keymudlen));
	assert ( L_keymudlen == sizeof ( L_keymud));
	assert ( M_keymudlen == sizeof ( M_keymud));
	assert (LM_keymudlen == sizeof (LM_keymud));
	assert (ML_keymudlen == sizeof (ML_keymud));
	//
	// We now have totally unworkable keys.  But we can extract them from mappings.
	//
	// Extract the keys from the mappings
	kipt_keyid K_via_L ;
	kipt_keyid K_via_M ;
	kipt_keyid K_via_LM;
	kipt_keyid K_via_ML;
	assert (kipkey_frommap (kip,  L_keymud,  L_keymudlen, &K_via_L ));
	assert (kipkey_frommap (kip,  M_keymud,  M_keymudlen, &K_via_M ));
	assert (kipkey_frommap (kip, LM_keymud, LM_keymudlen, &K_via_LM));
	assert (kipkey_frommap (kip, ML_keymud, ML_keymudlen, &K_via_ML));
	//
	// There, we did it.  We retrieved K as various K_via_XXX.
	printf ("Sizes of key mud: K_via_L #%d, K_via_M #%d, K_via_LM #%d, K_via_ML #%d\n",
			L_keymudlen, M_keymudlen, LM_keymudlen, ML_keymudlen);
	//
	// Construct initial data
	uint8_t A_in [256];
	uint8_t B_in [256];
	uint32_t A_len = sizeof (A_in);
	uint32_t B_len = sizeof (B_in);
	printf ("A_len = %d, B_len = %d\n", A_len, B_len);
	krb5_data a;
	krb5_data b;
	a.data = (char*)A_in;
	b.data = (char*)B_in;
	a.length = A_len;
	b.length = B_len;
	assert (kipdata_random (kip, A_len, A_in));
	assert (kipdata_random (kip, B_len, B_in));
	//
	// Encrypt A and B with K, K_via_L and so on (10 combinations)
	// We shall be lazy, and offer a lot of space for the output
	uint8_t A_K        [512]; uint32_t A_K_len;
	uint8_t B_K        [512]; uint32_t B_K_len;
	uint8_t A_K_via_L  [512]; uint32_t A_K_via_L_len;
	uint8_t B_K_via_L  [512]; uint32_t B_K_via_L_len;
	uint8_t A_K_via_M  [512]; uint32_t A_K_via_M_len;
	uint8_t B_K_via_M  [512]; uint32_t B_K_via_M_len;
	uint8_t A_K_via_LM [512]; uint32_t A_K_via_LM_len;
	uint8_t B_K_via_LM [512]; uint32_t B_K_via_LM_len;
	uint8_t A_K_via_ML [512]; uint32_t A_K_via_ML_len;
	uint8_t B_K_via_ML [512]; uint32_t B_K_via_ML_len;
	assert (kipdata_up (kip, K_id    , A_in, A_len, A_K       , 512, &A_K_len));
	assert (kipdata_up (kip, K_id    , B_in, B_len, B_K       , 512, &B_K_len));
	assert (kipdata_up (kip, K_via_L , A_in, A_len, A_K_via_L , 512, &A_K_via_L_len));
	assert (kipdata_up (kip, K_via_L , B_in, B_len, B_K_via_L , 512, &B_K_via_L_len));
	assert (kipdata_up (kip, K_via_M , A_in, A_len, A_K_via_M , 512, &A_K_via_M_len));
	assert (kipdata_up (kip, K_via_M , B_in, B_len, B_K_via_M , 512, &B_K_via_M_len));
	assert (kipdata_up (kip, K_via_LM, A_in, A_len, A_K_via_LM, 512, &A_K_via_LM_len));
	assert (kipdata_up (kip, K_via_LM, B_in, B_len, B_K_via_LM, 512, &B_K_via_LM_len));
	assert (kipdata_up (kip, K_via_ML, A_in, A_len, A_K_via_ML, 512, &A_K_via_ML_len));
	assert (kipdata_up (kip, K_via_ML, B_in, B_len, B_K_via_ML, 512, &B_K_via_ML_len));
	assert (A_K_len == B_K_len);
	assert (A_K_len == A_K_via_L_len);
	assert (A_K_len == A_K_via_M_len);
	assert (A_K_len == A_K_via_LM_len);
	assert (A_K_len == A_K_via_ML_len);
	assert (B_K_len == B_K_via_L_len);
	assert (B_K_len == B_K_via_M_len);
	assert (B_K_len == B_K_via_LM_len);
	assert (B_K_len == B_K_via_ML_len);
	// TODO: We might check that all the binary output is different (quadratic)
	//
	// Decrypt back to A and B with K, K_via_L and so on (10 combinations)
	// We shall assume that the proper size returns (else some checks will trigger)
	uint8_t reA_K        [512]; uint32_t reA_K_len;
	uint8_t reB_K        [512]; uint32_t reB_K_len;
	uint8_t reA_K_via_L  [512]; uint32_t reA_K_via_L_len;
	uint8_t reB_K_via_L  [512]; uint32_t reB_K_via_L_len;
	uint8_t reA_K_via_M  [512]; uint32_t reA_K_via_M_len;
	uint8_t reB_K_via_M  [512]; uint32_t reB_K_via_M_len;
	uint8_t reA_K_via_LM [512]; uint32_t reA_K_via_LM_len;
	uint8_t reB_K_via_LM [512]; uint32_t reB_K_via_LM_len;
	uint8_t reA_K_via_ML [512]; uint32_t reA_K_via_ML_len;
	uint8_t reB_K_via_ML [512]; uint32_t reB_K_via_ML_len;
	assert (kipdata_down (kip, K_id    , A_K       , A_K_len       , reA_K       , 512, &reA_K_len));
	assert (kipdata_down (kip, K_id    , B_K       , B_K_len       , reB_K       , 512, &reB_K_len));
	assert (kipdata_down (kip, K_via_L , A_K_via_L , A_K_via_L_len , reA_K_via_L , 512, &reA_K_via_L_len));
	assert (kipdata_down (kip, K_via_L , B_K_via_L , B_K_via_L_len , reB_K_via_L , 512, &reB_K_via_L_len));
	assert (kipdata_down (kip, K_via_M , A_K_via_M , A_K_via_M_len , reA_K_via_M , 512, &reA_K_via_M_len));
	assert (kipdata_down (kip, K_via_M , B_K_via_M , B_K_via_M_len , reB_K_via_M , 512, &reB_K_via_M_len));
	assert (kipdata_down (kip, K_via_LM, A_K_via_LM, A_K_via_LM_len, reA_K_via_LM, 512, &reA_K_via_LM_len));
	assert (kipdata_down (kip, K_via_LM, B_K_via_LM, B_K_via_LM_len, reB_K_via_LM, 512, &reB_K_via_LM_len));
	assert (kipdata_down (kip, K_via_ML, A_K_via_ML, A_K_via_ML_len, reA_K_via_ML, 512, &reA_K_via_ML_len));
	assert (kipdata_down (kip, K_via_ML, B_K_via_ML, B_K_via_ML_len, reB_K_via_ML, 512, &reB_K_via_ML_len));
	assert (reA_K_len == A_len);
	assert (reB_K_len == B_len);
	assert (reA_K_len == reA_K_via_L_len);
	assert (reA_K_len == reA_K_via_M_len);
	assert (reA_K_len == reA_K_via_LM_len);
	assert (reA_K_len == reA_K_via_ML_len);
	assert (reB_K_len == reB_K_via_L_len);
	assert (reB_K_len == reB_K_via_M_len);
	assert (reB_K_len == reB_K_via_LM_len);
	assert (reB_K_len == reB_K_via_ML_len);
	//
	// Finally... the output should be the same as the original!
	assert (memcmp (A_in, reA_K       , A_len) == 0);
	assert (memcmp (B_in, reB_K       , B_len) == 0);
	assert (memcmp (A_in, reA_K_via_L , A_len) == 0);
	assert (memcmp (B_in, reB_K_via_L , B_len) == 0);
	assert (memcmp (A_in, reA_K_via_M , A_len) == 0);
	assert (memcmp (B_in, reB_K_via_M , B_len) == 0);
	assert (memcmp (A_in, reA_K_via_LM, A_len) == 0);
	assert (memcmp (B_in, reB_K_via_LM, B_len) == 0);
	assert (memcmp (A_in, reA_K_via_ML, A_len) == 0);
	assert (memcmp (B_in, reB_K_via_ML, B_len) == 0);
	//
	// Cleanup the context
	kipctx_close (kip);
	kip_fini ();
	//
	// We are happy, nothing went wrong!
	printf ("Mapping of keys in a variety of forms -- perfect results!\n");
	exit (0);
}
