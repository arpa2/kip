/* SASL mechanism list mangling; restrict a mechlist and check.
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <assert.h>	/* TODO:FORNOW */

//TODO// #include <arpa2/except.h>
#include <arpa2/quick-diasasl.h>


int main (int argc, char *argv []) {
	assert (argc == 4);	/* TODO:FORNOW */
	//TODO// assertxt (argc == 4, "Usage: %s \"ORIGINAL MECHS\" \"MECHS FILTER\" \"RESULTING MECHS\"", argv [0]);
	printf ("Original: %s\n", argv [1]);
	printf ("Filter:   %s\n", argv [2]);
	printf ("Expected: %s\n", argv [3]);
	char *modder = strdup (argv [1]);
	char *filter = strdup (argv [2]);
	diasasl_mechfilter (modder, filter);
	printf ("Derived:  %s\n", modder  );
	assert (strcmp (filter, argv [2]) == 0);
	assert (strcmp (modder, argv [3]) == 0);
	printf ("Success!\n");
}

