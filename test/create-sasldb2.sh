#! /bin/sh
#
# Usage:
#
#   create-sasldb2.sh <filename> <user> <password> <realm>
#
# Does no error-checking at all.

echo "$3" | saslpasswd2 -f "$1" -u "$4" "$2"
