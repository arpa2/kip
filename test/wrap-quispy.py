#!/usr/bin/env python3
#
# Wrap around the quispy-* interfaces to respond to input fields.
#
# This is brutal, and assumes test passwords and test accounts,
# with a known proper flow.  Perfect for testing, terrible from
# a security perspective :-)
#
# Call as: $0 path/to/quispy-TRALAL line1 line2 line3
# (The path is relative to this script, so often no path is used.)
#
# From: Rick van Rein <rick@openfortress.nl>


import sys

import os
from os import path

import subprocess

here = path.dirname (path.realpath (__file__))

test2run = path.join (here, sys.argv [1])

sub = subprocess.Popen (test2run, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

txtin = ''.join ([ argi + '\n' for argi in sys.argv [2:] ])
txtin = bytes (txtin, 'utf-8')
(txtout,txterr) = sub.communicate (input=txtin)
txtout = str (txtout, 'utf-8')
txterr = str (txterr, 'utf-8')

print ('### STDOUT\n\n%s\n#### STDERR\n\n%s\n### EXIT %d' % (txtout,txterr,sub.returncode))

