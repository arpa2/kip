/* Test the KIP Service for signing (and verification)
 *
 * Send a checksum to the local KIP Service and produce a
 * signature.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#define DEBUG 1
#undef NDEBUG

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>

#include <unistd.h>
#include <errno.h>
#include <com_err.h>

#include <arpa2/quick-sasl.h>
#include <arpa2/kip.h>



void print_block (char *descr, uint8_t *blk, uint32_t blklen, bool skip) {
	int i;
	printf ("%s =", descr);
	for (i=0; i<blklen; i++) {
		printf (" %02x", blk [i]);
		if (skip && (i > 5) && (i < blklen - 5)) {
			printf (" ...");
			i = blklen - 5;
		}
	}
	printf (" -> #%d\n", blklen);
}


/* We use a simple command language:
 *   'A' and 'B' are for kip_up()
 *   'a' and 'b' are for kip_down()
 */
typedef char pathcommand_t;

struct pathtest {
	bool expected;
	char *K_path;
	char *L_path;
	char *testname;
};


struct pathtest all_tests [] = {
	{ true,  "",   "",   "No action on either side" },
	{ false, "A",  "",   "One side kip_up(A), the other does nothing" },
	{ true,  "A",  "A",  "Both sides kip_up(A)" },
	{ false, "A",  "B",  "One side kip_up(A), the other kip_up(B)" },
	{ true,  "A",  "a",  "One side kip_up(A), the other kip_down() to find A" },
	{ true,  "AB", "AB", "Both sides kip_up(A) then kip_up(B)" },
	{ false, "AB", "BA", "Both sides kip_up(A) and kip_up(B), but different order" },
	{ true,  "AB", "ab", "One side kip_up(A) then kip_up(B), the other kip_down() to find A then B" },
	// End marker has ( K_path==NULL ) || ( L_path==NULL )
	{ false, NULL, NULL }
};


uint8_t  keymud [512];
uint32_t keymudlen;

uint8_t  A [256],    B [256];
uint32_t Alen,       Blen;
uint8_t  Amud [512], Bmud [512];
uint32_t Amudlen,    Bmudlen;


void apply_commands (kipt_ctx kip, kipt_keyid keyid, char *path) {
	pathcommand_t *cmdpath = (pathcommand_t *) path;
	uint8_t dump [1024];
	uint32_t dumplen;
	while (*cmdpath) {
		switch (*cmdpath++) {
		case 'A':
			kipdata_up   (kip, keyid, A,    Alen,    dump, sizeof (dump), &dumplen);
			break;
		case 'B':
			kipdata_up   (kip, keyid, B,    Blen,    dump, sizeof (dump), &dumplen);
			break;
		case 'a':
			kipdata_down (kip, keyid, Amud, Amudlen, dump, sizeof (dump), &dumplen);
			break;
		case 'b':
			kipdata_down (kip, keyid, Bmud, Bmudlen, dump, sizeof (dump), &dumplen);
			break;
		default:
			fprintf (stderr, "Unknown path command '%c'\n", cmdpath [-1]);
			exit (1);
		}
	}
}


void run_test (kipt_ctx kip, struct pathtest *test) {
	kipt_keyid K;
	kipt_keyid L;
	uint8_t *sig1;
	uint8_t *sig2;
	uint32_t sig1len;
	uint32_t sig2len;
	//
	// Mention the test
	printf ("Test to run: %s (expected to %s)\n",
				test->testname,
				test->expected ? "match" : "differ");
	//
	// Restart keys, run paths, compute signatures
	assert (kipkey_frommap (kip,  keymud,  keymudlen, &K));
	assert (kipkey_frommap (kip,  keymud,  keymudlen, &L));
	apply_commands (kip, K, test->K_path);
	apply_commands (kip, L, test->L_path);
	printf ("Signing K...\n");
	assert (kipservice_sign (kip, K, 1, &K,
				strlen (test->K_path), test->K_path,
				&sig1, &sig1len));
	printf ("Signing L...\n");
	assert (kipservice_sign (kip, L, 1, &L,
				strlen (test->L_path), test->L_path,
				&sig2, &sig2len));
	printf ("Signing done\n");
	print_block ("sig1/K", sig1, sig1len, false);
	print_block ("sig2/L", sig1, sig1len, false);
	//
	// Restart keys, run paths
	assert (kipkey_frommap (kip,  keymud,  keymudlen, &K));
	assert (kipkey_frommap (kip,  keymud,  keymudlen, &L));
	apply_commands (kip, K, test->K_path);
	apply_commands (kip, L, test->L_path);
	//
	// Now crossover!  The same keys, different instances; swap signatures
	//
	// Check for validity or checksum failure, as expected
	errno=12345;
	printf ("Checking K against signature L... (start errno=%d)\n", errno);
	uint8_t *meta1;
	uint32_t meta1len;
	bool success1 = kipservice_verify (kip, K, 1, &K,
				sig2, sig2len,
				&meta1len, &meta1);
	int  errno1   = success1 ? 0 : errno;
	bool metaok1  = (meta1len == strlen (test->L_path)) && (memcmp (meta1, test->L_path, meta1len) == 0);
	printf ("Metadata from signature L is \"%.*s\"\n", meta1len, meta1);
	errno=12345;
	printf ("Checking L against signature K... (start errno=%d)\n", errno);
	uint8_t *meta2;
	uint32_t meta2len;
	bool success2 = kipservice_verify (kip, L, 1, &L,
				sig1, sig1len,
				&meta2len, &meta2);
	int  errno2   = success2 ? 0 : errno;
	bool metaok2  = (meta2len == strlen (test->K_path)) && (memcmp (meta2, test->K_path, meta2len) == 0);
	printf ("Metadata from signature K is \"%.*s\"\n", meta2len, meta2);
	printf ("Checking yielded errno %d and %d (expected match=%d)\n", errno1, errno2, test->expected);
	bool bad = false;
	if (test->expected) {
		if (!success1) {
			com_err ("side.one", errno1, "Unexpected difference (%d)", errno1);
		}
		if (!success2) {
			com_err ("side.two", errno2, "Unexpected difference (%d)", errno2);
		}
		if (!metaok1) {
			printf ("metadata 1 distorted: expected \"%s\", got \"%.*s\"\n",
					test->L_path, meta1len, meta1);
		}
		if (!metaok2) {
			printf ("metadata 2 distorted: expected \"%s\", got \"%.*s\"\n",
					test->K_path, meta2len, meta2);
		}
		bad = (!success1) || (!success2) || (!metaok1) || (!metaok2);
	} else {
		if (success1) {
			printf ("side.one: Unexpected match\n");
		} else if (errno1 != KIPERR_CHECKSUM_INVALID) {
			printf ("side.one: Unexpected error %s (%d)\n", error_message (errno1), errno1);
		}
		if (success2) {
			printf ("side.two: Unexpected match\n");
		} else if (errno2 != KIPERR_CHECKSUM_INVALID) {
			printf ("side.two: Unexpected error %s (%d)\n", error_message (errno2), errno2);
		}
		bad = (success1 || success2 || (errno1 != KIPERR_CHECKSUM_INVALID) || (errno2 != KIPERR_CHECKSUM_INVALID));
	}
	if (bad) {
		printf ("Fatally failing on bad result\n");
		exit (1);
	}
}


int main (int argc, char *argv []) {
	assert (argc > 0);
	//
	// Set an alarm to trigger in 30 seconds
	if (!isatty (0)) {
		alarm (30);
	}
	//
	// Process the parameter into an alternative /etc/hosts file for Unbound
	if (argc != 2) {
		printf ("Usage: %s /etc/hosts[.replacement]\n", argv [0]);
		exit (1);
	}
	setenv ("UNBOUND_HOSTS", argv [1], 1);
	//
	// Initialise KIP, KIP Service, QuickSASL
	assert (kip_init ());
	assert (kipservice_init (NULL, argv [1]));
	assert (qsasl_init (NULL, "KIP"));
	//
	// Create context
	kipt_ctx kip;
	assert (kipctx_open (&kip));
	assert (kipservice_start (kip));
	assert (kipservice_set_client_realm     (kip, "arpa2.net" ));
	assert (kipservice_set_clientuser_login (kip, "demo"      ));
	assert (kipservice_set_clientuser_acl   (kip, "demo+ali"  ));
	//
	// Signal that we are on to it
	printf ("--\n");
	fflush (stdout);
	//
	// Create two random sequences, A and B
	Alen = sizeof (A);
	Blen = sizeof (B);
	kipdata_random (kip, Alen, A);
	kipdata_random (kip, Blen, B);
	//
	// Introduce one key KL (to be known as both K and L later on)
	kipt_keyid KL;
	assert (kipkey_generate (kip, 17 /* aes128-cts-hmac-sha1-96 */, 1, &KL));
	//
	// Determine K-encrypted values for Amud and Bmud
	assert (kipdata_up (kip, KL, A, Alen, Amud, sizeof (Amud), &Amudlen));
	assert (kipdata_up (kip, KL, B, Blen, Bmud, sizeof (Bmud), &Bmudlen));
	//
	// Pack K and L for many-times-over reuse
	assert (kipkey_tomap (kip, KL, 1, &KL, keymud, sizeof (keymud), &keymudlen));
	//
	// Run the tests, which block rudely on the first sign of trouble
	struct pathtest *test = all_tests;
	while ((test->K_path != NULL) && (test->L_path != NULL)) {
		run_test (kip, test);
		test++;
	}
	printf ("All tests run\n");
	//
	// Close off
	kipservice_stop (kip);
	kipctx_close (kip);
	printf ("Closed context\n");
	qsasl_fini ();
	kipservice_fini ();
	kip_fini ();
	printf ("Closed Quick SASL\n");
	exit (0);
}
