/* Manipulations with mechanism names.
 *
 * Mechanism names occur in a number of forms, because these are not
 * standardised by SASL.  Quick SASL supports transformations in its
 * generic utility functions
 *
 * qsasl_mechiter_array2buffer
 * qsasl_mechiter_buffer2array
 * qsasl_mechconcat_ia5string
 *
 * We map back and forth between these forms below and check that
 * we always find the same results.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>



/* These are strings in canonical form, that is, mechanism names separated by spaces */
char *mechstrings [] = {
	"GSSAPI GSS-API GSS API",
	"SXOVER SXOVER SXOVER",
	"KRB5 KRB6 KRB7 KRB8",
	"SCRAM-SHA256 SCRAM-SHA1-PLUS",
	"JUSTONE",
	"AND-JUST-THIS-ONE",
	"A VERY LONG STRONG WITH A GREAT MANY NUMBER OF MECHANISMS THAT SHOULD REPLICATE PROPERLY IN SPITE OF THE ONE-LETTER FORMS INCLUDED - AND EVEN A SINGLE DASH",
	NULL
};



#define TEST(cmd) { bool test_ok = cmd; ok = ok && test_ok; if (!test_ok) printf ("ERROR on %s command %s\n", mechstr0, #cmd); }


#error "Silly, to do so much work for such simple routines -- but go ahead if you want to do it"


bool run_tests (char *mechstr0) {
	bool ok = true;
	// mechstr0 -> derarray0
	derrarray array0 = { 0 };
	TEST (str2array (mechstr0, &derarray0));
	// derarray0 -> derblob0
	dercursor derblob0 = { 0 };
	TEST (array2der (derarray0, &derblob0));
	// derarray0 -> mechstr1
	char *mechstr1 = NULL;
	TEST (array2str (derarray0, mechstr1));
	// derblob0 -> derarray1
	// derarray1 -> mechstr2
	// mechstr1 --> derblob1
	// mechstr1 --> derarray2
	// mechstr2 --> derblob2
	// mechstr2 --> derarray2
	// Test equality for mechstr0,1,2
	// Test equality for derarray0,1,2
	// Test equality for derblob0,1,2
}


int main (int argc, char *argv []) {
	// Iterate over all tests and invoke the test routine
	char **test = mechstrings;
	bool ok = true;
	while (*test) {
		bool test_ok = run_tests (test);
		printf ("%s on \"%s\"\n", test_ok ? "SUCCESS" : "FAILED");
		ok = ok && test_ok;
		test++;
	}
	// Reap the result
	exit (ok ? 0 : 1);
}
