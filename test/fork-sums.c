/* Fork checksums, and assure the same result.
 *
 * Start a checksum, insert A, then fork and insert B
 * in both.  One should be able to validate the other.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#define DEBUG 1
#undef NDEBUG

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>

#include <errno.h>
#include <com_err.h>

#include <arpa2/kip.h>



int main (int argc, char *argv []) {
	assert (argc > 0);
	//
	// Create context
	kipt_ctx kip;
	assert (kip_init ());
	assert (kipctx_open (&kip));
	//
	// Create two random sequences, A and B
	uint8_t A [256];
	uint8_t B [256];
	uint32_t Alen = sizeof (A);
	uint32_t Blen = sizeof (B);
	kipdata_random (kip, Alen, A);
	kipdata_random (kip, Blen, B);
	//
	// Introduce one key K
	kipt_keyid K;
	assert (kipkey_generate (kip, 17 /* aes128-cts-hmac-sha1-96 */, 1, &K));
	//
	// Append value A to the checksum of key K
	assert (kipsum_append (kip, K, A, Alen));
	//
	// Fork the checksum
	kipt_sumid S = 0;
	assert (kipsum_fork (kip, K, &S));
	//
	// Append value B to the checksum of key K
	assert (kipsum_append (kip, K, B, Blen));
	//
	// Produce a signature based on K
	uint8_t Ksum [1024];
	uint32_t Ksumsz = sizeof (Ksum);
	uint32_t Ksumlen;
	assert (kipsum_sign (kip, K, Ksumsz, &Ksumlen, Ksum));
	//
	// The signature by K should not (yet) be valid for S
	assert (!kipsum_verify (kip, S, Ksumlen, Ksum));
	//
	// Now add value B to the checksum of key S
	assert (kipsum_append (kip, S, B, Blen));
	//
	// Now the signature by K should be valid for S
	assert (kipsum_verify (kip, S, Ksumlen, Ksum));
	//
	// Close off
	kipctx_close (kip);
	kip_fini ();
	exit (0);
}
