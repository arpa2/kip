#!/usr/bin/env python3

from os import path
here = path.dirname (path.realpath (__file__))
base = path.dirname (here)

# from kip import kipcore
import _kipcore

# assert kipcore.kip_init ()
assert _kipcore.kip_init ()

# ctx2 = kipcore.kipctx_open2 ()

# print ('ctx2 = %r :: %r' % (ctx2,type(ctx2)) )

# kipcore.kipctx_close (ctx2)

# ctx = kipcore.new_ctx ()
ctx = _kipcore.new_ctx ()

print ('ctx = %r :: %r' % (ctx,type(ctx)) )
# print ('dir(ctx) = %r' % (dir(ctx),))

# print ('ctx ::', type (ctx))
# print ('ctx.dir ::', dir (ctx))

# kipcore.kipctx_open (ctx)
# ctx = kipcore.ctx_value (ctx)
_kipcore.kipctx_open (ctx)
ctx = _kipcore.ctx_value (ctx)

print ('ctx = %r :: %r' % (ctx,type(ctx)) )
# print ('dir(ctx) = %r' % (dir(ctx),))

# print ('ctx ::', type (ctx))
# print ('ctx.dir ::', dir (ctx))


#
# Generate random bytes
#

# bool kipdata_random (kipt_ctx ctx, uint32_t len_target, uint8_t *out_target);
rnd = bytearray (123)
print ('rnd.pre  = %r', rnd)
# bool kipdata_random (kipt_ctx ctx, uint32_t len_target, uint8_t *out_target);
# _kipcore.kipdata_random (ctx, len (rnd), memoryview (rnd))
assert _kipcore.kipdata_random (ctx, memoryview (rnd))
print ('rnd.post = %r', rnd)



# assert (kipkey_generate (kip, 17 /* aes128-cts-hmac-sha1-96 */, 11, &K_id));
# bool kipkey_generate (kipt_ctx ctx, kipt_alg alg, kipt_keynr   in_keynr, kipt_keyid *out_keyid);
k_id = _kipcore.new_keyid ()
l_id = _kipcore.new_keyid ()
m_id = _kipcore.new_keyid ()
assert _kipcore.kipkey_generate (ctx, 17, 11, k_id);
assert _kipcore.kipkey_generate (ctx, 18, 12, l_id);
assert _kipcore.kipkey_generate (ctx, 19, 11, m_id);

print ('k_id = %r :: %r' % (k_id,type(k_id)) )
k_id = _kipcore.keyid_value (k_id)
print ('k_id = %r :: %r' % (k_id,type(k_id)) )

print ('l_id = %r :: %r' % (l_id,type(l_id)) )
l_id = _kipcore.keyid_value (l_id)
print ('l_id = %r :: %r' % (l_id,type(l_id)) )

print ('m_id = %r :: %r' % (m_id,type(m_id)) )
m_id = _kipcore.keyid_value (m_id)
print ('m_id = %r :: %r' % (m_id,type(m_id)) )

k_alg = _kipcore.new_alg ()
l_alg = _kipcore.new_alg ()
m_alg = _kipcore.new_alg ()
assert _kipcore.kipkey_algorithm (ctx, k_id, k_alg)
assert _kipcore.kipkey_algorithm (ctx, l_id, l_alg)
assert _kipcore.kipkey_algorithm (ctx, m_id, m_alg)
k_alg = _kipcore.alg_value (k_alg)
l_alg = _kipcore.alg_value (l_alg)
m_alg = _kipcore.alg_value (m_alg)
print ('(k_alg,l_alg,m_alg) = %r, expect (17,18,19)' % ((k_alg,l_alg,m_alg),))


# assert ((kipdata_up (kip, K_id, NULL, A_len, NULL, 0, &X_len) == false) && (errno == KIPERR_OUTPUT_SIZE));
# bool kipdata_up (kipt_ctx ctx, kipt_keyid key,
# 			uint8_t *clear,   uint32_t clearsz,    /* input  */
# 			uint8_t *out_mud, uint32_t max_mudsz,  /* outbuf */
# 			uint32_t *out_mudsz);                  /* usedup */
lenp = _kipcore.new_length ()
assert not _kipcore.kipdata_up (ctx, k_id, rnd, None, lenp)
print ('lenp %r :: %r' % (lenp,type(lenp)) )
lenp0 = _kipcore.length_value (lenp)
print ('lenp0 %r :: %r' % (lenp0,type(lenp0)) )
#
ernd = bytearray (lenp0)
assert _kipcore.kipdata_up (ctx, k_id, rnd, ernd, lenp)
print ('rnd#%d = %r\nernd%d = %r' % (len(rnd),rnd,len(ernd),ernd) )

# assert ((kipdata_down (kip, K_id, X_mud, X_len, NULL, 0, &C_len) == false) && (errno == KIPERR_OUTPUT_SIZE));
# bool kipdata_down (kipt_ctx ctx, kipt_keyid key,
# 			uint8_t *mud,       uint32_t mudsz,        /* input  */
# 			uint8_t *out_clear, uint32_t max_clearsz,  /* outbuf */
# 			uint32_t *out_clearsz);                    /* usedup */
clearsz = _kipcore.new_length ()
rnd2 = bytearray(len (ernd))
assert _kipcore.kipdata_down (ctx, k_id, ernd, rnd2, clearsz)
clearsz = _kipcore.length_value (clearsz)

print ('Got #%d' % clearsz)
print ('rnd_#%d = %r\nrnd2#%d = %r' % (len(rnd),rnd,len(rnd2),rnd2[:clearsz]) )

assert rnd == rnd2 [:clearsz]


#
# bool kipkey_tomap (kipt_ctx ctx, kipt_keyid keyid,
# 			uint16_t    mappingdepth,                 /* AND count */
# 			kipt_keyid *mappingkeyids,                /* AND keyid */
# 			uint8_t *out_keymud, uint32_t max_keymudsz,  /* outbuf */
# 			uint32_t *out_keymudsz);                     /* usedup */
# assert ((kipkey_tomap (kip, K_id, 1,  L_mapids, NULL,     0,           &L_keymudlen) == false) && (errno = KIPERR_OUTPUT_SIZE));
# assert  (kipkey_tomap (kip, K_id, 1,  L_mapids, L_keymud, L_keymudlen, &L_keymudlen));

k_keymudlen = _kipcore.new_length ()
assert not _kipcore.kipkey_tomap (ctx, k_id, [m_id,l_id], None, k_keymudlen)
k_keymudlen0 = _kipcore.length_value (k_keymudlen)
k_keymud = bytearray (k_keymudlen0)
assert _kipcore.kipkey_tomap (ctx, k_id, [m_id,l_id], k_keymud, k_keymudlen)

print ('Got #%d or #%d bytes keymudlen... %r\n' % (k_keymudlen0,len(k_keymud),k_keymud) )

#
# bool kipkey_frommap (kipt_ctx ctx,
# 			uint8_t *keymud, uint32_t keymudsz,
# 			kipt_keyid *out_mappedkey);
# assert (kipkey_frommap (kip,  L_keymud,  L_keymudlen, &K_via_L ));

kviaml_id = _kipcore.new_keyid ()
assert _kipcore.kipkey_frommap (ctx, k_keymud, kviaml_id)
kviaml_id = _kipcore.keyid_value (kviaml_id)
print ('m-via-k-and-l-id = %d' % kviaml_id)

# assert ((kipdata_down (kip, K_id, X_mud, X_len, NULL, 0, &C_len) == false) && (errno == KIPERR_OUTPUT_SIZE));
# bool kipdata_down (kipt_ctx ctx, kipt_keyid key,
# 			uint8_t *mud,       uint32_t mudsz,        /* input  */
# 			uint8_t *out_clear, uint32_t max_clearsz,  /* outbuf */
# 			uint32_t *out_clearsz);                    /* usedup */
clearsz3 = _kipcore.new_length ()
rnd3 = bytearray(len (ernd))
assert _kipcore.kipdata_down (ctx, kviaml_id, ernd, rnd3, clearsz3)
clearsz3 = _kipcore.length_value (clearsz3)

print ('Got #%d' % clearsz3)
print ('rnd_#%d = %r\nrnd3#%d = %r' % (len(rnd),rnd,len(rnd3),rnd3[:clearsz3]) )

assert rnd == rnd3 [:clearsz3]


# bool kipsum_start (kipt_ctx ctx, kipt_keyid supporting_key,
# 			kipt_sumid *out_kipsum);
sumQ = _kipcore.new_keyid ()
assert _kipcore.kipsum_start (ctx, k_id, sumQ)
sumQ = _kipcore.keyid_value (sumQ)
print ('New sumQ is %x' % ((sumQ,)))

# bool kipsum_append (kipt_ctx ctx, kipt_sumid sumid_or_keyid,
# 			uint8_t *data, uint32_t datasz);
# assert (kipsum_append (kip, Q, A, Alen));
assert _kipcore.kipsum_append (ctx, sumQ, rnd)

# bool kipsum_fork (kipt_ctx ctx, kipt_sumid sumid_or_keyid, kipt_sumid *out_kipsum);
sumR = _kipcore.new_keyid ()
assert _kipcore.kipsum_fork (ctx, sumQ, sumR)
sumR = _kipcore.keyid_value (sumR)
print ('New sumR is %x' % ((sumR,)))

# bool kipsum_restart (kipt_ctx ctx, kipt_sumid sumid_or_keyid);
assert _kipcore.kipsum_restart (ctx, sumQ)

# bool kipsum_mark (kipt_ctx ctx, kipt_sumid sumid_or_keyid,
# 			char *opt_typing);
assert _kipcore.kipsum_mark (ctx, sumQ, "tralala");
assert _kipcore.kipsum_mark (ctx, sumR, None);

# bool kipsum_sign (kipt_ctx ctx, kipt_sumid sumid_or_keyid, uint32_t max_siglen,
# 				uint32_t *out_siglen, uint8_t *out_sig);
#TODO# This is a _terrible_ size exchange, and too much even for SWIG
sigmudlenp = _kipcore.new_length ()
assert not _kipcore.kipsum_sign (ctx, sumQ, 0, sigmudlenp, None)
sigmudlen = _kipcore.length_value (sigmudlenp)
sigmud = bytearray (sigmudlen)
assert _kipcore.kipsum_sign (ctx, sumQ, sigmudlen, sigmudlenp, sigmud)
print ('Got kipsum_sign output #%d in %r' % (sigmudlen,sigmud))

# bool kipsum_verify (kipt_ctx ctx, kipt_sumid sumid_or_keyid,
# 				uint32_t siglen, uint8_t *sig);
assert _kipcore.kipsum_verify (ctx, sumQ, sigmud)

# bool kipkey_fromkeytab (kipt_ctx ctx, char *opt_ktname,
# 			char *opt_hostname, char *domain,
# 			kipt_keynr kvno, kipt_alg enctype,
# 			kipt_keyid *out_keyid);
tabkid = _kipcore.new_keyid ()
ktname = path.join (base, 'test', 'bin', 'unicorn.keytab')
hostname = None
print ('KTNAME = %s' % (ktname,))
assert _kipcore.kipkey_fromkeytab (ctx, ktname, hostname, 'unicorn.demo.arpa2.org', 2019, 20, tabkid)
tabkid = _kipcore.keyid_value (tabkid)
print ('Got keyid %x from key table' % (tabkid,))

# kipcore.kipctx_close (ctx)
_kipcore.kipctx_close (ctx)

# kipcore.kip_fini ()
_kipcore.kip_fini ()

print ('That\'s all folks!')
