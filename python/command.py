#
# Commands "kip up" and "kip down".  Basic shell command and API for GUI.
#
# See doc/kip-command.md
#
# From: Rick van Rein <rick@openfortress.nl>
#


import sys

import os.path

import cbor


from document import Document, tags, TabKey, Meta, Lit, Ref, Mud, SigMud, VerboseInteractor

import arpa2.kip.io
from arpa2.kip.io.file import FileSource, FileTarget

from pkg_resources import iter_entry_points



# Default source_classes
#
source_classes = {
	'file': FileSource,
}

# Default target_classes
#
target_classes = {
	'file': FileTarget,
}

# Run through the package's entry points; for a good tutorial, see
# https://amir.rachum.com/blog/2017/07/28/python-entry-points/
#
def setup ():
	global source_classes, target_classes
	for ep in iter_entry_points ('arpa2.kip.io.sources'):
		source_classes [ep.name] = ep.load ()
	for ep in iter_entry_points ('arpa2.kip.io.targets'):
		target_classes [ep.name] = ep.load ()


class CommandInteractor (VerboseInteractor):

	def get_keytab_filename (self, userid):
		print ('Being asked for a keytab holding %s,' % userid, file=sys.stderr)
		base = os.path.dirname (os.path.dirname (os.path.realpath (__file__)))
		keytab = os.path.join (base, 'test', 'bin', 'unicorn.keytab')
		print ('I will present %s' % keytab, file=sys.stderr)
		return keytab

	def get_passphrase (self, reason):
		print ('Being asked for a passphrase for %s,' % reason, file=sys.stderr)
		passphrase = 'sekreet'
		print ('I will present %s' % passphrase, file=sys.stderr)
		return passphrase


def make_source (uri, cfg):
	#TODO# Use cfg to iterate through options
	attempt = FileSource.from_exact_match (uri)
	if attempt is None:
		raise Exception ('Unrecognised Source URI: %s' % (uri,))
	return attempt


def make_target (uri, cfg, must_be_dir=False, ext=None):
	#TODO# Use cfg to iterate through options
	#TODO# Ignoring must_be_dir, ext
	attempt = FileTarget.from_exact_match (uri)
	if attempt is None:
		raise Exception ('Unrecognised Target URI: %s' % (uri,))
	return attempt


def kip_up (kip_up, uris, cfg, opts):
	"""Perform the "kip up" command.  One source, any number of
	   targets.
	"""
	if len (uris) > 0:
		src = uris [0]
	else:
		src = '.'
	if len (uris) > 1:
		tgts = uris [1:]
	else:
		tgts = [ '.' ]
	src =    make_source (src, cfg)
	tgts = [ make_target (tgt, cfg, ext='kip') for tgt in tgts ]
	doc = Document ()
	# keynr = doc.claim_keynr ()
	keynr = 2020
	key = TabKey ('pink.unicorn.demo.arpa2.org', 'unicorn.demo.arpa2.org', keynr, 20)
	sig = SigMud (keynr, None)
	doc.append (key)
	with src.open () as sf:
		#TODO# Read in smaller blocks
		blk = sf.read (16384)
		while len (blk) > 0:
			lit = Lit (blk)
			mud = Mud (keynr, lit)
			doc.append (mud)
			blk = sf.read (16384)
	doc.append (sig)
	ia = CommandInteractor ()
	doc.kip_up (interactor=ia)
	for tgt in tgts:
		tf = tgt.open (origin=src)
		for frag in doc.generate_cbor ():
			tf.write (frag)
		tf.close ()


def kip_down (kip_down, uris, cfg, opts):
	"""Perform the "kip down" command.  Any number of sources,
	   one target.
	"""
	if len (uris) > 1:
		srcs = uris [:-1]
		tgt  = uris [ -1]
	elif len (uris) == 1:
		srcs = uris [:1]
		tgt  =   '.'
	else:
		srcs = [ '.' ]
		tgt  =   '.'
	multisrc = len (srcs) > 1
	srcs = [ make_source (src, cfg) for src in srcs ]
	tgt  =   make_target (tgt, cfg, must_be_dir=multisrc, ext='')
	ia = CommandInteractor ()
	for src in srcs:
		sf = src.open ()
		doc = Document.from_cbor_file (sf)
		sf.close ()
		doc.kip_down (interactor=ia)
		tf = tgt.open (origin=src)
		for chunk in doc.generate_data ():
			if isinstance (chunk, Meta):
				print ('TODO: Pass Metadata from %r' % (chunk,), file=sys.stderr)
			elif isinstance (chunk, Lit):
				tf.write (chunk.get_content ())
			elif isinstance (chunk, Ref):
				print ('TODO: Dereference and pass from %r' % (chunk,), file=sys.stderr)
			else:
				print ('TODO: Unknown chunk?!?')
		tf.close ()


def kip_dump (kip_dump, uris, cfg, opts):
	"""Make a textual dump of a KIP Document.  One source only.
	"""
	if len (uris) > 1:
		raise Exception ('Only one <source> can be dumped')
	elif len (uris) == 1:
		src = uris [0]
	else:
		src = '.'
	# src = make_source (src, cfg)
	ia = CommandInteractor ()
	#TODO# Really brutal to just assume/read a file
	doc = Document.from_cbor_file (open (src, 'rb'))
	for raw_chunk in doc.generate_cbor ():
		if raw_chunk.startswith (b'ARPA2KIP') and raw_chunk.endswith (b'\n') and len (raw_chunk) == 19:
			print ('MAGIC HEADER: %s' % (str(raw_chunk,'ascii')).rstrip ())
			continue
		try:
			raw = ' '.join ([ '%02x' % byte for byte in raw_chunk ])
			print ('%s' % (raw,))
			chunk = cbor.loads (raw_chunk)
			if type (chunk) == list and len (chunk) >= 2 and chunk [0] in tags:
				cls = tags [chunk [0]]
				chunk = tuple ([cls.tag, *chunk [1:]])
				print ('%s\t%r' % (cls.__name__,chunk))
			else:
				print ('UNKNOWN: %r#%d' % (chunk,len(raw_chunk)))
		except Exception as e:
			print ('INVALID: %r' % (raw_chunk,))
			raise


# Map from subcommand (the word after "kip") and the handler function
#
cmdmap = {
	'up'  : kip_up,
	'down': kip_down,
	'dump': kip_dump,
}


def fetch_config ():
	# ... setup() ...
	pass


def parse_opts (argv, cfg):
	cmd = os.path.basename (argv [0])
	subcmd = argv [1]
	assert subcmd in cmdmap, 'Unknown command \"%s\"' % (argv[1],)
	return (cmd, None, subcmd, argv [2:])


def main (argv=None, config=None):
	"""The main command processes sys.argv or, though it may also be
	   called with an alternate list of strings.  Similarly, there
	   is a possibility to provide an explicit configuration object.
	"""
	if argv is None:
		argv = sys.argv
	if config is None:
		config = fetch_config ()
	try:
		(cmd,opts,subcmd,uris) = parse_opts (argv, config)
	except Exception as e:
		if len (argv) > 0:
			cmd = os.path.basename (argv [0])
		print (e, file=sys.stderr)
		print ('Usage:\n   %s <flags> up [<source> [<target>...]]\n   %s <flags> down [<target> [<source>...]]\n   %s dump <source>' % (cmd,cmd,cmd), file=sys.stderr)
		print ('Flags:\n   -i|--integrity for ensuring (just) integrity\n   -e|--encryption for ensuring (just) privacy\n   -a|--authentic for ensuring authenticity', file=sys.stderr)
		print ('For <source> and <target> use URIs, or a single dot for the default', file=sys.stderr)
		raise  #DEBUG#
		sys.exit (1)
	cmdname = '%s %s' % (cmd, subcmd)
	cmdmap [subcmd] (cmdname, uris, config, opts)
	sys.exit (0)


if __name__ == '__main__':
	main ()

