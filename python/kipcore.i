%module kipcore

%include "stdint.i"
%include "typemaps.i"
%include "cpointer.i"

%{

#undef  SUPPRESS_KIP_CORE
#define SUPPRESS_KIP_SERVICE 1
#define SUPPRESS_KIP_DAEMON  1
#include <arpa2/kip.h>

%}

/*
%inline %{

inline kipt_ctx kipctx_open2 (void) {
        kipt_ctx newctx;
        if (!kipctx_open (&newctx)) {
                return NULL;
        }
        return newctx;
}

%}
*/

/* Construct new_T() and T_value() functions -- as well as refcount support */
%pointer_functions (kipt_ctx,ctx);
%pointer_functions (kipt_alg,alg);
%pointer_functions (kipt_keynr,keynr);
%pointer_functions (kipt_keyid,keyid);
%pointer_functions (uint32_t,length);

/* %typemap(argout) kipt_ctx *out_ctx { $result = * $1; } */

/* %typemap(inout) uint8_t * { $result = (uint8_t *) $1; } */
/* %typemap(in)    uint8_t * { $result = (uint8_t *) $1; } */
/*%typemap(argout)   (uint32_t,uint8_t *) { $result = PyBuffer_FromMemory ($2, $1); }*/
/* %typemap(in)   (uint32_t,uint8_t *) { $result = PyBuffer_FromMemory ($2, $1); } */
/* %typemap(in)   (uint32_t len_target,uint8_t *out_target) { Py_buffer view; if (0 != PyObject_GetBuffer ($input, &view, PyBUF_ANY_CONTIGUOUS | PyBUF_WRITABLE)) { SWIG_exception_fail (SWIG_ArgError ($input), "need buffer"); } $1 = (uint32_t) view.len; $2 = (uint8_t *) view.buf; } */


/* TODO: Silly variation in (ptr,len) passing, see issue #15 */
/* TODO: Especially kipsum_sign() is unworkable for SWIG, change it */


/* Typemap for buf output in kipsum_sign() -- luckily its _name_ is unique... */
%typemap(in)
        (uint8_t *out_sig) {
                printf ("Type mapping out_sig [for terrible kipsum_sign() signature]\n");
                if ($input == Py_None) {
                        printf ("Recognised Py_None, passing NULL\n");
                        $1 = NULL;
                } else {
                        Py_buffer view;
                        PyObject_GetBuffer ($input, &view, PyBUF_ANY_CONTIGUOUS | PyBUF_WRITABLE);
                        $1 = (uint8_t *) view.buf;
                }
        }


/* Typemap for (len,buf) pair; may be None; otherwise writeable */
%typemap(in)
        (uint32_t len_target,uint8_t *out_target) {
                printf ("Type mapping len_target,out_target\n");
                if ($input == Py_None) {
                        printf ("Recognised Py_None, passing 0,NULL\n");
                        $1 = 0;
                        $2 = NULL;
                } else {
                        Py_buffer view;
                        PyObject_GetBuffer ($input, &view, PyBUF_ANY_CONTIGUOUS | PyBUF_WRITABLE);
                        $1 = (uint32_t ) view.len;
                        $2 = (uint8_t *) view.buf;
                }
        }

/* Typemap for (len,buf) pair; may be None; readonly is acceptable */
%typemap(in)
        (uint32_t siglen,uint8_t *sig) {
                printf ("Type mapping len_target,out_target\n");
                if ($input == Py_None) {
                        printf ("Recognised Py_None, passing 0,NULL\n");
                        $1 = 0;
                        $2 = NULL;
                } else {
                        Py_buffer view;
                        PyObject_GetBuffer ($input, &view, PyBUF_ANY_CONTIGUOUS);
                        $1 = (uint32_t ) view.len;
                        $2 = (uint8_t *) view.buf;
                }
        }

/* Typemap for (buf,len) pair; may be None; readonly is ok */
%typemap(in)
        (uint8_t *clear,uint32_t clearsz),
        (uint8_t *data,uint32_t datasz) {
                printf ("Type mapping clear,clearsz\n");
                if ($input == Py_None) {
                        printf ("Recognised Py_None, passing NULL,0\n");
                        $1 = NULL;
                        $2 = 0;
                } else {
                        Py_buffer view;
                        PyObject_GetBuffer ($input, &view, PyBUF_ANY_CONTIGUOUS);
                        $1 = (uint8_t *) view.buf;
                        $2 = (uint32_t ) view.len;
                }
        }

/* Typemap for (buf,len) pair; may be None; otherwise writeable */
%typemap(in)
        (uint8_t *out_mud,uint32_t max_mudsz),
        (uint8_t *out_clear,uint32_t max_clearsz),
        (uint8_t *out_keymud,uint32_t max_keymudsz) {
                printf ("Type mapping out_mud,max_mudsz\n");
                if ($input == Py_None) {
                        printf ("Recognised Py_None, passing NULL,0\n");
                        $1 = NULL;
                        $2 = 0;
                } else {
                        Py_buffer view;
                        PyObject_GetBuffer ($input, &view, PyBUF_ANY_CONTIGUOUS | PyBUF_WRITABLE);
                        $1 = (uint8_t *) view.buf;
                        $2 = (uint32_t ) view.len;
                }
        }

/* Typemap for (buf,len) pair; must not be None; readonly is ok */
%typemap(in)
        (uint8_t *mud,uint32_t mudsz),
        (uint8_t *keymud,uint32_t keymudsz),
        (const uint8_t *salt,uint32_t salt_len),
        (uint8_t *usermud,uint32_t usermud_len) {
                printf ("Type mapping mud/mudsz annex keymud/keymudsz\n");
                Py_buffer view;
                PyObject_GetBuffer ($input, &view, PyBUF_ANY_CONTIGUOUS);
                $1 = (uint8_t *) view.buf;
                $2 = (uint32_t ) view.len;
        }

/* Typemap for (num,ptr) array of integers; must not be None */
%typemap(in)
        // NOTE: %typemap(freearg) below must mirror this one
        (uint16_t mappingdepth,kipt_keyid *mappingkeyids) {
                printf ("Type mapping mappingdepth,mappingkeyids\n");
                $1 = PyList_Size ($input);
                // $1 = PyTuple_Size ($input);
                $2 = (kipt_keyid *) malloc (sizeof (kipt_keyid [$1]));
                assert ($2 != NULL); //TODO//
                int i;
                for (i=0; i<($1); i++) {
                        ($2) [i] = (kipt_keyid) PyLong_AsUnsignedLong (PyList_GetItem ($input, i));
                        // ($2) [i] = (kipt_keyid) PyLong_AsUnsignedLong (PyTuple_GetItem ($input, i));
                        // ($2) [i] = (kipt_keyid) PyTuple_GetItem ($input, i);
                        printf (" - Entry %d of %d is %lx\n", i, ($1), (unsigned long int) (($2)[i]));
                }
                printf ("Type mapped to %d entries, %lx,%lx,...\n", (int) $1, (unsigned long int) ($2)[0], (unsigned long int) ($2)[1]);
        }
%typemap(freearg)
        // NOTE: %typemap(in) above allocated memory to clean up here
        (uint16_t mappingdepth,kipt_keyid *mappingkeyids) {
                free ($2);
        }

/* Specific type mapping for [ (keyid0,salt0), (keyid1,salt1), ... ] in kipkey_mixer()
 */
%typemap(in)
        // NOTE: %typemap(freearg) below must mirror this one
        (uint16_t num_keys,const kipt_keyid *keysin,const uint8_t **salts,const uint32_t *salt_lens) {
                printf ("Type mapping num_keys,keysin,salts,salt_lens\n");
                $1 = PyList_Size ($input);
                $2 = (kipt_keyid *) malloc (sizeof (kipt_keyid [$1]));
                assert ($2 != NULL); //TODO//
                $3 = (uint8_t **) malloc (sizeof (uint8_t * [$1]));
                assert ($3 != NULL); //TODO//
                $4 = (uint32_t *) malloc (sizeof (uint32_t [$1]));
                assert ($4 != NULL); //TODO//
                int i;
                for (i=0; i<($1); i++) {
                        PyObject *elem = PyList_GetItem ($input, i);
                        assert (elem != NULL); //TODO//
                        assert (PyTuple_Size (elem) == 2); //TODO//
                        ($2) [i] = (kipt_keyid) PyLong_AsUnsignedLong (PyTuple_GetItem (elem, 0));
                        PyObject *salt = PyTuple_GetItem (elem, 1);
                        assert (salt != NULL); //TODO//
                        ($3) [i] = (uint8_t *) PyBytes_AsString (salt);
                        Py_ssize_t saltlen = PyBytes_Size (salt);
                        assert (saltlen <= 65535);
                        ($4) [i] = saltlen;
                }
                printf ("Type mapped to %d entries\n", (int) $1);
        }
%typemap(freearg)
        // NOTE: %typemap(in) above allocated memory to clean up here
        (uint16_t num_keys,const kipt_keyid *keysin,const uint8_t **salts,const uint32_t *salt_lens) {
                free ($4);
                free ($3);
                free ($2);
        }

/* %typemap(argout) kipt_keyid *out_keyid { $result = PyInt_AsLong ($1); } */

/* %typemap(out) kipt_ctx *out_ctx { * $result = $1 ; } */

/* %typemap(out) uint32_t, uint8_t * { $result = $2; } */

#undef  SUPPRESS_KIP_CORE
#define SUPPRESS_KIP_SERVICE 1
#define SUPPRESS_KIP_DAEMON  1
%include <arpa2/kip.h>

/*
bool kip_init (void);
void kip_fini (void);
*/

/* kipt_ctx kipctx_open2 (void); */
/* void kipctx_close (kipt_ctx); */

