#
# KIP Document -- The KIP exchange format (based on CBOR)
#
# This module defines operations to work with KIP Documents.
#
# From: Rick van Rein <rick@openfortress.nl>
#


import sys
import os
import io
import abc
import struct

import re
import uuid
import base64

import cbor

import arpa2.kip.api as api


# The version string as used in the magic tag in CBOR.
# The format is YYYYMM, and should not change as long
# as it is extended without branching, so under the
# control of one registration authority, namely ARPA2.
# See //cbor.arpa2.org/kip-document for details.  Note
# that the mere addition of safe-to-ignore flags and
# tags will not produce other produced_versions.
#
produced_version = b'202003'
understood_versions = [ b'202003' ]


# The MIME-type.  As long as it is not registered
# we need the "x." prefix to be formally correct.
#
# produced_mimetype = 'message/x.kip'
produced_mimetype = 'message/x.kip'
understood_mimetypes = [ 'message/x.kip', 'message/kip' ]


# The integer values that start arrays in CBOR to tag their type.
# The remainder of the contents follow a suitable syntax, and are
# subsequently loaded into subclasses of Chunk, defined below.
#
TAG0_META      = 0
TAG0_LIT       = 1
TAG0_REF       = 2
TAG0_MUD       = 3
TAG0_KEYMUD    = 4
TAG0_SIGMUD    = 5
TAG0_TABKEY    = 6
TAG0_OFFKEY    = 7
TAG0_SVCKEYMUD = 8
TAG0_SVCSIGMUD = 9
TAG0_PUBKEY    = 10
TAG0_PUBDIG    = 11
TAG0_PUBSIG    = 12


# 'tags', defined below, maps TAG0_LIT to the Lit chunk class;
# each of these classes define a tag attribute like 'TAG0_LIT';
# 'revtags' maps those string tags back to the tag TAG0_LIT.
#
revtags = {
	'TAG0_META':      TAG0_META,
	'TAG0_LIT':       TAG0_LIT,
	'TAG0_REF':       TAG0_REF,
	'TAG0_MUD':       TAG0_MUD,
	'TAG0_KEYMUD':    TAG0_KEYMUD,
	'TAG0_SIGMUD':    TAG0_SIGMUD,
	'TAG0_TABKEY':    TAG0_TABKEY,
	'TAG0_OFFKEY':    TAG0_OFFKEY,
	'TAG0_SVCKEYMUD': TAG0_SVCKEYMUD,
	'TAG0_SVCSIGMUD': TAG0_SVCSIGMUD,
	'TAG0_PUBKEY':    TAG0_PUBKEY,
	'TAG0_PUBDIG':    TAG0_PUBDIG,
	'TAG0_PUBSIG':    TAG0_PUBSIG,
}



class Document (list):
	"""KIP Documents are essentially lists of chunks, each of which
	   is an object that inherits from Chunk.  It is possible to
	   directly manipulate the list, but care should be taken that
	   this may break signatures.
	   
	   Signatures assume Canonical CBOR format, but the chunks themselves
	   also have a canonical format to maintain, so manipulation of
	   individual chunks through their methods is a required default.
	"""

	#TODO# Currently strict: Key..Sig must nest like brackets, and close
	#TODO# Current spec requires plaintext signing if possible... unstable?
	#TODO# Signing requires canonical CBOR, which is not well-defined yet...

	def __init__ (self):
		"""Create a KIP Document.  The initial Document is empty, so it
		   has no Chunks.
		"""
		super (Document,self).__init__ ()
		self.kipctx = None

	def check_range (self, first=None, last=None):
		"""Check that the given range of chunks is valid in this KIP Document.
		   First defaults to the first Chunk; last defaults to the last Chunk.
		   Exceptions are raised on error; otherwise, filled-out values for
		   (first,last) are returned.
		"""
		first = first or 0
		last = last or len (self) - 1;
		if first < 0:
			raise Exception ('Range from non-existing chunk %d', first)
		if last >= len (self):
			raise Exception ('Range until non-existing chunk %d', last)
		if first > last:
			raise Exception ('Range in chunk order from %d to %d', first, last)
		return (first,last)

	def magic_prefix (self):
		"""Generate the magic prefix for the CBOR document.
		"""
		flagbytes = b'===='
		return b'ARPA2KIP%s%s\n' % (produced_version,flagbytes)

	def generate_cbor (self, first=None, last=None, magic=True):
		"""Generate the document as a sequence of CBOR byte strings.
		   The KIP Document consists of these strings concatenated.
		   Optionally indicate the first and last chunk to include.
		   The magic header is automatically prefixed if it seems
		   sensible; set to True or False if you know what you need.
		"""
		(first,last) = self.check_range (first=first, last=last)
		if magic:
			yield self.magic_prefix ()
		for chunk in self [first:last+1]:
			yield chunk.to_cbor ()

	def to_cbor_file (self, filehandle):
		"""Write the content provided to the CBOR file, which would
		   normally have a name ending in ".kip".  It is assumed
		   that magic information should be inserted, but when you
		   indicate a MIME-typed environment this is only done if
		   so required.  Normal files are not MIME-typed.
		"""
		for frag in self.generate_cbor ():
			f.write (frag)

	def to_data_uri (self):
		"""Write the content provided to a data: URI, using the
		   MIME type and probably no magic header.  This may be
		   used for small KIP Documents, such as references.
		   Signed and/or encrypted references add great value
		   and should have a reasonable size for this purpose.
		"""
		uri = 'data:%s' % (produced_mimetype,)
		data = b''
		for frag in self.generate_cbor (magic=False):
			data += frag
		uri += ';base64,' + str (base64.b64encode (data), 'ascii')
		return uri

	def cbor_to_hash (self, updatable, first=None, last=None, magic=False):
		"""Invoke the .update() method on updateable with bytes
		   that form the CBOR serialisation for the Chunk range.
		   This can be used to update a hash for a signature.
		   The .update() method may be called any number of
		   times from zero to (almost) infinity.  The magic
		   prefix is normally suppressed, but you can order it
		   setting it to True, or auto-generate if it would be
		   part of a generated document with None.
		"""
		for frag in self.generate_cbor (first=first, last=last, magic=magic):
			updatable.update (frag)

	def have_kip_context (self, kipContext=None):
		"""Return the KIP context for this object.  If none set yet,
		   one will be constructed anew.  In this case, it is also
		   permitted to supply a kipContext as argument, which will
		   then be used; this may help to preload keys.  The KIP
		   context will live throughout the life of the KIP Document.
		"""
		if kipContext is not None:
			assert self.kipctx is None, 'KIP Document already has a KIP Context'
			assert isInstance (kipContext, api.Context)
			self.kipctx = kipContext
		elif self.kipctx is None:
			self.kipctx = api.Context ()
		return self.kipctx

	def OLD_kip_down (self, interactor=None):
		#DEPRECATED_BUT_CHECK_IF_ALL_LOGIC_REPLACED#
		"""Pass through this Document to resolve as many keys as
		   possible, decrypt data and check signatures.  In short,
		   kip_down security.
		   
		   Some keys may not resolve in their context, which is not
		   a fatal problem.  Even inability to decrypt content or
		   validate signatures is not considered fatal.  This may
		   however be considered a problem during later processing
		   and should then be detected.
		   
		   The interactor is used for user interaction.  This is
		   used for acces to encrypted private keys, PKCS #11 use,
		   but also to use the KIP Service.  Each interaction will
		   clearly indicate what its intention is.  The object can
		   be used to enforce policies, such as not looking for
		   decryption keys in remote realms when KIP Service is
		   configured for the local realm.  It also performs the
		   client side of the SASL handshake for KIP Service, and
		   may influence the mechanisms to be used.
		"""
		self.have_kip_context ()
		for chunk in self:
			used = False
			if isinstance (chunk, Mud) and not chunk.got_clarity ():
				try:
					used = True
					chunk.clarify_content (self.kipctx, interactor)
					if chunk.got_clarity ():
						print ('DEBUG: Got Clear as Mud')
					else:
						print ('DEBUG: Failed to get Clear as Mud')
				except:
					pass
			elif isinstance (chunk, KeyIntro) and not chunk.got_keys ():
				try:
					used = True
					print ('%r.extract_keys (%r, %r)' % (chunk, self.kipctx, interactor))
					chunk.extract_keys (self.kipctx, interactor)
					if chunk.got_keys ():
						print ('DEBUG: Got Key')
					else:
						print ('DEBUG: Failed to get Key')
				except:
					pass
			elif isinstance (chunk, Validator) and not chunk.got_signers ():
				try:
					used = True
					chunk.validate_signature (self.kipctx, interactor)
					if chunk.got_signers ():
						print ('DEBUG: Computed Signature Check:', chunk.get_signers ())
					else:
						print ('DEBUG: Failed to compute Signature Check')
				except:
					pass
			if used and interactor is not None:
				interactor.reset ()
				used = False

	#TODO#DEPRECATED# We now know what kip_up() will be like
	def sign (self, addedKeyIntro, addedValidator, first=None, last=None):
		"""Sign a KIP Document, or optionally a range from first to
		   last Chunk index.  Zooming in on a range is not possible
		   when this breaks signatures made by others.  After this
		   call succeeds, it has inserted the key before first, and
		   the signature after last, so offsets have changed.  The
		   range including the key and signature is returned.
		"""
		openkeys = 0
		(first,last) = self.check_range (first=first, last=last)
		for chunk in self [:first]:
			if isinstance (chunk, KeyIntro):
				openkeys += 1
			if isinstance (chunk, Validator):
				if openkeys <= 0:
					raise Exception ('More keys sign than were announced')
				openkeys -= 1
		if openkeys > 0:
			raise Exception ('Cannot sign from chunk %d: Leader signatures would break')
		openkeys = 0
		if last < len (self):
			for chunk in self [last+1:]:
				if isinstance (chunk, KeyIntro):
					#TODO# keyS and counting...?
					openkeys += 1
				if isinstance (chunk, Validator):
					#TODO# keyS and counting...?
					openkeys -= 1
			if openkeys > 0:
				raise Exception ('The document ends with open keys')
			if openkeys < 0:
				raise Exception ('Cannot sign until chunk %d: Trailer signatures would break')
		last = last or len (self)
		self.insert (first, addedKeyIntro)
		first += 1
		last += 1
		self.insert (last+1, addedValidator)
		return (first-1,last+1)

	#TODO#DEPRECATED# We now know what kip_down() is like
	def verifiable_ranges (self):
		"""Return ranges of a KIP Document that could be verified.
		   For each range, return (first,last) where first points
		   at the KeyIntro and last to the corresponding Validator.
		   Since a KIP Document is a list, these can then be used
		   to retrieve these values.
		"""
		retval = [ ]
		keys = [ ]
		for (j,chunk) in enumerate (self):
			if isinstance (chunk, KeyIntro):
				keys.append (j)
			if isinstance (chunk, Validator):
				assert len (keys) > 0, 'Malformed KIP Document: Validator without KeyIntro'
				i = keys.pop ()
				retval.append ( (i,j) )
		assert len (keys) == 0, 'Malformed KIP Document: KeyIntro without Validator'
		return retval

	def claim_keynr (self):
		"""Return a fresh key number, not used before.  Claim it, so
		   no other claim_keynr() can be made against it.  There is no
		   ban on anyone claiming the same number however; this is a
		   voluntary mechanism.
		   
		   A side effect of this call is creating a KIP Context if
		   none is assigned to this KIP Document yet.  That's the
		   context within which the key number is unique.
		"""
		return self.have_kip_context ().claim_keynr ()

	def _kip_updown_gather_keys (self, interactor):
		"""Make a pass over the KIP Document to gather keys that
		   are available, and either on-key or off-key at each
		   point.
		   
		   This is an idempotent call: Repeated calls on the same data
		   have no effect.  When something has been changed however,
		   you will be able to give that another try.
		   
		   This call is designed to be called in the kip_up() and
		   kip_down() sequence.
		"""
		on_key  = set ()
		off_key = set ()
		success = [ ]
		failure = [ ]
		for (i,chunk) in enumerate (self):
			if isinstance (chunk, Content):
				chunk.set_on_off_key (on_key, off_key)
			if isinstance (chunk, KeyIntro):
				if not chunk.got_keys ():
					chunk.extract_keys (self.kipctx, interactor)
				if chunk.got_keys ():
					on_key.update (chunk.get_keys ())
					success.append ( (i,chunk) )
				else:
					failure.append ( (i,chunk) )
			if isinstance (chunk, OffKey):
				togo = set ()
				for keynr in chunk.get_off_keys ():
					togo.update (self.kipctx.key_fromnumber (keynr))
				print ('Keys togo = %r' % (togo,))
				total = set (on_key + off_key)
				on_key  = total.difference (togo)
				off_key = togo
		interactor.report_gather_keys (success, failure)

	def _kip_up_make_signatures (self, interactor):
		"""Make a pass making signatures in this KIP Document.
		   This involves detecting introduction of each Key by a
		   KeyIntro and resetting its hash, then updating its Sum
		   with Content material inasfar as the various Keys are
		   on-key.  Note that nothing is done with encryption in
		   this phase, as that would alter the Sum.  Encryption is
		   assumed to have been applied before this run.
		   
		   Signatures mention a keynr, which is not unique.  There may
		   be document merges, leading to overlapping numbers.  This
		   is not a problem; a successful signature can be traced back
		   with full certainty to the exact Key that matched, and any
		   associated identity of that Key.  For this reason, it is
		   safe to consider a signature successful if only one Key for
		   a given keynr matches.
		   
		   This is an idempotent call: Repeated calls on the same data
		   have no effect.  When something has been changed however,
		   you will be able to give that another try.
		   
		   This call is designed to be called in the kip_up() sequence.
		"""
		success = [ ]
		failure = [ ]
		all_key = set ()
		off_key = set ()
		for (i,chunk) in enumerate (self):
			if isinstance (chunk, KeyIntro):
				keys = chunk.get_keys ()
				for key in keys:
					key.restart ()
					all_key.add (key)
			if isinstance (chunk, OffKey):
				off_key = chunk.get_off_key ()
			if isinstance (chunk, Content):
				chunk.reset_trust ()
				cbor = chunk.to_cbor ()
				#TODO# Double work, can use get_on_keys()
				for k in all_key.difference (off_key):
					k.append (cbor)
			if isinstance (chunk, Validator):
				# Might skip in repeated cases?
				chunk.make_signature (self.kipctx, interactor)
				print ('DEBUG: chunk.validkeys = %r' % (chunk.validkeys,))
				if chunk.is_signed ():
					print ('Validator %d is signed' % (i,))
					success.append ( (i,chunk) )
				else:
					print ('Validator %d is NOT signed' % (i,))
					failure.append ( (i,chunk) )
		interactor.report_make_signatures (success, failure)

	def _kip_down_validate_signatures (self, interactor):
		"""Make a pass checking signatures used in this KIP Document.
		   This involves detecting introduction of each Key by a
		   KeyIntro and resetting its hash, then updating its Sum
		   with Content material inasfar as the various Keys are
		   on-key.  Note that nothing is done with encryption in
		   this phase, as that would alter the Sum.
		   
		   Signatures mention a keynr, which is not unique.  There may
		   be document merges, leading to overlapping numbers.  This
		   is not a problem; a successful signature can be traced back
		   with full certainty to the exact Key that matched, and any
		   associated identity of that Key.  For this reason, it is
		   safe to consider a signature successful if only one Key for
		   a given keynr matches.
		   
		   This is an idempotent call: Repeated calls on the same data
		   have no effect.  When something has been changed however,
		   you will be able to give that another try.
		   
		   This call is designed to be called in the kip_down() sequence.
		"""
		success = [ ]
		failure = [ ]
		all_key = set ()
		off_key = set ()
		for (i,chunk) in enumerate (self):
			if isinstance (chunk, KeyIntro):
				keys = chunk.get_keys ()
				for key in keys:
					key.restart ()
					all_key.add (key)
			if isinstance (chunk, OffKey):
				off_key = chunk.get_off_key ()
			if isinstance (chunk, Content):
				chunk.reset_trust ()
				cbor = chunk.to_cbor ()
				#TODO# Double work, can use get_on_keys()
				for k in all_key.difference (off_key):
					k.append (cbor)
			if isinstance (chunk, Validator):
				# Might skip in repeated cases?
				chunk.validate_signature (self.kipctx, interactor)
				if chunk.is_signed ():
					print ('Validator %d is signed' % (i,))
					success.append ( (i,chunk) )
				else:
					print ('Validator %d is NOT signed' % (i,))
					failure.append ( (i,chunk) )
		interactor.report_validate_signatures (success, failure)

	def _kip_down_windback_trust   (self, interactor):
		"""Make a pass in backward direction to relay trust information
		   from signatures in Validator to the signed info in Content.
		   The Content may have its trust set while signatures are being
		   collected in a forward pass, and then set by this reverse pass.
		   
		   This is an idempotent call: Repeated calls on the same data
		   have no effect.  When something has been changed however,
		   you will be able to give that another try.
		   
		   This call is designed to be called in the kip_down() sequence.
		"""
		success = [ ]
		failure = [ ]
		keys_ok = set ()
		jmax = len (self) - 1
		for (j,chunk) in enumerate (reversed (self)):
			i = jmax - j
			print ('Chunk %d is-a %s' % (i,type(chunk).__name__))
			if isinstance (chunk, Validator):
				if chunk.is_signed ():
					signers = chunk.get_signers ()
					# Credit each succeeded Key; ignore other that failed
					keys_ok.update (signers)
					print ('Validator %d adds signers %r' % (i,signers))
				else:
					# Signatures made by unknown Keys may silently fail
					# but keys known by number should (probably) succeed
					keynr = chunk.get_keynr ()
					nonsigners = self.kipctx.key_fromnumber (keynr)
					keys_ok.difference_update (nonsigners)
					print ('Validator %d removes signers %r' % (i,nonsigners))
			if isinstance (chunk, KeyIntro):
				keys = chunk.get_keys ()
				print ('KeyIntro %d introduced keys %r' % (i,keys))
				if not keys.isdisjoint (keys_ok):
					print ('KeyIntro %d is the first position for signer %r' % (i,keys))
					keys_ok.difference_update (keys)
			if isinstance (chunk, Content):
				print ('Content %d setup with %r, while it is on_key for %r' % (i,keys_ok,chunk.get_on_key ()))
				chunk.set_trust (keys_ok)
				if chunk.is_trusted ():
					print ('Content %d is trusted' % i)
					success.insert (0, (i,chunk) )
				else:
					print ('Content %d not trusted' % i)
					failure.insert (0, (i,chunk) )
		interactor.report_windback_trust (success, failure)

	def _kip_down_clarify_content (self, interactor):
		"""Make a pass decrypting data in this KIP Document.
		   This applies to Mud, which may then result in Lit,
		   Ref or Meta to be exposed.
		   Data extraction is orthogonal to signature checks,
		   because signatures are made on the encrypted format.
		   
		   This is an idempotent call: Repeated calls on the same data
		   have no effect.  When something has been changed however,
		   you will be able to give that another try.
		   
		   This call is designed to be called in the kip_down() sequence.
		"""
		success = [ ]
		failure = [ ]
		for (i,chunk) in enumerate (self):
			if isinstance (chunk, Mud) and not chunk.got_clarity ():
				chunk.clarify_content (self.kipctx, interactor)
				if not chunk.got_clarity ():
					failure.append ( (i,chunk) )
				else:
					success.append ( (i,chunk) )
		interactor.report_clarify_content (success, failure)

	def _kip_up_muddle_content (self, interactor):
		"""Make a pass encrypting data in this KIP Document.
		   This applies to Mud, which may then result in Lit,
		   Ref or Meta to be concealed.
		   Data muddling is orthogonal to signature checks,
		   because signatures are made on the encrypted format.
		   
		   This is an idempotent call: Repeated calls on the same data
		   have no effect.  When something has been changed however,
		   you will be able to give that another try.
		   
		   This call is designed to be called in the kip_up() sequence.
		"""
		success = [ ]
		failure = [ ]
		for (i,chunk) in enumerate (self):
			if isinstance (chunk, Mud) and not chunk.got_muddled ():
				chunk.muddle_content (self.kipctx, interactor)
				if not chunk.got_muddled ():
					failure.append ( (i,chunk) )
				else:
					success.append ( (i,chunk) )
		interactor.report_muddle_content (success, failure)

	def kip_down (self, interactor):
		"""KIP Down on security.  This makes a pass over the document
		   to validate its security and perform the separate kip_down
		   invocations on Chunks of Mud in the KIP Document.  This is
		   done in a few phases, each of which reports success and
		   failure to the interactor.
		   
		   This is an idempotent call: Repeated calls on the same data
		   have no effect.  When something has been changed however,
		   you will be able to give that another try.
		   
		   Effectively, kip_up() and kip_down() are reverse operations,
		   just like the C library calls do for individual Chunks.
		"""
		self.have_kip_context ()
		self._kip_updown_gather_keys       (interactor)
		self._kip_down_validate_signatures (interactor)
		self._kip_down_windback_trust      (interactor)
		self._kip_down_clarify_content     (interactor)

	def kip_up (self, interactor):
		"""KIP Up on security.  This makes a pass over the document
		   to make kip_up invocations on Chunks of Mud and then
		   (re)produce signatures.  This is done in a few phases,
		   each of which reports success and failure to the
		   interactor.  Where possible, existing signatures by others
		   will be retained.  The same applies to existing encryption
		   with others' keys to unchanged data.
		   
		   This is an idempotent call: Repeated calls on the same data
		   have no effect.  When something has been changed however,
		   you will be able to give that another try.
		   
		   Effectively, kip_up() and kip_down() are reverse operations,
		   just like the C library calls do for individual Chunks.
		"""
		self.have_kip_context ()
		self._kip_updown_gather_keys (interactor)
		self._kip_up_muddle_content  (interactor)
		self._kip_up_make_signatures (interactor)

	def generate_data (self, obj=True, ref=True, meta=True,
			nested=False, untrusted=False, encrypted=False):
		"""Generate the chunks of protected data in this KIP Document.
		   The production defaults to the Lit, Ref and Meta chunks,
		   but a flag for each can stop them from being generated.
		   When Mud has extracted data, it will be dealt with instead.
		   
		   When nested is set, any ref that has been resolved will be
		   entered into; when ref is also True, it will be generated
		   too, before and after entering into the nested document.
		   It is not too late to resolve a Ref during the before-call.
		   
		   When untrusted is set, then even chunks with no signature
		   or with more than one failing signatures are presented;
		   these are otherwise skipped.  When encrypted is set, chunks
		   chunks that failed to decrypt are also shown, also contrary
		   to default behaviour.
		"""
		for chunk in self:
			print ('generate_data() considers %r' % chunk)
			# Mud is extracted to clear data
			if isinstance (chunk, Mud) and chunk.got_clarity ():
				chunk = chunk.get_clarity ()
				print ('generate_data() clarified %r' % chunk)
			# Content objects are shown by default
			show = isinstance (chunk, Content)
			print ('generate_data() shows content, so for %r, if %r' % (type(chunk),show))
			# Test for trust (now we know we have a Content)
			if show and not chunk.is_trusted ():
				print ('generate_data() shows untrusted data if %r' % untrusted)
				show = untrusted
			# Test for unreadable Mud
			if show and isinstance (chunk, Mud):
				print ('generate_data() shows encrypted data if %r' % encrypted)
				show = encrypted
			# Generate the value, possibly nest
			if show:
				print ('generate_data() yields %r' % chunk)
				yield chunk
				# Consider nesting
				if nested and isinstance (chunk, Ref):
					subdoc = chunk.get_subdoc ()
					if subdoc is not None:
						# Nest into the sub-document
						yield from self.generate_data (
								obj=obj, ref=ref, meta=meta,
								nested=True,
								untrusted=untrusted,
								encrypted=encrypted)
						# Generate the Ref once more
						yield chunk

	@classmethod
	def from_cbor_generator (cls, gen):
		"""Given a generator that produces one CBOR data fragment
		   at a time, construct a KIP Document (or subclass thereof,
		   as supplied in cls).  Consume until the generator ends.
		   Raise exceptions when anything is wrong, such as syntax
		   errors in the content.
		"""
		assert issubclass (cls, Document)
		def prefixgen (t,g):
			yield t
			yield from g
		token = next (gen)
		if type (token) == bytes and token == b'R':
			token = next (gen)
			if type (token) != bytes or len (token) != 16 or token [:5] != b'A2KIP' or token [15] != 10:
				raise Exception ('Error in magic header')
			version = token [5:11]
			if version not in understood_versions:
				raise Exception ('Unsupported version %s in magic header' % (version,))
			#IGNORE# flags = token [11:15]
		else:
			gen = prefixgen (token, gen)
		doc = cls ()
		for token in gen:
			chunk = Chunk.from_cbor_array (token)
			doc.append (chunk)
		return doc

	@classmethod
	def from_cbor_file (cls, filehandle):
		"""Given a file that contains CBOR encoded data, construct
		   construct a KIP Document (or subclass thereof, as supplied
		   in cls).  Consume until the generator ends.
		   Raise exceptions when anything is wrong, such as syntax
		   errors in the content.
		"""
		def cbor_generator (f):
			try:
				while True:
					yield cbor.load (f)
			except EOFError:
				pass
		return cls.from_cbor_generator (cbor_generator (filehandle))

	@classmethod
	def from_cbor_bytes (cls, cbor_bytes):
		"""Given a string of CBOR encoded bytes, construct a
		   KIP Document (or subclass thereof, as supplied in cls).
		   Raise exceptions when anything is wrong, such as syntax
		   errors in the content.
		"""
		file_ish = io.BytesIO (cbor_bytes)
		return cls.from_cbor_file (file_ish)

	@classmethod
	def from_data_uri (cls, uri):
		"""Decode a data: URI holding a KIP Document.
		"""
		if uri [:5] != 'data:':
			raise Exception ('Only data: URI support for a KIP Document')
		comma = uri.index (';base64,')
		words = uri [5:comma].split (';')
		if words [0] not in understood_mimetypes:
			raise Exception ('MIME-type %s is not a KIP Document', words [0])
		if len (words) > 1:
			raise Exception ('data: URI has funny flags', words [1:])
		cbor_data = base64.b64decode (uri [comma+8:])
		return cls.from_cbor_bytes (cbor_data)


class Chunk ():
	"""Chunk is the superclass for each sequenced chunk in a KIP Document.
	"""
	@abc.abstractmethod
	def to_cbor (self):
		"""Subclass should override this method with one that produces
		   Canonical CBOR to represent its Chunk in a KIP Document.
		"""
		raise NotImplementedError ('Chunk:to_cbor()')

	@classmethod
	def from_cbor_array (cls, token):
		"""Factory for generation of a Chunk subclass instance from a
		   CBOR array that starts with the tag indicating what class
		   is meant.  Raise an exception on (parsing) errors.
		"""
		if type (token) != list or len (token) == 1:
			raise Exception ('CBOR syntax: Expected a non-empty array')
		if token [0] not in tags:
			raise Exception ('KIP Document syntax: Unregognised tag %d' % (token[0],))
		subcls = tags [token [0]]
		#DEBUG# print ('Parsing for class %s' % (subcls.__name__,))
		argstx = subcls.argstx
		if argstx is not None:
			if len (argstx) + 1 != len (token):
				print ('DEBUG: %r' % (token,))
				raise Exception ('KIP Document syntax: List length for %s should be %d, not %d' % (subcls.__name__,len(argstx)+1,len(token)))
			for (i,t) in enumerate (argstx, 1):
				if type (token [i]) != t:
					raise Exception ('KIP Document syntax: List entry %d for %s should be %r, not %r' % (i,subcls.__name__,t,type(token[i])))
		return subcls (*token[1:])

	@classmethod
	def from_cbor (cls, cbor_bytes):
		"""Factory for generation of a Chunk subclass instance from
		   CBOR bytes that start with the tag indicating what class
		   is meant.  Raise an exception on (parsing) errors.
		"""
		cbor_array = cbor.loads (cbor_bytes)
		print ('intermediate cbor_array = %r' % cbor_array)
		return cls.from_cbor_array (cbor_array)


class KeyIntro (Chunk):
	"""KeyIntro is the superclass for key-introducing objects in KIP Documents.
	   One object can introduce multiple keys, which is especially the case
	   for KIP Service, which may yield alternate KIP-Group elements for the
	   various black/white list and from/till timer settings.
	"""
	def __init__ (self):
		self.keys = None

	@abc.abstractmethod
	def extract_keys (self, kipctx, interactor):
		"""Subclass should override this method and call set_keys()
		   when successful.
		"""
		raise NotImplementedError ('KeyIntro:extract_keys()')

	@abc.abstractmethod
	def get_keynrs (self):
		"""Subclasses return the key numbers introduced by this KeyIntro.
		"""

	def set_keys (self, keys):
		"""Subclasses call this method to set one or more Key objects
		   that are managed by the KIP Document's own KIP Context.
		"""
		assert self.keys is None, 'KeyIntro already has a key'
		for key in keys:
			assert isinstance (key, api.Key)
		self.keys = set (keys)

	def get_keys (self):
		"""Return the set of (extracted) Key objects or None if
		   none is present.  This would be a security problem if
		   we were not among friends.
		"""
		return self.keys

	def got_keys (self):
		"""Return a boolean to indicate the presence of a Key.
		"""
		return self.keys is not None

class Content (Chunk):
	"""Content is the superclass for objects to be digested for signing in KIP Documents.
	"""
	def __init__ (self):
		self.on_key  = None
		self.off_key = None
		self.trusted = set ()

	def set_on_off_key (self, on_key, off_key):
		self.on_key  = set (on_key)
		self.off_key = set (off_key)
		self.reset_trust ()

	def get_on_key (self):
		return self.on_key

	def get_off_key (self):
		return self.off_key

	def set_trust (self, checked):
		"""Store which Keys were checked, without finding an
		   invalid signature.  Usually, trust means that
		   checks is non-empty and invalid is empty.  The set
		   of keys will be reduced to the on-key subset, which
		   is assumed to have been set before.
		"""
		self.trusted = set (self.on_key.intersection (checked))

	def reset_trust (self):
		"""Reset the trust in this Content.  This is useful
		   while making a forward pass of signing, while
		   results are pending.
		"""
		self.trusted = None

	def get_trust (self):
		"""Return the Keys that express trust in this Content
		   through a later Validator incorporating this.
		"""
		return self.trusted

	def is_trusted (self):
		"""Return the conclusion whether trust was reached.
		   This means that at least one Key validated the
		   signature while none failed to validate it.  This
		   limits the scope to the Keys available in this
		   KIP Document and its KIP context.
		"""
		return self.trusted is not None and len (self.trusted) > 0

class Validator (Chunk):
	"""Validator is the superclass for objects holding signatures in KIP Documents.
	"""
	def __init__ (self):
		self.validkeys = None

	@abc.abstractmethod
	def get_keynrs (self):
		"""Subclass returns the keynrs that are to be used in the signature.
		"""
		raise NotImplementedError ('Validator:get_keynr()')

	@abc.abstractmethod
	def validate_signature (self, kipctx, interactor):
		"""Subclass should override this method and call set_signers()
		   when successful.  It will probably have to obtain a list of
		   Key objects for a given keynr from kipctx, in light of the
		   non-unique key numbering system in KIP Documents.
		"""
		raise NotImplementedError ('Validator:validate_signature()')

	@abc.abstractmethod
	def make_signature (self, kipctx, interactor):
		"""Subclass should override this method and call TODO:set_signers()
		   when successful.  It will probably have to obtain a list of
		   Key objects for a given keynr from kipctx, in light of the
		   non-unique key numbering system in KIP Documents.
		"""
		raise NotImplementedError ('Validator:validate_signature()')

	def set_signers (self, validkeys):
		"""Subclasses call this method to set the keys that formed
		   correct signatures.
		"""
		self.validkeys = set (validkeys)

	def got_signers (self):
		return self.validkeys is not None and len (self.validkeys) > 0

	def get_signers (self):
		return self.validkeys

	def is_signed (self):
		return len (self.validkeys) > 0


class Meta (Content):
	"""Meta data chunks contain a key with one or more values.
	   The key name is an UTF-8 string, each value is a byte sequence
	   or UTF-8 string.  Being signed content, so it is a Content.
	   
	   Since different people may inject different bits of metadata, 
	   the overall metadata for a KIP Document requires combining
	   these chunks.  Since the signers for a chunk may vary across
	   a KIP Document, the question "who says so" is valid for each
	   of the values, especially because the same name may be met
	   with multiple meta data chunks.
	"""
	tag = 'TAG0_META'
	argstx = None

	def __init__ (self, name, *values):
		super(Meta,self).__init__ ()
		assert type (name) == str
		assert len (values) > 0
		assert all ([ type(v) in [str,bytes] for v in values ])
		self.name = name
		self.values = values

	def to_cbor (self):
		return cbor.dumps ([TAG0_META, self.name, *self.values])

class Lit (Content):
	"""Literal chunks contain user data, in plaintext form or as bytes.
	   Its inner contents are subject to an application's logic.  Look
	   in metadata for a MIME type or other information to help identify
	   how to drive that into further applications.  The content may have
	   various types; for now, bytes and str are supported, so binary
	   content or UTF-8 encoded character content.
	"""
	tag = 'TAG0_LIT'
	argstx = None

	def __init__ (self, *one_lit):
		super(Lit,self).__init__ ()
		if len (one_lit) != 1:
			raise Exception ('KIP Document syntax: Literal must have a single argument')
		lit = one_lit [0]
		if type (lit) not in [bytes,str]:
			raise Exception ('KIP Document syntax: Literal must be one of bytes, str')
		self.lit = lit

	def to_cbor (self):
		return cbor.dumps ([TAG0_LIT, self.lit])

	def set_content (self, litval):
		self.lit = litval

	def got_content (self):
		return self.lit is not None

	def get_content (self):
		return self.lit

class Ref (Content):
	"""References include data from another source, possibly under the
	   protection of a (secure) hash.  Applications can retrieve the
	   data and use it to locate a nested KIP Document.  After validating
	   checksums and anything else they care about, they may set it up
	   as the sub-document behind this Ref.  The KIP Document can then
	   nest into the sub-document and insert its Chunks as part of the
	   document, to effectively replace this Ref.
	   
	   The separation of Ref resolution from the KIP library allows
	   flexible handling.  Perhaps try a number of distributed hash tables
	   based on a secure hash, or look at a nearby file based on a file
	   name, or perhaps download from one of many URLs.
	"""
	tag = 'TAG0_REF',
	argstx = [ dict ]

	def __init__ (self, ref):
		super(Ref,self).__init__ ()
		assert type (ref) is dict
		#DEBUG# print ('Storing reference %r' % (ref,))
		self.ref = ref
		self.subdoc = None

	def to_cbor (self):
		return cbor.dumps ([TAG0_REF, self.ref])

	def set_subdoc (self, kipdoc):
		"""Setup the indicated KIP Document as a sub-document.
		   Reference resolution is not part of the KIP library
		   because methods may vary wildly.  It is assumed that
		   the dictionary provided via Ref is interpreted by
		   an application using KIP.  Note well that this even
		   includes validation of hashes.  This call is merely
		   administrative; it allows the KIP Document to use a
		   Ref as a pointer to a nested KIP Document, and serve
		   out its Chunks.
		"""
		self.subdoc = kipdoc

	def got_subdoc (self):
		"""Test if a KIP Document was setup as sub-document.
		"""
		return self.subdoc is not None

	def get_subdoc (self):
		"""Request the sub-document set up behind this Ref.
		   If none is present, return None.
		"""
		return self.subdoc

class Mud (Content):
	"""Mud is opaque, and cannot be read.  Inside these blocks is a
	   bit of actual data, notably a Lit or a Ref.  You need a Key
	   to be able to clear the water.  Since it is acting as object
	   data, Mud does count as Content object, or part of hasing
	   in preparation of signature validation.
	"""
	tag = 'TAG0_MUD'
	argstx = [ int, bytes ]

	def __init__ (self, keynr, mud_or_content=None):
		if mud_or_content==None:
			raise Exception ('You need to initalise Mud (keynr, mudbytes) or Mud (keynr, content)')
		elif type (mud_or_content) == bytes:
			mud = mud_or_content
			content = None
		else:
			mud = None
			content = mud_or_content
			assert isinstance (content, Content), 'Mud() can be initialised from muddy bytes or wrapped around an instance of a Content subclass like Lit or Ref or Meta'
			assert content.__class__ != Content, 'Mud() cannot be wrapped around the abstract Content class'
			assert content.__class__ != Mud, 'Mud() cannot be wrapped around other Mud; you should use KeyMud for key manipulations'
		super(Mud,self).__init__ ()
		self.keynr   = keynr
		self.mud     = mud
		self.content = content

	def to_cbor (self):
		return cbor.dumps ([TAG0_MUD, self.keynr, self.get_muddled ()])

	def set_muddled (self, mud):
		"""Set mud, but this is assumed to be a new value,
		   so the clear content is removed.  This may later
		   be restored with clarify_content().
		"""
		self.mud = mud
		self.content = None

	def got_muddled (self):
		return self.mud is not None

	def get_muddled (self):
		if self.mud is None:
			raise Exception ('Please perform a Kip Up first')
		return self.mud

	def muddle_content (self, kipctx, interactor):
		"""Given that clear content has been set but the mud
		   was thrown out, reproduce the mud with a kip_up()
		   invocation on the/a key for this Mud object.
		"""
		keys = kipctx.key_fromnumber (self.keynr)
		data = self.content.to_cbor ()
		for key in keys:
			try:
				mud = key.kip_up (data)
				if mud is not None:
					self.set_muddled (mud)
					break
			except:
				pass

	def set_clarity (self, content_bytes):
		"""Set clear content, but this is assumed to be a
		   new value, so the muddled content is removed.
		   This may later be restored with muddle_content().
		"""
		print ('aMud.set_clarity() on tag 0x%02x' % (content_bytes [0]))
		content = Chunk.from_cbor (content_bytes)
		print ('aMud.set_clarity (%r)' % content)
		if not isinstance (content, Content):
			print ('Mud cleared up, but revealed no Content -- refusing it')
			return
		elif isinstance (content, Mud):
			print ('Mud cleared up, only to reveal more Mud -- refusing it')
			return
		self.mud = None
		self.content = content
		self.content.set_on_off_key (self.get_on_key (), self.get_off_key ())
		self.content.set_trust (self.get_trust ())

	def got_clarity (self):
		return self.content is not None

	def get_clarity (self):
		if self.content is None:
			raise Exception ('Please perform a Kip Down first')
		return self.content

	def clarify_content (self, kipctx, interactor):
		"""Given that mud has been set but clarity has
		   gone, reproduce the clear content with kip_down()
		   invoked on the/a key for this Mud object.
		"""
		keys = kipctx.key_fromnumber (self.keynr)
		print ('Clarifying with keys %r' % keys)
		for key in keys:
			try:
				content = key.kip_down (self.mud)
				print ('Clarified to %r' % content)
				if content is not None:
					self.set_clarity (content)
					break
			except:
				pass

class KeyMud (KeyIntro):
	"""Key Mud is an opaque symmetric key.  Being opaque means that
	   another Key is necessary to decode it.  This is the key mapping
	   idea that is rather important to the flexibility of KIP Documents.
	   In general, it is even possible to require the use of multiple
	   keys, effectively imposing an AND on key availability.  This is
	   countered with an OR by allowing multiple Key Mud chunks derive
	   the same symmetric key.  The result is a flexible logic that the
	   KIP Core library refers to as Key Mapping.
	"""
	tag = 'TAG0_KEYMUD'
	argstx = [ list, int, int, bytes ]

	@staticmethod
	def _capi2cborarray (blob):
		"""The header format for key mapping is well-defined in the
		   C API; it is useful in C programs but storage requirements
		   lean more towards compression.  Even a single-key map may
		   add (in base64) AAEAAAfkAAAAFAAAAEIAAAAR while this could
		   be compressed from 18 bytes to 8 bytes; better results for
		   longer sequences.  Ideally, with low keyid values, a quart
		   of the length should suffice.
		   This function unpacks a C API blob to a CBOR structure
		   [ TAG0_KEYMUD, keynr,alg, [ keynr,alg, ...], body ]
		"""
		(num_pairs,) = struct.unpack ('>H', blob [:2])
		idx = 2 + 8 * num_pairs + 8
		pairs = list (struct.unpack ('>' + 'II' * (num_pairs+1), blob [2:idx]))
		return [ TAG0_KEYMUD, pairs [:-2], pairs [-2], pairs [-1], blob [idx:] ]

	@staticmethod
	def _cbordata2capi (map_pairs, keynr, alg, body):
		"""The header format for key mapping is well-defined in the
		   C API; it is useful in C programs but storage requirements
		   lean more towards compression.  Even a single-key map may
		   add (in base64) AAEAAAfkAAAAFAAAAEIAAAAR while this could
		   be compressed from 18 bytes to 8 bytes; better results for
		   longer sequences.  Ideally, with low keyid values, a quart
		   of the length should suffice.
		   This function packs a CBOR structure [ TAG0_KEYMUD, keynr,
		   alg, [ keynr, alg, ...] ] to a blob for the C API.
		"""
		num_pairs = len (map_pairs) // 2
		assert num_pairs * 2 == len (map_pairs)
		fmt = '>H' + 'II' * len (map_pairs)
		blob = struct.pack (fmt, num_pairs, *map_pairs, keynr, alg)
		return blob + body

	def __init__ (self, map_pairs, keynr, alg, body):
		super(KeyMud,self).__init__ ()
		assert len (map_pairs) > 0, 'map_pairs must not be empty'
		assert len (map_pairs) % 2 == 0, 'map_pairs must be a list of pairs'
		assert all ([type (i) == int for i in map_pairs]), 'map_pairs must be an array of int'
		self.map_pairs = map_pairs
		self.keynr = keynr
		self.alg = alg
		self.body = body

	def to_cbor (self):
		return cbor.dumps ([TAG0_KEYMUD, self.map_pairs, self.keynr, self.alg, self.body])

	def get_keynr (self):
		return self.keynr

	def extract_keys (self, kipctx, interactor):
		try:
			keymud = KeyMud._cbordata2capi (self.map_pairs, self.keynr, self.alg, self.body)
			print ('%r.key_frommap (%r)' % (kipctx,keymud))
			key = kipctx.key_frommap (keymud)
			self.set_keys ([key])
		except:
			pass

class SigMud (Validator):
	"""Signature Mud is opaque, but holds a secure hash to validate the
	   preceding range of chunks in this KIP Document.  Mark that: any
	   referenced content is not included, but if a secure hash is part
	   of the reference then that is included (to form a Merkle tree).
	   This allows local validation of an isolated KIP Document, while
	   supporting secure inclusion of referenced documents and with an
	   option to bail out from this security by not having secure hashes
	   in the references.
	"""
	tag = 'TAG0_SIGMUD'
	argstx = [ int, bytes ]

	def __init__(self, keynr, sigmud):
		super(SigMud,self).__init__ ()
		self.keynr = keynr
		self.sigmud = sigmud

	def to_cbor (self):
		return cbor.dumps ([TAG0_SIGMUD, self.keynr, self.sigmud])

	def get_keynr (self):
		return self.keynr

	def make_signature (self, kipctx, interactor):
		if kipctx is None:
			raise Exception ('SigMud:make_signature() called before established KIP context')
		keys = kipctx.key_fromnumber (self.keynr)
		print ('Keys for %x are, using first if any: %r' % (self.keynr,keys))
		for key in keys:
			try:
				print ('Signing with key %x' % (key.keyid()))
				self.sigmud = key.sign ()
				self.set_signers ([key])
				return self.sigmud
			except:
				pass
		self.set_signers ([])
		return None

	def validate_signature (self, kipctx, interactor):
		keys = kipctx.key_fromnumber (self.keynr)
		print ('Keys for %x are: %r' % (self.keynr,keys))
		good = ()
		for key in keys:
			try:
				print ('Verifying key %x on %r' % (key.keyid(),self.sigmud))
				sig_ok = key.verify (self.sigmud)
				if sig_ok:
					good = (*good,key)
					pass #TODO# raise NotImplementedError ('validate_signature() :- distinguish verify-False causes')
				# Continue with next key; we want to learn about all that work
			except:
				pass
		self.set_signers (good)


class TabKey (KeyIntro):
	"""Table Keys are symmetric keys, taken from Kerberos keytabs.
	   These may be difficult to protect on client systems, and the
	   general advise is to only use them on servers.  Having said
	   that, it may beat the lack of entropy of a password, but the
	   big problem is that keytabs do not protect the key at all.
	   The only keys that can be obtained via TabKey are named as
	   kip/host@DOMAIN, to protect users from leaking other patterns.
	"""
	tag = 'TAG0_TABKEY'
	argstx = [ str, str, int, int ]

	def __init__ (self, hostname, domain, keynr, alg):
		super(TabKey,self).__init__ ()
		if hostname == 'MASTER':
			raise Exception ('Attempt to attack your KIP Daemon master key, kip/MASTER@%s' % hostname)
		self.domain = domain
		self.hostname = hostname
		self.keynr = keynr
		self.alg = alg

	def to_cbor (self):
		return cbor.dumps ([TAG0_TABKEY, self.hostname, self.domain, self.keynr, self.alg])

	def get_keynr (self):
		return self.keynr

	def extract_keys (self, kipctx, interactor):
		try:
			# Give the interactor an opportunity to provide a keytab file name
			ktname = interactor.get_keytab_filename ('kip/' + self.hostname + '@' + self.domain.upper ())
		except:
			ktname = None
		try:
			key = kipctx.key_fromkeytab (self.keynr, self.alg, self.domain, hostname=self.hostname, ktname=ktname)
			if key is not None:
				self.set_keys ([key])
		except:
			pass

class OffKey (Chunk):
	"""While editing a KIP Document, signatures get invalidated.  Own
	   signatures can be remade, but that does not apply to signing by
	   others.  What is needed, is to allow some signers to be off-key
	   while others stay on key and continue signing, eh, singing, eh
	   not signing it is.  Think of a melody where not everyone is in
	   tune all the time.
	   
	   The purpose of this is to allow edits to be made on shared work.
	   New insertions would make all other signers go off-key, whereas
	   removals may be proposed by a signer removing his signature and
	   going off-key on the part that he wants to see gone.
	   
	   The mechanism works by listing key numbers that will be off-key
	   for the upcoming chunks, until this is replaced by a future
	   OffKey chunk.  It is valid to set an empty list as an indication
	   that no signer will henceforth be off-key.
	"""
	tag = 'TAG0_OFFKEY'
	argstx = None

	def __init__ (self, *keynrs):
		assert all ([type(k) == int for k in keynrs])
		self.keynrs = set (keynrs)

	def to_cbor (self):
		return cbor.dumps ([TAG0_OFFKEY, *self.keynrs])

	def get_off_key (self):
		return self.keynrs

class SvcKeyMud (KeyIntro):
	"""Service Key Mud is an opaque symmetric key.  To clear the water,
	   it is necessary to use the KIP Service logic to connect to a
	   realm's KIP Daemon.  What realm, can be derived beforehand, so
	   as to judge whether it would bring anything trustworthy (and if
	   we are willing to authenticate via SASL, or perhaps only through
	   the SXOVER mechanism).
	"""
	#TODO# Consider opening the C-API format here
	tag = 'TAG0_SVCKEYMUD'
	argstx = [ bytes ]

	def __init__ (self, svckeymud):
		super(SvcKeyMud,self).__init__ ()
		self.svckeymud = svckeymud

	def to_cbor (self):
		return cbor.dumps ([TAG0_SVCKEYMUD, self.svckeymud])

	def get_keynr (self):
		raise NotImplementedError ('Extract keynr from C-API format?')

	def extract_keys (self, kipctx, interactor):
		try:
			print ('%r.key_fromservice (%r)' % (kipctx,self.svckeymud))
			keys = kipctx.key_fromservice (self.svckeymud)
			print ('key_fromservice() --> keys %r', keys)
			self.set_keys (keys)
		except Exception as e:
			raise
			pass

class SvcSigMud (Validator):
	"""Service Signature Mud is opaque, but holds a secure hash to
	   validate the preceding range of chunks in this KIP Document.
	   As with SigMud, referenced content is not signed, but the
	   references themselves are, possibly including secure hashes.
	   Since an interaction over the KIP Service is required, which
	   may incur contacting a remote KIP Daemon, some privacy is
	   built in with an added salt in the signature, but for a given
	   document this is still fixed.  Choose beforehand what realm
	   you intend to trust with this; but mostly it will be the
	   realm of a sender that you would be likely to trust if known.
	"""
	#TODO# Consider opening the C-API format here
	tag = 'TAG0_SVCSIGMUD'
	argstx = [ bytes ]

	def __init__ (self, svcsigmud):
		super(SvcSigMud,self).__init__ ()
		self.svcsigmud = svcsigmud

	def to_cbor (self):
		return cbor.dumps ([TAG0_SVCSIGMUD, self.svcsigmud])

	def validate_signature (self, kipctx, interactor):
		raise NotImplementedError ('SvcSigMud:validate_signature()')

	def make_signature (self, kipctx, interactor):
		raise NotImplementedError ('SvcSigMud:make_signature()')

class PubKey (KeyIntro):
	"""Public Keys introduce a symmetric key, commonly referred to as a
	   session key, for use in otherwise normal KIP Core operations.
	   Any Validator relying on this key need not be a PubSig; it may
	   just as well be SigMud or SvcSigMud.  Various public key systems
	   are supported, and named in a string.
	"""
	tag = 'TAG0_PUBKEY'
	argstx = [ str, int, int, bytes ]
	def __init__ (self, pkitag, keynr, alg, pubkey):
		super(PubKey,self).__init__ ()
		self.keynr = keynr
		self.pkitag = pkitag
		self.keynr = keynr
		self.alg = alg
		self.pubkey = pubkey
	def to_cbor (self):
		return cbor.dumps ([TAG0_PUBKEY, self.pkitag, self.keynr, self.alg, self.pubkey])
	def get_keynr (self):
		return self.keynr

class PubDig (KeyIntro):
	"""Public Signatures end a sequence that starts with a PubDig marker.
	   This is the point where a hash algorithm starts its action.
	"""
	tag = 'TAG0_PUBDIG'
	argstx = [ int, int ]
	def __init__ (self, keynr, alg):
		super(PubSig,self).__init__ ()
		self.keynr = keynr
		self.alg = alg
	def to_cbor (self):
		return cbor.dumps ([TAG0_PUBDIG, self.keynr, self.alg])
	def get_keynr (self):
		return keynr

class PubSig (Validator):
	"""Public Signatures are made with a public key, and not necessarily
	   related to PubKey chunks.  The start of the signed chunks is marked
	   by a PubDig chunk.
	"""
	tag = 'TAG0_PUBSIG'
	argstx = [ str, int, int, bytes ]
	def __init__ (self, pkitag, keynr, alg, pubsig):
		super(PubSig,self).__init__ ()
		self.pkitag = pkitag
		self.keynr = keynr
		self.alg = alg
		self.pubsig = pubsig
	def to_cbor (self):
		return cbor.dumps ([TAG0_PUBSIG, self.pkitag, self.keynr, self.alg, self.pubsig])


# 'tags' maps TAG0_LIT to the Lit chunk class; each of these classes
# define a tag attribute like 'TAG0_LIT'; 'revtags', defined above,
# maps  those string tags back to the tag TAG0_LIT.
#
tags = {
	TAG0_META:      Meta,
	TAG0_LIT:       Lit,
	TAG0_REF:       Ref,
	TAG0_MUD:       Mud,
	TAG0_KEYMUD:    KeyMud,
	TAG0_SIGMUD:    SigMud,
	TAG0_TABKEY:	TabKey,
	TAG0_OFFKEY:    OffKey,
	TAG0_SVCKEYMUD: SvcKeyMud,
	TAG0_SVCSIGMUD: SvcSigMud,
	TAG0_PUBKEY:    PubKey,
	TAG0_PUBDIG:    PubDig,
	TAG0_PUBSIG:    PubSig,
}


class BaseInteractor (object):
	"""Interactor classes collect instructions or reports about kip up and
	   kip down operations on a KIP Document.  The BaseInteractor provides
	   a silent and ignorant class with methods that can be overridden.
	   In contrast, the VerboseInteractor is a class that reports everything
	   on the standard error output stream.
	"""

	def reset (self):
		"""Subclasses may override this method to clear the Interactor state.
		"""
		pass

	def get_keytab_filename (self, userid):
		"""Subclasses may override this method to provide a file name for
		   a Kerberos5 keytab.  This may be useful on servers or, as a
		   special case, during testing and development.  It should not
		   be used on clients, because keytabs store long-term secrets
		   without any protection.
		"""
		return None

	def get_passphrase (self, reason):
		"""Subclasses may override this method to provide a passphrase
		   to methods that require it for the given reason; this will
		   never be reduced directly to a key in KIP Core, but it may
		   be used to authenticate in KIP Service, PKCS #11 or to get
		   to a private key.
		"""
		return None

	def report_gather_keys (self, success, failure):
		"""Subclasses may override this to receive a report on
		   gathered keys during either kip_up() or kip_down()
		   methods on a KIP Document.
		"""
		pass

	def report_validate_signatures (self, success, failure):
		"""Subclasses may override this to receive a report on
		   the validation of signatures the kip_down() method
		   on a KIP Document.
		"""
		pass

	def report_make_signatures (self, success, failure):
		"""Subclasses may override this to receive a report on
		   the creation of signatures during the kip_up()
		   method on a KIP Document.
		"""
		pass

	def report_windback_trust (self, success, failure):
		"""Subclasses may override this to receive a report on
		   trusted content during the kip_down() method on a
		   KIP Document.
		"""
		pass

	def report_clarify_content (self, success, failure):
		"""Subclasses may override this to receive a report on
		   clarifying content during the kip_down() method on a
		   KIP Document.
		"""
		pass

	def report_muddle_content (self, success, failure):
		"""Subclasses may override this to receive a report on
		   muddling content during the kip_up() methods on a
		   KIP Document.
		"""
		pass


class VerboseInteractor (BaseInteractor):
	"""This Interactor reports about everything it is being asked.
	"""

	def reset (self):
		print ('Resetting MyFirstInteractor [shrug]', file=sys.stderr)

	def get_keytab_filename (self, userid):
		print ('Being asked for a keytab holding %s,' % userid, file=sys.stderr)
		return None

	def get_passphrase (self, reason):
		print ('Being asked for a passphrase for %s,' % reason, file=sys.stderr)
		return None

	def report_gather_keys (self, success, failure):
		print ('\nGATHERED KEYS, #%d success, #%d failure\n' % (len(success),len(failure)), file=sys.stderr)

	def report_validate_signatures (self, success, failure):
		print ('\nVALIDATED SIGNATURES, #%d success, #%d failure\n' % (len(success),len(failure)), file=sys.stderr)

	def report_make_signatures (self, success, failure):
		print ('\nCREATED SIGNATURES, #%d success, #%d failure\n' % (len(success),len(failure)), file=sys.stderr)

	def report_windback_trust (self, success, failure):
		print ('\nWOUND BACK TRUST, #%d success, #%d failure\n' % (len(success),len(failure)), file=sys.stderr)

	def report_clarify_content (self, success, failure):
		print ('\nCLARIFIED DATA, #%d success, #%d failure\n' % (len(success),len(failure)), file=sys.stderr)

	def report_muddle_content (self, success, failure):
		print ('\nMUDDLED DATA, #%d success, #%d failure\n' % (len(success),len(failure)), file=sys.stderr)



if __name__ == '__main__':

	###
	### TEST DATA
	###

	ref = {
		'uri' : ['http://example.com/xyz'],
		'hash': {
			'sha256':b'\xe3\xb0\xc4\x42\x98\xfc\x1c\x14\x9a\xfb\xf4\xc8\x99\x6f\xb9\x24\x27\xae\x41\xe4\x64\x9b\x93\x4c\xa4\x95\x99\x1b\x78\x52\xb8\x55',
			'_size':bytes ((123,)),
		},
		'ranges': [
			None, [3,0],
			[4,0], None
		],
	}

	jref = {
		'uri' : ['http://example.com/xyz'],
		'hash': {
			'sha256':'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855',
			'_size':123,
		},
		'ranges': [
			None, [3,0],
			[4,0], None
		],
	}

	tabkey = [ TAG0_TABKEY, 'pink.unicorn.demo.arpa2.org', 'unicorn.demo.arpa2.org', 2020, 20 ]
	keymud = [ TAG0_KEYMUD, [2020, 20], 66, 17, b'\x80\x89\x8f\xf7\xeb\x14\xca[\x8a\xad\xe6\xdb\t<i\xb3\x84\xe6\x10\xc2\xfa\xc7\x8d \xce\xfe\nb\x83u\x92~\x97\xeb\xc2\x10Y\x06\x11\xd4\t\xc3"\xb4[-\x9d\xe9\xe2|\xf6\xc8\xf5\xf2m\x92' ]
	kyemud = [ TAG0_KEYMUD, [2020, 20], 66, 17, b'h\xaa\xa7\xaa0~ \x89kP\x7f\xa71\xef\xbc\xe5\xf0\x07\xd8\xe7\x0f\xa0yIL\x93\xf4\xf3\xa74\x15 \x1e\xf2adL\x8ak\t4\xcd\x85\x12\xec0z\x07\x97\xc1O\xa6\xdcC\x1b.' ]
	txtmud = [ TAG0_MUD, 66, b'\x82\xd5\xf9b`Y2#\x98U\xe6R\xd97G\xc7\x1bt/\xf8\xde*34\x80\xad\x89\xf0\x80@\x009\xc0;\xa9\xf1r\x0b\xfe\xea\x87\'/\xa4\x8c\x1c\xd7\xd7' ]
	#OLD# sigmud = [ TAG0_SIGMUD, 66, b'aLd\xdbH\xb5\x11j\xbb\x16C\xb9\xdd/~\x87\xa7\xd7\xc9`\x12_\xfb\t\x9b\xe3\xfd\xac\x03\'\x92\xc3\x85^(B\x1f\xb1\xc0\x14\x0bt\xf9\xda\xe7\xca\x81)', ]
	sigmud = [ TAG0_SIGMUD, 66, b'\xd8\xdb@\x0ev\xf5ok\x8b\xc8\xc8\x00\x19\xa7\x1bwKqW*\x1b\x94\x89\xaaP\x97\x99&\xc5\xc7=\x05\x81\x10m\x02E=\xe0\x7f<\x9b\xe0\xa8z-C&', ]
	#OK2# sigmud = [ TAG0_SIGMUD, 66, b'\x18\x8eR\xd6\xa7\xfc\xc6P\xe3\xe0&\xcf\x08O\x86q`CC\xaf`\xe2W\x8a#\xa5\x8c\x9f\x8a\tf\xf3;iO\xb2\xce\xf9\x82\xce\x15.\xf4pT5\xcc\xdb', ]
	pubkey = [ TAG0_PUBKEY, 123, 'pgp0', 'sha256', b'\xaa\xbb\xcc\xdd', ]
	pubsig = [ TAG0_PUBSIG, 123, b'\xaa\xbb\xcc\xdd', ]

	tabkey64 = [ 'tabkey0', 'unicorn.demo.arpa2.org', 'pink.unicorn.demo.arpa2.org', 2020, 20 ]
	keymud64 = [ 'keymud0', [2020, 20], 66, 17, 'gImP9+sUyluKrebbCTxps4TmEML6x40gzv4KYoN1kn6X68IQWQYR1AnDIrRbLZ3p4nz2yPXybZI=' ]
	kyemud64 = [ 'keymud0', [2020, 20], 66, 17, 'aKqnqjB+IIlrUH+nMe+85fAH2OcPoHlJTJP086c0FSAe8mFkTIprCTTNhRLsMHoHl8FPptxDGy4=' ]
	txtmud64 = [ 'mud0', 66, 'gtX5YmBZMiOYVeZS2TdHxxt0L/jeKjM0gK2J8IBAADnAO6nxcgv+6ocnL6SMHNfX', ]
	sigmud64 = [ 'sigmud0', 66, '2NtADnb1b2uLyMgAGacbd0txVyoblImqUJeZJsXHPQWBEG0CRT3gfzyb4Kh6LUMm', ]
	pubkey64 = [ 'pubkey0', 123, 'pgp0', ['sha256'], 'qrvM3Q==', ]
	pubsig64 = [ 'pubsig0', 123, 'qrvM3Q==', ]


	###
	### TEST CODE
	###

	import hashlib
	import json

	class MyFirstInteractor (VerboseInteractor):

		def get_keytab_filename (self, userid):
			print ('Being asked for a keytab holding %s,' % userid, file=sys.stderr)
			base = os.path.dirname (os.path.dirname (os.path.realpath (__file__)))
			keytab = os.path.join (base, 'test', 'bin', 'unicorn.keytab')
			print ('I will present %s' % keytab, file=sys.stderr)
			return keytab

		def get_passphrase (self, reason):
			print ('Being asked for a passphrase for %s,' % reason, file=sys.stderr)
			passphrase = 'sekreet'
			print ('I will present %s' % passphrase, file=sys.stderr)
			return passphrase


	def ll1write_magic (f):
		flags = b'===='
		f.write ( b'ARPA2KIP%s%s\n' % (produced_version,flags) )

	def ll1write_meta (d, f):
		if d is not None:
			f.write (cbor.dumps (d))

	def ll1write_token (t, f):
		f.write (cbor.dumps (t))

	with open ('/tmp/cbortest.kip', 'wb') as f:
		ll1write_magic (f)
		ll1write_meta ([TAG0_META, 'documentTitle', 'My First KIP Document', 'A lesser fattened KIP'], f)
		# ll1write_token ('hello', f)
		# ll1write_token ('world', f)
		ll1write_token (tabkey, f)
		ll1write_token (keymud, f)
		ll1write_token (kyemud, f)
		ll1write_token (txtmud, f)
		# ll1write_token (pubkey, f)
		ll1write_token ([TAG0_REF, ref], f)
		ll1write_token (sigmud, f)
		# ll1write_token (pubsig, f)

	with open ('/tmp/jsontest.kip', 'w') as f:
		# f.write ('{ magic="ARPA2KIP202003AAAD", content=[ ')
		f.write ('{"magic"="ARPA2KIP====","version"="202003","content"=[ ')
		json.dump (["meta0", "documentTitle", "My First KIP Document", "A lesser fattened KIP"], f)
		f.write (',')
		json.dump (tabkey64, f)
		f.write (',')
		json.dump (keymud64, f)
		f.write (',')
		json.dump (kyemud64, f)
		f.write (',')
		json.dump (txtmud64, f)
		f.write (',')
		# json.dump (pubkey64, f)
		json.dump (["ref0", jref], f)
		f.write (',')
		json.dump (sigmud64, f)
		# json.dump (pubsig64, f)
		f.write ('}')

	# The results from the above two are (on Python 3.7.3)
	# -rw-r--r-- 1 root root 483 Mar 11 07:36 /tmp/cbortest.kip
	# -rw-r--r-- 1 root root 816 Mar 11 07:36 /tmp/jsontest.kip

	with open ('/tmp/cbortest.kip', 'rb') as f:
		d = Document.from_cbor_file (f)

	print ('###\n### Loading KIP Document\n###')
	with open ('/tmp/cbortest.kip', 'rb') as f:
		d = Document.from_cbor_file (f)

	print ('###\n### Saving KIP Document\n###')
	with open ('/tmp/cborcopy.kip', 'wb') as f:
		d.to_cbor_file (f)

	print ('###\n### Checking KIP Document\n###')
	with open ('/tmp/cborcopy.kip', 'rb') as f:
		d = Document.from_cbor_file (f)

	print ('###\n### Producing KIP Document URI\n###')
	uri = d.to_data_uri ()
	print (uri)

	print ('###\n### Parsing back KIP Document URI\n###')
	d = Document.from_data_uri (uri)
	uri2 = d.to_data_uri ()
	print (uri2)
	assert uri == uri2

	print ('###\n### Resolving KIP Document keys\n###')
	ia = MyFirstInteractor ()
	d.kip_down (interactor=ia)
	print ('Got %r' % d[4].content)

	print ('###\n### Kipping Down once more\n###')
	d.kip_down (interactor=ia)

	print ('###\n### Kipping Up, just to See if it Works\n###')
	d.kip_up (interactor=ia)

	print ('###\n### Kipping Up Again, it should Work Again\n###')
	d.kip_up (interactor=ia)

	print ('###\n### Down-then-Up to be Written to File\n###')
	with open ('/tmp/cbordnup.kip', 'wb') as f:
		d.to_cbor_file (f)

	print ('###\n### Reading back Down-then-Up File\n###')
	with open ('/tmp/cbordnup.kip', 'rb') as f:
		d = Document.from_cbor_file (f)

	print ('###\n### Kipping Down the Down-then-Up Document\n###')
	d.kip_down (interactor=ia)

	print ('###\n### Kipping Down once More\n###')
	d.kip_down (interactor=ia)

	print ('###\n### Producing sample data (perhaps for next time)\n###')
	print ('CTX = %r' % d.kipctx)
	print ('d[1] is-a %r offering %r' % (type(d[1]),dir(d[1])))
	print ('KEY = %r (ought to be just one)' % (d[1].keys,))
	[oldkey] = d[1].keys
	newkey = d.kipctx.key_generate (17, 66)
	print ('NEWKEY = %r' % newkey)
	keymud = newkey.tomap (oldkey)
	print ('KEYMUD = %r' % keymud)
	data = b'Veni, vidi, foetsie!'
	mud = newkey.kip_up (data)
	print ('MUD = %r' % mud)
	sig = d [-1].make_signature (d.have_kip_context (), ia)
	print ('SIG = %r' % sig)

