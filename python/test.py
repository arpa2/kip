#!/usr/bin/env python3

import api


#
# Context construction, and cleanup setup
#
ctx = api.Context ()

#
# Random Generator
#
print ('Random: %r' % ((ctx.random (12),)))
print ('Random: %r' % ((ctx.random (12),)))
print ('Random: %r' % ((ctx.random (12),)))
print ('Random: %r' % ((ctx.random (12),)))

#
# Key Generator
#
key1 = ctx.key_generate (17, 1)
key123a = ctx.key_generate (17, 123)
key123b = ctx.key_generate (17, 123)
print ('Key is %r' % key1)
print ('Key is %r' % key123a)
print ('Key is %r' % key123b)

#
# Data Encryption
#
rnd500 = ctx.random (500)
enc500 = key1.kip_up (rnd500)
assert len (enc500) > len (rnd500) == 500
dec500 = key1.kip_down (enc500)
assert len (dec500) == len (rnd500) == 500
assert dec500 == rnd500

#
# Key Mapping
#
keymud1 = key1.tomap (key123a, key123b)
key1b = ctx.key_frommap (keymud1)
assert key1b.keyid () != key1.keyid ()
assert key1b.keynr () == key1.keynr ()
vfy500 = key1b.kip_down (enc500)
assert len (vfy500) == len (rnd500) == 500
assert vfy500 == dec500 == rnd500

#
# Summing -- like test/reload-sums.c
#
rndA = ctx.random (250)
rndB = ctx.random (250)
sumQ = key1.sum_start ()
sumR = key1.sum_start ()
sumS = key1.sum_start ()
print ('ids A,Q,R,S: %x,%x,%x,%x' % (key1.keyid(),sumQ.sumid(),sumR.sumid(),sumS.sumid()))
# Append value A to two checksums, B to the third
sumQ.append (rndA)
sumR.append (rndA)
sumS.append (rndB)
# Now restart the first checksum -- forgetting A
sumQ.restart ()
# Produce the checksums for Q, R and S
sigmudQ = sumQ.sign ()
sigmudR = sumR.sign ()
sigmudS = sumS.sign ()
# The signatures should not cross-check
assert     sumQ.verify (sigmudQ)
assert not sumQ.verify (sigmudR)
assert not sumQ.verify (sigmudS)
assert not sumR.verify (sigmudQ)
assert     sumR.verify (sigmudR)
assert not sumR.verify (sigmudS)
assert not sumS.verify (sigmudQ)
assert not sumS.verify (sigmudR)
assert     sumS.verify (sigmudS)
# Now append value A to the restarted checksum
sumQ.append (rndA)
sigmudQ = sumQ.sign ()
# Ensure that Q and R are the same, but S is not
assert     sumQ.verify (sigmudQ)
assert     sumQ.verify (sigmudR)
assert not sumQ.verify (sigmudS)
assert     sumR.verify (sigmudQ)
assert     sumR.verify (sigmudR)
assert not sumR.verify (sigmudS)
assert not sumS.verify (sigmudQ)
assert not sumS.verify (sigmudR)
assert     sumS.verify (sigmudS)
# Now restart the checksum with B, and append A
sumS.restart ()
sumS.append (rndA)
sigmudS = sumS.sign ()
# Ensure the harmonious final chord -- all equal
assert     sumQ.verify (sigmudQ)
assert     sumQ.verify (sigmudR)
assert     sumQ.verify (sigmudS)
assert     sumR.verify (sigmudQ)
assert     sumR.verify (sigmudR)
assert     sumR.verify (sigmudS)
assert     sumS.verify (sigmudQ)
assert     sumS.verify (sigmudR)
assert     sumS.verify (sigmudS)

#
# Report success, but leave the giggling to Daffy Duck or the user
#
print ('That\'s all folks!')

