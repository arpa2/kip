#!/usr/bin/env python3
#
# Demo van een s2s encryptie, in Python maar vertaalt simpel naar C.
# Dit is een lokale keuze van de server, maar zo is het zeker veilig.
#
# Feitelijk plak ik een hash achter de input, en versleutel het geheel.
# Lekker simpel allemaal.
#
# Alles is binair, mogelijk met NUL-karakters, ASCII input werkt ook.
#
# From: Rick van Rein <rick@openfortress.nl>


from hashlib import sha256
from base64 import b64encode, b64decode
from struct import pack, unpack

import api


ctx = api.Context ()

s2skey = ctx.key_generate (20, 1)


def encrypt_s2s (s2s_plain):
	assert type (s2s_plain) == bytes
	h = sha256 (s2s_plain).digest ()
	s2s_blob = s2skey.kip_up (s2s_plain + h)
	return s2s_blob

def decrypt_s2s (s2s_blob):
	s2s_plain_hash = s2skey.kip_down (s2s_blob)
	s2s_plain = s2s_plain_hash [:-32]
	h = s2s_plain_hash [-32:]
	assert h == sha256 (s2s_plain).digest ()
	return s2s_plain

def pure_kip_encrypt_s2s (s2s_plain):
	assert type (s2s_plain) == bytes
	s2skey.restart ()
	s2s_enc = s2skey.kip_up (s2s_plain)
	s2s_sig = s2skey.sign ()
	# print ('E:s2s_enc = %r' % (s2s_enc,))
	# print ('E:s2s_enc = %r' % (s2s_sig,))
	# print ('E:lengths = %d,%d' % (len(s2s_enc),len(s2s_sig)))
	s2s_len = pack ('>h', len (s2s_sig))
	assert len (s2s_len) == 2
	s2s_blob = s2s_len + s2s_enc + s2s_sig
	return s2s_blob

def pure_kip_decrypt_s2s (s2s_blob):
	s2s_len = s2s_blob [:2]
	siglen = unpack ('>h', s2s_len) [0]
	s2s_enc = s2s_blob [2:-siglen]
	s2s_sig = s2s_blob [-siglen:]
	# print ('D:s2s_enc = %r' % (s2s_enc,))
	# print ('D:s2s_enc = %r' % (s2s_sig,))
	# print ('D:lengths = %d,%d' % (len(s2s_enc),len(s2s_sig)))
	s2skey.restart ()
	s2s_plain = s2skey.kip_down (s2s_enc)
	assert s2skey.verify (s2s_sig)
	return s2s_plain

rnd = ctx.random (50)

s2s_blob = encrypt_s2s (rnd)
print ('s2s="%s"' % str (b64encode (s2s_blob), 'ascii'))

orig = decrypt_s2s (s2s_blob)

assert len (rnd) == len (orig)
assert rnd == orig

for i in range(10):

	rnd = ctx.random (50)

	s2s_blob = pure_kip_encrypt_s2s (rnd)
	print ('s2s="%s"' % str (b64encode (s2s_blob), 'ascii'))

	orig = pure_kip_decrypt_s2s (s2s_blob)

	assert len (rnd) == len (orig)
	assert rnd == orig

print ('SUCCESS')
