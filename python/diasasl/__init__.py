# DiaSASL interface in Pure Python
#
# This is neither a server nor a client, but a proxy protocol.
# The idea is that a simple can implement SASL really easily
# by relaying it over the DiaSASL protocol, to a local and
# trusted node that processes it with as much vigor as desired.
#
# The vigor employed may include advanced uses, such as SXOVER
# for InternetWide Realm Crossover.  This involves passing the
# traffic over Diameter to other realms, where the client has
# its own identity provider vouching for them; the realm is
# basically the client's domain, for which the vouching server
# is validated by the trusted local node.
#
# This is a pure Python implementation; the ASN.1 defined for
# the protocol is downright simple, and can be parsed with a
# modest amount of byte juggling.  Even though SASL is passed
# to another node, it will report back on its results just as
# a local SASL server-side would have.
#
# Author: Rick van Rein <rick.vanrein@arpa2.org>
# SPDX-License-Identifier: BSD-2-Clause 


import re
import socket
import select
import errno


APPTAG_OPEN_REQ  = b'\x6a'
APPTAG_CLOSE_REQ = b'\x6b'
APPTAG_AUTHN_REQ = b'\x6c'
APPTAG_OPEN_ANS  = b'\x6d'
APPTAG_AUTHN_ANS = b'\x6e'

TAG_0 = b'\xa0'
TAG_1 = b'\xa1'
TAG_2 = b'\xa2'
TAG_3 = b'\xa3'
TAG_4 = b'\xa4'
TAG_5 = b'\xa5'
TAG_6 = b'\xa6'
TAG_7 = b'\xa7'

TAG_INTEGER     = b'\x02'
TAG_OCTETSTRING = b'\x04'
TAG_NULL        = b'\x05'
TAG_UTF8STRING  = b'\x0c'
TAG_IA5STRING   = b'\x16'
TAG_SEQUENCE    = b'\x30'


__doc__ = """
# DiaSASL for Python SASL server

DiaSASL relays server-side SASL interactions to a locally trusted
identity node over plain TCP/IP.  This identity node can be central
to a server site, and address many protocols in a similar way.

Normally, the local identity node relays the traffic to Diameter
and passes it to the service realm or, if a SASL mechanism suited
for Realm Crossover is used, to the client's realm for an identity
that ends in the client's domain name.

**About the Protocol**

The protocol definition of DiaSASL is language-independent, and
each tagged message consists of tuples with elements that are typed
and possibly optional.  It is defined in ASN.1 and encoded in DER,
but this is so straightforward that we need no specialised library.

These messages are concealed by this module's pythonic interface.
One would first connect to a node and then open any number of
sessions over it.

The following protocol messages are defined:

  * **Open** a session for a given service realm.  The empty string
    is defined as a default realm, and the suggested use is that
    this is dedicated to Realm Crossover.  This does not mean that
    Realm Crossover cannot be explicitly allowed in realms that
    also welcome other SASL mechanisms.

  * **Close** is the reverse, but is only needed when the session
    has not reported a final positive or negative result.

  * **Authn** is a SASL token interaction.  The first includes a
    mechanism choice, such as `SXOVER-PLUS` for Realm Crossover.
    Any may send a token from client to server, and a response token
    may be replied.  The reply may also be final, either reporting
    an error or success.  An error should never be accompanied by
    a (challenge) token, but a success may be.

**Background information:**

This code can be used in services that want to facilitate
Bring Your Own IDentity to clients, rather than forcing them to
have yet another login under your domain.  Technically, this
requires a mechanism through which users can validate a userid
under their own domain, and your service should (use an identity
node to) validate the client's domain before accepting the
userid underneath it.

This architecture enables clients much more control over their
online identity, both through (service-specific) aliases and
by selecting an authentication mechanisms that fits their needs.
Service providers gain too; they are relieved from the burden
of managing users and their passwords.

Here is a selection of blog articles on this approach:

  * [Bring Your Own IDentity](http://www.internetwide.org/blog/2015/04/22/id-2-byoid.html)
  * [Forms of Identity](http://internetwide.org/blog/2015/04/23/id-3-idforms.html)
  * [Be Your Own IDentity Provider](http://www.internetwide.org/blog/2020/03/18/id-13-idp.html)
  * [Support Levels for Realm Crossover](http://internetwide.org/blog/2020/09/16/id-16-xover-levels.html)
  * [Web Authentication with SASL](http://internetwide.org/blog/2018/11/15/somethings-cooking-4.html)
  * [Bootstrapping Online Identity](http://www.internetwide.org/blog/2020/04/13/id-14-bootstrap.html)

We also publish a Domain Owner's Manual to get users started in
establishing their online freedom and regain first-class
citizenship on the Internet:

  * [Domain Owner's Manual](https://gitlab.com/arpa2/domain-owners-manual)
"""


_diapass_mapping = {
	'SXOVER-PLUS': (True, re.compile (
		'^p=(?P<cbtyp>[^,]+),,(?P<realm>[^,@]+),')),
}


def _diapass_info (saslmech, c2s0=None):
	"""Given a SASL mechanism, and optional first token, derive
	   a tuple with the following elements:
	   
	    1. The client realm, or None if it follows the server
	    2. The channel binding type name, or None
	    3. A flag indicating whether the mechanism is fit4plain
	   
	   Most DiaSASL clients will want to know the type name
	   for channel binding, because that needs to be setup for
	   mechanisms that end in "-PLUS".  It is passed to a
	   remote realm during Realm Crossover, to validate that
	   no intermediates are hijacking the SASL exchange.
	   
	   The client realm is often different when Realm Crossover
	   is used.  This is the vital part that allows foreign
	   clients to access a public service while controlling
	   their online identity.  The other end of the DiaSASL
	   connection redirects to the client realm, but it is
	   not necessarily used here.
	   
	   The flag indicator validates the mechanism for fitness
	   to crossover between realms, and being inspected on the
	   way.  This is a bit less scary than passing it completely
	   in plaintext, and local settings may be used to indicate
	   whether this could be problematic.
	"""
	global _diapass_mapping
	if not saslmech in _diapass_mapping:
		return (None, None, False)
	(fit4plain, matcher) = _diapass_mapping [saslmech]
	if c2s0 is None:
		return (None, None, fit4plain)
	m = matcher.match (c2s0)
	if m is None:
		return (None, None, False)
	else:
		d = m.groupdict ()
		return (d ['realm'], d ['cbtyp'], fit4plain)


def _der (tag, contents):
	"""Construct a DER representation with the given tag and
	   packaging the contents.
	"""
	if contents is None:
		return b''
	contlen = len (contents)
	assert contlen < 65536
	if contlen < 128:
		contlen = b'%c' % contlen
	elif contlen < 256:
		contlen = b'\x81%c' % contlen
	else:
		contlen = b'\x82%c%c' % (contlen >> 8, contlen & 0xff)
	print ('DEBUG: diasasl._der() INFO: Concatenating %r + %r + %r' % (tag,contlen,contents) )
	return tag + contlen + contents


def _app (apptag, tup):
	"""Construct an IMPLICIT SEQUENCE with the given apptag.
	"""

	def encode_integer (val):
		if -128 <= val <= 127:
			b = 1
		elif -32768 <= val <= 32767:
			b = 2
		else:
			b = 3
		return val.to_bytes (b, byteorder='big', signed=True)
	def encode_utf8str (val):
		return val.encode ('utf-8')
	def encode_ascii (val):
		return val.encode ('ascii')
	def encode_bytestr (val):
		return val

	tupkeys = [
		( TAG_0, TAG_INTEGER,     encode_integer ),
		( TAG_1, TAG_UTF8STRING,  encode_utf8str ),
		( TAG_2, TAG_OCTETSTRING, encode_bytestr ),
		( TAG_3, TAG_IA5STRING,   encode_ascii   ),
		( TAG_4, TAG_OCTETSTRING, encode_bytestr ),
		( TAG_5, TAG_OCTETSTRING, encode_bytestr ),
		( TAG_6, TAG_UTF8STRING,  encode_utf8str ),
		( TAG_7, TAG_UTF8STRING,  encode_utf8str ),
	]
	tupseq = b''
	for idx in range (8):
		val = tup [idx]
		if val is not None:
			(outer,inner,mapper) = tupkeys [idx]
			if val is not None:
				val = mapper (val)
				tupseq += _der (outer, _der (inner, val))
	appder = _der (apptag, tupseq)
	print ('DEBUG: diasasl._app() INFO: %d bytes starting 0x%02x' % (len(appder),appder[0]))
	return appder


def _derlen (raw):
	"""Given a raw (beginning of a) DER blob, return a pair
	   (hdlen,contlen) with the length of the header and content.
	   It is possible that less than the complete contents are
	   available; it is however an error if not the complete
	   header is available.
	"""
	contlen = raw [1]
	if contlen < 0x80:
		hdlen = 2
	elif contlen == 0x81:
		hdlen = 3
		contlen = raw [2]
	elif contlen == 0x82:
		hdlen = 4
		contlen = (raw [2] << 8) | raw [3]
	else:
		print ('DEBUG: diasasl._derlen() ERROR: content length exceeds 65535')
		return None
	print ('DEBUG: diasasl._derlen() --> (%d,%d)' % (hdlen,contlen) )
	return (hdlen,contlen)


def _split (derbytes, request=True):
	"""Split the derbytes by TAG_n to form an array with [n] keys
	   and None as the value for any undefined entry.  Return a
	   pair (tag,[1st,2nd,3rd...])
	"""

	def _untag (raw, tags):
		"""Parse a tag (which must match a value or set) from
		   the raw content, and return (tag,content,remainder)
		   or, in case of error, None.  Whether the
		   None result is fatal or replaced with a
		   default is decided by the caller.
		"""
		apptag = b'%c' % raw [0]
		if apptag != tags and apptag not in tags:
			print ('DEBUG: diasasl._untag() ERROR: unexpected tag 0x%02x' % ord (apptag))
			return None
		(hdlen,contlen) = _derlen (raw)
		if len (raw) < hdlen + contlen:
			print ('hdlen=%d, contlen=%d, rawlen=%d' % (hdlen,contlen,len(raw)))
			print ('DEBUG: diasasl._untag() ERROR: insufficient length')
			return None
		return (apptag, raw [hdlen:hdlen+contlen], raw [hdlen+contlen:])

	def _untag_all (raw, tags):
		"""Like _untag() but require no trailer and cut it.
		"""
		res = _untag (raw, tags)
		if res is not None:
			if len (res [2]) > 0:
				print ('DEBUG: diasasl._untag_all() ERROR: trailing garbage')
				return None
			else:
				return res [:2]
		else:
			print ('DEBUG: diasasl._untag_all() ERROR: no proper output from diasasl._untag()')
			return None

	def _untag_tuple (raw):
		"""Sequential _untag() for [0] through [7].
		"""

		def decode_integer (val):
			return int.from_bytes (val, byteorder='big', signed=True)
		def decode_utf8str (val):
			return val.decode ('utf-8')
		def decode_ascii (val):
			return val.decode ('ascii')
		def decode_bytestr (val):
			return val

		tupkeys = [
			( TAG_0, TAG_INTEGER,     decode_integer ),
			( TAG_1, TAG_UTF8STRING,  decode_utf8str ),
			( TAG_2, TAG_OCTETSTRING, decode_bytestr ),
			( TAG_3, TAG_IA5STRING,   decode_ascii   ),
			( TAG_4, TAG_OCTETSTRING, decode_bytestr ),
			( TAG_5, TAG_OCTETSTRING, decode_bytestr ),
			( TAG_6, TAG_UTF8STRING,  decode_utf8str ),
			( TAG_7, TAG_UTF8STRING,  decode_utf8str ),
		]
		tuplebuilder = [ ]
		for (outer,inner,mapper) in tupkeys:
			if raw [:1] == outer:
				(_tag,val,raw) = _untag (raw, outer)
				#TODO# Decode val
				print ('DEBUG: diasasl._untag_tuple() INFO: Found tag 0x%02x, content 0x%02x' % (ord(_tag),val[0]))
				(_tag,val    ) = _untag_all (val, inner)
				val = mapper (val)
			else:
				val = None
			tuplebuilder.append (val)
		if len (raw) > 0:
			print ('DEBUG: diasasl._untag_tuple() ERROR: trailing garbage after tuple')
			return None
		return tuple (tuplebuilder)

	def _untag_app (raw, apps):
		"""Complete _untag() for one of apps.
		"""
		appneeds = {
			APPTAG_OPEN_REQ:  ( 0,2,0,0,0,0,0,0 ),
			APPTAG_CLOSE_REQ: ( 0,0,2,0,0,0,0,0 ),
			APPTAG_AUTHN_REQ: ( 0,0,2,1,1,1,0,0 ),
			APPTAG_OPEN_ANS:  ( 1,2,2,2,0,0,0,0 ),
			APPTAG_AUTHN_ANS: ( 1,0,2,0,0,1,1,1 ),
		}
		(app,tupbin) = _untag_all (raw, apps)
		tup = _untag_tuple (tupbin)
		for (idx,val,need) in zip (range(8),tup,appneeds[app]):
			print ('DEBUG: diasasl._untag_app() INFO: idx=%d, val=%r, need=%d' % (idx,val,need))
			if val is None:
				assert need != 2, 'Missing [%d] in app tag 0x%02x tuple %r' % (idx,ord(app),tup)
			else:
				assert need != 0, 'Unexpected [%d] in app tag 0x%02x tuple %r' % (idx,ord(app),tup)
		return (app,tup)

	try:
		return _untag_app (derbytes, [APPTAG_OPEN_REQ,APPTAG_CLOSE_REQ,APPTAG_AUTHN_REQ] if request else [APPTAG_OPEN_ANS,APPTAG_AUTHN_ANS] )
	except:
		# Syntax error, blablabla
		raise


class Node:
	"""Nodes represent a TCP/IP connection to a locally trusted
	   identity service.  The connection carries the messages
	   for DiaSASL, and may be relayed over Diameter if so
	   desired; that, however is a configuration matter on the
	   identity service.
	   
	   The actual SASL traffic is exchanged through Session
	   objects, of which any number may be active on this link.
	"""

	def __init__ (self, sockaddr):
		"""Connect to the sockaddr that is the (host,port)
		   pair of a locally trusted identity node, which
		   is usually a Diameter peer able to answer to
		   SASL authentication and, if desired, to engage
		   in secure connections to client domains to
		   support them to "Bring Your Own IDentity" via
		   Realm Crossover for SASL.
		   
		   The connection is not protected because it
		   is assumed to be on a trusted local network.
		   Nothing is stopping tunnels from being used,
		   but it is not the anticipated usage pattern.
		"""
		print ('Trying to connect to %r' % (sockaddr,))
		try:
			sox = socket.socket (socket.AF_INET6, socket.SOCK_STREAM)
			sox.connect (sockaddr)
		except:
			sox = socket.socket (socket.AF_INET,  socket.SOCK_STREAM)
			sox.connect (sockaddr)
		self.sox = sox
		self.replies = { }

	def __del__ (self):
		self.close ()

	def close (self):
		"""Close the connection.  Send an error to any
		   Session that is still active.
		"""
		err_tup = ( errno.EPIPE,None,None,None,None,None,None,None )
		for cbkey in self.replies:
			for cb in self.replies [cbkey]:
				cb (err_tup)
		if self.sox is not None:
			self.sox.close ()
			self.sox = None

	def send (self, request, keyval, callback):
		"""Send a request and register a callback to
		   invoke when a reply arrives.
		"""
		req2resp = {
			APPTAG_OPEN_REQ:  APPTAG_OPEN_ANS,
			APPTAG_CLOSE_REQ: None,
			APPTAG_AUTHN_REQ: APPTAG_AUTHN_ANS,
		}
		print ('DEBUG: diasasl.Node.send() INFO: req2resp = %r' % (req2resp,))
		# if type (keyval) == str:
		# 	keyval = keyval.encode ('utf-8')
		key = (req2resp [request [:1]], keyval)
		if key [0] is not None:
			if key not in self.replies:
				self.replies [key] = []
			self.replies [key].append (callback)
		print ('DEBUG: diasasl.Node.send() INFO: Sending a %d byte request keyed %r' % (len(request),key))
		pkg = 'DEBUG:'
		for rb in request:
			pkg += ' %02x' % rb
		print (' %s' % pkg)
		self.sox.send (request)

	def recv (self):
		"""Receive one response and invoke the callback
		   registered for it.  This is a blocking call,
		   but only for a single message.  You can poll
		   for socket events to time this well.
		"""
		answer = self.sox.recv (5)
		(hdlen,contlen) = _derlen (answer)
		if hdlen + contlen > len (answer):
			answer += self.sox.recv (hdlen + contlen - len (answer))
		print ('DEBUG: diasasl.Node.recv() INFO: Received a %d byte answer' % len (answer))
		pkg = 'DEBUG:'
		for ab in answer:
			pkg += ' %02x' % ab
		print (' %s' % pkg)
		(tag,fields) = _split (answer, request=False)
		if tag == APPTAG_OPEN_ANS:
			key = (tag, fields [1])
		elif tag == APPTAG_AUTHN_ANS:
			key = (tag, fields [2])
		else:
			self.close ()
		if key not in self.replies:
			self.close ()
		print ('DEBUG: diasasl.Node.recv() INFO: Callback lookup key %r' % (key,))
		cb = self.replies [key] [0]
		del self.replies [key] [0]
		cb (fields)

	def poll (self, justone=False, timeout=None):
		"""Poll this Node for incoming messages, and deliver
		   them to their respective callbacks.  Optionally
		   specify a floating-point timeout in seconds to
		   override the default of indefinite waiting.  When
		   justone is set, the routine returns after having
		   handled (at most) a single message.
		   TODO: Timeout is between messages, not the total.
		"""
		while True:
			(rds,_wrs,xcs) = select.select ([self.sox], [], [], timeout)
			if self.sox in rds:
				self.recv ()
			if justone:
				return

class Session:
	"""Sessions represent an individual SASL authentication
	   exchange for a single client/server connection.  Any
	   number of Sessions can be multiplexed over the same
	   backend Node connection.
	"""

	def __init__ (self, node, service_realm=''):
		"""Initiate a fresh SASL authentication exchange
		   over the provided Node object.  The service_realm
		   will be used by the backend node to determine the
		   support mechanishm list.  The default is an empty
		   string, which may be configured to support no
		   mechanisms at all, or SXOVER-PLUS for full
		   Realm Crossover with SASL.
		"""
		assert isinstance (node, Node)
		self.node = node
		self.server_realm = service_realm
		self.client_realm = service_realm
		self.sasl_mechanisms = None
		self.session_id = None
		self.success = False
		self.failure = False
		self.mech_choice = None
		self.mech_chosen = False
		open_tup = ( None,service_realm,None,None,None,None,None,None )
		open_req = _app (APPTAG_OPEN_REQ, open_tup)
		self.node.send (open_req, service_realm, self._cb_open)

	def __del__ (self):
		self.close ()

	def close (self):
		if self.session_id is not None:
			if self.node.sox is not None:
				close_tup = ( None,None,self.session_id,None,None,None,None,None )
				close_req = _app (APPTAG_CLOSE_REQ, close_tup)
				self.node.send (close_req, None, None)
			self.session_id = None
		if not self.success:
			self.failure = True

	def _cb_open (self, fields):
		"""Callback to process the open answer.
		"""
		print ('DEBUG: diasasl.Session._cb_open() INFO: Callback fields %r' % (fields,))
		err = fields [0]
		if err is not None and err != 0:
			self.failure = True
			self.session_id = None
			self.recv_failure (fields [0])
		else:
			self.session_id = fields [2]
			if fields [3] is not None:
				assert self.mech_choice is None
				assert self.mech_chosen is False
				self.sasl_mechanisms = fields [3]
				self.recv_mechlist (self.sasl_mechanisms)
			self.recv_challenge (None)

	def recv_mechlist (self, mech_list):
		"""Overrule in subclass, and store your choice
		   in self.mech_choice for future reference.
		   When sent, the flag self.mech_chosen is set.
		"""
		pass

	def recv_challenge (self, s2c):
		"""Overrule in subclass, if so desired.
		   By default, this method has no effect.
		   You can call self.send_response() from here.
		   You can reference self.mech_choice as you
		   set it, and self.mech_chosen is set after
		   the first token sent it; the flag can be
		   used to decide whether channel binding is
		   still necessary (along with the first C2S
		   token).
		"""
		pass

	def recv_success (self, extra):
		"""Overrule in subclass, if so desired.
		   By default, this method has no effect.
		"""
		pass

	def recv_failure (self, errno):
		"""Overrule in subclass, if so desired.
		   By default, this method raises an exception.
		"""
		raise OSError (errno)

	def send_response (self, c2s, chan_bind=None):
		"""Send a token or, if none is to be sent, None.
		   Optionally choose a mechanism and channel binding.
		"""
		assert self.failure == False and self.success == False
		if self.mech_choice is not None:
			assert chan_bind is not None or self.mech_choice [-5:] != '-PLUS'
		assert self.session_id is not None
		self.s2c = None
		if self.mech_chosen:
			mc = None
		else:
			mc = self.mech_choice
			self.mech_chosen = True
		authn_tup = ( None,None,self.session_id,mc,chan_bind,c2s,None,None )
		authn_req = _app (APPTAG_AUTHN_REQ, authn_tup)
		self.node.send (authn_req, self.session_id, self._cb_authn)

	def _cb_authn (self, fields):
		"""Callback to process the authentication answer.
		"""
		print ('DEBUG: diasasl.Session._cb_authn() INFO: Callback fields %r' % (fields,))
		err = fields [0]
		if err is not None and err != 0:
			self.failure = True
			self.session_id = None
			self.recv_failure (fields [0])
		else:
			self.s2c = fields [5]
			self.client_userid = fields [6]
			self.client_realm  = fields [7]
			if err == 0:
				self.success = True
				self.recv_success (self.s2c)
			else:
				self.recv_challenge (self.s2c)

