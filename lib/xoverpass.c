/* xoverpass.c -- XoverSASL parsers for pass-through proxies.
 *
 * These functions help to parse first tokens of Realm Crossover
 * mechanisms, without further knowledge of the actual mechanism.
 * The purpose of these functions is to facilitate intermediate
 * nodes that pass the tokens for Realm Crossover with no more
 * access than to plain text functions.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>



/* Parse the initial C2S token to retrieve the remote realm
 * and the channel binding type.
 */
typedef void xoverpass_parse_fun (const uint8_t *c2s0, uint32_t c2s0len,
			char **realm, uint32_t *realmlen,
			char **cbtyp, uint32_t *cbtyplen);


/* Descriptive structures of mechanisms for Realm Crossover,
 * for which a zeroed structure marks the end.  Each structure
 * describes (1) a SASL mechanism, (2) its parser function,
 * and (3) whether it has end-to-end protection builtin.
 */
struct xoverpass {
	const char *mechanism;
	xoverpass_parse_fun *parser;
	bool fit4plain;
};


/* Recognise a number of fields separated by commas.
 * Returns true when the requested number of commas were found.
 */
static bool split_commas (const uint8_t *token, uint32_t tokenlen, int commacount, const uint8_t **commas_out) {
	const uint8_t *lastpos = token;
	while (commacount-- > 0) {
		size_t n = &token[tokenlen] - lastpos;
		lastpos = memchr (lastpos, ',', n);
		if (lastpos == NULL) {
			return false;
		}
		*commas_out++ = lastpos++;
	}
	return true;
}


/* Parse the first C2S token in the SXOVER-PLUS mechanism.
 * Leave the output fields (realm/len and cbtyp/len) as they
 * are when returning early.
 */
static void do_sxover (const uint8_t *c2s0, uint32_t c2s0len,
			char **realm, uint32_t *realmlen,
			char **cbtyp, uint32_t *cbtyplen) {
	//
	// Correctness assumption: we have a token
	if (c2s0 == NULL) {
		return;
	}
	//
	// Split the token at 3 comma positions
	const uint8_t *commas [3];
	if (!split_commas (c2s0, c2s0len, 3, commas)) {
		return;
	}
	//
	// Field syntax check
	if (commas [1] - commas [0] != 1) {
		return;
	}
	if ((c2s0 [0] != 'p') || (c2s0 [1] != '=')) {
		return;
	}
	//
	// Set the output positions inside the token
	*realm    = (char*)(commas [1] + 1);
	*realmlen = commas [2] - (commas [1] + 1);
	*cbtyp    = (char*)(c2s0       + 2);
	*cbtyplen = commas [0] - (c2s0 + 2);
	//
	// Return success
	return;
}


/* An array of all parsers for SASL mechanisms.  It ends in
 * an entry with all zeroes, including a NULL mechanism.
 *
 * When the parser function is absent, we can still
 * learn from the fit4plain flag.
 */
struct xoverpass xoverpassfun [] = {
	{ "SXOVER-PLUS", do_sxover, true },
	{ NULL, NULL, false }
};


/* Given a SASL mechanism name and a first C2S token, determine
 *  (1) what client realm it is addressing (NULL if not applicable)
 *  (2) what channel binding type it uses
 *  (3) whether it can be revealed in plaintext to intermediates
 *
 * Return a finding in all cases, being cautious with unknown
 * SASL mechanisms.  When the c2s0 token is not provided, this
 * function will be able to extract fit4plain, but not strings.
 *
 * This function is not part of the normal XoverSASL library, and
 * it does not target SASL end points.  Instead, it is meant to
 * help intermediate elements in the Realm Crossover chain.  This
 * is why it may link separately.
 */
void xoverpass_info (const char *mech_name,
			const uint8_t *c2s0, uint32_t c2s0len,
			char **realm, uint32_t *realmlen,
			char **cbtyp, uint32_t *cbtyplen,
			bool *fit4plain) {
	*realm = NULL; *realmlen = 0;
	*cbtyp = NULL; *cbtyplen = 0;
	*fit4plain = false;
	for (struct xoverpass *xop = xoverpassfun;
				xop->mechanism != NULL;
				xop++) {
		if (strcmp (xop->mechanism, mech_name) != 0) {
			continue;
		}
		if (xop->parser != NULL) {
			xop->parser (c2s0, c2s0len,
					realm, realmlen,
					cbtyp, cbtyplen);
		}
		if (xop->fit4plain) {
			*fit4plain = true;
		}
	}
}

