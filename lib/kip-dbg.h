/* internal debugging definitions for libkip -- do not distribute
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#ifdef DEBUG
#include <stdio.h>
#include <arpa2/except.h>

static inline void print_block (char *descr, const uint8_t *blk, uint32_t blklen, bool skip) {
	char buf [strlen (descr) + ( skip ? 30 : 3 * blklen ) + 50];
	int count = snprintf (buf, sizeof(buf), "%s =", descr);
	if (blk == NULL) {
		snprintf (buf + count, sizeof(buf) - count, "NULL");
		log_debug ("%s", buf);
		return;
	}
	for (int i=0; i < blklen; i++) {
		count += sprintf (buf + count, " %02x", blk [i]);
		if (skip && (i > 5) && (i < blklen - 5)) {
			count += snprintf (buf + count, sizeof(buf) - count,  " ...");
			i = blklen - 5;
		}
	}
	count += snprintf (buf + count, sizeof(buf) - count, " -> #%d", blklen);
	log_debug ("%s", buf);
}

#else

#define print_block(descr,blk,blken,skip) { ; }

#endif
