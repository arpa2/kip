/* libkip -- Core routines for the Keyful Identity Protocol
 *
 * This library implements the routines defined in <arpa2/kip.h>
 * to manage keys.  It uses MIT krb5 as its underlying library,
 * but the other Kerberos variants would work too.  There is
 * no other need than the crypto code; the entire framework of
 * KDC interaction is not needed.
 *
 * The reason to use Kerberos directly, rather than the higher
 * level GSS-API that is commonly advised, is that the latter
 * requires a protocol to establish its context.  The uses of
 * libkip are not shaped like that.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <ctype.h>

#include <unistd.h>
#include <limits.h>
#include <fcntl.h>

#include <errno.h>
#include <com_err.h>

#include <krb5.h>

#include <arpa2/socket.h>
#include <arpa2/except.h>

#include <arpa2/kip.h>

#include "utlist.h"

#include "kip-int.h"



/* Globally initialise the KIP module.
 */
bool kip_init (void) {
	initialize_KIP_error_table ();
	return true;
}

/* Global cleanup for the KIP module.
 */
void kip_fini (void) {
	;
}



/* Open a context for kipping up and/or down.  You can use different
 * contexts for the kip up and kip down processes.  The context is
 * a place in which to mount keys and checksums.  Once done, the
 * contents may be eradicated.
 */
bool kipctx_open (kipt_ctx *out_ctx) {
	//
	// Allocate the data structure, filled with zeroes
	kipt_ctx ctx = calloc (1, sizeof (struct kipt_context));
	if (ctx == NULL) {
		errno = ENOMEM;
		goto fail;
	}
	//
	// The lists must be cleared, which calloc() did for us.
	;
	//
	// Initialise the fields
	krb5_error_code k5err = 0;
	k5err = krb5_init_context (&ctx->k5ctx);
	if (k5err != 0) {
		errno = k5err;
		goto free_fail;
	}
	ctx->service_uplink_login =
	ctx->service_uplink_anonymous = -1;
	//
	// Return the new management structure
	*out_ctx = ctx;
	return true;
	//
	// Cleanup and fail
free_fail:
	zap_free (ctx);
fail:
	return false;
}


/* Close a context for kip, destroying all the resources that were
 * allocated to it.  Any key material is first overwritten with
 * zeroes before it is removed.
 */
void kipctx_close (kipt_ctx ctx) {
	assert (ctx != NULL);
	kipt_key *key, *tmp;
	LL_FOREACH_SAFE (ctx->keys_head, key, tmp) {
		// Allocated after the key, zap entropy + key contents but don't free
		krb5_octet *entropy = (krb5_octet *) (&key [1]);
		memset (entropy, 0, ((intptr_t) key->k5keyblk.contents)
					+ key->k5keyblk.length
					- (intptr_t) entropy);
		zap_free (key);
	}
	krb5_free_context (ctx->k5ctx);
	if (ctx->service_uplink_login >= 0) {
		socket_close (ctx->service_uplink_login);
	}
	if (ctx->service_uplink_anonymous >= 0) {
		socket_close (ctx->service_uplink_anonymous);
	}
	zap_free (ctx);
}


/* Produce random bytes with the given address and length.
 *
 * This function returns true on success, or false with errno set
 * on failure.  The errno values are enhanced with codes from the
 * com_err table "KIP" as defined in this package, and those in
 * use with Kerberos.
 */
bool kipdata_random (kipt_ctx ctx, uint32_t len_target, uint8_t *out_target) {
	krb5_data ktgt;
	ktgt.length = len_target;
	ktgt.data   = (char*)out_target;
	krb5_error_code k5err;
	k5err = krb5_c_random_make_octets (ctx->k5ctx, &ktgt);
	if (k5err != 0) {
		errno = k5err;
		return false;
	} else {
		return true;
	}
}


/* Given a KIP key, determine its algorithm.
 */
bool kipkey_algorithm (kipt_ctx ctx, kipt_keyid keyid,
			kipt_alg *out_alg) {
	//
	// Find the key to conceal
	kipt_key *kk = NULL;
	LL_SEARCH_SCALAR (ctx->keys_head, kk, keyid, keyid);
	if (kk == NULL) {
		errno = KIPERR_KEYID_NOT_FOUND;
		goto fail;
	}
	//
	// Return success
	*out_alg = kk->k5keyblk.enctype;
	return true;
	//
	// Return failure
fail:
	return false;
}



/* Encrypt or *kip up* a clear text string into a bucket of mud.
 * The routine encrypts a block of clear text in isolation; your
 * application may however develop a system that splits content
 * into blocks of a moderate size and treat those separately.
 *
 * When your application splits data into blocks, the entirety
 * of the transported information is not protected by a sequence
 * of kipdata_up() calls.  The suggested remedy is to have an overall
 * checksum under protection of each key.  You can feed data into
 * the checksum with the routines below.
 *
 * This function returns true on success, or false with errno set
 * on failure.  The errno values are enhanced with codes from the
 * com_err table "KIP" as defined in this package, and those in
 * use with Kerberos.
 *
 * If the max_mudsz is too low, this function sets KIPERR_OUTPUT_SIZE
 * and nothing will get written to mud, not even a NULL value would
 * be noticed.  The out_mudsz value will be filled, though, and you
 * can use this to measure the desired output size.  When you set
 * max_mudsz=0 you can trigger this deliberately.
 */
bool kipdata_up (kipt_ctx ctx, kipt_keyid key,
			uint8_t *clear,   uint32_t clearsz,    /* input  */
			uint8_t *out_mud, uint32_t max_mudsz,  /* outbuf */
			uint32_t *out_mudsz) {                 /* usedup */
	//
	// Find the key to conceal
	kipt_key *kk = NULL;
	LL_SEARCH_SCALAR (ctx->keys_head, kk, keyid, key);
	if (kk == NULL) {
		errno = KIPERR_KEYID_NOT_FOUND;
		goto fail;
	}
	//
	// Check if we have sufficient room for output
	krb5_error_code k5err = 0;
	size_t needlen = ~0;
	k5err = krb5_c_encrypt_length (ctx->k5ctx, kk->k5keyblk.enctype, clearsz, &needlen);
	if (k5err != 0) {
		errno = k5err;
		goto fail;
	}
	if ((needlen >> 32) != 0) {
		errno = ERANGE;
		goto fail;
	}
	*out_mudsz = (uint32_t) needlen;
	if (needlen > max_mudsz) {
		errno = KIPERR_OUTPUT_SIZE;
		goto fail;
	}
	//
	// Actually encrypt
	krb5_data     plain;
	krb5_enc_data crypt;
	plain.data   = (char*)clear;
	plain.length = clearsz;
	crypt.kvno    = (krb5_kvno) key;	/* cut down to wire size */
	crypt.enctype = kk->k5keyblk.enctype;
	crypt.ciphertext.data   = (char*)out_mud;
	crypt.ciphertext.length = max_mudsz;
	k5err = krb5_c_encrypt (ctx->k5ctx, &kk->k5keyblk, KIP_KEYUSAGE_USERDATA,
			NULL, &plain, &crypt);
	if (k5err != 0) {
		errno = k5err;
		goto wipe_fail;
	}
	//
	// Update the key-implied hash with the data before encryption
	_hash_update (&kk->keysum, clear, clearsz);
	//
	// Return success
	*out_mudsz = crypt.ciphertext.length;
	return true;
	//
	// Cleanup and return failure
wipe_fail:
	memset (out_mud, 0, max_mudsz);  /* Never ever leak */
	*out_mudsz = 0;
fail:
	return false;
}



static bool _loop_decrypt (kipt_ctx ctx, krb5_keyusage keyusage,
				kipt_keyid key_id_nr, kipt_alg enctype,
				kipt_key **actual_key,
				uint8_t *cur_data, uint32_t  cur_len,
				uint8_t *nxt_data, uint32_t *nxt_len) {
	//
	// Pass through all keys for usable ones
	kipt_key *curkey;
	bool done = false;
	uint64_t mask = ~0L;
	if (key_id_nr < 4294967296L) {
		mask = 0xffffffffL;
	}
	LL_FOREACH (ctx->keys_head, curkey) {
		if (key_id_nr != (curkey->keyid & mask)) {
			continue;
		}
		if (enctype != curkey->k5keyblk.enctype) {
			if (enctype != _KIP_ALG_ANY) {
				continue;
			}
		}
		//
		// Just _try_ the key (partial identity!)
		krb5_error_code k5err = 0;
		krb5_enc_data crypt;
		krb5_data     plain;
		crypt.kvno    = (krb5_kvno) key_id_nr;
		crypt.enctype = curkey->k5keyblk.enctype;
		crypt.ciphertext.data   = (char*)cur_data;
		crypt.ciphertext.length = cur_len;
		plain.data   = (char*)nxt_data;
		plain.length = cur_len;
		k5err = krb5_c_decrypt (ctx->k5ctx, &curkey->k5keyblk, keyusage,
				NULL, &crypt, &plain);
		if (k5err == 0) {
			//
			// We were successful
			if (actual_key != NULL) {
				*actual_key = curkey;
			}
			cur_data = (uint8_t*)plain.data;
			*nxt_len = plain.length;
			return true;
		}
		errno = k5err;
	}
	//
	// Finished, and no key caught our imagination
	return false;
}


static void _nokey_errno (kipt_keyid id_or_nr) {
	if (id_or_nr < 4294967296L) {
		errno = KIPERR_NO_MATCHING_KEY;
	} else {
		errno = KIPERR_KEYID_NOT_FOUND;
	}
}



/* Decrypt or *kip down* a bucket of mud into a clear text string.
 * The routine decrypts a bucket of mud in isolation, and always
 * as a whole; the application may however compose multiple of
 * these buckets in some way.  See kipdata_up() for how this is done,
 * and be sure to validate the checksums it generates to retain
 * consistency.
 *
 * The key may be a unique key identity or the key number hints
 * that form a subset thereof; hints may require a few passes
 * through the decryption process though, in case of clashes
 * of those key numbers.
 *
 * This function returns true on success, or false with errno set
 * on failure.  The errno values are enhanced with codes from the
 * com_err table "KIP" as defined in this package, and those in
 * use with Kerberos.
 *
 * If the max_clearsz is too low, this function sets
 * KIPERR_OUTPUT_SIZE and nothing will get written to clean, not
 * even a NULL value would be noticed.  The out_clearsz value will
 * be filled though, and you can use this to measure the desired
 * output size.  When you set max_clearsz=0 you can trigger this
 * deliberately.
 */
bool kipdata_down (kipt_ctx ctx, kipt_keyid key_id_nr,
			uint8_t *mud,       uint32_t mudsz,        /* input  */
			uint8_t *out_clear, uint32_t max_clearsz,  /* outbuf */
			uint32_t *out_clearsz) {                   /* usedup */
	//
	// Check if we have enough space for the output
	*out_clearsz = mudsz;
	if (max_clearsz < mudsz) {
		/* Too cautious, but how can we calc it? */
		errno = KIPERR_OUTPUT_SIZE;
		goto fail;
	}
	//
	//
	// Actually decrypt -- allow key numbers via looped matching
	uint32_t clearlen = 0;
	kipt_key *actual_key;
	bool done = _loop_decrypt (ctx, KIP_KEYUSAGE_USERDATA,
				key_id_nr, _KIP_ALG_ANY,
				&actual_key,
				mud, mudsz,
				out_clear, &clearlen);
	if (!done) {
		_nokey_errno (key_id_nr);
		goto wipe_fail;
	}
	//
	// Update the key-implied hash with the data after decryption
	_hash_update (&actual_key->keysum, out_clear, clearlen);
	//
	// Return success
	*out_clearsz = clearlen;
	return true;
wipe_fail:
	memset (out_clear, 0, max_clearsz);	/* Never ever leak */
fail:
	return false;
}


#define _KIP_ALG_LAST 26
static int _enctype_acceptance [_KIP_ALG_LAST+1] = {
	KIPERR_ALGORITHM_UNKNOWN,	// 0    reserved    [RFC6448]
	KIPERR_ALGORITHM_DEPRECATED,	// 1    des-cbc-crc (deprecated)    [RFC6649]
	KIPERR_ALGORITHM_DEPRECATED,	// 2    des-cbc-md4 (deprecated)    [RFC6649]
	KIPERR_ALGORITHM_DEPRECATED,	// 3    des-cbc-md5 (deprecated)    [RFC6649]
	KIPERR_ALGORITHM_UNKNOWN,	// 4    Reserved    [RFC3961]
	KIPERR_ALGORITHM_DEPRECATED,	// 5    des3-cbc-md5 (deprecated)    [RFC8429]
	KIPERR_ALGORITHM_UNKNOWN,	// 6    Reserved    [RFC3961]
	KIPERR_ALGORITHM_DEPRECATED,	// 7    des3-cbc-sha1 (deprecated)    [RFC8429]
	KIPERR_ALGORITHM_UNKNOWN,	// 8    Unassigned
	KIPERR_ALGORITHM_PURPOSE,	// 9    dsaWithSHA1-CmsOID    [RFC4556]
	KIPERR_ALGORITHM_PURPOSE,	// 10    md5WithRSAEncryption-CmsOID    [RFC4556]
	KIPERR_ALGORITHM_PURPOSE,	// 11    sha1WithRSAEncryption-CmsOID    [RFC4556]
	KIPERR_ALGORITHM_PURPOSE,	// 12    rc2CBC-EnvOID    [RFC4556]
	KIPERR_ALGORITHM_PURPOSE,	// 13    rsaEncryption-EnvOID    [RFC4556][from PKCS#1 v1.5]]
	KIPERR_ALGORITHM_PURPOSE,	// 14    rsaES-OAEP-ENV-OID    [RFC4556][from PKCS#1 v2.0]]
	KIPERR_ALGORITHM_DEPRECATED,	// 15    des-ede3-cbc-Env-OID    [RFC4556]
					// (DES-EDE is locally deprecated)
	KIPERR_ALGORITHM_DEPRECATED,	// 16    des3-cbc-sha1-kd (deprecated)    [RFC8429]
	0,				// 17    aes128-cts-hmac-sha1-96    [RFC3962]
	0,				// 18    aes256-cts-hmac-sha1-96    [RFC3962]
	0,				// 19    aes128-cts-hmac-sha256-128    [RFC8009]
	0,				// 20    aes256-cts-hmac-sha384-192    [RFC8009]
	KIPERR_ALGORITHM_UNKNOWN,
	KIPERR_ALGORITHM_UNKNOWN,	// 21-22    Unassigned
	KIPERR_ALGORITHM_DEPRECATED,	// 23    rc4-hmac (deprecated)    [RFC8429]
	KIPERR_ALGORITHM_DEPRECATED,	// 24    rc4-hmac-exp (deprecated)    [RFC6649]
	0,				// 25    camellia128-cts-cmac    [RFC6803]
	0,				// 26    camellia256-cts-cmac    [RFC6803]
};


/* need to share with service.c */
bool _newkey (kipt_ctx ctx, kipt_alg alg,
			kipt_keynr in_keynr,
			kipt_key **out_key,
			krb5_data *out_entropy) {
	//
	// Verify that the algorithm is usable
	if ((alg > _KIP_ALG_LAST) || (alg <= 0)) {
		/* reject NULL encryption algorithm == 0 */
		/* local algorithms <0 are incompatible */
		errno = KIPERR_ALGORITHM_UNKNOWN;
		goto fail;
	} else if (_enctype_acceptance [alg] != 0) {
		errno = _enctype_acceptance [alg];
		goto fail;
	}
	assert (alg != _KIP_ALG_ANY);
	//
	// Find out how long the key store should be
	*out_key = NULL;
	out_entropy->data   = NULL;
	out_entropy->length = 0;
	krb5_error_code k5err = 0;
	size_t keybytes = 0;
	size_t keylength = 0;
	k5err = krb5_c_keylengths (ctx->k5ctx, alg, &keybytes, &keylength);
	if (k5err != 0) {
		errno = k5err;
		goto fail;
	}
	//
	// Allocate storage for struct kip_key, key contents, and entropy if seprate
	struct kip_key *newkey;
	newkey = calloc (1, sizeof (struct kip_key) + keybytes + keylength);
	if (newkey == NULL) {
		errno = ENOMEM;
		goto fail;
	}
	//
	// Store entropy after newkey and before the internal key contents
	newkey->entropy = ((krb5_octet *) &newkey [1]) + 0;
	//
	// Initialise, including an empty key block for krb5
	newkey->k5keyblk.magic    = KV5M_KEYBLOCK;
	newkey->k5keyblk.enctype  = alg;
	newkey->k5keyblk.contents = newkey->entropy + keybytes;
	newkey->k5keyblk.length   = keylength;
	//
	// Construct a new keyid and set it -- ensure it is unique
	++ctx->uniqtr;
	if (ctx->uniqtr & 0x80000000) {
		/* out of range... say what?!? */
		errno = KIPERR_TOO_MANY_KEYS;
		goto fail;
	}
	newkey->keyid = (((kipt_keyid) ctx->uniqtr) << 32) | in_keynr;
	assert ((newkey->keyid & 0xffffffff) == in_keynr);
	assert ((newkey->keyid >> 32) == ctx->uniqtr);
	//
	// Initialise the hash algorithm built into each key
	if (!_hash_init (&newkey->keysum, newkey)) {
		/* We failed to initialise the hash -- error should repeat later */
		// errno = KIPERR_ALGORITHM_SUPPORT_TODO;
		// goto fail;
		// quietly ignore
	}
	//
	// Return successfully (still need to queue the key and sum!)
	*out_key = newkey;
	out_entropy->data   =                             (char*)newkey->entropy; // Octets in newkey, chars in out_entropy
	out_entropy->length = newkey->k5keyblk.contents - newkey->entropy;
	return true;
	//
	// Cleanup and fail
fail:
	return false;
}


/* need to share with service.c */
bool _setkey (kipt_ctx ctx, krb5_data *entropy, kipt_key *key_to_set) {
	krb5_error_code k5err;
	k5err = krb5_c_random_to_key (ctx->k5ctx,
			key_to_set->k5keyblk.enctype,
			entropy,
			&key_to_set->k5keyblk);
	if (k5err != 0) {
		errno = k5err;
		return false;
	}
	return true;
}


/* need to share with service.c */
bool _addkey (kipt_ctx ctx, kipt_key *newkey) {
	//
	// Enqueue the key into the context
	LL_PREPEND (ctx->keys_head, newkey);
	//
	// Construct the default hash for this key
	newkey->keysum.sumid = newkey->keyid;
	LL_PREPEND (ctx->sums_head, &newkey->keysum);
	//
	// Return successfully
	return true;
}


/* Generate a KIP key.  There can be a few ways in which we
 * can get hold of the key; we do not define those yet but
 * ought to, at some point -- we need it for decryption.
 *
 * The input provides the key number as it will appear on
 * the wire.  The ideal situation is that this is unique,
 * but it is not required by KIP.  For use in the library,
 * we have stricter needs, so the context adds extra bits
 * to arrange this locally.
 *
 * The key identity is exported, but not the key itself.
 * As long as it resides in the context, the proper clearing
 * of the key material can be arranged by the KIP context.
 */
bool kipkey_generate (kipt_ctx ctx, kipt_alg alg,
			kipt_keynr   in_keynr,
			kipt_keyid *out_keyid) {
	//
	// Prepare the key by allocating and zeroing it
	kipt_key *tmpkey;
	krb5_data key_entropy;
	if (!_newkey (ctx, alg, in_keynr, &tmpkey, &key_entropy)) {
		goto fail;
	}
	//
	// Fill the key with random information, via entropy
	krb5_error_code k5err = 0;
	k5err = krb5_c_random_make_octets (ctx->k5ctx, &key_entropy);
	if (k5err != 0) {
		errno = k5err;
		goto free_fail;
	}
	if (!_setkey (ctx, &key_entropy, tmpkey)) {
		goto free_fail;
	}
	if (!_addkey (ctx, tmpkey)) {
		goto free_fail;
	}
	//
	// Return successfully
	*out_keyid = tmpkey->keyid;
	return true;
	//
	// Cleanup and fail
free_fail:
	zap_free (tmpkey);
fail:
	return false;
}



/* Produce a key mapping for a key.  This makes it possible to
 * have alternative paths, like a logical OR on the access
 * paths to a key and its contents.  To also provide an AND
 * compisition mechanism, it is possible to require the use
 * of multiple keys in a key mapping.  The result will be a
 * nested key mapping.
 *
 * The storage format for a nested key mapping is a header
 * with the mapping depth in 16 bits, (kvno,etype) pairs in
 * each 2x 32 bits, and then the nested encryption result
 * with encryption applied in the forward direction of the
 * list.  MACs and padding may pile up, but the out_keymudsz
 * will be set to represent all of the header and padding.
 *
 * This function returns true on success, or false with errno set
 * on failure.  The errno values are enhanced with codes from the
 * com_err table "KIP" as defined in this package, and those in
 * use with Kerberos.  Among the KIP values may be ones that were
 * generated on the KIP Service.
 *
 * If the keymudszmax is too low, this function sets
 * KIPERR_OUTPUT_SIZE and nothing will get written to keymud, not
 * even a NULL value would be noticed.  The out_keymudsz value
 * will be filled though, and you can use this to measure the
 * desired output size.  When you set keymudszmax=0 you can
 * trigger this deliberately.  (Note that out_keymudsz covers
 * all the mappings, even with depth above one.)
 */
bool kipkey_tomap (kipt_ctx ctx, kipt_keyid keyid_to_conceal,
			uint16_t    mappingdepth,                 /* AND count */
			kipt_keyid *mappingkeyids,                /* AND keyid */
			uint8_t *out_keymud, uint32_t max_keymudsz,  /* outbuf */
			uint32_t *out_keymudsz) {                    /* usedup */
	//
	// Sanity checks on mapping depth
	if (mappingdepth == 0) {
		errno = KIPERR_KEY_REQUIRED;
		return false;
	}
	if (mappingdepth > 100) {
		errno = KIPERR_INSANE_REQUEST;
		return false;
	}
	//
	// Allocate nesting keys on stack
	kipt_key *mappingkeys [mappingdepth];
	int i;
	for (i=0; i<mappingdepth; i++) {
		LL_SEARCH_SCALAR (ctx->keys_head, mappingkeys [i], keyid, mappingkeyids [i]);
		if (mappingkeys [i] == NULL) {
			errno = KIPERR_KEYID_NOT_FOUND;
			return false;
		}
	}
	//
	// Precompute the header size: depth, (kvno,enctp)+, (kvno,enctp)
	uint32_t hdrsz = 2 + 8 * mappingdepth + 8;
	//
	// Find the key to conceal, and its storage length
	kipt_key *kk_to_conceal = NULL;
	LL_SEARCH_SCALAR (ctx->keys_head, kk_to_conceal, keyid, keyid_to_conceal);
	if (kk_to_conceal == NULL) {
		errno = KIPERR_KEYID_NOT_FOUND;
		return false;
	}
	//
	// Precompute the size of the key to conceal
	krb5_error_code k5err;
	size_t keybytes = ~0;
	size_t _keylength = ~0;
	k5err = krb5_c_keylengths (ctx->k5ctx, kk_to_conceal->k5keyblk.enctype,
				&keybytes, &_keylength);
	if (k5err != 0) {
		errno = k5err;
		return false;
	}
	//
	// Compute the size of the nested-encrypted data
	size_t pre_len = keybytes;
	size_t post_len = ~0;
	for (i=0; i<mappingdepth; i++) {
		kipt_alg mapalg = mappingkeys [i]->k5keyblk.enctype;
		k5err = krb5_c_encrypt_length (ctx->k5ctx, mapalg, pre_len, &post_len);
		if (k5err != 0) {
			errno = k5err;
			return false;
		}
		pre_len = post_len;
	}
	pre_len = keybytes;	/* recover original */
	//
	// Check that we have enough space, warn otherwise
	*out_keymudsz = hdrsz + post_len;
	if (*out_keymudsz > max_keymudsz) {
		errno = KIPERR_OUTPUT_SIZE;
		return false;
	}
	//
	// Fill the header: depth#16, ( key#32, alg#32 )+, key#32, alg#32
	// No integrity checks, as the keys would fail on their MACs
	// (but the size of the final key should indeed be validated)
	* (uint16_t *) (out_keymud + 0) = htons( mappingdepth );
	for (i=0; i<mappingdepth; i++) {
		kipt_keynr mapkey = (uint32_t) mappingkeys [i]->keyid;
		kipt_alg   mapalg = mappingkeys [i]->k5keyblk.enctype;
		* (uint32_t *) (out_keymud + 2 + 8 * i) = htonl( mapkey );
		* ( int32_t *) (out_keymud + 6 + 8 * i) = htonl( mapalg );
	}
	* (uint32_t *) (out_keymud + 2 + 8 * i) = htonl( kk_to_conceal->keyid            );
	* ( int32_t *) (out_keymud + 6 + 8 * i) = htonl( kk_to_conceal->k5keyblk.enctype );
	//
	// Actually encrypt, with nesting, so rolling over itself...
	uint8_t tic [post_len];
	uint8_t toc [post_len];
	uint8_t *pre_data = kk_to_conceal->entropy;
	uint8_t *nxt_data = NULL;
	size_t cur_len = pre_len;
	for (i=0; i<mappingdepth; i++) {
		nxt_data = (pre_data != tic) ? tic : toc;
		krb5_data     plain;
		krb5_enc_data crypt;
		plain.data = (char*)pre_data;
		plain.length = cur_len;
		crypt.ciphertext.data = (char*)nxt_data;
		crypt.ciphertext.length = post_len;
		crypt.kvno    = (krb5_kvno) mappingkeys [i]->keyid;	/* cut down to wire size */
		crypt.enctype = mappingkeys [i]->k5keyblk.enctype;
		k5err = krb5_c_encrypt (ctx->k5ctx, &mappingkeys [i]->k5keyblk, KIP_KEYUSAGE_MAPPING,
				NULL, &plain, &crypt);
		if (k5err != 0) {
			errno = k5err;
			goto wipe_fail;
		}
		pre_data = nxt_data;
		cur_len = crypt.ciphertext.length;
	}
	assert (cur_len == post_len);
	//
	// Copy the result, then clear intermediate storage
	memcpy (out_keymud + hdrsz, nxt_data, post_len);
	memset (tic, 0, post_len);
	memset (toc, 0, post_len);
	//
	// Return successfully
	return true;
	//
	// Return failure
wipe_fail:
	memset (out_keymud, 0, max_keymudsz);
fail:
	return false;
}


/* Use already-known keys to extract a key that was hidden in "key
 * mud" by a previous call to kipkey_tomap().  Store the resulting
 * key in the KIP context and provide the customary locally unique
 * keyid for it.  The extension from the wire format of a keynr_t
 * into a handle of a keyid_t means that the latter could differ
 * from the keyid_t originally sent.  The key value is not exported.
 *
 * This function returns true on success, or false with errno set
 * on failure.  The errno values are enhanced with codes from the
 * com_err table "KIP" as defined in this package, and those in
 * use with Kerberos.  Among the KIP values may be ones that were
 * generated on the KIP Service.
 */
struct _mappingpair { uint32_t kvno; int32_t enctype; };
bool kipkey_frommap (kipt_ctx ctx,
			uint8_t *keymud, uint32_t keymudsz,
			kipt_keyid *out_mappedkey) {
	//
	// Find mappingdepth, precompute headersize, point to pairs
	uint16_t mappingdepth = ntohs( * (uint16_t *) (keymud + 0) );
	uint32_t hdrsz = 2 + 8 * mappingdepth + 8;
	struct _mappingpair *pairs = (void *) (keymud + 2);
	uint32_t pre_len = keymudsz - hdrsz;
	uint8_t *pre_ptr = keymud   + hdrsz;
	//
	// Sanity checks on mapping depth
	if (mappingdepth == 0) {
		errno = KIPERR_KEY_REQUIRED;
		return false;
	}
	if (mappingdepth > 100) {
		errno = KIPERR_INSANE_REQUEST;
		return false;
	}
	//
	// Start iteration over mappingpairs
	uint8_t tic [pre_len];
	uint8_t toc [pre_len];
	uint8_t *cur_data = pre_ptr;
	uint32_t cur_len  = pre_len;
	uint8_t *nxt_data = NULL;
	uint32_t nxt_len  = 0;
	int i = mappingdepth;
	while (i-- > 0) {
		krb5_kvno    cur_kvno = ntohl( pairs [i].kvno    );
		krb5_enctype cur_alg  = ntohl( pairs [i].enctype );
		nxt_data = (cur_data != tic) ? tic : toc;
		bool done = _loop_decrypt (ctx, KIP_KEYUSAGE_MAPPING,
				cur_kvno, cur_alg,
				NULL,
				cur_data, cur_len,
				nxt_data, &nxt_len);
		//
		// Complain if no usable key found
		if (!done) {
			errno = KIPERR_NO_MATCHING_KEY;
			goto wipe_fail;
		}
		//
		// Move on to the next stage
		cur_data = nxt_data;
		cur_len  = nxt_len;
	}
	//
	// Validate the length of the eventual key
	krb5_kvno tgt_kvno   = ntohl( pairs [mappingdepth].kvno    );
	krb5_enctype tgt_alg = ntohl( pairs [mappingdepth].enctype );
	//
	// Construct a new key block for the key
	kipt_key *tgt_key;
	krb5_data tgt_entropy;
	if (!_newkey (ctx, tgt_alg, tgt_kvno, &tgt_key, &tgt_entropy)) {
		goto wipe_fail;
	}
	if (cur_len != tgt_entropy.length) {
		errno = KIPERR_KEYMAP_SCRAMBLED;
		goto wipe_fail;
	}
	memcpy (tgt_entropy.data, cur_data, tgt_entropy.length);
	memset (tic, 0, keymudsz - hdrsz);
	memset (toc, 0, keymudsz - hdrsz);
	//
	// Derive the key from its entropy
	if (!_setkey (ctx, &tgt_entropy, tgt_key)) {
		goto fail;
	}
	//
	// Insert the key block and its checksum into lists
	if (!_addkey (ctx, tgt_key)) {
		zap_free (tgt_key);
		goto fail;
	}
	//
	// Return to report with success
	*out_mappedkey = tgt_key->keyid;
	return true;
	//
	// Return failure
wipe_fail:
	memset (tic, 0, keymudsz - hdrsz);
	memset (toc, 0, keymudsz - hdrsz);
fail:
	return false;
}



/* Integrity checks may be used to span independently encrypted
 * blocks.  Without these checks, it could be possible to remove
 * blocks, insert or duplicate older ones, or change the order
 * in which they occur.  This is generally undesirable, but it
 * is not a requirement for protocols employing single blocks.
 *
 * These integrity checks are implemented as secure checksums
 * under protection of a key.  Only those who hold the key
 * could alter the document without breaking the checksum, so
 * that includes you when you can check it, but at least it
 * excludes anyone outside your trusted group.  Under symmetric
 * crypto, there is no difference between reading and writing
 * privileges, but there is a difference between the in-crowd
 * and the out-laws.
 *
 * There is a difference between integrity checks on encrypted
 * or decrypted content.  Note however, that a complex document
 * with sections encrypted to different keys may not be readable
 * in full by everyone.  Such unreadable content is best not
 * included in the integrity check, or it should be included
 * in its encrypted form.  Other applications may choose to
 * represent separate views on a document from each of the keys
 * used in it, since integrity checks are made with keys anyway.
 * This would mean that encrypted content is used where it
 * cannot be decrypted with the integrity-checking key, but use
 * decrypted content where it can be decrypted with that key.
 * This is all pretty wild, but there is room for diversity,
 * and that being a good thing we aim to support it here, while
 * leaving a simple-yet-secure case as a default.
 *
 * By default, every key starts its own checksum and will hash
 * everything that it decrypts.  You can additionally define
 * insertions; either with plain or muddy data, or perhaps you
 * feel a need to note where the checksummed blocks start and
 * end, as that is lost in the default approach.
 *
 * Checksums can be finished and exported at any time.  Their
 * value is a "muddy sum", ready to be inserted into a document
 * or other application flow.
 */



static bool _hash_init_context (kipt_sum *sum) {
	//
	// Clear out the marker fields
	sum->marking = false;
	sum->separation = 0;
	//
	// Initialise the hash context
	switch (sum->keyalg) {
	case ENCTYPE_AES128_CTS_HMAC_SHA1_96:
	case ENCTYPE_AES256_CTS_HMAC_SHA1_96:
		return (SHA1_Init (&sum->hctx.sha1) == 1);
	case ENCTYPE_AES128_CTS_HMAC_SHA256_128:
		return (SHA256_Init (&sum->hctx.sha256) == 1);
	case ENCTYPE_AES256_CTS_HMAC_SHA384_192:
		return (SHA384_Init (&sum->hctx.sha384) == 1);
	case ENCTYPE_CAMELLIA128_CTS_CMAC:	/* TODO: Add support */
	case ENCTYPE_CAMELLIA256_CTS_CMAC:	/* TODO: Add support */
	default:
		return false;
	}
}


static bool _hash_init (kipt_sum *sum, kipt_key *key) {
	//
	// Extract data from the key; look it up later
	sum->keyalg = key->k5keyblk.enctype;
	sum->keyid  = key->keyid;
	//
	// Initialise the hash context
	return _hash_init_context (sum);
}


static bool _hash_update_hctx (union kip_sum_context_union *hctx, kipt_alg keyalg, uint8_t *data, uint32_t datalen) {
	switch (keyalg) {
	case ENCTYPE_AES128_CTS_HMAC_SHA1_96:
	case ENCTYPE_AES256_CTS_HMAC_SHA1_96:
		return (SHA1_Update   (&hctx->sha1,   data, datalen) == 1);
	case ENCTYPE_AES128_CTS_HMAC_SHA256_128:
		return (SHA256_Update (&hctx->sha256, data, datalen) == 1);
	case ENCTYPE_AES256_CTS_HMAC_SHA384_192:
		return (SHA384_Update (&hctx->sha384, data, datalen) == 1);
	case ENCTYPE_CAMELLIA128_CTS_CMAC:	/* TODO: Add support */
	case ENCTYPE_CAMELLIA256_CTS_CMAC:	/* TODO: Add support */
	default:
		return false;
	}
}


static bool _hash_update (kipt_sum *sum, uint8_t *data, uint32_t datalen) {
	if (!_hash_update_hctx (&sum->hctx, sum->keyalg, data, datalen)) {
		return false;
	}
	sum->separation += datalen;
	return true;
}


uint16_t _hash_size (kipt_sum *sum) {
	switch (sum->keyalg) {
	case ENCTYPE_AES128_CTS_HMAC_SHA1_96:
	case ENCTYPE_AES256_CTS_HMAC_SHA1_96:
		#if KIPSUM_MAXSIZE < 20
		#error "Update KIPSUM_MAXSIZE to accommodate SHA1"
		#endif
		return 20;
	case ENCTYPE_AES128_CTS_HMAC_SHA256_128:
		#if KIPSUM_MAXSIZE < 32
		#error "Update KIPSUM_MAXSIZE to accommodate SHA256"
		#endif
		return 32;
	case ENCTYPE_AES256_CTS_HMAC_SHA384_192:
		#if KIPSUM_MAXSIZE < 48
		#error "Update KIPSUM_MAXSIZE to accommodate SHA384"
		#endif
		return 48;
	case ENCTYPE_CAMELLIA128_CTS_CMAC:	/* TODO: Add support */
	case ENCTYPE_CAMELLIA256_CTS_CMAC:	/* TODO: Add support */
	default:
		return 0;
	}
}


bool _hash_final (kipt_sum *sum, uint8_t *opt_scrambler, uint8_t *out_data) {
	/* Is this a locked hash?  Then we can output directly */
	if (sum->locked) {
		if (opt_scrambler != NULL) {
			return false;
		}
		uint16_t hsz = _hash_size (sum);
		if (hsz == 0) {
			return false;
		}
		memcpy (out_data, sum->hctx.output, hsz);
		return true;
	}
	/* Clone the contents to stack, so we cannot consume it */
	union kip_sum_context_union stacked;
	memcpy (&stacked, &sum->hctx, sizeof (stacked));
	/* If we are marking, insert a final length */
	if (sum->marking) {
		if ((sum->separation >> 32) != 0) {
			return false;
		}
		uint8_t marking [5];
		marking [0] = 0x84;
		* (uint32_t *) &marking [1] = htonl( sum->separation );
		if (!_hash_update_hctx (&stacked, sum->keyalg, marking, 5)) {
			return false;
		}
	}
	/* If we have a scrambler, append _hash_size() bytes from it */
	if (opt_scrambler != NULL) {
		if (!_hash_update_hctx (&stacked, sum->keyalg, opt_scrambler, _hash_size (sum))) {
			return false;
		}
	}
	/* Assume out_data can hold _hash_size() bytes */
	switch (sum->keyalg) {
	case ENCTYPE_AES128_CTS_HMAC_SHA1_96:
	case ENCTYPE_AES256_CTS_HMAC_SHA1_96:
		return (SHA1_Final   (out_data, &stacked.sha1  ) == 1);
	case ENCTYPE_AES128_CTS_HMAC_SHA256_128:
		return (SHA256_Final (out_data, &stacked.sha256) == 1);
	case ENCTYPE_AES256_CTS_HMAC_SHA384_192:
		return (SHA384_Final (out_data, &stacked.sha384) == 1);
	case ENCTYPE_CAMELLIA128_CTS_CMAC:	/* TODO: Add support */
	case ENCTYPE_CAMELLIA256_CTS_CMAC:	/* TODO: Add support */
	default:
		return false;
	}
}


/* Start a new checksum, to be protected with the indicated
 * key identity.  This call is implicit for any key added to
 * the context; the kipt_keyid identity serves as the checksum
 * identity for this implied checksum; the reverse is not true.
 *
 * TODO: Does this make sense -- can applications use it, if
 * they have no way of setting the checksum identity?  Or is
 * perhaps something we should add here?
 */
bool kipsum_start (kipt_ctx ctx, kipt_keyid supporting_key,
			kipt_sumid *out_kipsum) {
	//
	// Find the key that supports this hash
	kipt_key *kk = NULL;
	LL_SEARCH_SCALAR (ctx->keys_head, kk, keyid, supporting_key);
	if (kk == NULL) {
		errno = KIPERR_KEYID_NOT_FOUND;
		goto fail;
	}
	//
	// Allocate memory for the checksum
	kipt_sum *newsum = NULL;
	newsum = calloc (sizeof (kipt_sum), 1);
	if (newsum == NULL) {
		errno = ENOMEM;
		goto fail;
	}
	//
	// Initialise the hash
	_hash_init (newsum, kk);
	//
	// Find a new sumid value
	++ctx->uniqtr;
	if (ctx->uniqtr & 0x80000000) {
		/* out of range... say what?!? */
		errno = KIPERR_TOO_MANY_SUMS;
		goto fail;
	}
	newsum->sumid = 1 + ~ ctx->uniqtr;
	//
	// Enable future finding by sumid
	LL_PREPEND (ctx->sums_head, newsum);
	//
	// Report success
	*out_kipsum = newsum->sumid;
	return true;
	//
	// Report error after cleanup
zap_fail:
	zap_free (newsum);
fail:
	return false;
}


/* Restart a checksum, by flushing its current state and
 * assuming nothing passed through it yet.  Do not loose
 * the coupling with the key; both key identity and, if
 * this is the default sum for a key its future data, will
 * be inserted here.
 */
bool kipsum_restart (kipt_ctx ctx, kipt_sumid sumid_or_keyid) {
	//
	// Find the checksum object
	kipt_sum *cs = NULL;
	LL_SEARCH_SCALAR (ctx->sums_head, cs, sumid, sumid_or_keyid);
	if (cs == NULL) {
		errno = KIPERR_SUMID_NOT_FOUND;
		goto fail;
	}
	//
	// Reset the checksum data
	if (!_hash_init_context (cs)) {
		errno = KIPERR_CHECKSUM_ALGORITHM;
		goto fail;
	}
	//
	// Return success
	return true;
	//
	// Return failure
fail:
	return false;
}


/* Fork a checksum, meaning that its current state will be
 * cloned and the old and new checksum can continue from
 * this same state.  The old one will not show any change
 * and the new one would be able to compute the same
 * output.
 *
 * If the old checksum happened to be a key's coupled
 * checksum, then only the old checksum will continue
 * to receive data automatically; the new checksum will
 * only have what was automatically inserted up to the
 * point of forking.
 *
 * TODO: For now, we require *out_kipsum to be 0 on
 *       calling; we may later reuse existing sums.
 */
bool kipsum_fork (kipt_ctx ctx, kipt_sumid sumid_or_keyid, kipt_sumid *out_kipsum) {
	//
	// Find the checksum object
	kipt_sum *cs = NULL;
	LL_SEARCH_SCALAR (ctx->sums_head, cs, sumid, sumid_or_keyid);
	if (cs == NULL) {
		errno = KIPERR_SUMID_NOT_FOUND;
		goto fail;
	}
	//
	// Allocate memory for the checksum
	assert (*out_kipsum == 0);
	kipt_sum *newsum = NULL;
	newsum = calloc (sizeof (kipt_sum), 1);
	if (newsum == NULL) {
		errno = ENOMEM;
		goto fail;
	}
	//
	// Clone the hash: keyalg/id, marking, and hash context
	newsum->keyalg     = cs->keyalg    ;
	newsum->keyid      = cs->keyid     ;
	newsum->marking    = cs->marking   ;
	newsum->separation = cs->separation;
	memcpy (&newsum->hctx, &cs->hctx, sizeof (newsum->hctx));
	//
	// Find a new sumid value
	++ctx->uniqtr;
	if (ctx->uniqtr & 0x80000000) {
		/* out of range... say what?!? */
		errno = KIPERR_TOO_MANY_SUMS;
		goto fail;
	}
	newsum->sumid = 1 + ~ ctx->uniqtr;
	//
	// Enable future finding by sumid
	LL_PREPEND (ctx->sums_head, newsum);
	//
	// Return success
	*out_kipsum = newsum->sumid;
	return true;
	//
	// Return failure
fail:
	zap_free (newsum);
	return false;
}


/* Update a checksum by inserting the bytes given into the
 * flow of hashed data.
 *
 * Note that a keyid is always a sumid; you can send data
 * to the checksum for your key by using its keyid in this
 * position.  Also note that this does not extend to
 * key numbers, as these are not sufficiently selective.
 */
bool kipsum_append (kipt_ctx ctx, kipt_sumid sumid_or_keyid,
			uint8_t *data, uint32_t datasz) {
	//
	// Find the checksum object
	kipt_sum *cs = NULL;
	LL_SEARCH_SCALAR (ctx->sums_head, cs, sumid, sumid_or_keyid);
	if (cs == NULL) {
		errno = KIPERR_SUMID_NOT_FOUND;
		goto fail;
	}
	//
	// Actually update the hash context
	if (!_hash_update (cs, data, datasz)) {
		errno = KIPERR_CHECKSUM_UPDATE;
		goto fail;
	}
	//
	// Return success
	return true;
	//
	// Return failure
fail:
	return false;
}


/* Insert a marker separating data pieces.  This solves
 * problems that may arise when fields just run from one
 * into the next, and a separator cannot be seen; added
 * information to the first field may now be taken from
 * the second field without changing the checksum, which
 * is a possible security risk.
 *
 * As a general rule, try to compose your input to checksum
 * such that they could be parsed unambiguously.  One way
 * of doing this is inserting separators (and be mindful
 * about escapes!) and this is why this function exists.
 * It will insert a marker that forms a chain until the
 * end (where we will insert an extra marker, even if you
 * do that too).
 *
 * To help you write unambiguously parsing input to the
 * checksum, you can even add typing information, in the
 * form of a string.  The string will be inserted too.
 * It is up to you what it covers; the marker, text
 * before or after it, or perhaps both.  You can insert
 * any number of markers that you like.
 *
 * These functions are likely to be incompatible with
 * things done in other software, or hand-crafted
 * procedures such as standardised in RFCs.  For those,
 * you should manually provide your input, byte by byte.
 * For local uses, you may benefit from the simplicity
 * and not-having-to-be-a-cryptographer just to get
 * your security organised.
 */
bool kipsum_mark (kipt_ctx ctx, kipt_sumid sumid_or_keyid,
			char *opt_typing) {
	//
	// Find the checksum object
	kipt_sum *cs = NULL;
	LL_SEARCH_SCALAR (ctx->sums_head, cs, sumid, sumid_or_keyid);
	if (cs == NULL) {
		errno = KIPERR_SUMID_NOT_FOUND;
		goto fail;
	}
	//
	// Construct the marker, cautious about escaping
	uint8_t marker;
	assert ((cs->separation >> 32) == 0);
	marker = 0x04;
	if (opt_typing != NULL) {
		marker |= 0x80;
	}
	uint8_t marker_update [5];
	marker_update [0] = marker;
	* (uint32_t *) &marker_update [1] = htonl( cs->separation );
	//
	// Reset the separation counter -- allowing kipsum_sign ()
	cs->separation = 0;
	cs->marking = true;
	//
	// Actually update the hash context
	if (!_hash_update (cs, marker_update, 5)) {
		errno = KIPERR_CHECKSUM_UPDATE;
		goto fail;
	}
	if ((opt_typing != NULL) && !_hash_update (cs, (uint8_t*)opt_typing, 1 + strlen (opt_typing))) {
		errno = KIPERR_CHECKSUM_UPDATE;
		goto fail;
	}
	//
	// Return success
	return true;
	//
	// Return failure
fail:
	return false;
}


/* Merge another checksum into this one.  This is done
 * by computing its value and appending its output.  Do
 * keep this separatable from other content, or risk the
 * consequences.  (If you could not parse back what you
 * put into a checksum, you may end up allowing abuse.)
 *
 * The checksums should be compatible, that is, they
 * should use the same key and algorithm.  This may be
 * relaxed at a later time.
 */
bool kipsum_merge (kipt_ctx ctx, kipt_sumid accumulator, kipt_sumid addend) {
	//
	// Find the accumulator checksum object
	kipt_sum *cs_accu = NULL;
	LL_SEARCH_SCALAR (ctx->sums_head, cs_accu, sumid, accumulator);
	if (cs_accu == NULL) {
		errno = KIPERR_SUMID_NOT_FOUND;
		goto fail;
	}
	//
	// Find the addend checksum object
	kipt_sum *cs_addend = NULL;
	LL_SEARCH_SCALAR (ctx->sums_head, cs_addend, sumid, addend);
	if (cs_addend == NULL) {
		errno = KIPERR_SUMID_NOT_FOUND;
		goto fail;
	}
	//
	// Check that they use the same key and alg
	assert (cs_accu->keyalg == cs_addend->keyalg);
	assert (cs_accu->keyid == cs_addend->keyid);
	//
	// Find the length of the raw hash value
	uint16_t hashsz = _hash_size (cs_addend);
	if (hashsz == 0) {
		errno = KIPERR_CHECKSUM_ALGORITHM;
		goto fail;
	}
	//
	// Compute the checksum from the addend
	uint8_t rawhash [KIPSUM_MAXSIZE];
	assert (hashsz <= sizeof (rawhash));
	assert (_hash_final (cs_addend, NULL, rawhash));
	//
	// Update the checksum of the accumulator
	if (!_hash_update (cs_accu, rawhash, hashsz)) {
		errno = KIPERR_CHECKSUM_UPDATE;
		goto fail;
	}
	//
	// Return success
	return true;
	//
	// Return failure
fail:
	return false;
}


/* Sign a document: Export the checksum, which is a key-encrypted
 * form of the encryption type's mandatory hash.
 *
 * This operation does not consume the checksum; it can simply
 * continue to add data and/or be verified or signed again.
 */
bool kipsum_sign (kipt_ctx ctx, kipt_sumid sumid_or_keyid, uint32_t max_siglen,
				uint32_t *out_siglen, uint8_t *out_sig) {
	//
	// Find the checksum object
	kipt_sum *cs = NULL;
	LL_SEARCH_SCALAR (ctx->sums_head, cs, sumid, sumid_or_keyid);
	if (cs == NULL) {
		errno = KIPERR_SUMID_NOT_FOUND;
		goto fail;
	}
	//
	// Find the supporting key object
	kipt_key *supkey = NULL;
	LL_SEARCH_SCALAR (ctx->keys_head, supkey, keyid, cs->keyid);
	if (supkey == NULL) {
		errno = KIPERR_KEYID_NOT_FOUND;
		goto fail;
	}
	//
	// Find the length of the raw hash value
	uint16_t hashsz = _hash_size (cs);
	if (hashsz == 0) {
		errno = KIPERR_CHECKSUM_ALGORITHM;
		goto fail;
	}
	//
	// Determine if the size for encrypting the raw hash is available
	size_t needlen;
	krb5_error_code k5err;
	k5err = krb5_c_encrypt_length (ctx->k5ctx, supkey->k5keyblk.enctype, hashsz, &needlen);
	if (k5err != 0) {
		errno = k5err;
		goto fail;
	}
	if ((needlen >> 32) != 0) {
		errno = ERANGE;
		goto fail;
	}
	*out_siglen = (uint32_t) needlen;
	if (needlen > max_siglen) {
		errno = KIPERR_OUTPUT_SIZE;
		goto fail;
	}
	//
	// Determine the hash -- this should not fail anymore
	uint8_t rawhash [KIPSUM_MAXSIZE];
	assert (hashsz <= sizeof (rawhash));
	assert (_hash_final (cs, NULL, rawhash));
	//
	// Encrypt the hash -- this should not fail anymore
	krb5_data     plain;
	krb5_enc_data crypt;
	plain.data   = (char*)rawhash;
	plain.length = hashsz;
	crypt.kvno    = (krb5_kvno) supkey->keyid;	/* cut down to wire size */
	crypt.enctype = supkey->k5keyblk.enctype;
	crypt.ciphertext.data   = (char*)out_sig;
	crypt.ciphertext.length = max_siglen;
	k5err = krb5_c_encrypt (ctx->k5ctx, &supkey->k5keyblk, KIP_KEYUSAGE_INTEGRITY,
			NULL, &plain, &crypt);
	//
	// Assure that encryption succeeded; the caller expects to reuse the hash on failure
	assert (k5err == 0);
	//
	// Report success
	return true;
	//
	// Report error after any cleanup
wipe_fail:
	memset (out_sig, 0, max_siglen);
fail:
	return false;
}


/* Verify a document: Compare the checksum, which was key-encrypted
 * with the encryption type's mandatory hash.
 *
 * This operation does not consume the checksum; it can simply
 * continue to add data and/or be verified or signed again.
 *
 * In general, those are the outcomes that you are looking for:
 *
 *  - true  for successful verifcation
 *  - false with errno=KIPERR_CHECKSUM_INVALID for signature failure
 *  - false with other errno for various operational problems
 */
bool kipsum_verify (kipt_ctx ctx, kipt_sumid sumid_or_keyid,
				uint32_t siglen, uint8_t *sig) {
	//
	// Find the checksum object
	kipt_sum *cs = NULL;
	LL_SEARCH_SCALAR (ctx->sums_head, cs, sumid, sumid_or_keyid);
	if (cs == NULL) {
		errno = KIPERR_SUMID_NOT_FOUND;
		goto fail;
	}
	//
	// Find the supporting key object
	kipt_key *supkey = NULL;
	LL_SEARCH_SCALAR (ctx->keys_head, supkey, keyid, cs->keyid);
	if (supkey == NULL) {
		errno = KIPERR_KEYID_NOT_FOUND;
		goto fail;
	}
	//
	// Find the length of the raw hash value
	uint16_t hashsz = _hash_size (cs);
	if (hashsz == 0) {
		errno = KIPERR_CHECKSUM_ALGORITHM;
		goto fail;
	}
	//
	// Decrypt with the supporting key, and find a raw hash
	uint8_t sighash [KIPSUM_MAXSIZE];
	assert (hashsz <= sizeof (sighash));
	krb5_error_code k5err = 0;
	krb5_enc_data crypt;
	krb5_data     plain;
	crypt.kvno    = (krb5_kvno) supkey->keyid;	/* Scale down to wire size */
	crypt.enctype = supkey->k5keyblk.enctype;
	crypt.ciphertext.data   = (char*)sig;
	crypt.ciphertext.length = siglen;
	plain.data   = (char*)sighash;
	plain.length = KIPSUM_MAXSIZE;
	k5err = krb5_c_decrypt (ctx->k5ctx, &supkey->k5keyblk,
			KIP_KEYUSAGE_INTEGRITY,
			NULL, &crypt, &plain);
	if (k5err != 0) {
		// Includes KRB5KRB_AP_ERR_BAD_INTEGRITY (useful info)
		errno = k5err;
		goto fail;
	}
	if (plain.length != hashsz) {
		errno = KIPERR_CHECKSUM_INVALID;
		goto fail;
	}
	//
	// Determine the hash from the context
	uint8_t rawhash [KIPSUM_MAXSIZE];
	assert (hashsz <= sizeof (rawhash));
	assert (_hash_final (cs, NULL, rawhash));
	if (memcmp (sighash, rawhash, hashsz) != 0) {
		errno = KIPERR_CHECKSUM_INVALID;
		goto fail;
	}
	//
	// Report success
	return true;
	//
	// Reprt error after any cleanup
fail:
	return false;
}



/* Key Tables -- An extension to KIP for using key tables
 *
 * Key tables are used in Kerberos for storing fixed secrets, usually
 * for servers.  They are not used by clients since it would bind them
 * to the location of the keytab and because clients' credentials are
 * more difficult to protect than a server's.  This is why it is
 * probably a good idea to make libkiptab only available to servers.
 *
 * When used, the functionality consists of an extra call
 * kipkey_fromkeytab() that imports a given key from the
 * keytab.
 */



/* Load a KIP key from a key table.  If an opt_hostname is
 * given, the key should be named "kip/opt_hostname@DOMAIN"
 * and otherwise it will be a local name such as
 * "kip/MASTER@DOMAIN" for a master key.
 *
 * Whether the master key is accessible is not a matter of
 * hiding it from this API.  It is a matter of access
 * rights to the keytab containing it.  It is strongly
 * advisable to have "kip/MASTER@DOMAIN" on a keytab of
 * its own, and constrain access to that key.
 *
 * You may specify a service if it is not "kip", but that
 * must mean this is a virtual service reference, so it
 * will require no opt_hostname and domain must be ".".
 *
 * TODO: The opt_hostname has no use in the proper usecase
 *       of realm-centric use for KIP Service.  Or do we
 *       keep it for realm crossover between KIP Services?
 *
 * Note: Consider kipkey_fromvhosttab() instead; it is
 *       easier to use, and integrates with management
 *       commands for service virtual hosts.
 */
bool kipkey_fromkeytab (kipt_ctx ctx,
			const char *opt_ktname, const char *opt_service,
			const char *opt_hostname, const char *domain,
			kipt_keynr kvno, kipt_alg enctype,
			kipt_keyid *out_keyid) {
#if 0
	//
	// Do not allow kvno or enctype to be zero
	assert (kvno != 0);
	assert (enctype != 0);
#endif
	//
#if 0
	//
	// Check that the opt_hostname, if mentioned, includes a dot
	if (strchr (opt_hostname, '.') == NULL) {
		/* Is this a good-enough error message? */
		errno = KIPERR_INSANE_REQUEST;
com_err ("libkip", errno, "Fault in %s:%d", __FILE__, __LINE__);
		return false;
	}
#endif
	//
	// If a service is provided, it constrains the
	// opt_hostname and domain.
	if (opt_service != NULL) {
		if ((opt_hostname != NULL) || (domain [0] != '.') || (domain [1] != '\0')) {
			errno = KIPERR_NO_MATCHING_KEY;
			return false;
		}
	} else {
		opt_service = "kip";
	}
	//
	// Check the domain, map it to a realm "DOMAIN"
	assertxt (domain != NULL, "Missing realm/domain name");
	int realmlen = strlen (domain);
	char realm [realmlen + 1];
	int i;
	for (i=0; i<realmlen; i++) {
		realm [i] = toupper (domain [i]);
	}
	realm [i] = '\0';
	//
	// Load the keytab
	krb5_error_code k5err;
	krb5_keytab kt;
	char ktbuf [MAX_KEYTAB_NAME_LEN + 3];
	if (opt_ktname == NULL) {
		char *vardir = getenv ("KIP_VARDIR");
		if (vardir == NULL) {
			vardir = "/var/lib/arpa2/kip";
		}
		snprintf (ktbuf, MAX_KEYTAB_NAME_LEN + 1,
			"FILE:%s/master.keytab", vardir);
		opt_ktname = ktbuf;
	}
	k5err = krb5_kt_resolve (ctx->k5ctx, opt_ktname, &kt);
	if (k5err != 0) {
		errno = k5err;
		goto fail;
	}
	//
	// Build the PrincipalName to lookup
	krb5_principal princ;
	if (opt_hostname == NULL) {
		opt_hostname = "MASTER";
	}
	k5err = krb5_build_principal (ctx->k5ctx, &princ,
				realmlen, realm, opt_service, opt_hostname, NULL);
	if (k5err != 0) {
		errno = k5err;
		goto kt_fail;
	}
	//
	// Load the keytab entry
	krb5_keytab_entry ktent;
	memset (&ktent, 0, sizeof (ktent));
	//TODO// k5err = krb5_kt_get_entry (ctx->k5ctx, kt, princ, kvno, enctype, &ktent);
	k5err = krb5_kt_get_entry (ctx->k5ctx, kt, princ, kvno, enctype, &ktent);
	if (k5err != 0) {
		errno = k5err;
		goto princ_kt_fail;
	}
	//
	// Setup a new key (write directly to the contents of the key)
	kipt_key *ktkey;
	krb5_data no_entropy;
	if (!_newkey (ctx, ktent.key.enctype, ktent.vno, &ktkey, &no_entropy)) {
		goto ent_princ_kt_fail;
	}
	if (ktent.key.length != ktkey->k5keyblk.length) {
		errno = EINVAL;
		goto ent_princ_kt_fail;
	}
	memcpy (ktkey->k5keyblk.contents, ktent.key.contents, ktent.key.length);
	if (!_addkey (ctx, ktkey)) {
		goto ent_princ_kt_fail;
	}
	//
	// Keys in a keytab have no entropy for passing them around
	memset (no_entropy.data, 0, no_entropy.length);
	ktkey->entropy = NULL;
	//
	// Cleanup and return successfully
	krb5_free_keytab_entry_contents (ctx->k5ctx, &ktent);
	krb5_free_principal (ctx->k5ctx, princ);
	krb5_kt_close (ctx->k5ctx, kt);
	*out_keyid = ktkey->keyid;
	return true;
	//
	// Return failure
ent_princ_kt_fail:
	krb5_free_keytab_entry_contents (ctx->k5ctx, &ktent);
princ_kt_fail:
	krb5_free_principal (ctx->k5ctx, princ);
kt_fail:
	krb5_kt_close (ctx->k5ctx, kt);
fail:
	return false;
}


/* Load a KIP key for a service at a virtual host/domain.
 *
 * This bootstraps from a service master key found in a
 * special place.  Where this special place is, depends
 * on the installation setup; it may be a keytab or PKCS #11
 * store, or just a higher-up process with more privileges
 * from which we were just a constrained clone.
 *
 * Using on this service key, a key specific to the virtual
 * host is derived with kipkey_frommap().  The virtual host
 * mapping can be just a file in the local file system under
 * protection of the master key, but again it depends on the
 * installation setup if it is that simple or more advanced.
 *
 * In general, the idea of a vhosttab is a dictionary lookup
 * for a given (service,vhost) name pair.  In the specific
 * form stored on a file system, it stores a directory named
 * "xxx-vhost" for the service name "xxx", with underneath
 * it a single file for every vhost carrying its name and
 * containing the keymud as produced by kipkey_tomap().
 * File systems with inline data may store this in the inode!
 * Take note that vhosts may or may not be case sensitive.
 *
 * This is a replacement for the simple and implementation-
 * specific kipkey_fromkeytab() function.  It adds a layer
 * of (virtual host) indirection that can be managed with
 * tools.  It also supports key migration, by using info in
 * the keymap as a hint for the key to load, both its number
 * and algorithm.  The result is that a vhost indicates what
 * master key it relies on.  It is possible to upgrade the
 * key setup for a vhost to another master key, which can be
 * helpful during key migrations, including moving of vhost
 * definitions to another host.
 */
bool kipkey_fromvhosttab (kipt_ctx ctx,
			const char *service, const char *vhost,
			kipt_keyid *out_keyid) {
	//
	// Load the key map from the implementation origin.
	// For now that is only possible through the file system.
	char *kipvardir = getenv ("KIP_VARDIR");
	if (kipvardir == NULL) {
		kipvardir = "/var/lib/kip";
	} else if (*kipvardir != '/') {
		log_error ("Only absolute paths are permitted in KIP_VARDIR");
		errno = KIPERR_VARDIR_NOTFOUND;
		return false;
	}
	char keymapfile [PATH_MAX+3];
	ssize_t kmflen = snprintf (keymapfile, PATH_MAX+2,
				"%s/%s-vhost/%s", kipvardir, service, vhost);
	if ((kmflen > PATH_MAX) || (kmflen <= 0)) {
		log_error ("Path name to virttab too long");
		errno = KIPERR_VHOST_BADTAB;
		return false;
	}
	uint8_t km [2048+2];
	ssize_t kmsz = -1;
	int kmf = open (keymapfile, O_RDONLY);
	if (kmf < 0) {
		/* errno already set */
		log_errno ("Failed to open virtmap for service %s, vhost %s", service, vhost);
		return false;
	}
	kmsz = read (kmf, km, sizeof (km)-1);
	close (kmf);
	if ((kmsz < 10) || (kmsz > sizeof (km)-2)) {
		log_error ("Intolerable virtmap for service %s, vhost %s", service, vhost);
		errno = KIPERR_VHOST_NOTFOUND;
		return false;
	}
	//
	// Extract the desired key number and encryption algorithm.
	if ((km [0] != 0x00) || (km [1] != 0x01)) {
		log_error ("Mapping depth too high in virtmap");
		errno = KIPERR_KEYMAP_SCRAMBLED;
		return false;
	}
	//
	// When we already have the master key, we can unmap now.
	if (kipkey_frommap (ctx, km, kmsz, out_keyid)) {
		/* Success with a previously loaded master key */
		return true;
	}
	//
	// Load the master key from the implementation origin.
	// For now, loading is only possible from a keytab.
	char *opt_ktname = getenv ("KIP_KEYTAB");
	struct _mappingpair *tab_pair = (void *) &km [2];
	krb5_kvno    tab_kvno = ntohl( tab_pair->kvno    );
	krb5_enctype tab_alg  = ntohl( tab_pair->enctype );
	kipt_keyid tab_keyid;
	if (!kipkey_fromkeytab (ctx,
				opt_ktname, service, NULL, ".",
				tab_kvno, tab_alg,
				&tab_keyid)) {
		log_error ("Failed to load keytab entry for service %s, keynr %x, alg %d", service, tab_kvno, tab_alg);
		return false;
	}
	//
	// Based on the newly loaded master key, retry unmap.
	if (kipkey_frommap (ctx, km, kmsz, out_keyid)) {
		/* Success with the newly loaded master key */
		return true;
	}
	//
	// We failed, both initially and with a loaded master key.
	log_error ("Failed to decode virttab for service %s, vhost %s", service, vhost);
	/* errno already set */
	return false;
}


/* Derive key material to be used outside of KIP, without actual
 * key export.  An existing KIP key can be used in a PRF, along
 * with an input salt, to produce seemingly random output that
 * can be used outside of KIP as derived key material.  The same
 * key material can be mapped from the same salt and same key in
 * another time and place.
 *
 * This implements the PRF+ function defined in RFC 6113.
 *
 * The length of the input salt must be at least the length of
 * the usermud, but longer is possible if the entropy is not
 * as dense as a full random sequence.  Proze, for instance, is
 * only good for one 1 bit per character and passwords may not
 * be better.  Hashing and encryption produces output that may
 * _seem_ more random, but the entropy does not rise due to
 * these operations; it is just more difficult to see that.
 *
 * The salt may be public material, because the entropy derives
 * from the key and the output cannot be traced back to the key.
 * This means that the salt is not extremely precious, not like
 * a password.  Do note that if its contents could be manipulated
 * before making this call, then replay attacks might be mounted;
 * so do protect the path that delivers the salt here.
 */
bool kipdata_usermud (kipt_ctx ctx, kipt_keyid keyin,
			const uint8_t *salt, uint32_t salt_len,
			uint8_t *usermud, uint32_t usermud_len) {
	//
	// Check trivial constraints
	if (salt_len < usermud_len) {
		errno = KIPERR_NEED_MORE_SALT;
		goto fail;
	}
	//
	// Find the key that supports this hash
	kipt_key *kk = NULL;
	LL_SEARCH_SCALAR (ctx->keys_head, kk, keyid, keyin);
	if (kk == NULL) {
		errno = KIPERR_KEYID_NOT_FOUND;
		goto fail;
	}
	//
	// Prepare input/output buffers
	krb5_data indata;
	krb5_data outdata;
	indata.length = salt_len;
	indata.data = (char*)salt;
	outdata.length = usermud_len;
	outdata.data = (char*)usermud;
	//
	// Apply the PRF+ algorithm from RFC 6113
	krb5_error_code k5err;
	k5err = krb5_c_prfplus (ctx->k5ctx, &kk->k5keyblk, &indata, &outdata);
	if (k5err != 0) {
		errno = k5err;
		goto wipe_fail;
	}
	//
	// Return success
	return true;
wipe_fail:
	memset (usermud, 0, usermud_len);
fail:
	return false;
}


/* Mix keys and salt into a new key.  Each of the keys gets
 * its own salt to allow variations of mixtures that depend
 * on (possibly public) key material.  The output key is given
 * the same encryption algorithm as the first key.  To be an
 * equal contributor, the second key and salt must provide at
 * least the entropy that goes into the output key; an easy way
 * of ensuring this is using the same algorithm for both keys.
 * This is not verified however.
 *
 * The two salts must at least be as long as the output key's
 * need for random material.  The same warnings about salt and
 * compression as for kipdata_usermud() apply here.
 *
 * This implements the algorithm KRB-FX-CF2 from RFC 6113, but
 * generalised to any number of key / salt pairs.  You should
 * be careful not to cancel any of these by repeating them, as
 * the combination is made with XOR.
 */
bool kipkey_mixer (kipt_ctx ctx, uint16_t num_keys,
			const kipt_keyid *keysin, const uint8_t **salts, const uint32_t *salt_lens,
			kipt_keynr in_keynr, kipt_keyid *out_keyid) {
	//
	// Check trivial constraints
	if (num_keys < 1) {
		errno = KIPERR_KEY_REQUIRED;
		return false;
	}
	//
	// Find the first key that supports this hash
	kipt_key *kk0 = NULL;
	LL_SEARCH_SCALAR (ctx->keys_head, kk0, keyid, keysin [0]);
	if (kk0 == NULL) {
		errno = KIPERR_KEYID_NOT_FOUND;
		return false;
	}
	//
	// Prepare the new key by allocating and zeroing it
	kipt_key *tmpkey;
	krb5_data key_entropy;
	if (!_newkey (ctx, kk0->k5keyblk.enctype, in_keynr, &tmpkey, &key_entropy)) {
		return false;
	}
	//
	// Now iterate over the input keys and merge into key_entropy
	uint8_t xorkey [key_entropy.length];
	while (num_keys-- > 0) {
		memset (xorkey, 0, key_entropy.length);		/* Do not leak */
		bool goodmud;
		goodmud = kipdata_usermud (ctx, keysin [num_keys],
				salts [num_keys], salt_lens [num_keys],
				xorkey, key_entropy.length);
		int i;
		for (i=0; i < key_entropy.length; i++) {
			key_entropy.data [i] ^= xorkey [i];
		}
		memset (xorkey, 0, key_entropy.length);		/* Do not leak */
		if (!goodmud) {
			goto free_fail;
		}
	}
	//
	// Derive the key from the collected entropy
	if (!_setkey (ctx, &key_entropy, tmpkey)) {
		goto free_fail;
	}
	if (!_addkey (ctx, tmpkey)) {
		goto free_fail;
	}
	//
	// Return successfully
	*out_keyid = tmpkey->keyid;
	return true;
	//
	// Cleanup and fail
free_fail:
	zap_free (tmpkey);
fail:
	return false;
}

