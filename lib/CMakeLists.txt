add_com_err_table (kip SOURCE kip-errno EXPORT KIP)

if (WIN32)
	set(WSOCK_LIBRARIES wsock32 ws2_32)
else()
	set(WSOCK_LIBRARIES)
endif()

# Make explicit that targets depend on the building of
# ASN.1 mappings to header files for Quick-DER.  Do not
# distinguish the individual generated files but rather
# require that this phase has passed.
add_custom_target(asn1-kip-service
	DEPENDS asn1-generated)

add_custom_target(asn1-sxover
	DEPENDS asn1-generated)

library_pair(Kip
    OUTPUT_NAME kip
    SOURCES core.c
    LIBRARIES
        ${Cyrus-SASL2_LIBRARIES}
        ${MIT-krb5_LIBRARIES}
        ${Unbound_LIBRARIES}
        ${OPENSSL_CRYPTO_LIBRARY}
	com_err::kip
    PAIRED_LIBRARIES Quick-DER Quick-DERMEM Quick-SASL_cyrus
    EXPORT KIP
)
library_pair_link_directories(Kip ${MIT-krb5_LIBRARY_DIRS})
#TODO#WONTWORK# add_dependencies (kip_shared asn1-kip-service)
#TODO#WONTWORK# add_dependencies (kip_static asn1-kip-service)

library_pair(KipXover
    OUTPUT_NAME kipxover
    SOURCES core.c
    LIBRARIES
        ${Cyrus-SASL2_LIBRARIES}
        ${MIT-krb5_LIBRARIES}
        ${Unbound_LIBRARIES}
        ${OPENSSL_CRYPTO_LIBRARY}
        com_err::kip
    PAIRED_LIBRARIES Quick-DER Quick-DERMEM Quick-SASL_cyrus
    EXPORT KIP
)
library_pair_link_directories(KipXover ${MIT-krb5_LIBRARY_DIRS})
#TODO#WONTWORK# add_dependencies (kipxover_shared asn1-kip-service)
#TODO#WONTWORK# add_dependencies (kipxover_static asn1-kip-service)

library_pair(KipService
    OUTPUT_NAME kipservice
    SOURCES service.c
    LIBRARIES
        com_err::kip
    PAIRED_LIBRARIES Kip
    EXPORT KIP
)
#TODO#WONTWORK# add_dependencies (kipservice_shared asn1-kip-service)
#TODO#WONTWORK# add_dependencies (kipservice_static asn1-kip-service)

library_pair(KipDaemon
    OUTPUT_NAME kipdaemon
    SOURCES daemon.c
    PAIRED_LIBRARIES Kip identity
    EXPORT KIP
)
#TODO#WONTWORK# add_dependencies (kipdaemon_shared asn1-kip-service)
#TODO#WONTWORK# add_dependencies (kipdaemon_static asn1-kip-service)

library_pair(XoverSASL
    OUTPUT_NAME xoversasl
    SOURCES xoversasl.c
    PAIRED_LIBRARIES KipService Quick-SASL_cyrus
    EXPORT KIP
)
#TODO#WONTWORK# add_dependencies (xoversasl_shared asn1-sxover)
#TODO#WONTWORK# add_dependencies (xoversasl_static asn1-sxover)

