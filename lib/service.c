/* libkipservice -- Common routines for the Keyful Identity Protocol
 *
 * This library implements the routines defined in <arpa2/kip.h>
 * that are a client to the KIP Service.  This code freely uses
 * the libkip core functions and should be linked before it.
 * It is optional, unlike the libkip core functions.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>

#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <arpa2/socket.h>

/* Hash functions from OpenSSL:
 * https://www.openssl.org/docs/man1.1.0/man3/SHA1.html
 */
#include <openssl/sha.h>

#include <unbound.h>

#include <errno.h>
#include <com_err.h>
#include <arpa2/except.h>

#include <krb5.h>

#include <arpa2/quick-der.h>
#include <arpa2/quick-dermem.h>
#include <arpa2/quick-sasl.h>
#include <quick-der/KIP-Service.h>

#include <arpa2/kip.h>
#include "utlist.h"


#include "kip-int.h"



/* Unbound is specific to the KIP Service module.
 *
 * The service is designed to be shared, even when threads
 * use the same handle at the same time.  To set it up,
 * use kip_init() and cleanup with kip_fini() once during
 * the life cycle of the module.
 */
static struct ub_ctx *ubctx = NULL;



/* KIP Service support is concealed in the KIP library.  The calls
 * kipkey_toservice() and kipkey_fromservice() serve to connect to the
 * KIP Service and export or import keys, where a server functions
 * as an authentication oracle with key dissimination according to
 * the occurrence of the authenticated identity in an ACL.
 *
 * Simply put, the KIP Service functions as a remote key map, and
 * the calls are very similar to the function pair kipkey_tomap()
 * and kipkey_frommap().
 *
 * The most influental place to specify the KIP Service domain is
 * in the KIP_REALM environment variable.  Lacking this, the realm
 * will be requested through getdomainname().  If that also yields
 * no result, the connection will not succeed.
 */


#define HOSTNAME_BUFSIZE 256

#define CL_IN     1
#define RR_SRV   33
#define RR_A      1
#define RR_AAAA  28


typedef uint16_t net_uint16_t;
bool _dns2hostport (char *data, int datalen, char hostname [HOSTNAME_BUFSIZE], net_uint16_t *netport) {
	int hostofs = 0;
	char dataofs = 6;
	//
	// Harvest the port number as-is, that is, in network byte order
	if (datalen < 10) {
		/* Sillily small */
		return false;
	}
	* netport = * (uint16_t *) &data [4];
	//
	// Collect the labels of the host name
	while (data [dataofs] != 0) {
		unsigned int eltlen = data [dataofs];
		if (dataofs + eltlen + 1 > datalen) {
			return false;
		}
		if (hostofs + eltlen + 1 > HOSTNAME_BUFSIZE) {
			return false;
		}
		memcpy (hostname + hostofs, data + dataofs + 1, eltlen);
		hostname [hostofs + eltlen] = '.';
		hostofs += eltlen + 1;
		dataofs += eltlen + 1;
	}
	//
	// We should not return an empty string
	if (hostofs == 0) {
		return false;
	} else {
		hostname [hostofs-1] = 0;
		return true;
	}
}


static bool _derio (void *mempool, int sox,
			const derwalk *pack_req, void *data_req,
			const derwalk *pack_rep, void *data_rep,
			struct dercursor *opt_der_rep) {
	log_debug ("libkipservice enters _derio() with sox=%d", sox);
	//
	// Send the request as DER data
	if (!dermem_send (mempool, sox, pack_req, data_req)) {
		/* errno is set */
		log_errno ("!dermem_send()");
		return false;
	}
	log_debug ("libkipservice _derio() sent DER request");
	//
	// Read one block of DER data with the response
	if (!dermem_recv_der (mempool, sox, pack_rep, data_rep, opt_der_rep)) {
		/* errno is set */
		log_errno ("!dermem_recv()");
		return false;
	}
	log_debug ("libkipservice _derio() response DER unpacked, success");
	return true;
}


/* Take note of the final outcome of a SASL exchange.
 */
static void _sasl_callback (void *cbdata, QuickSASL qsasl, enum qsaslt_state newstate) {
	bool **fail_succ_ptrs = cbdata;
	assert ((newstate == QSA_SUCCESS) || (newstate == QSA_FAILURE));
	if (newstate == QSA_FAILURE) {
		*fail_succ_ptrs [0] = true;
	} else {
		*fail_succ_ptrs [1] = true;
	}
}


static bool _secure_kip_client (kipt_ctx ctx, int sox_plain, char *domain_name, char *server_name, bool anonymous, int *sox_secure) {
	log_info ("libkipservice _secure_kip_client() started on socket %d", sox_plain);
//TODO//ANONYMOUS//BYPASS//
	*sox_secure = -1;
	int sox = -1;
	//
	// Initialise the TLS, KIP and SASL handshake
	bool success = false;
	bool failure = false;
	//
	// Allocate a memory pool
	log_debug ("libkipservice allocates a memory pool around SASL");
	QuickSASL qs = NULL;
	bool *fail_succ_ptrs [2] = { &failure, &success };
	failure = failure || !qsasl_client (
			&qs, "kip",
			_sasl_callback, fail_succ_ptrs,  /* cb,cbdata to signal on success/failure */
			0);
	//
	// Setup client identity information; not fatal; having more merely enables more mechanisms
	if (!anonymous) {
		if (ctx->client_realm != NULL) {
			dercursor clrlm = { .derptr = (uint8_t*)ctx->client_realm, .derlen = strlen (ctx->client_realm) };
			qsasl_set_client_realm (qs, crs2buf (clrlm));
		}
		if (ctx->clientuser_login != NULL) {
			dercursor clid = { .derptr = (uint8_t*)ctx->clientuser_login, .derlen = strlen (ctx->clientuser_login) };
			qsasl_set_clientuser_login (qs, crs2buf (clid));
			if (ctx->clientuser_acl != NULL) {
				clid.derptr =         (uint8_t*)ctx->clientuser_acl ;
				clid.derlen = strlen (ctx->clientuser_acl);
				qsasl_set_clientuser_acl (qs, crs2buf (clid));
			}
		}
	}
	//
	// Setup a callback that will set failure or success when QuickSASL decides
	//
	// Start TLS on sox_plain
	//TODO//TLS// tlsdata... = ...; -- notably, server_name
	sox = sox_plain; //TODO//TLS// sox = starttls (sox_plain, &tlsdata);
	//
	// Initiate KIP with the KIP-INIT-REQ/REP exchange
	static uint8_t protocol [] = { 'K', 'I', 'P', ' ', 'S', 'e', 'r', 'v', 'i', 'c', 'e' };
	static uint8_t version  [] = { 0x00 };  /* "...one or more octets" */
	static uint8_t emptyseq [] = { DER_TAG_SEQUENCE, 0 };
	char *client_realm = anonymous ? "ANONYMOUS" : getenv ("KIPSERVICE_CLIENT_REALM");
	assert (client_realm != NULL);
	log_debug ("libkipservice constructs KIP-INIT-REQ and prepares for KIP-INIT-REP");
	KIP_INIT_REQ init_req;
	KIP_INIT_REP init_rep;
	static const derwalk pack_init_req [] = {
			DER_PACK_KIP_Service_KIP_INIT_REQ,
			DER_PACK_END
	};
	static const derwalk pack_init_rep [] = {
			DER_PACK_KIP_Service_KIP_INIT_REP,
			DER_PACK_END
	};
	memset (&init_req, 0, sizeof (init_req));
	init_req.protocol  .derptr =        (protocol);
	init_req.protocol  .derlen = sizeof (protocol);
	init_req.version   .derptr =        (version);
	init_req.version   .derlen = sizeof (version);
	init_req.flags.wire.derptr =        (emptyseq);
	init_req.flags.wire.derlen = sizeof (emptyseq);
	init_req.crealm    .derptr = (uint8_t*)(client_realm);
	init_req.crealm    .derlen = strlen (client_realm);
	init_req.srealm    .derptr = (uint8_t*)(domain_name);
	init_req.srealm    .derlen = strlen (domain_name);
	log_debug ("libkipservice calls _derio() on socket %d for DER-INIT-REQ/REP", sox);
	if (!_derio (qs, sox, pack_init_req, &init_req, pack_init_rep, &init_rep, NULL)) {
		errno = EPROTO;
		failure = true;
		goto cleanup;
	}
	log_debug ("libkipservice checks  received DER-INIT-REP for _derio()");
	if (der_cmp (init_req.protocol,   init_rep.protocol  ) != 0) {
		errno = EPROTO;
		failure = true;
		goto cleanup;
	}
	if (der_cmp (init_req.version,    init_rep.version   ) != 0) {
		errno = EPROTO;
		failure = true;
		goto cleanup;
	}
	if (der_cmp (init_req.flags.wire, init_rep.flags.wire) != 0) {
		errno = EPROTO;
		failure = true;
		goto cleanup;
	}
	log_debug ("libkipservice checked received DER-INIT-REP for _derio()");
	//
	// We have a server name; did we also get a client name?
	char *opt_client_name = NULL;
	//TODO//TLS// if (strchr (tlsdata.localid, '@') != NULL) {
		//TODO//TLS// opt_client_name = tlsdata.localid;
	//TODO//TLS// }
	log_debug ("libkipservice INIT-REQ/REP failure=%d success=%d at %s:%d", failure, success, __FILE__, __LINE__);
	dercursor srealm = {
		.derptr = (uint8_t*)server_name,
		.derlen = strlen (server_name)
	};
	//TODO// This reports the host  :-  Setting srealm to "kipsvc.unicorn.demo.arpa2.lab"
	log_info ("Setting srealm to \"%.*s\"", (int) srealm.derlen, srealm.derptr);
	failure = failure || !qsasl_set_server_realm (qs, crs2buf (srealm));
	log_debug ("libkipservice INIT-REQ/REP failure=%d success=%d at %s:%d", failure, success, __FILE__, __LINE__);
	dercursor clientid = {
		.derptr = anonymous ? NULL : (uint8_t*)opt_client_name,
		.derlen = (anonymous || (opt_client_name == NULL)) ? 0 : strlen (opt_client_name)
	};
	log_debug ("libkipservice INIT-REQ/REP failure=%d success=%d at %s:%d", failure, success, __FILE__, __LINE__);
	if (clientid.derptr != NULL) {
		failure = failure || !qsasl_set_clientid_external (qs, crs2buf (clientid));
	}
	log_debug ("libkipservice INIT-REQ/REP failure=%d success=%d at %s:%d", failure, success, __FILE__, __LINE__);
	//TODO//DO_BETTER_THAN:
	dercursor chanbind;
	chanbind.derptr = (uint8_t*)(QSA_X_MANUAL_SESSION_KEY ":0123456789ab");
	chanbind.derlen = strlen((char*)chanbind.derptr);
	failure = failure || !qsasl_set_chanbind (qs, false, crs2buf (chanbind));
	log_debug ("libkipservice SASL-REQ/REP failure=%d success=%d at %s:%d", failure, success, __FILE__, __LINE__);
	dercursor mech_anonymous = {
		.derptr = "ANONYMOUS",
		.derlen = 9
	};
	failure = failure || !qsasl_set_mech (qs,
				anonymous
					? crs2buf (mech_anonymous)
					: crs2buf (init_rep.saslmech));
	log_debug ("libkipservice SASL-REQ/REP failure=%d success=%d at %s:%d", failure, success, __FILE__, __LINE__);
	//
	// Setup s2c for our first loop (which initiates the exchange)
	dercursor s2c = {
		.derptr = NULL,
		.derlen = 0
	};
	bool rep_success = false;
	//
	// Loop the SASL client handshake until the result
	while (! (success || failure)) {
		/* Pack the output data */
		log_debug ("libkipservice SASL-REQ/REP failure=%d success=%d at %s:%d", failure, success, __FILE__, __LINE__);
		KIP_SASL_REQ sasl_req;
		KIP_SASL_REP sasl_rep;
		memset (&sasl_req, 0, sizeof (sasl_req));
		failure = failure || !qsasl_step_client (qs, crs2buf (s2c), crs2bufp (&sasl_req.mech), crs2bufp (&sasl_req.c2s.token));
		if (sasl_req.c2s.token.derptr == NULL) {
			/* avoid Quick DER issue #76 */
			sasl_req.c2s.no_token.derptr = (uint8_t*)"";
		}
		log_debug ("libkipservice SASL-REQ/REP failure=%d success=%d at %s:%d", failure, success, __FILE__, __LINE__);
		if (rep_success) {
			failure = failure || (sasl_req.mech.derptr != NULL);
			failure = failure || ((sasl_req.c2s.token.derptr != NULL) && (sasl_req.c2s.token.derlen != 0));
			success = success || rep_success;
			break;
		}
		/* Perform I/O with the DER information */
		static const derwalk pack_sasl_req [] = {
				DER_PACK_KIP_Service_KIP_SASL_REQ,
				DER_PACK_END
		};
		static const derwalk pack_sasl_rep [] = {
				DER_PACK_KIP_Service_KIP_SASL_REP,
				DER_PACK_END
		};
		failure = failure || !_derio (qs, sox, pack_sasl_req, &sasl_req, pack_sasl_rep, &sasl_rep, NULL);
		/* Pass information from the challenge into the next SASL step */
		s2c     = sasl_rep.s2c.token;
		der_get_bool (sasl_rep.success, &rep_success);
		log_debug ("libkipservice SASL-REQ/REP failure=%d success=%d at %s:%d", failure, success, __FILE__, __LINE__);
	}
	log_debug ("libkipservice SASL-REQ/REP failure=%d success=%d at %s:%d", failure, success, __FILE__, __LINE__);
	//
	// Cleanup
cleanup:
	log_info ("libkipservice ends KIP SASL with failure=%d, success=%d", failure, success);
	// Do we need information from SASL for later use?
	// Then harvest it here and now
	qsasl_close (&qs);
	//
	// Process failure, if any
	if (failure) {
		if (sox >= 0) {
			close (sox);
		}
		return false;
	}
	//
	// Report success
	assert (success);
	*sox_secure = sox;
	return true;
}

#ifdef _WIN32
int getdomainname(char *name, size_t len)
{
	if (len < 7) { // length of "(none)\0"
		errno = EINVAL;
		return -1;
	}
	strcpy(name, "(none)");
	return 0;
}
#endif

static bool _send_to_kip_service (kipt_ctx ctx, char *kip_service_domain, bool anonymous,
				const derwalk *syntax, const struct dercursor *in_cursors) {
	//
	// Use the anonymous setting to point to the cached uplink socket variable
	int *uplink = anonymous ? &ctx->service_uplink_anonymous : &ctx->service_uplink_login;
	//
	// See if we have a cached connection; if so, try to use it
	bool cached = true;
	if (*uplink >= 0) {
		if (kip_service_domain == NULL) {
			goto try_cached_uplink;
		} else {
			close (*uplink);
			*uplink = -1;
		}
	}
flush_cache:
	cached = false;
	//
	// Construct the SRV query name
	char srvname [HOSTNAME_BUFSIZE];
	strcpy (srvname, "_kip._tcp.");
	char *domname = srvname + 10;
	int srvnamelen = strlen (srvname);
	//
	// Find the desired KIP Service via SRV records under $KIP_REALM
	char *envrealm;
	if (kip_service_domain != NULL) {
		envrealm = kip_service_domain;
		log_info ("Using supplied KIP Service domain \"%s\"", envrealm);
	} else {
		envrealm = getenv ("KIP_REALM");
		log_info ("Retrieved from env KIP_REALM=\"%s\"", envrealm ? envrealm : "NULL");
	}
	if (envrealm != NULL) {
		if (srvnamelen + strlen (envrealm) + 1 > sizeof (srvname)) {
			errno = KIPERR_INSANE_REQUEST;
			log_errno ("srvname too long");
			goto fail;
		}
		strcpy (srvname + srvnamelen, envrealm);
	} else if (getdomainname (srvname + srvnamelen, sizeof (srvname) - 1 - srvnamelen) != 0) {
		/* errno has been set */
		log_errno ("getdomainname()");
		goto fail;
	}
	//
	// Lookup the SRV record to find one or more host/port pairs
	struct ub_result *srv = NULL;
	log_debug ("Resolving IN SRV for %s", srvname);
	if (ub_resolve (ubctx, srvname, RR_SRV, CL_IN, &srv) != 0) {
		errno = KIPERR_SERVICE_NOTFOUND;
		log_errno ("ub_resolve");
		goto fail;
	}
	assert (srv != NULL);
	if ((!srv->havedata) || (srv->bogus) || (srv->nxdomain)) {
		/* TODO: Consider if we also need srv->secure */
		errno = KIPERR_SERVICE_NOTFOUND;
		log_errno ("srv->unfit_data");
		goto srv_fail;
	}
	int iter_srv = -1;
	//
	// Find host name and port for each SRV record
	struct ub_result *aaaa = NULL;
	struct ub_result *a    = NULL;
another_srv:
	if (aaaa) {
		ub_resolve_free (aaaa);
		aaaa = NULL;
	}
	if (a) {
		ub_resolve_free (a);
		a = NULL;
	}
	iter_srv++;
	if (srv->data [iter_srv] == NULL) {
		errno = KIPERR_SERVICE_NOCONNECT;
		log_errno ("ran out of SRV RR");
		goto srv_fail;
	}
	net_uint16_t netport;
	char hostname [HOSTNAME_BUFSIZE];
	if (!_dns2hostport (srv->data [iter_srv], srv->len [iter_srv], hostname, &netport)) {
		goto another_srv;
	}
	log_debug ("Following SRV #%d to %s:%d", iter_srv, hostname, ntohs (netport));
	//
	// Find the IP addresses for the host name in the SRV record
	log_debug ("Resolving IN AAAA for %s", hostname);
	if (ub_resolve (ubctx, hostname, RR_AAAA, CL_IN, &aaaa) != 0) {
		/* This should never fail */
		log_errno ("un_resolve (AAAA) != 0");
		goto another_srv;
	}
	log_debug ("Resolving IN A for %s", hostname);
	if (ub_resolve (ubctx, hostname, RR_A,    CL_IN, &a   ) != 0) {
		/* This should never fail */
		log_errno ("un_resolve (A) != 0");
		goto another_srv;
	}
	int iter_aaaa = -1;
	int iter_a    = -1;
	log_debug ("aaaa->nxdomain=%d, aaaa->bogus=%d, aaaa->havedata=%d", aaaa->nxdomain, aaaa->bogus, aaaa->havedata);
	if ((!aaaa->nxdomain) && (!aaaa->bogus) && aaaa->havedata) {
		iter_aaaa = 0;
	}
	log_debug ("a   ->nxdomain=%d, a   ->bogus=%d, a   ->havedata=%d", a   ->nxdomain, a   ->bogus, a   ->havedata);
	if ((!a   ->nxdomain) && (!a   ->bogus) && a   ->havedata) {
		iter_a    = 0;
	}
	//
	// Construct a socket address for IPv6 _or_ IPv4
	struct sockaddr_storage sa;
	memset(&sa, 0, sizeof(sa));
	socklen_t salen;
another_aaaa_a:
	log_debug ("Iterating AAAA #%d and A #%d", iter_aaaa, iter_a);
	if ((iter_aaaa >= 0) && (aaaa->data [iter_aaaa] != NULL)) {
		/* Copy IPv6 address and port, set salen, then increment iter_aaaa */
		log_debug ("copy IPv6 address");
		         ((struct sockaddr_in6 *) &sa)->sin6_family = AF_INET6;
		memcpy (&((struct sockaddr_in6 *) &sa)->sin6_addr, aaaa->data [iter_aaaa], 16);
		         ((struct sockaddr_in6 *) &sa)->sin6_port = netport;
		salen = sizeof (struct sockaddr_in6);
		iter_aaaa++;
	} else if ((iter_a >= 0) && (a->data [iter_a] != NULL)) {
		/* Copy IPv4 address and port, set salen, then increment iter_a */
		log_debug ("copy IPv4 address");
		         ((struct sockaddr_in *) &sa)->sin_family = AF_INET;
		memcpy (&((struct sockaddr_in *) &sa)->sin_addr, a->data [iter_a], 4);
		         ((struct sockaddr_in *) &sa)->sin_port = netport;
		salen = sizeof (struct sockaddr_in);
		iter_a++;
	} else {
		/* Iterators fell through, try another host:port */
		goto another_srv;
	}
	//
	// Connect to the KIP Service over TCP
	int sox = -1;
	sox = socket (sa.ss_family, SOCK_STREAM, 0);
	if (sox < 0) {
		goto another_aaaa_a;
	}
	if (connect (sox, (struct sockaddr *) &sa, salen) != 0) {
		log_debug ("connect fails");
		close (sox);
		goto another_aaaa_a;
	}
	//
	// Upgrade the connection security with TLS and SASL
	if (!_secure_kip_client (ctx, sox, domname, hostname, anonymous, &sox)) {
		/* We might fail now, but instead let's be robust */
		/* TODO: TLS failure differs from SASL failure... */
		goto another_aaaa_a;
	}
	//
	// We are connected, and will no longer need our DNS handles
	ub_resolve_free (a);
	ub_resolve_free (aaaa);
	ub_resolve_free (srv);
	//
	// Cache our uplink to the KIP Service for later reuse attempts
	*uplink = sox;
	//
	// Send the prepared upstream data to the KIP Service
	ssize_t sent;
try_cached_uplink:
	if (!dermem_send (ctx->reqpool, *uplink, syntax, in_cursors)) {
		close (*uplink);
		*uplink = -1;
		if (cached) {
			goto flush_cache;
		} else {
			log_errno ("!cached");
			goto fail;
		}
	}
	//
	// Cleanup and report success
	return true;
	//
	// Cleanup and report failure
a_srv_fail:
	ub_resolve_free (a);
aaaa_srv_fail:
	ub_resolve_free (aaaa);
srv_fail:
	ub_resolve_free (srv);
fail:
	return false;
}


bool _acl2seqof (kipt_ctx ctx, char **acl, dercursor *out_seqof) {
	//
	// Sheer kindness: Permit NULL to denote an empty ACL
	if (acl == NULL) {
		static char *empty_acl [1] = { NULL };
		acl = empty_acl;
	}
	//
	// Allocate a buffer for the white list -> SEQUENCE OF IA5String
	dercursor acl_seq = { .derptr = NULL, .derlen = 0 };
	if (!dermem_buffer_open (ctx->reqpool, 0, &acl_seq)) {
		log_errno ("!dermem_buffer_open()");
		goto fail;
	}
	log_debug ("DEBUG: Buffer open in %p at %p size %zd", ctx->reqpool, acl_seq.derptr, acl_seq.derlen);
	//
	// Fill the white list buffer
	static const derwalk pack_user [] = {
			DER_PACK_STORE | DER_TAG_IA5STRING,
			DER_PACK_END
	};
	size_t buflen = 0;
	while (*acl != NULL) {
		dercursor this_user;
		this_user.derptr = (uint8_t*)(*acl);
		this_user.derlen = strlen (*acl);
		acl++;
		size_t pcklen = der_pack (pack_user, &this_user, NULL);
		if (pcklen == 0) {
			goto buf_fail;
		}
		buflen += pcklen;
		if (!dermem_buffer_resize (ctx->reqpool, buflen, &acl_seq)) {
			log_errno ("!dermem_buffer_resize()");
			goto buf_fail;
		}
		log_debug ("DEBUG: Buffer resize in %p at %p size %zd", ctx->reqpool, acl_seq.derptr, acl_seq.derlen);
		der_pack (pack_user, &this_user, acl_seq.derptr + buflen);
	}
	//
	// Close the white list buffer
	dermem_buffer_close (ctx->reqpool, buflen, &acl_seq);
	log_debug ("DEBUG: Buffer close in %p at %p size %zd for %zd", ctx->reqpool, acl_seq.derptr, acl_seq.derlen, buflen);
	*out_seqof = acl_seq;
	//
	// Return success
	return true;
	//
	// Return failure
buf_fail:
	dermem_buffer_delete (ctx->reqpool, &acl_seq);
	log_debug ("DEBUG: Buffer delete");
fail:
	out_seqof->derptr = NULL;
	out_seqof->derlen = 0;
	log_debug ("_acl2seqof fails");
	return false;
}



/* Use the KIP Service to map a key into service key mud.
 * The result is opaque, and requires the use of the same
 * KIP Service, identified in the opaque data, to extract
 * the original key.  You may still want to do additional
 * key mapping with kipkey_tomap() locally for better
 * flexibility in key management.
 *
 * You can define a series of identities on a white list
 * and a black list.  The most concrete match with entries
 * on these lists determines whether an attempt by others
 * succeeds when they wish to extract key material from
 * the key mud.
 *
 * You should split the key mud per domain.  The reason is
 * that we need the ACL to be processed by the KIP Service
 * appointed by the recipient's domain, if one is present.
 * This KIP Service has the prerogative to decide about
 * user identities, including potentially creative aliases
 * for them.  Also, you would be expecting your audience
 * to login to a remote KIP Service and authenticate there,
 * which is unreasonable when they run their own.  When
 * they don't, things change and you could choose any
 * domain, where the sender's domain is the first fallback
 * and "general service" domains are not likely to be
 * trusted by all recipients.  You will get a false result
 * and errno set to KIPERR_SERVICE_NOTFOUND when a domain
 * has no KIP Service configured.
 *
 * The request looks like this:
 *
 * KIP-ENCR-REQ ::= [APPLICATION 10] SEQUENCE OF KIP-ACL-Group
 *
 * KIP-ACL-Group ::= SEQUENCE {
 *    acl    [0] KIP-AccessControl,
 *    group  [1] KIP-Group,
 *    from   [2] TimeStamp OPTIONAL,
 *    till   [3] TimeStamp OPTIONAL
 * }
 *
 * KIP-AccessControl ::= CHOICE {
 *    inline-acl  [0] KIP-AccessControl-Inline,
 *    localref    [1] OCTET STRING,
 *    ...
 * }
 *
 * KIP-AccessControl-Inline ::= SEQUENCE {
 *    blacklist  [0] SEQUENCE OF ARPA2Selector,
 *                   -- may be empty
 *    whitelist  [1] SEQUENCE OF ARPA2Selector
 *                   -- may be empty
 * }
 *
 * KIP-Group ::= SEQUENCE {
 *    keyno    [0] KeyNumber,
 *    encalg   [1] EncryptAlg,
 *    entropy  [2] OCTET STRING
 * }
 *
 * The response packs this information into opaque data
 * under KIP Service encryption to hold it together:
 *
 * KIP-ENCR-REP ::= [APPLICATION 11] KIP-Opaque-Data
 *
 * KIP-Opaque-Data ::= SEQUENCE {
 *    realm    [0] IA5String,
 *    keyno    [1] KeyNumber,
 *    encalg   [2] EncryptAlg,
 *    payload  [3] OCTET STRING
 * }
 *
 * TODO: This API is unsatisfactory.  It is complex,
 *       and does not even allow us to send a list
 *       of KIP-ACL-Group.  We may need  more of a
 *       "builder" structure and more functions.
 *       Expect incompatible API and ABI changes...
 */
bool kipservice_tomap (kipt_ctx ctx, char *domain,
			kipt_keyid keytohide,
			char **blacklist_users,  /* NULL terminated */
			char **whitelist_users,  /* NULL terminated */
			time_t from_or_0,
			time_t till_or_0,
			uint8_t **out_svckeymud,          /* outbuf */
			uint32_t *out_svckeymudlen) {     /* buflen */
	log_info ("kipservice_tomap() starts");
	//
	// This is a request, so we recycle the request memory pool
	dermem_recycle (~0, ctx->reqpool);
	//
	// Find the key to hide
	kipt_key *kk = NULL;
	LL_SEARCH_SCALAR (ctx->keys_head, kk, keyid, keytohide);
	if (kk == NULL) {
		errno = KIPERR_KEYID_NOT_FOUND;
		log_errno ("kk == NULL");
		goto fail;
	}
	//
	// Determine the key's entropy for random-to-key
	dercursor entropy;
	entropy.derptr = (uint8_t *) (&kk [1]),
	entropy.derlen = ((intptr_t) kk->k5keyblk.contents) -
	                 ((intptr_t) entropy.derptr);
	log_debug ("kipservice_tomap() found %d bytes of entropy", (int)entropy.derlen);
	//
	// Store the white and black list in SEQUENCE OF buffers
	dercursor bkl_seq = { .derptr = NULL, .derlen = 0 };
	dercursor whl_seq = { .derptr = NULL, .derlen = 0 };
	if (!_acl2seqof (ctx, blacklist_users, &bkl_seq)) {
		goto fail;
	}
	if (!_acl2seqof (ctx, whitelist_users, &whl_seq)) {
		goto fail;
	}
	log_debug ("kipservice_tomap() produced black list in %ld bytes, white list in %ld bytes", bkl_seq.derlen, whl_seq.derlen);
	//
	// Prepare the "from" and "till" attributes
	dercursor from = { .derptr = NULL, .derlen = 0 };
	dercursor till = { .derptr = NULL, .derlen = 0 };
	//TODO// Construct KerberosTime if != 0
	//
	// Encode keyno and encalg in small DER buffers
	der_buf_uint32_t buf_keyno;
	dercursor keyno  = der_put_uint32 (buf_keyno,  (uint32_t) kk->keyid           );
	der_buf_int32_t buf_encalg;
	dercursor encalg = der_put_int32  (buf_encalg, (int32_t)  kk->k5keyblk.enctype);
	//
	// Now fill one KIP-ACL-Group
	KIP_ACL_Group kag;
	memset (&kag, 0, sizeof (kag));
	kag.from = from;
	kag.till = till;
	kag.group.keyno   = keyno  ;
	kag.group.encalg  = encalg ;
	kag.group.entropy = entropy;
	kag.acl.inline_acl.blacklist.wire = bkl_seq;
	kag.acl.inline_acl.whitelist.wire = whl_seq;
	//
	// Encode this one KIP-ACL-Group as a simple KIP-ENCR-REQ
	static const derwalk pack_simple_encr_req [] = {
			DER_PACK_ENTER | DER_TAG_APPLICATION(10),
			DER_PACK_ENTER | DER_TAG_SEQUENCE,
			DER_PACK_KIP_Service_KIP_ACL_Group,
			DER_PACK_LEAVE,
			DER_PACK_LEAVE,
			DER_PACK_END
	};
	log_info ("Sending   to   KIP Service: KIP-ENCR-REQ");
	if (!_send_to_kip_service (ctx, domain, true,
				pack_simple_encr_req,
				(const dercursor *) &kag)) {
		log_errno ("_send_to_kip_service()");
		goto fail;
	}
	log_info ("Sent      to   KIP Service: KIP-ENCR-REQ");
	//
	// Read the KIP-ENCR-REP from the KIP Service
	KIP_ENCR_REP rep;
	dercursor der_rep = { .derptr = NULL, .derlen = 0 };
	static const derwalk pack_encr_rep [] = {
			DER_PACK_KIP_Service_KIP_ENCR_REP,
			DER_PACK_END
	};
	log_info ("Receiving from Anonymous KIP Service: KIP-ENCR-REP on socket %d", ctx->service_uplink_anonymous);
	if (!dermem_recv_der (ctx->cnxpool, ctx->service_uplink_anonymous, pack_encr_rep, (dercursor *) &rep, &der_rep)) {
		/* errno is set */
		log_errno ("!dermem_recv_der()");
		goto fail;
	}
	log_debug ("Received %zd bytes starting with 0x%02x", der_rep.derlen, *der_rep.derptr);
	//
	// Harvest output from the (already parsed) KIP-ENCR-REP
	if (der_enter (&der_rep) != 0) {
		log_errno ("der_enter()");
		goto fail;
	}
	if ((der_rep.derlen >> 32) != 0) {
		errno = ERANGE;
		log_errno ("(der_rep.derlen >> 32) != 0");
		goto fail;
	}
	//
	// We received to the connection pool so we can return it
	log_debug ("Returning %zd bytes starting with 0x%02x", der_rep.derlen, *der_rep.derptr);
	*out_svckeymud    = der_rep.derptr;
	*out_svckeymudlen = der_rep.derlen;
	//
	// Return success
	return true;
	//
	// Return failure
fail:
	log_errno ("kipservice_tomap() fails");
	dermem_recycle (~0, ctx->reqpool);
	*out_svckeymud    = NULL;
	*out_svckeymudlen = 0;
	return false;
}


/* Find the realm mentioned in the KIP-Opaque-Data value
 * of either service key mud between kipservice_tomap()
 * and kipservice_frommap(), or between kipservice_sign()
 * and kipservice_verify() calls.
 *
 * When realms run their own KIP Service, it is common
 * to desire encryption to be made against that node,
 * but KIP itself can crossover if so desired.  Before
 * calling kipservice_frommap() you will want to test
 * whether the user agrees with crossover.
 *
 * Provide the service mud in svcmud; in case of success,
 * domain will be filled with the realm indicated in it.
 */
bool kipservice_realm (dercursor svcmud, dercursor *realm) {
	derwalk svcmud2realm [] = {
			DER_WALK_ENTER | DER_TAG_SEQUENCE,
			DER_WALK_ENTER | DER_TAG_CONTEXT(0),
			DER_WALK_ENTER | DER_TAG_IA5STRING,
			DER_WALK_END
	};
	dercursor crs = svcmud;
	int walkder = der_walk (&crs, svcmud2realm);
	if (walkder != 0) {
		if (walkder > 0) {
			errno = EINVAL;
		}
		realm->derptr = NULL;
		realm->derlen = 0;
		return false;
	}
	*realm = crs;
	return true;
}


/* Set the Client Realm.  This is useful when addressing a
 * Remote KIP Service, to indicate it is crossing over, but
 * also when addressing one local to the realm, because it
 * still needs to know that the client is realm-local (and,
 * in support of multiple realms, for which realm it is
 * requesting access).
 *
 * The pointer may be NULL.  The string is internally
 * copied, and reset during stop or restart of the
 * KIP Service.
 */
bool kipservice_set_client_realm (kipt_ctx ctx, char *crealm) {
	log_info ("DEBUG: kipservice_set_client_realm (\"%s\")", crealm);
	dercursor newval = {
		.derptr = (uint8_t*)crealm,
		.derlen = (crealm != NULL) ? strlen (crealm) : 0,
	};
	ctx->client_realm = NULL;
	dermem_strdup (ctx->cnxpool, newval, &ctx->client_realm);
	return true;
}


/* Set the Client username for authentication (the "login user").
 * This is used for SASL authentication, so any credentials
 * provided must match this.
 *
 * The pointer may be NULL.  The string is internally
 * copied, and reset during stop or restart of the
 * KIP Service.
 */
bool kipservice_set_clientuser_login (kipt_ctx ctx, char *user_login) {
	log_info ("DEBUG: kipservice_set_clientuser_login (\"%s\")", user_login);
	dercursor newval = {
		.derptr = (uint8_t*)user_login,
		.derlen = (user_login != NULL) ? strlen (user_login) : 0,
	};
	ctx->clientuser_login = NULL;
	dermem_strdup (ctx->cnxpool, newval, &ctx->clientuser_login);
	return true;
}


/* Set the Client username to act for (the "ACL user").
 * This may be useful when addressing a resource that is
 * made accessible to another user, such as a group.
 *
 * The KIP Service evaluates the transition from the
 * authenticated user to the authorisation user.  When
 * it relies on a backend through SASL Realm Crossover,
 * this is delegated too and the authorisation user is
 * reported as the effective user identity.
 *
 * The value may well depend on the KIP Service realm
 * that is being addressed; for example, in support of
 * aliases or pseudonyms that vary between realms.
 *
 * The pointer may be NULL.  The string is internally
 * copied, and reset during stop or restart of the
 * KIP Service.
 */
bool kipservice_set_clientuser_acl (kipt_ctx ctx, char *user_acl) {
	log_info ("DEBUG: kipservice_set_clientuser_acl (\"%s\")", user_acl);
	dercursor newval = {
		.derptr = (uint8_t*)user_acl,
		.derlen = (user_acl != NULL) ? strlen (user_acl) : 0,
	};
	ctx->clientuser_acl = NULL;
	dermem_strdup (ctx->cnxpool, newval, &ctx->clientuser_acl);
	return true;
}


/* Use a KIP Service to retrieve a key from the "service
 * key mud" output by a previous call to the function
 * kipservice_tomap().
 *
 * This could crossover to another domain, which may not
 * be accepable to the user, especially if they run their
 * own KIP Service.  See kipservice_realm() for more.
 *
 * The input data is a KIP-Opaque-Data block, and the
 * output is a newly added key, as with kipkey_frommap().
 * Also comparably, the new key is given a unique keyid
 * that has at least the key number part in the lower
 * 32 bits matching the originally mapped key number.
 *
 * Very often, kipservice_frommap() bootstraps a keying
 * scheme and kipkey_frommap() may add its more complex
 * functions for AND/OR logic on key access.  This level
 * of complexity is not needed in kipservice_frommap().
 *
 * The message sent takes the following form, with the
 * KIP-Opaque-Data literally in the service key mud,
 *
 * KIP-DECR-REQ ::= [APPLICATION 12] KIP-Opaque-Data
 *
 * KIP-Opaque-Data ::= SEQUENCE {
 *    realm    [0] IA5String,
 *    keyno    [1] KeyNumber,
 *    encalg   [2] EncryptAlg,
 *    payload  [3] OCTET STRING
 * }
 *
 * The output is a service-filtered form of the data,
 * based on ACL and possibly constrained to a certain
 * period of validity.  This is not reported, but the
 * list of accepted KIP-Group values is,
 *
 * KIP-DECR-REP ::= [APPLICATION 13] SEQUENCE OF KIP-Group
 *
 * KIP-Group ::= SEQUENCE {
 *    keyno    [0] KeyNumber,
 *    encalg   [1] EncryptAlg,
 *    entropy  [2] OCTET STRING
 * }
 *
 * It is possible that an empty sequence is delivered;
 * note that this is considered a valid result from this
 * function, as you generally have no idea how many keys
 * are there anyway.  If you want to enforce a number of
 * keys you can inspect the length of the returned array.
 */
bool kipservice_frommap (kipt_ctx ctx,
			uint8_t *svckeymud, uint32_t svckeymudlen,
			kipt_keyid **out_mappedkeys,
			uint32_t *out_mappedkeyslen) {
	//
	// Repack the mud into a dercursor structure
	dercursor mud = { .derptr = svckeymud, .derlen = svckeymudlen };
	//
	// Have a fresh memory pool to handle this request
	dermem_recycle (~0, ctx->reqpool);
	//
	// Fetch the KIP Service realm from the opaque data
	static const derwalk pack_opaque [] = {
			DER_PACK_KIP_Service_KIP_Opaque_Data,
			DER_PACK_END
	};
	dercursor mudcrs = { .derptr = svckeymud, .derlen = svckeymudlen };
	KIP_Opaque_Data req;
	if (der_unpack (&mudcrs, pack_opaque, (dercursor *) &req, 1) != 0) {
		log_errno ("der_unpack() != 0");
		goto fail;
	}
	char *service_realm = NULL;
	if (!dermem_strdup (ctx->reqpool, req.realm, &service_realm)) {
		log_errno ("!dermem_strdup()");
		goto fail;
	}
	log_debug ("opaque_data.realm \"%.*s\" strdup()d to \"%s\"", (int)req.realm.derlen, req.realm.derptr, service_realm);
	//
	// Send the mud to the signature-originating KIP Service
	derwalk pack_decr_req [] = {
			DER_PACK_KIP_Service_KIP_DECR_REQ,
			DER_PACK_END
	};
	if (!_send_to_kip_service (ctx, service_realm, false, pack_decr_req, (dercursor *) &req)) {
		log_errno ("_send_to_kip_service()");
		goto fail;
	}
	//
	// Receive the KIP-DECR-REP from the KIP Service
	KIP_DECR_REP rep;
	static const derwalk pack_decr_rep [] = {
			DER_PACK_KIP_Service_KIP_DECR_REP,
			DER_PACK_END
	};
	if (!dermem_recv (ctx->cnxpool, ctx->service_uplink_login, pack_decr_rep, (dercursor *) &rep)) {
		/* errno is set */
		log_errno ("!dermem_recv_der()");
		goto fail;
	}
	//
	// Allocate response array and restart counting
	uint32_t keyarraylen = der_countelements (&rep.wire);
	kipt_keyid *keyarray = NULL;
	if (!dermem_alloc (ctx->cnxpool, keyarraylen * sizeof (kipt_keyid), (void **) &keyarray)) {
		goto fail;
	}
	log_debug ("Looping over %d entries in %zd bytes: 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x...", keyarraylen, rep.wire.derlen, rep.wire.derptr[0], rep.wire.derptr[1], rep.wire.derptr[2], rep.wire.derptr[3], rep.wire.derptr[4]);
	keyarraylen = 0;
	//
	// Loop over entries, and silently skip any problematic entries
	dercursor iter;
	if (der_iterate_first (&rep.wire, &iter)) do {
		log_debug ("Loop addresses KIP-Group in %zd bytes", iter.derlen);
		dercursor elem = iter;
		der_focus (&elem);
		//
		// Unpack the SEQUENCE OF element into a KIP-Group
		derwalk pack_kip_group [] = {
				DER_PACK_KIP_Service_KIP_Group,
				DER_PACK_END
		};
		KIP_Group group;
		if (der_unpack (&elem, pack_kip_group, (dercursor *) &group, 1) != 0) {
			log_errno ("der_unpack() on KIP-Group");
			continue;
		}
		//
		// Verify algorithm and keyno of the local key
		uint32_t decr_keyno;
		if ((der_get_uint32 (group.keyno, &decr_keyno) != 0) || (decr_keyno == 0)) {
			log_errno ("der_get_uint32() != 0 or keyno == 0");
			continue;
		}
		int32_t decr_encalg;
		if (der_get_int32 (group.encalg, &decr_encalg) != 0) {
			log_errno ("der_get_int32() != 0");
			continue;
		}
		//
		// Construct a new kipt_key from this KIP-Group
		kipt_key *key = NULL;
		krb5_data key_entropy;
		if (!_newkey (ctx, decr_encalg, decr_keyno, &key, &key_entropy)) {
			log_errno ("_newkey()");
			key = NULL;
			continue;
		}
		//
		// Check entropy size -> or cleanup/continue
		if (group.entropy.derlen != key_entropy.length) {
			log_errno ("entropy.derlen != key_entropy.length");
			key = NULL;
			continue;
		}
		//
		// Setup entropy in the key
		memcpy (key_entropy.data, group.entropy.derptr, key_entropy.length);
		if (!_setkey (ctx, &key_entropy, key)) {
			key = NULL;
			continue;
		}
		//
		// Enlist the newly constructed key
		if (!_addkey (ctx, key)) {
			log_errno ("_addkey()");
			//TODO//CLEANUP//
			key = NULL;
			continue;
		}
		//
		// Insert the key into the array
		if (key != NULL) {
			keyarray [keyarraylen++] = key->keyid;
		}
	//
	// Continue looping over the KIP Service output
	} while (der_iterate_next (&iter));
	//
	// Return keys and report success (even for 0 keys)
	*out_mappedkeys    = keyarray;
	*out_mappedkeyslen = keyarraylen;
	return true;
	//
	// Report failure
fail:
	dermem_recycle (~0, ctx->reqpool);
	return false;
}



/* Use the KIP Service to sign a message.  This constructs
 * a hash with your identity, in a manner that anyone can
 * validate at your KIP Service.  The message is publicly
 * visible; you may want to use an additional pass through
 * kipdata_up() if you want to constrain public viewing of
 * your identity to those who can kipdata_down() to your
 * retrievable identity.
 *
 * In its most general form, multiple checksums can be signed
 * at once.  You provide an array, and the single-element
 * form is as simple in C as a pointer to the one element.
 * It is not an error to provide an array of zero length.
 *
 * The checksums are expanded by this routine; you simply
 * point to an array of ones to include and they will be
 * included, in the order given.  It is up to applications
 * to know what checksum goes where.  Note that checksums
 * can only be combined when they use the same key.  We
 * let you specify the key so we may loosen this behaviour
 * in the future (without API change).
 *
 * The network protocol sends, with empty author field:
 *
 *	KIP-SIGN-REQ ::= [APPLICATION 5] KIP-Signature
 *
 *	KIP-Signature ::= SEQUENCE {
 *		author  [0] ARPA2Identity,
 *		keyno   [1] KeyNumber,
 *		encalg  [2] EncryptAlg,
 *		salt    [3] OCTET STRING,
 *		hashes  [4] OCTET STRING,
 *		meta    [5] OCTET STRING OPTIONAL
 *	}
 *
 * Where the concatenated hashes are passed through the
 * signing key, and into the keyed-hash field.  The reply
 * from the KIP Service is:
 *
 *	KIP-SIGN-REP ::= [APPLICATION 6] KIP-Opaque-Data
 *
 *	KIP-Opaque-Data ::= SEQUENCE {
 *		realm    [0] IA5String,
 *		keyno    [1] KeyNumber,
 *		encalg   [2] EncryptAlg,
 *		payload  [3] OCTET STRING
 *	}
 *
 * The metadata is included as-is.  It is not processed
 * by the KIP Service, but cryptographically bound to
 * the identity of the signer, to assure it has been
 * originated there.  A good use of this field would be
 * a signature timestamp, but applications can use this
 * as they please.  Empty data is signified by a zero
 * value for metalen, while absent data is signified
 * with a NULL value for opt_meta.
 *
 * The output from this routine is the KIP-Opaque-Data,
 * stored in the mud and mudlen.
 *
 * Local copies of the checksums are hashed after adding
 * a salt at the end, intended to turn the KIP Service
 * signature into a (symmetric) blind signature.  The
 * salt is incorporated into the service signature mud,
 * so that kipservice_verify() can quietly incorporate
 * it in the same manner.
 *
 * Since this involves network traffic, memory allocation
 * is dynamic.  TODO: memory pool.
 */
bool kipservice_sign (kipt_ctx ctx,
		kipt_keyid keyid,
		uint16_t sumslen, kipt_sumid *sums,
		uint32_t metalen, uint8_t *opt_meta,
		uint8_t **out_mud, uint32_t *out_mudlen) {
	uint8_t *allhashes = NULL;
	//
	// This is a new request, so we recycle the request pool
	dermem_recycle (~0, ctx->reqpool);
#if 0
//TODO// DEALT WITH BELOW, AFAIK
	//
	// Be sure that an uplink to the service exists
	if (ctx->service_uplink_login < 0) {
		//TODO// Reconnect or... just connect
		errno = KIPERR_SERVICE_NOCONNECT;
		log_errno ("ctx->service_uplink_login < 0");
		goto fail;
	}
#endif
	//
	// Find the key to use
	kipt_key *kk = NULL;
	LL_SEARCH_SCALAR (ctx->keys_head, kk, keyid, keyid);
	if (kk == NULL) {
		errno = KIPERR_KEYID_NOT_FOUND;
		log_errno ("kk == NULL");
		goto fail;
	}
	//
	// Determine the size of the key's hash
	uint16_t hashsz = _hash_size (&kk->keysum);
	if (hashsz == 0) {
		errno = KIPERR_CHECKSUM_ALGORITHM;
		log_errno ("hashsz == 0");
		goto fail;
	}
	//
	// Determine the size of all hashes and the signed form
	uint32_t allhashsz = hashsz * sumslen;
	log_debug ("kipservice_sign() operates on %d checksums, hash size %d, total %zd", sumslen, hashsz, (size_t)allhashsz);
	//
	// Allocate memory for the hashes
	if (!dermem_alloc (ctx->reqpool, allhashsz, (void **) &allhashes)) {
		log_errno ("allhashes == NULL");
		goto fail;
	}
	//
	// Generate a random salt of length hashsz
	dercursor salt = { .derptr = NULL, .derlen = hashsz };
	if (!dermem_alloc (ctx->reqpool, hashsz, (void **) &salt.derptr)) {
		log_errno ("!dermem_alloc()");
		goto zap_fail;
	}
	krb5_data k5salt;
	k5salt.data   = (char*)salt.derptr;
	k5salt.length = salt.derlen;
	krb5_error_code k5err;
	k5err = krb5_c_random_make_octets (ctx->k5ctx, &k5salt);
	if (k5err != 0) {
		errno = k5err;
		log_errno ("krb5_c_random_make_octets()");
		goto zap_fail;
	}
	//
	// Iterate over checksums and insert the output
	uint8_t *iterhash = allhashes;
	uint16_t i;
	for (i=0; i < sumslen; i++) {
		//
		// Find the checksum object
		kipt_sum *cs = NULL;
		LL_SEARCH_SCALAR (ctx->sums_head, cs, sumid, sums [i]);
		if (cs == NULL) {
			errno = KIPERR_SUMID_NOT_FOUND;
			log_errno ("cs == NULL");
			goto zap_fail;
		}
		//
		// Verify that the checksum's keyid and keyalg match the key
		if ((cs->keyid != kk->keyid) && (cs->keyalg != kk->k5keyblk.enctype)) {
			errno = KIPERR_CHECKSUM_INVALID;
			log_errno ("keyid/keyalg mismatch: kk,cs");
			goto zap_fail;
		}
		//
		// Harvest the hash output and move on
		assert (_hash_final (cs, salt.derptr, iterhash));
		iterhash += hashsz;
	}
	//
	// Construct the KIP-SIGN-REQ, preparing to send to the KIP Service
	KIP_SIGN_REQ req;
	memset (&req, 0, sizeof (req));
	req.author.derptr = (uint8_t*)"";
	der_buf_uint32_t buf_keyno;
	req.keyno  = der_put_uint32 (buf_keyno,  (uint32_t) kk->keyid           );
	der_buf_int32_t buf_encalg;
	req.encalg = der_put_int32  (buf_encalg, (int32_t)  kk->k5keyblk.enctype);
	req.salt = salt;
	req.hashes.derptr = allhashes;
	req.hashes.derlen = allhashsz;
	req.meta.derptr = opt_meta;
	req.meta.derlen = (opt_meta != NULL) ? metalen : 0;
	//
	// Send the KIP-SIGN-REQ to the KIP Service
	// (connect first, if necessary)
	static const derwalk pack_sign_req [] = {
			DER_PACK_KIP_Service_KIP_SIGN_REQ,
			DER_PACK_END
	};
	if (!_send_to_kip_service (ctx, NULL /* TODO_kip_service_domain */, false, pack_sign_req, (const dercursor *) &req)) {
		log_errno ("_send_to_kip_service()");
		goto pool_zap_fail;
	}
	//
	// Read the KIP-SIGN-REP from the KIP Service
	KIP_SIGN_REP rep;
	dercursor der_rep = { .derptr = NULL, .derlen = 0 };
	static const derwalk pack_sign_rep [] = {
			DER_PACK_KIP_Service_KIP_SIGN_REP,
			DER_PACK_END
	};
	if (!dermem_recv_der (ctx->cnxpool, ctx->service_uplink_login, pack_sign_rep, (dercursor *) &rep, &der_rep)) {
		/* errno is set */
		log_errno ("!dermem_recv_der()");
		goto zap_fail;
	}
	log_debug ("Received %zd bytes starting with 0x%02x", der_rep.derlen, *der_rep.derptr);
	//
	// Harvest output from the (already parsed) KIP-SIGN-REP
	if (der_enter (&der_rep) != 0) {
		log_errno ("der_enter()");
		goto zap_fail;
	}
	if ((der_rep.derlen >> 32) != 0) {
		errno = ERANGE;
		log_errno ("(der_rep.derlen >> 32) != 0");
		goto zap_fail;
	}
	//
	// We received into cnxpool so we can output this data now
	*out_mud    = der_rep.derptr;
	*out_mudlen = der_rep.derlen;
	//
	// Clear the hashes for privacy (we could have used a function-local memory pool)
	memset (allhashes, 0, allhashsz);
	//
	// Return success
	log_debug ("Returning %zd bytes starting with 0x%02x", (size_t)(*out_mudlen), **out_mud);
	return true;
	//
	// Return failure
pool_zap_fail:
zap_fail:
fail:
	log_errno ("kipservice_sign() fails");
	*out_mud    = NULL;
	*out_mudlen = 0;
	dermem_recycle (~0, ctx->reqpool);
	return false;
}


/* Use the KIP Service to verify a message signed before by
 * that same KIP Service.  The actual signature verification
 * is done locally, but the signature passes through the
 * KIP Service to find the attached signer's identity.  As
 * the signer logged in before using kipservice_sign(),
 * their identity is known in relation to the signature.
 *
 * In the simplest form, you supply the routine with the
 * KIP-Opaque-Data and a (reference to a) single checksum
 * to compare under a given key.  If the checksum differs,
 * we return false and errno is KIPERR_CHECKSUM_INVALID.
 * Only if it all works out, would we return true.
 *
 * In its most general form, multiple checksums can be
 * verified.  You provide an array, and the single-element
 * form is as simple in C as a pointer to the one element.
 * It is not an error to provide an array of zero length.
 *
 * This routine iterates over your checksum array and can
 * invoke a callback on the success or failure while
 * checking it, plus the number of server-supplied sums
 * that it had to skip to get there.  The returned value
 * from the checksum is collated into an overall result.
 * If you provide no callback, the default action is to
 * only accept one-by-one matching of everything.  The
 * callback option exists to give you more control.  We
 * will set KIPERR_CHECKSUM_INVALID if a callback or the
 * default cannot verify a checksum, but your callbacks
 * will continue to be invoked until the bitter end, so
 * you can process in whatever way seems fit.  Verfiers
 * may remain after all checksums have had a callback;
 * in that case, an extra callback is made with both
 * sum_pos==0 and vfy_pos==0 but with vfy_skip>0 set to
 * the number of extraneous signatures.  Note that this
 * would not normally combine with vfy_pos==0.  For this
 * callback, the signature will be set to accepted, as
 * is the case with other skips, though your callback
 * may disagree in any way it wants -- our default
 * handler also never accepts skipping verifiers.
 *
 * The checksums are expanded by this routine; you simply
 * point to an array of ones to include and they will be
 * included, in the order given.  It is up to applications
 * to know what checksum goes where.  Note that checksums
 * can only be combined when they use the same key.  We
 * let you specify the key so we may loosen this behaviour
 * in the future (without API change).
 *
 * The network protocol sends, with empty author field:
 *
 *	KIP-VRFY-REQ ::= [APPLICATION 7] KIP-Opaque-Data
 *
 * Where the contained KIP-Opaque-Data should have come
 * from a KIP-SIGN-REP, which you supply to this function.
 * We retrieve the realm from it, contact the KIP Service
 * underneath and send it this request.  Your user may
 * have a fit about this, so ask them about contacting
 * the realm's KIP Service before calling this function,
 * as it may have privacy implications.
 *
 * The response is verify similar to the KIP-SIGN-REQ:
 *
 *	KIP-VRFY-REP ::= [APPLICATION 8] KIP-Signature
 *
 *	KIP-Signature ::= SEQUENCE {
 *		author  [0] ARPA2Identity,
 *		keyno   [1] KeyNumber,
 *		encalg  [2] EncryptAlg,
 *		salt    [3] OCTET STRING,
 *		hashes  [4] OCTET STRING,
 *		meta    [5] OCTET STRING OPTIONAL
 *	}
 *
 * The author field can be trusted as authenticated by
 * the KIP Service at the time of the signature.
 *
 * The keyno and encalg are as they were supplied back
 * then, and if you have most of the corresponding
 * context we should be able to find it and decrypt
 * the keyed-hash with it.  Do realise that we need it,
 * or we shall have to return an error.
 *
 * The mud includes a salt to turn the KIP Service into
 * a blind signing mechanism: it cannot see what data
 * it is signing.  The salt is appended to local copies
 * of the checksums so they compute to the same outcome
 * as during kipservice_sign().
 *
 * The metadata is returned as-is.  It is not processed
 * by the KIP Service, but cryptographically bound to
 * the identity of the signer, to assure it has been
 * originated there.  A good use of this field would be
 * a signature timestamp, but applications can use this
 * as they please.  Empty data is signified by a zero
 * value in *out_metalen, while absent data is signified
 * with a NULL value in *out_opt_meta.
 *
 * Since this involves network traffic, memory allocation
 * is dynamic.  TODO: memory pool.
 */
bool kipservice_verify (kipt_ctx ctx,
		kipt_keyid keyid,
		uint16_t sumslen, kipt_sumid *sums,
		uint8_t *mud, uint32_t mudlen,
		uint32_t *out_metalen, uint8_t **out_opt_meta) {
	log_info ("kipservice_verify (keyid=%lu, sumslen=%d, mudlen=%d)", keyid, sumslen, mudlen);
	//
	// This is a request, so we can clear any data from a prior request
	dermem_recycle (~0, ctx->reqpool);
	//
	// Initially signal that there is no metadata output
	*out_opt_meta = NULL;
	*out_metalen  = 0;
	//
	// Find the key to use
	kipt_key *kk = NULL;
	LL_SEARCH_SCALAR (ctx->keys_head, kk, keyid, keyid);
	if (kk == NULL) {
		errno = KIPERR_KEYID_NOT_FOUND;
		log_errno ("kk == NULL");
		goto fail;
	}
	//
	// Determine the size of the key's hash
	uint16_t hashsz = _hash_size (&kk->keysum);
	if (hashsz == 0) {
		errno = KIPERR_CHECKSUM_ALGORITHM;
		log_errno ("hashsz == 0");
		goto fail;
	}
	//
	// Fetch the KIP Service realm from the opaque data
	static const derwalk pack_opaque [] = {
			DER_PACK_KIP_Service_KIP_Opaque_Data,
			DER_PACK_END
	};
	dercursor mudcrs = { .derptr = mud, .derlen = mudlen };
	KIP_Opaque_Data req;
	if (der_unpack (&mudcrs, pack_opaque, (dercursor *) &req, 1) != 0) {
		log_errno ("der_unpack() != 0");
		goto fail;
	}
	char *service_realm = NULL;
	if (!dermem_strdup (ctx->reqpool, req.realm, &service_realm)) {
		log_errno ("!dermem_strdup()");
		goto fail;
	}
	//
	// Send the mud to the signature-originating KIP Service
	derwalk pack_vrfy_req [] = {
			DER_PACK_KIP_Service_KIP_VRFY_REQ,
			DER_PACK_END
	};
	if (!_send_to_kip_service (ctx, service_realm, true, pack_vrfy_req, (dercursor *) &req)) {
		log_errno ("_send_to_kip_service()");
		goto fail;
	}
	//
	// Receive the KIP-VRFY-REP from the KIP Service
	KIP_VRFY_REP rep;
	static const derwalk pack_vrfy_rep [] = {
			DER_PACK_KIP_Service_KIP_VRFY_REP,
			DER_PACK_END
	};
	if (!dermem_recv (ctx->reqpool, ctx->service_uplink_anonymous, pack_vrfy_rep, (dercursor *) &rep)) {
		/* errno is set */
		log_errno ("!dermem_recv_der()");
		goto fail;
	}
	//
	// Verify algorithm and keyno of the local key
	uint32_t vrfy_keyno;
	if (der_get_uint32 (rep.keyno, &vrfy_keyno) != 0) {
		errno = ERANGE;
		log_errno ("der_get_uint32() != 0");
		goto fail;
	}
	if (vrfy_keyno != (uint32_t) keyid) {	/* trim to wire size */
		errno = KIPERR_KEYID_NOT_FOUND;
		log_errno ("vrfy_keyno");
		goto fail;
	}
	int32_t vrfy_encalg;
	if (der_get_int32 (rep.encalg, &vrfy_encalg) != 0) {
		errno = ERANGE;
		log_errno ("der_get_int32() != 0");
		goto fail;
	}
	if (vrfy_encalg != kk->k5keyblk.enctype) {
		errno = KIPERR_KEYID_NOT_FOUND;
		log_errno ("vrfy_encalg");
		goto fail;
	}
	//
	// Issue #21 - better wrap local encryption around this function
	uint8_t *hashes  = rep.hashes.derptr;
	uint32_t hashlen = rep.hashes.derlen;
	//
	// Count the number of hashes in the decryption output
	if ((hashlen % hashsz != 0) || (rep.salt.derlen != hashsz)) {
		log_debug ("hashlen=%d, hashsz=%d, hashlen %% hashsz = %d", hashlen, hashsz, hashlen % hashsz);
		errno = KIPERR_CHECKSUM_INVALID;
		log_errno ("hashlen mod hashsz is not 0");
		goto fail;
	}
	uint16_t vfy_num = hashlen / hashsz;
	log_debug ("hashlen=%d / hashsz=%d -> vfy_num := %d", hashlen, hashsz, vfy_num);
	//
	// Iterate over the provided checksums
	bool so_far_so_good = true;
	uint16_t sum_pos  = 0;
	uint16_t vfy_pos  = 0;
	uint16_t vfy_skip = 0;
	while ((sum_pos < sumslen) && (vfy_pos < vfy_num)) {
		kipt_sum *cs = NULL;
		LL_SEARCH_SCALAR (ctx->sums_head, cs, sumid, sums [sum_pos]);
		if (cs == NULL) {
			errno = KIPERR_SUMID_NOT_FOUND;
			log_errno ("cs == NULL");
			goto fail;
		}
		uint8_t rawhash [KIPSUM_MAXSIZE];
		assert (hashsz <= sizeof (rawhash));
		assert (_hash_final (cs, rep.salt.derptr, rawhash));
		bool found = false;
		while (vfy_pos + vfy_skip < vfy_num) {
			log_debug ("Comparing %02x %02x...%02x to %02x %02x...%02x", rawhash[0], rawhash[1], rawhash[hashsz-1], hashes [hashsz*(vfy_pos+vfy_skip)], hashes [hashsz*(vfy_pos+vfy_skip)+1], hashes [hashsz*(vfy_pos+vfy_skip+1)-1]);
			if (memcmp (rawhash, hashes + hashsz * (vfy_pos + vfy_skip), hashsz) == 0) {
				found = true;
				break;
			}
			vfy_skip++;
		}
		if (found) {
			vfy_pos += vfy_skip;
			log_debug ("Found a match between sum %d and vfy %d, after extra skip %d", sum_pos, vfy_pos, vfy_skip);
			       {
				/* Default handling rejects offset; stop now */
				if (vfy_skip > 0) {
					so_far_so_good = false;
					break;
				}
			}
		} else {
			log_debug ("Failed to match sum %d with a vfy >= %d", sum_pos, vfy_pos);
			/* Default handling rejects failure; stop now */
			so_far_so_good = false;
			break;
		}
		vfy_pos++;
		vfy_skip = 0;
	}
	//
	// If verifiers remain, make another callback
	vfy_skip = vfy_num - vfy_pos;
	log_debug ("Checked all checksums; now sum_pos=%d, vfy_pos=%d, vfy_num=%d, vfy_skip=%d", sum_pos, vfy_pos, vfy_num, vfy_skip);
	if (vfy_skip > 0) {
		log_error ("Found %d unattended signatures at the end of sumslen", vfy_skip);
		so_far_so_good = false;
	}
	//
	// Collect the overall response from the callback output
	if (!so_far_so_good) {
		errno = KIPERR_CHECKSUM_INVALID;
		log_errno ("!so_far_so_good");
		errno = KIPERR_CHECKSUM_INVALID;
		goto fail;
	}
	//
	// Copy any metadata into the output variables
	if (rep.meta.derptr != NULL) {
		log_debug ("kipservice_verify() -> metadata \"%.*s\"", rep.meta.derlen, rep.meta.derptr);
		if (!dermem_alloc (ctx->cnxpool, rep.meta.derlen, (void **) out_opt_meta)) {
			log_errno ("!dermem_alloc() for out_opt_meta");
			goto fail;
		}
		memcpy (*out_opt_meta, rep.meta.derptr, rep.meta.derlen);
		*out_metalen  = rep.meta.derlen;
	}
	//
	// Return success
	log_info ("kipservice_verify() -> success");
	return true;
	//
	// Return failure
fail:
	log_errno ("kipservice_verify() -> failure");
	dermem_recycle (~0, ctx->reqpool);
	return false;
}


/* Start KIP Service on a KIP context.
 *
 * This prepares a kipt_ctx for use with KIP Service.
 * The call should be paired with kipservice_stop().
 */
bool kipservice_start (kipt_ctx ctx) {
	//
	// Have memory pools for KIP Service connections and requests
	if (!dermem_open (0, &ctx->cnxpool)) {
		goto fail;
	}
	if (!dermem_open (0, &ctx->reqpool)) {
		goto cnx_fail;
	}
	//
	// Return success
	return true;
	//
	// Return failure
cnx_fail:
	dermem_close (&ctx->cnxpool);
fail:
	return false;
}


/* Stop KIP Service on a KIP context after kipservice_start().
 */
void kipservice_stop (kipt_ctx ctx) {
	//
	// Drop references into the cnxpool
	ctx->client_realm     = NULL;
	ctx->clientuser_login = NULL;
	ctx->clientuser_acl   = NULL;
	//
	// Cleanup memory pools for KIP Service
	dermem_close (&ctx->reqpool);
	dermem_close (&ctx->cnxpool);
}


/* Restart KIP Service; this is functionally equivalent to
 * kipservice_stop() followed by kipservice_start(), but more
 * efficient.  What it does is recycle the connection pool
 * into which output parameter from kipservice_xxx() piles up.
 * In normal use, such piling-up is just what the user wants.
 */
void kipservice_restart (kipt_ctx ctx) {
	//
	// Drop references into the cnxpool
	ctx->client_realm     = NULL;
	ctx->clientuser_login = NULL;
	ctx->clientuser_acl   = NULL;
	//
	// Recycle both memory pools for KIP Service
	dermem_recycle (~0, ctx->reqpool);
	dermem_recycle (~0, ctx->cnxpool);
}

/** @brief Checks @p rootkeyfile for it's format; returns true for BIND-style
 *
 * A BIND-style root-key file looks like this:
 *    ```
 *    ; // key 20326 (key-rollover 2017/2018)
 *    trusted-keys {
 *    ```
 * while unbound-style looks like
 *    ```
 *    . IN DNSKEY 258 3 8 <base64-data>
 *    ```
 * We need to know which one it is so we can call the right unbound
 * root-key-addition functions. As a side effect, if @p exists
 * is non-NULL, the value it points to is set to false if the
 * file does not exist at all, true otherwise.
 */
static bool is_bind_style_root (const char* rootkeyfile, bool* exists) {
	int fd = open(rootkeyfile, O_RDONLY);
	char buffer[4];
	if (exists) {
		*exists = fd >= 0;
	}
	if (fd<0) {
		log_errno("is_bind_style_root(open)");
		return false;
	}
	int r = read(fd, buffer, sizeof(buffer));
	if (r<1) {
		log_errno("is_bind_style_root(read)");
		close(fd);
		return false;
	}
	close(fd);
	return (buffer[0]==';' || buffer[0]=='t');
}

/* Initialise the KIP Service module.
 *
 * This mostly installs Unbound configuration:
 *  - optional root trust file (default "/etc/unbound/root.key" on Linux, "./root.key" on Windows)
 *  - optional test hosts file (fallback $UNBOUND_HOSTS, default "/etc/hosts")
 *  - optional local DNS setup (setting  $UNBOUND_CONFIG, no default)
 *  - optional DNS forwarder   (setting  $UNBOUND_FORWARD, no default)
 */
bool kipservice_init (char *opt_rootkey, char *opt_hosts) {
	assert (ubctx == NULL);
	//
	// Open the Unbound library
	ubctx = ub_ctx_create ();
	if (ubctx == NULL) {
		log_errno ("ub_ctx_create ()");
		goto fail;
	}
	//
	// Resolve in a thread rather than a process
	if (ub_ctx_async (ubctx, 1) != 0) {
		log_errno ("ub_ctx_async()");
		goto ub_fail;
	}
	//
	// Setup the root trust file
	if (opt_rootkey == NULL) {
		#ifndef _WIN32
			opt_rootkey = "/etc/unbound/root.key";
		#else
			static char root_key[1024 + 9]; //  is length of "root.key\0"
			DWORD filename_length = GetModuleFileName(NULL, root_key, 1024);
			assert(filename_length != 0);
			assert(root_key[filename_length] == '\0');
			if (filename_length < 1024) {
				char *last_backslash = strrchr(root_key, '\\');
				assert(last_backslash != NULL);
				strcpy(last_backslash + 1, "root.key");
				opt_rootkey = root_key;
			} else {
				log_error ("root_key length too small");
			}
		#endif
	}
	log_debug ("opt_rootkey=%s", opt_rootkey);
	bool has_rootkey = true;
	bool is_bind = is_bind_style_root (opt_rootkey, &has_rootkey);
	if (has_rootkey && is_bind) {
		if (ub_ctx_trustedkeys (ubctx, opt_rootkey) != 0) {
			log_errno ("ub_ctx_trustedkeys()");
			goto ub_fail;
		}
	} else if (has_rootkey) {
		if (ub_ctx_add_ta_file (ubctx, opt_rootkey) != 0) {
			log_errno ("ub_ctx_add_ta_file ()");
			goto ub_fail;
		}
	}
	//
	// Load the hosts file, can be overridden with envvar UNBOUND_HOSTS
	if (opt_hosts == NULL) {
		opt_hosts = getenv ("UNBOUND_HOSTS");
	}
	if (opt_hosts == NULL) {
		#ifndef _WIN32
			opt_hosts = "/etc/hosts";
		#else
			char *system_root;
			assert((system_root = getenv ("SystemRoot")) != NULL);
			static char default_hosts[256];
			if (snprintf(default_hosts, sizeof(default_hosts), "%s\\System32\\drivers\\etc\\hosts", system_root) < sizeof(default_hosts)) {
				opt_hosts = default_hosts;
			}
		#endif
	}
	log_debug ("opt_hosts=%s", opt_hosts);
	if (ub_ctx_hosts (ubctx, opt_hosts) != 0) {
		log_errno ("ub_ctx_hosts ()");
		goto ub_fail;
	}
	//
	// Setup an Unbound configuration file, if provided in $UNBOUND_CONFIG
	char *opt_config = getenv ("UNBOUND_CONFIG");
	if (opt_config != NULL) {
		log_debug ("opt_config=%s", opt_config);
		if (ub_ctx_config (ubctx, opt_config) != 0) {
			log_errno ("ub_ctx_config()");
			goto ub_fail;
		}
	}
	//
	// Setup an Unbound forwarder address, if provided in $UNBOUND_FORWARD
	char *opt_forward = getenv ("UNBOUND_FORWARD");
	if (opt_forward != NULL) {
		log_debug ("opt_forward=%s", opt_forward);
		if (ub_ctx_set_fwd (ubctx, opt_forward) != 0) {
			log_errno ("ub_ctx_set_fwd()");
			goto ub_fail;
		}
	}
	//
	// Return success
	return true;
	//
	// Return failure
ub_fail:
	ub_ctx_delete (ubctx);
	ubctx = NULL;
fail:
	errno = KIPERR_SERVICE_NOTFOUND;	/* TODO: Better error... */
	return false;
}


/* Cleanup the KIP Service module.
 */
void kipservice_fini (void) {
	assert (ubctx != NULL);
	ub_ctx_delete (ubctx);
	ubctx = NULL;
}
