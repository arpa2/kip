/* internal definitions for libkip -- do not distribute
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */



#include "kip-dbg.h"

#include <krb5.h>

/* Hash functions from OpenSSL:
 * https://www.openssl.org/docs/man1.1.0/man3/SHA1.html
 */
#include <openssl/sha.h>

#include <arpa2/quick-der.h>
#include <arpa2/quick-dermem.h>
#include <arpa2/quick-sasl.h>
#include <quick-der/KIP-Service.h>

#include <arpa2/kip.h>



/* A few structure abbreviations for use with DER */
typedef DER_OVLY_KIP_Service_KIP_INIT_REQ KIP_INIT_REQ;
typedef DER_OVLY_KIP_Service_KIP_INIT_REP KIP_INIT_REP;

typedef DER_OVLY_KIP_Service_KIP_SASL_REQ KIP_SASL_REQ;
typedef DER_OVLY_KIP_Service_KIP_SASL_REP KIP_SASL_REP;

typedef DER_OVLY_KIP_Service_KIP_ENCR_REQ KIP_ENCR_REQ;
typedef DER_OVLY_KIP_Service_KIP_ENCR_REP KIP_ENCR_REP;

typedef DER_OVLY_KIP_Service_KIP_DECR_REQ KIP_DECR_REQ;
typedef DER_OVLY_KIP_Service_KIP_DECR_REP KIP_DECR_REP;

typedef DER_OVLY_KIP_Service_KIP_SIGN_REQ KIP_SIGN_REQ;
typedef DER_OVLY_KIP_Service_KIP_SIGN_REP KIP_SIGN_REP;

typedef DER_OVLY_KIP_Service_KIP_VRFY_REQ KIP_VRFY_REQ;
typedef DER_OVLY_KIP_Service_KIP_VRFY_REP KIP_VRFY_REP;

typedef DER_OVLY_KIP_Service_KIP_Opaque_Data KIP_Opaque_Data;
typedef DER_OVLY_KIP_Service_KIP_ACL_Group KIP_ACL_Group;
typedef DER_OVLY_KIP_Service_KIP_Group KIP_Group;



/* Identifiers are 64-bit and somewhat ordered by the top 32-bits:
 *  - Value 0 is for key numbers (wire format keys)
 *  - Values 1 to 0x7fffffff are added to keyids to make them unique
 *  - Values 0x80000000 to 0xffffffff are unique sum identities
 *
 * We are lazy.  Assuming that no context will have more than
 * 2147483647 keys and dito checksums over their complete life.
 * Since the average context exists only to work on a document, even
 * if it is to edit one, it should be no problem to assume this.
 *
 * So, we will simply increment a unique number for each key and
 * checksum that the user adds, and use that on top.  We just need
 * to flip the sign for sums that are not implicit for a key.  It
 * is quite simply the most efficient, and seems practical.
 */


typedef struct kip_sum {
	//
	// The list key and links
	kipt_sumid sumid;
	struct kip_sum *next;
	//
	// Whether we are marking, and separation
	bool marking;
	uint64_t separation;
	//
	// Whether the checksum is locked (finished form)
	bool locked;
	//
	// Storage depends on the hash type
	kipt_alg keyalg;
	kipt_keyid keyid;
	union kip_sum_context_union {
		SHA_CTX sha1;
		SHA256_CTX sha256;
		SHA512_CTX sha384;	/* SHA384 is a prefix of SHA512 with its own init */
		SHA512_CTX sha512;
		uint8_t output [KIPSUM_MAXSIZE];
	} hctx;
} kipt_sum;


typedef struct kip_key {
	//
	// The list key and links
	kipt_keyid keyid;
	struct kip_key *next;
	//
	// The entropy needed to reproduce this key
	// though it may share internal key store
	krb5_octet *entropy;
	//
	// The krb5 key structure
	krb5_keyblock k5keyblk;
	//
	// The checksum for this key
	kipt_sum keysum;
} kipt_key;


typedef struct kipt_context {
	//
	// Underlying MIT krb5 context
	krb5_context k5ctx;
	//
	// Lists of inserted data
	struct kip_key *keys_head;
	struct kip_sum *sums_head;
	//
	// Unique counter, lazily used for keys and checksums
	uint32_t uniqtr;
	//
	// Cached sockets, uplinks to my own KIP Service,
	// one for authenticated and one for ANONYMOUS uses
	int service_uplink_login;
	int service_uplink_anonymous;
	//
	// SASL context handle
	void *sasl_handle;
	void *sasl_cbdata;
	//
	// DER Memory Pools per Connection, per Request
	void *cnxpool;
	void *reqpool;
	//
	// Client identity information stored in cnxpool
	char *client_realm;
	char *clientuser_login;
	char *clientuser_acl;
} *kipt_ctx;


uint16_t _hash_size (kipt_sum *sum);
bool _hash_final (kipt_sum *sum, uint8_t *opt_scrambler, uint8_t *out_data);
static bool _hash_init   (kipt_sum *sum, kipt_key *key);
static bool _hash_update (kipt_sum *sum, uint8_t *data, uint32_t datalen);

#define zap_free(m) { memset ((m), 0, sizeof (*(m))); free ((m)); }


// Internal algorithm code, from the local range, indicating anything
#define _KIP_ALG_ANY 0xcf2bd9aa


bool _newkey (kipt_ctx ctx, kipt_alg alg,
			kipt_keynr in_keynr,
			kipt_key **out_key,
			krb5_data *out_entropy);

bool _setkey (kipt_ctx ctx, krb5_data *entropy, kipt_key *key_to_set);

bool _addkey (kipt_ctx ctx, kipt_key *newkey);
