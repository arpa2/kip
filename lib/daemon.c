/* KIP Service, daemon library.
 *
 * The KIP Service is very much a "remote keytab" that unpacks keys
 * as directed by the ACL that is packed along with them.  When the
 * KIP library calls kipkey_fromservice() or kipkey_toservice() are
 * called they will connect to a KIP Service to get some work done,
 * but always just on the keys and never on the data
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>
#include <assert.h>

#include <unistd.h>
#include <time.h>
#include <signal.h>

#include <krb5.h>
#include "arpa2/kip.h"

#include <arpa2/socket.h>
#include <arpa2/com_err.h>
#include <arpa2/except.h>

#include <arpa2/identity.h>
#include <arpa2/access.h>

#include <arpa2/quick-der.h>
#include <arpa2/quick-dermem.h>
#include "arpa2/quick-sasl.h"
#include "quick-der/KIP-Service.h"

#include "../lib/kip-int.h"



/* A few extra structure abbreviations for use with DER */
typedef DER_OVLY_KIP_Service_KIP_Service_Request  KIP_Request ;
typedef DER_OVLY_KIP_Service_KIP_Service_Response KIP_Response;
typedef DER_OVLY_KIP_Service_KIP_ACL_Group KIP_ACL_Group;
typedef DER_OVLY_KIP_Service_KIP_Opaque_Data KIP_Opaque_Data;



/* Client Session information */
typedef struct kipt_daemon_context {
	//
	// Inherited KIP Context
	kipt_ctx kipctx;
	//
	// Inherited Quick SASL Context
	QuickSASL dersasl;
	//
	// Daemon-side reqpool (note, cnxpool is kipt_dctx)
	void *reqpool;
	//
	// The socket being serviced
	int sox;
	//
	// Client identity information
	dercursor client_userid; /* Extra: .derptr [.derlen] == '\0' */
	dercursor client_domain; /* Extra: .derptr [.derlen] == '\0' */
	dercursor srealm;	 /* Extra: .derptr [.derlen] == '\0' */
	dercursor crealm;	 /* Extra: .derptr [.derlen] == '\0' */
	//
	// Key table information
	kipt_keyid keytab_keyid;
	uint32_t keytab_keyno;
	int32_t keytab_encalg;
	//
	// Buffers for Quick DER
	der_buf_uint32_t keytab_keyno_derbuf;
	der_buf_int32_t keytab_encalg_derbuf;
	dercursor keytab_keyno_dercrs;
	dercursor keytab_encalg_dercrs;
	//
	// Explicit flag, only set after non-ANONYMOUS login success
	bool anonymous_user;
	bool auth_success;
	bool auth_failure;
} *kipt_dctx;


/* Static data, possibly moduled during module setup */

//GONE//


/* KIP-DECR-REP-simple ::= [APPLICATION 13] ANY
 *
 * KIP-VRFY-REP-simple ::= [APPLICATION  8] ANY
 *
 * KIP-Response-simple ::= CHOICE {
 *    encr         KIP-ENCR-REP,
 *    decr_simple  KIP-DECR-REP-simple,
 *    sign         KIP-SIGN-REP,
 *    vrfy_simple  KIP-VRFY-REP-simple
 * }
 */


/* Construct a KIP-Opaque-Data structure.
 */
static bool _put_opaque_data (kipt_dctx clises, dercursor pre_encrypt, KIP_Opaque_Data *opaq) {
	//
	// Clear out the KIP-Opaque-Data structure
	memset (opaq, 0, sizeof (*opaq));
	//
	// Encrypt the input data structure with the active keytab key
	uint32_t mudlen;
	assert (!kipdata_up (clises->kipctx, clises->keytab_keyid,
			pre_encrypt.derptr, pre_encrypt.derlen,
			NULL, 0, &mudlen));
	//TODO// key not found!
	assert (errno == KIPERR_OUTPUT_SIZE);
	errno = 0;
	uint8_t *mud = NULL;
	if (!dermem_alloc (clises->reqpool, mudlen, (void **) &mud)) {
		goto fail;
	}
	if (!kipdata_up (clises->kipctx, clises->keytab_keyid,
			pre_encrypt.derptr, pre_encrypt.derlen,
			mud, mudlen, &mudlen)) {
		goto fail;
	}
	//
	// Fill the fields of the KIP-Opaque-Data
	opaq->realm   = clises->srealm;
	log_debug ("Set KIP-Opaque-Data.realm to %.*s", (int)opaq->realm.derlen, opaq->realm.derptr);
	opaq->keyno   = clises->keytab_keyno_dercrs;
	opaq->encalg  = clises->keytab_encalg_dercrs;
	opaq->payload.derptr = mud;
	opaq->payload.derlen = mudlen;
	//
	// Return success
	return true;
	//
	// Return failure
fail:
	return false;
}


/* Extract a KIP-Opaque-Data structure.
 */
static bool _get_opaque_data (kipt_dctx clises, KIP_Opaque_Data *opaq, dercursor *out_decrypted_payload) {
	//
	// Clear the output for now
	out_decrypted_payload->derptr = NULL;
	out_decrypted_payload->derlen = 0;
	//
	// Retrieve the values of keyno and encalg
	uint32_t keynr = 0;
	int32_t encalg = 0;
	if (der_get_uint32 (opaq->keyno,  &keynr ) != 0) {
		errno = ERANGE;
		goto fail;
	}
	if (der_get_int32  (opaq->encalg, &encalg) != 0) {
		errno = ERANGE;
		goto fail;
	}
	//
	// Allocate memory to store the result
	uint32_t paylen = opaq->payload.derlen;
	if (!dermem_alloc (clises->reqpool, paylen, (void **) &out_decrypted_payload->derptr)) {
		goto fail;
	}
	//
	// Decrypt the payload with the selected key (ignore enctype)
	if (!kipdata_down (clises->kipctx, keynr,
			opaq->payload.derptr, opaq->payload.derlen,
			out_decrypted_payload->derptr, paylen, &paylen)) {
		goto fail;
	}
	out_decrypted_payload->derlen = paylen;
	//
	// Return success
	return true;
	//
	// Return failure
fail:
	return false;
}


/* Process KIP-ENCR-REQ into a KIP-ENCR-REP.
 *
 * This requires a basic check of syntax and consistency,
 * followed by encryption of the parts that pass.
 *
 * This is a public service, no login required.
 */
bool process_encr_req (kipt_dctx clises, dercursor raw_req, KIP_ENCR_REQ *req, KIP_ENCR_REP *rep) {
	//
	// Encrypt, and build the response's KIP-Opaque-Data structure
	dercursor opaque = { .derptr = NULL, .derlen = 0 };
	if (!_put_opaque_data (clises, raw_req, rep)) {
		goto fail;
	}
	//
	// Return success
	return true;
	//
	// Return failure
fail:
	return false;
}


/* Internal.
 *
 * Iterate over a SEQUENCE OF ARPA2Selector, chasing for the
 * lowest "steps" value from a2sel_abstractions().  An initial
 * high value is already setup, to be lowered by new findings.
 *
 * This is used to run over the blacklist and whitelist in a
 * KIP-AccessControl-Inline structure.  The default levels are
 * such that the black list wins out.  When both lists result
 * in the same value, then too the black list wins.  When no
 * entries are found, then too the black list wins.
 *
 * This function returns false on problems, true on success.
 */
bool _aclgroup_chase_lowest (uint16_t *lowest_so_far, const a2id_t *user, dercursor seq_of) {
	//
	// Iterate over the SEQUENCE OF
	dercursor iter;
	if (der_iterate_first (&seq_of, &iter)) do {
		//
		// Parse the ARPA2 Selector -- silently skip syntax errros
		dercursor selcrs = iter;
		a2sel_t selector;
		if (der_enter (&selcrs) != 0) {
			//
			// The list entry is not even an ASN.1 value
			log_error ("Offensive SEQUENCE OF ARPA2Selector, acccess denied");
			return false;
		} else if (a2sel_parse (&selector, (char*)selcrs.derptr, (unsigned) selcrs.derlen)) {
			//
			// Test if the new ARPA2 Selector is fewer steps away
			log_debug ("Comparing to ARPA2 Selector %s", selector.txt);
			uint16_t new_steps = ~0;
			if (a2sel_abstractions (user, &selector, &new_steps)) {
				log_debug ("Found less abstraction in %s", selector.txt);
				if (new_steps < *lowest_so_far) {
					//
					// The new steps value is the lowest so far
					*lowest_so_far = new_steps;
				}
			}
		} else {
			//
			// Syntax error in the listed ARPA2 Selector
			log_debug ("Skipping over poor ARPA2 Selector %.*s",
				(int) selcrs.derlen, (char *) selcrs.derptr);
		}
		//
		// Next entry in the SEQUENCE OF
	} while (der_iterate_next (&iter));
	//
	// Return success -- no technical problems occurred
	return true;
}


/* Internal.
 *
 * Check if the client session, with its authenticated clientid,
 * is authorised by the aclgrp.  This is the structure of alcgrp:
 *
 * KIP-ACL-Group ::= SEQUENCE {
 *    acl    [0] KIP-AccessControl,
 *    group  [1] KIP-Group,
 *    from   [2] TimeStamp OPTIONAL,
 *    till   [3] TimeStamp OPTIONAL
 * }
 *
 * KIP-AccessControl ::= CHOICE {
 *    inline-acl  [0] KIP-AccessControl-Inline,
 *    localref    [1] OCTET STRING,
 *    ...
 * }
 *
 * KIP-AccessControl-Inline ::= SEQUENCE {
 *    blacklist  [0] SEQUENCE OF ARPA2Selector,
 *                   -- may be empty
 *    whitelist  [1] SEQUENCE OF ARPA2Selector
 *                   -- may be empty
 * }
 *
 * TimeStamp  ::= KerberosTime
 *
 * When specified, we should also test against the time.
 */
bool _aclgroup_accept (kipt_dctx clises, KIP_ACL_Group *aclgrp) {
	//
	// Verify the distribution over clises->client_domain and clises->client_userid
	unsigned sasluserid_len = clises->client_userid.derlen;
	unsigned sasldomain_len = clises->client_domain.derlen;
	char *sasluserid = (sasluserid_len > 0) ? (char *) clises->client_userid.derptr : "";
	char *sasldomain = (sasldomain_len > 0) ? (char *) clises->client_domain.derptr : "";
	char saslname [sasluserid_len + 1 + sasldomain_len + 1];
	if (sasluserid_len == 0) {
		log_error ("Client did not authenticate, access denied");
		goto fail;
	}
	//
	// Construct the user@domain while demanding exactly one domain.
	// Local SASL mechanisms may provide a domain/realm if they want to;
	// Realm Crossover SASL derives the domain and rejects it in the user.
	if (memchr (sasluserid, '@', sasluserid_len) == NULL) {
		if (sasldomain_len == 0) {
			log_error ("Client %.*s without domain, access denied",
				sasluserid_len, sasluserid);
			goto fail;
		}
		memcpy (saslname, sasluserid, sasluserid_len);
		saslname [sasluserid_len] = '@';
		memcpy (saslname + sasluserid_len + 1, sasldomain, sasldomain_len);
		saslname [sasluserid_len + 1 + sasldomain_len] = '\0';
	} else {
		if (sasldomain_len != 0) {
			log_error ("Client %.*s also gave domain %.*s, access denied",
				sasluserid_len, sasluserid,
				sasldomain_len, sasldomain);
			goto fail;
		}
		memcpy (saslname, sasluserid, sasluserid_len);
		saslname [sasluserid_len] = '\0';
	}
	//
	// Parse the authenticated name as an ARPA2 Identity
	a2id_t sasl_a2id;
	if (!a2id_parse (&sasl_a2id, saslname, sasluserid_len + 1 + sasldomain_len)) {
		log_error ("Failed to parse client ARPA2 Identity %s, access denied",
			saslname);
		goto fail;
	}
	//
	// Check the from/till timestamps, inasfar as they are given
	bool now_ok = true;
	time_t now_time;
	struct tm now_tm;
	char now_krb [30];
	size_t now_len;
	now_ok  = now_ok && (time (&now_time) != (time_t) -1);
	now_ok  = now_ok && (gmtime_r (&now_time, &now_tm) != NULL);
	now_len = now_ok ? strftime (now_krb, 25, "%Y%m%d%H%M%SZ", &now_tm) : 0;
	now_ok  = now_ok && (now_len != 0);
	if (!now_ok) {
		log_error ("Internal error: Cannot produce current KerberosTime, access denied");
		goto fail;
	}
	log_debug ("Comparing from/till timestamps at KerberosTime %s", now_krb);
	if (aclgrp->from.derptr != NULL) {
		now_ok = now_ok && (aclgrp->from.derlen == now_len);
		now_ok = now_ok && (memcmp (aclgrp->from.derptr, now_krb, aclgrp->from.derlen) <= 0);
	}
	if (!now_ok) {
		log_error ("Content is valid from %.*s, it is now %s, access denied",
				(int   ) aclgrp->from.derlen,
				(char *) aclgrp->from.derptr,
				now_krb);
		goto fail;
	}
	if (aclgrp->till.derptr != NULL) {
		now_ok = now_ok && (aclgrp->till.derlen == now_len);
		now_ok = now_ok && (memcmp (now_krb, aclgrp->till.derptr, aclgrp->till.derlen) <= 0);
	}
	if (!now_ok) {
		log_error ("Content is valid till %.*s, it is now %s, access denied",
				(int   ) aclgrp->till.derlen,
				(char *) aclgrp->till.derptr,
				now_krb);
		goto fail;
	}
	//
	// Iterate the blacklist and whitelist, racing to the bottom level.
	// The initial value may be lowered.  The black list wins when its
	// lowest level equals the white list's lowest level.
	uint16_t lowest_white = ~0;
	uint16_t lowest_black = ~0;
	bool iterok = true;
	iterok = iterok && _aclgroup_chase_lowest (&lowest_black, &sasl_a2id, aclgrp->acl.inline_acl.blacklist.wire);
	log_debug ("Chased blacklist down to %d steps", (int) lowest_black);
	iterok = iterok && _aclgroup_chase_lowest (&lowest_white, &sasl_a2id, aclgrp->acl.inline_acl.whitelist.wire);
	log_debug ("Chased whitelist down to %d steps", (int) lowest_white);
	if (!iterok) {
		log_error ("Iteration failed for client %s, access denied",
				saslname);
		goto fail;
	}
	//
	// Is the most specific match in the whitelist lower than in the blacklist?
	if (lowest_black <= lowest_white) {
		log_error ("White list not more specific than black list for %s, access denied",
				saslname);
		goto fail;
	}
	//
	// Return success
	return true;
	//
	// Return a negative result for any kind of failure
fail:
	return false;
}


/* Process KIP-DECR-REQ into a KIP-DECR-REP.
 *
 * This requires a decryption, then iteration over the parts
 * to filter them based on the current user and timestamp.
 * The parts that remain will be sent back as keys.
 *
 * Save for the types and walks, this is process_vrfy_req()
 *
 * This is a private service, it requires proper login.
 */
bool process_decr_req (kipt_dctx clises, dercursor raw_req, KIP_DECR_REQ *req, KIP_DECR_REP *rep) {
	//
	// Test if the user did a successful login, not ANONYMOUS
	if (clises->anonymous_user) {
		errno = KIPERR_SERVICE_ACCESS;
		log_notice ("Rejecting unauthenticated KIP-DECR-REQ by %s", "TODO@TODO");
		goto fail;
	}
	//
	// Decrypt the KIP-Opaque-Data that arrived in req, extracting the payload
	dercursor payload;
	if (!_get_opaque_data (clises, req, &payload)) {
		goto fail;
	}
	//
	// Unpack the payload, which should be a KIP-ENCR-REQ
	derwalk pack_encr_req [] = {
			DER_PACK_ENTER | DER_TAG_APPLICATION(10),
			DER_PACK_STORE | DER_TAG_SEQUENCE,
			DER_PACK_LEAVE,
			DER_PACK_END
	};
	dercursor wlkr = payload;
	dercursor seq_of = { .derptr = NULL, .derlen = 0 };
	if (der_unpack (&wlkr, pack_encr_req, &seq_of, 1) != 0) {
		goto fail;
	}
	//
	// Write the (never longer) output over the input
	dercursor out_seq_of = seq_of;
	out_seq_of.derlen = 0;
	//
	// Iterate over the sequence stored in seq_of
	derwalk pack_acl_group [] = {
			DER_PACK_KIP_Service_KIP_ACL_Group,
			DER_PACK_END
	};
	derwalk aclgroup2group [] = {
			DER_WALK_ENTER | DER_TAG_SEQUENCE,
			DER_WALK_SKIP  | DER_TAG_CONTEXT(0),
			DER_WALK_ENTER | DER_TAG_CONTEXT(1),
			DER_WALK_END
	};
	print_block ("payload-SEQ-OF", seq_of.derptr, seq_of.derlen, false);
	bool todo_next;
	dercursor iter, iter_next;
	if (der_iterate_first (&seq_of, &iter)) do {
		log_debug ("Iterator 0 at %p length %zd", iter.derptr, iter.derlen);
		log_debug ("Some iteration over KIP-ACL-Group in SEQUENCE OF with derlen=%ld", iter.derlen);
		KIP_ACL_Group aclgrp;
		dercursor aclgroup = iter;
		der_focus (&aclgroup);
		print_block ("aclgroup", aclgroup.derptr, aclgroup.derlen, true);
		if (der_unpack (&aclgroup, pack_acl_group, (dercursor *) &aclgrp, 1) != 0) {
			/* TODO: Perhaps just skip */
			log_errno ("!der_unpack()");
			goto fail;
		}
		/* Before we overwrite the current block, update to the next iterator */
		iter_next = iter;
		todo_next = der_iterate_next (&iter_next);
		/* See if this entry passes for this client */
		if (_aclgroup_accept (clises, &aclgrp)) {
			/* Yes, so we move aclgrp to the internal group */
			aclgroup = iter;
			der_focus (&aclgroup);
			if (der_walk (&aclgroup, aclgroup2group) != 0) {
				/* TODO: Perhaps just skip */
				log_errno ("der_walk()");
				goto fail;
			}
			/* Copy the aclgroup to the output */
			memcpy (out_seq_of.derptr + out_seq_of.derlen,
					aclgroup.derptr, aclgroup.derlen);
			out_seq_of.derlen += aclgroup.derlen;
		}
		iter = iter_next;
	} while (todo_next);
	log_debug ("Done iterating over KIP-ACL-Group in SEQUENCE OF");
	//
	// Produce the output, just the single SEQUENCE OF value
	rep->wire = out_seq_of;
	//
	// Return success
	return true;
	//
	// Return failure
fail:
	return false;
}


/* Process KIP-SIGN-REQ into a KIP-SIGN-REP.
 *
 * This requires a basic check of syntax and consistency,
 * incorporation of information about the current user,
 * followed by encryption of the parts that pass.
 *
 * TODO: We should try to authorise as the requested author
 *
 * This is a private service, it requires proper login.
 */
bool process_sign_req (kipt_dctx clises, dercursor raw_req, KIP_SIGN_REQ *req, KIP_SIGN_REP *rep) {
	//
	// Test if the user did a successful login, not ANONYMOUS
	if (clises->anonymous_user) {
		errno = KIPERR_SERVICE_ACCESS;
		log_notice ("Rejecting unauthenticated KIP-SIGN-REQ by %s", "TODO@TODO");
		goto fail;
	}
	//
	// Override the author with the identity from SASL
	//TODO// client_domain
	if (clises->client_userid.derptr == NULL) {
		errno = KIPERR_SERVICE_ACCESS;
		log_errno ("client_userid.derptr == NULL");
		goto fail;
	}
	//TODO// client_domain
	req->author = clises->client_userid;
	//
	// Validate the signature size (TODO: do we need to be that picky?)
	//TODO	1. determine hash size
	//TODO	2. sigsize modulo hash size
	//TODO  3. require that this yields 0
	//
	// Form the input to the encryption algorithm
	// !! do this by encrypting the updated request
	static const derwalk pack_sign_req [] = {
			DER_PACK_KIP_Service_KIP_SIGN_REQ,
			DER_PACK_END
	};
	dercursor pre_enc = { .derptr = NULL, .derlen = 0 };
	int i; for (i=0; i < sizeof (KIP_SIGN_REQ) / sizeof (dercursor); i++) {
		log_debug ("KIP_SIGN_REQ [%d] = \"%.*s\"", i, (int)(((dercursor *) req) [i].derlen), ((dercursor *) req) [i].derptr);
	}
	if (!dermem_pack (clises->reqpool, pack_sign_req, (dercursor *) req, &pre_enc)) {
		goto fail;
	}
	//
	// Encrypt, and build the response's KIP-Opaque-Data structure
	dercursor opaque = { .derptr = NULL, .derlen = 0 };
	if (!_put_opaque_data (clises, pre_enc, rep)) {
		goto fail;
	}
	//
	// Return success
	return true;
	//
	// Return failure
fail:
	return false;
}


/* Process KIP-VRFY-REQ into a KIP-VRFY-REP.
 *
 * This requires a decryption, then delivery of the
 * KIP-Signature so obtained.
 *
 * Save for the types and walks, this is process_decr_req()
 *
 * This is a public service, no login required.
 */
bool process_vrfy_req (kipt_dctx clises, dercursor raw_req, KIP_VRFY_REQ *req, KIP_VRFY_REP *rep) {
	//
	// Decrypt the KIP-Opaque-Data that arrived in req, extracting the payload
	log_debug ("process_vry_req() called");
	dercursor payload;
	if (!_get_opaque_data (clises, req, &payload)) {
		log_errno ("!_get_opaque_data()");
		goto fail;
	}
	//
	// Decrypt the payload's DER into the reply structure
	// (Certainly not optimal; we could unpack/pack somehow)
	static const derwalk pack_sign_req [] = {
			DER_PACK_KIP_Service_KIP_SIGN_REQ,
			DER_PACK_END
	};
	if (der_unpack (&payload, pack_sign_req, (dercursor *) rep, 1) != 0) {
		log_errno ("!der_unpack()");
		goto fail;
	}
	//
	// Return success
	log_info ("process_vrfy_req() succeeds");
	return true;
	//
	// Return failure
fail:
	log_errno ("process_vrfy_req() fails");
	return false;
}


/* We have received a command request, and produce an answer for it.
 */
bool process_cmd_req (kipt_dctx clises, dercursor raw_req, dercursor *dercrs_req, dercursor *dercrs_rep, bool *no_rep) {
	//
	// Initialise the entire switches covering all commands
	KIP_Request  *cmd_req = (KIP_Request  *) dercrs_req;
	KIP_Response *cmd_rep = (KIP_Response *) dercrs_rep;
	memset (cmd_rep, 0, sizeof (*cmd_rep));
	//
	// Now find the one the was decoded, and handle only that bit
	if (cmd_req->encr.wire.derptr != NULL) {
		//
		// Handle KIP-ENCR-REQ
		return process_encr_req (clises, raw_req, &cmd_req->encr, &cmd_rep->encr);
	} else if (cmd_req->decr.payload.derptr != NULL) {
		//
		// Handle KIP-DECR-REQ
		return process_decr_req (clises, raw_req, &cmd_req->decr, &cmd_rep->decr);
	} else if (cmd_req->sign.hashes.derptr != NULL) {
		//
		// Handle KIP-SIGN-REQ
		return process_sign_req (clises, raw_req, &cmd_req->sign, &cmd_rep->sign);
	} else if (cmd_req->vrfy.payload.derptr != NULL) {
		//
		// Handle KIP-VRFY-REQ
		return process_vrfy_req (clises, raw_req, &cmd_req->vrfy, &cmd_rep->vrfy);
	} else {
		//
		// Signal unknown command as an error
		log_error ("Unknown command for kipd, not ENCR, DECR, SIGN or VRFY");
		errno = ENOSYS;
		return false;
	}
}


bool process_init_req (kipt_dctx clises, dercursor raw_req, dercursor *dercrs_init_req, dercursor *dercrs_init_rep, bool *no_rep) {
	QuickSASL qs = clises->dersasl;
	//
	// Setup to validate the initiation request and fill the response
	log_debug ("process_init_req() processing KIP-INIT-REQ and forming KIP-INIT-REP");
	KIP_INIT_REQ *init_req = (KIP_INIT_REQ *) dercrs_init_req;
	KIP_INIT_REP *init_rep = (KIP_INIT_REP *) dercrs_init_rep;
	memset (init_rep, 0, sizeof (*init_rep));
	static uint8_t protocol [] = { 'K', 'I', 'P', ' ', 'S', 'e', 'r', 'v', 'i', 'c', 'e' };
	static uint8_t version  [] = { 0x00 };  /* "...one or more octets" */
	static uint8_t emptyseq [] = { DER_TAG_SEQUENCE, 0 };
	log_debug ("INIT-REQ has crealm==%.*s and srealm==%.*s", (int)init_req->crealm.derlen, init_req->crealm.derptr, (int)init_req->srealm.derlen, init_req->srealm.derptr);
	//
	// Require that protocol and version match our expectations
	if ((init_req->protocol.derlen != sizeof (protocol)) || (memcmp (init_req->protocol.derptr, protocol, sizeof (protocol)) != 0)) {
	log_error ("protocol wrongly set to \"%.*s\"", (int)init_req->protocol.derlen, init_req->protocol.derptr);
		errno = KIPERR_SERVICE_INITIATION_WRONG;
		return false;
	}
	if ((init_req->version .derlen != sizeof (version )) || (memcmp (init_req->version .derptr, version,  sizeof (version )) != 0)) {
	log_error ("version wrongly set to length / value is %zu / 0x%02x...", init_req->version.derlen, *init_req->version.derptr);
		errno = KIPERR_SERVICE_INITIATION_WRONG;
		return false;
	}
	init_rep->protocol.derptr =         protocol ;
	init_rep->protocol.derlen = sizeof (protocol);
	init_rep->version .derptr =         version  ;
	init_rep->version .derlen = sizeof (version );
	//
	// Ignore flags and software; we overrule flags as empty and provide no software
	init_rep->flags.wire.derptr =         emptyseq ;
	init_rep->flags.wire.derlen = sizeof (emptyseq);
	//
	// Find the service realm, default value is the client realm
	dercursor srealm;
	if (der_isnonempty (&init_req->srealm)) {
		srealm = init_req->srealm;
	} else {
		srealm = init_req->crealm;
	}
	//
	// Prepare for a SASL handshake over this connection
	bool failure = false;
	bool success = false;
	log_debug ("DEBUG: libkip INIT-REQ/REP failure=%d success=%d at %s:%d", failure, success, __FILE__, __LINE__);
	//
	// Clone information into the cnxpool: crealm, srealm
	failure = failure || !dermem_strdup (clises/*IMPLIED->cnxpool*/, init_req->crealm, (char **) &clises->crealm.derptr);
	failure = failure || !dermem_strdup (clises/*IMPLIED->cnxpool*/, srealm, (char **) &clises->srealm.derptr);
	clises->crealm.derlen = strlen ((char*)clises->crealm.derptr);
	clises->srealm.derlen = strlen ((char*)clises->srealm.derptr);
	failure = failure || !qsasl_set_client_realm (clises->dersasl, crs2buf (clises->crealm));
	failure = failure || !qsasl_set_server_realm (clises->dersasl, crs2buf (clises->srealm));
	log_info ("daemon _secure_kip_server() enter SASL with crealm %.*s, srealm %.*s at %s:%d", (int)clises->crealm.derlen, clises->crealm.derptr, (int)clises->srealm.derlen, clises->srealm.derptr, __FILE__, __LINE__);
	log_debug ("libkip INIT-REQ/REP failure=%d success=%d at %s:%d", failure, success, __FILE__, __LINE__);
	//
	// Find the list of SASL mechanisms
	static const dercursor word_anonymous = {
		.derptr = "ANONYMOUS",
		.derlen = 9
	};
	if (0 == der_cmp (word_anonymous, clises->crealm)) {
		init_rep->saslmech = word_anonymous;
	} else if (!qsasl_get_mech (qs, crs2bufp (&init_rep->saslmech))) {
		return false;
	}
	log_debug ("libkip INIT-REQ/REP retrieved mechanism string \"%.*s\"", (int)init_rep->saslmech.derlen, init_rep->saslmech.derptr);
	log_debug ("libkip INIT-REQ/REP failure=%d success=%d at %s:%d", failure, success, __FILE__, __LINE__);
	//
	// Return successfully
	log_debug ("KIP-INIT-REP successfully constructed");
	return true;
}


/* Read DER data with a request,
 * invoke a callback on it,
 * send DER with a response.
 */
static bool _derio (kipt_dctx clises,
			bool (*cb) (kipt_dctx clises, dercursor raw_in, dercursor *in, dercursor *out, bool *no_rep),
			const derwalk *pack_req, void *data_req,
			const derwalk *pack_rep, void *data_rep) {
	log_debug ("deamon _derio() entered");
	bool no_rep = false;
	//
	// Read one block of DER data
	dercursor raw_req = { .derptr = NULL, .derlen = 0 };
	if (!dermem_recv_der (clises->reqpool, clises->sox, pack_req, data_req, &raw_req)) {
		/* errno is set */
		log_errno ("!dermem_recv()");
		return false;
	}
	log_debug ("Request received");
	//
	// Invoke the callback to produce the response
	dercursor raw_rep = { .derptr = NULL, .derlen = 0 };
	if (!cb (clises, raw_req, (dercursor *) data_req, (dercursor *) data_rep, &no_rep)) {
		log_errno ("!cb()");
		return false;
	}
	log_debug ("Callback completed");
	//
	// Refrain from the response if so flagged
	if (no_rep) {
		log_debug ("Refraining from response");
		return true;
	}
	//
	// Send the response as DER data
	if (!dermem_send (clises->reqpool, clises->sox, pack_rep, data_rep)) {
		/* errno is set */
		log_errno ("!dermem_send()");
		return false;
	}
	log_debug ("Sending done");
	//
	// Return success
	log_debug ("daemon _derio() returns successfully");
	return true;
}


/* Process a KIP-SASL-REQ into a KIP-SASL-REP.
 * This takes a step from the server perspective.
 */
bool process_sasl_req (kipt_dctx clises, dercursor raw_req, dercursor *dercrs_sasl_req, dercursor *dercrs_sasl_rep, bool *no_rep) {
	QuickSASL qs = clises->dersasl;
	KIP_SASL_REQ *req = (KIP_SASL_REQ *) dercrs_sasl_req;
	KIP_SASL_REP *rep = (KIP_SASL_REP *) dercrs_sasl_rep;
	memset (rep, 0, sizeof (*rep));
	//TODO// mech
	bool ok = true;
	if (req->mech.derptr != NULL) {
		//
		// Locally handle the general ANONYMOUS mechanism
		// by returning success and no response token.
		static const dercursor mech_anonymous = {
			.derptr = "ANONYMOUS",
			.derlen = 9
		};
		if (ok && (0 == der_cmp (req->mech, mech_anonymous))) {
			//
			// Note that we deliberately set the flag for
			// improper login, clises->anonymous_user,
			// which will make non-public services fail.
			// This flag is never reset by explicit code,
			// only kip_dctx initialisation clears it.
			clises->anonymous_user = true;
			//
			// Mimic a sasl_callback() with QSA_SUCCESS
			clises->auth_success = true;
			static der_buf_bool_t tautology;
			rep->success = der_put_bool (tautology, true);;
			//
			// Give the DER NULL a mouth to speak through
			rep->s2c.no_token.derptr = (uint8_t*)"";
			//
			// Return with a happy feeling
			return true;
		}
		ok = ok && qsasl_set_mech (qs, crs2buf (req->mech));
	}
	ok = ok && qsasl_step_server (qs, crs2buf (req->c2s.token), crs2bufp (&rep->s2c.token));
	/* Note that sasl_callback() may have set rep->success */
	if (ok && (rep->s2c.token.derptr == NULL)) {
		if (rep->success.derptr != NULL) {
			*no_rep = true;
		} else {
			/* avoid Quick DER issue #76 */
			rep->s2c.no_token.derptr = (uint8_t*)"";
		}
	}
	return ok;
}


/* Take note of the final outcome of a SASL exchange.
 */
void sasl_callback (void *cbdata, QuickSASL qsasl, enum qsaslt_state newstate) {
	void **fail_succ_ptrs = (void **) cbdata;
	static der_buf_bool_t tautology;
	assert ((newstate == QSA_SUCCESS) || (newstate == QSA_FAILURE));
	if (newstate == QSA_FAILURE) {
		* (bool      *) fail_succ_ptrs [0] = true;
	} else {
		* (bool      *) fail_succ_ptrs [1] = true;
		* (dercursor *) fail_succ_ptrs [2] = der_put_bool (tautology, true);
	}
}


/* Perform the TLS handshake, then go through Initiation and SASL handshake.
 *
 * The entire procedure uses clises->reqpool for storage, and will clear it
 * when done.  Only clientid, crealm, srealm are copied into clises (as cnxpool)
 * from where it may serve upcoming requests.  When login used SASL ANONYMOUS
 * as its mechanism, it will be clearly marked as such, and non-public services
 * will reject any request.
 *
 * TODO: Retain the information upon success: authzid@crealm, srealm.
 *
 * This function returns true on success or false with errno on failure.
 * Upon success, the (possibly new) secured connection is in clises->sox.
 * At failure, it will also have closed clises->sox and have set it to -1.
 */
static bool _secure_kip_server (kipt_dctx clises) {
	log_debug ("daemon _secure_kip_server() called on socket = %d", clises->sox);
	KIP_SASL_REQ sasl_req;
	KIP_SASL_REP sasl_rep;
	//
	// Have a memory pool centered around QuickSASL
	void *fail_succ_ptrs [3] = { &clises->auth_failure, &clises->auth_success, &sasl_rep.success };
	clises->auth_failure = clises->auth_failure || !qsasl_server (
			&clises->dersasl, "kip",
			sasl_callback, fail_succ_ptrs,  /* cb,cbdata to signal on success/failure */
			QSA_SERVERFLAG_ALLOW_FINAL_S2C);
	//
	//TODO// Switch to TLS over this connection
	char *TODO_servername = "kipsvc.unicorn.demo.arpa2.lab";
	char *TODO_clientname = NULL;
	//
	// Ask for tls-unique channel binding information
	//TODO//DO_BETTER_THAN:
#ifdef TODO_RUN_AFTER_SETTING_CREALM_AND_SREALM_AND_SUPPRESS_QSASLHAVE
	dercursor chanbind;
	chanbind.derptr = QSA_X_MANUAL_SESSION_KEY ":0123456789ab";
	chanbind.derlen = strlen(chanbind.derptr);
	clises->auth_failure = clises->auth_failure || !qsasl_set_chanbind (clises->dersasl, false, chanbind);
	log_debug ("daemon _secure_kip_server() after channel binding, failure=%d success=%d at %s:%d", clises->auth_failure, clises->auth_success, __FILE__, __LINE__);
#endif
	//TODO//EXTERNAL// dercursor clientid = { .derptr = opt_client_name, .derlen = (opt_client_name == NULL) ? 0 : strlen (opt_client_name) };
	//TODO//EXTERNAL// log_debug ("daemon _secure_kip_server() failure=%d success=%d at %s:%d", clises->auth_failure, clises->auth_success, __FILE__, __LINE__);
	//TODO//EXTERNAL// if (clientid.derptr != NULL) {
	//TODO//EXTERNAL// 	failure = failure || !qsasl_set_clientid (clises->dersasl, clientid);
	//TODO//EXTERNAL// }
	log_debug ("daemon _secure_kip_server() after optional EXTERNAL setup, failure=%d success=%d at %s:%d", clises->auth_failure, clises->auth_success, __FILE__, __LINE__);
	//
	// Start exchanging the KIP-INIT-REQ/REP
	KIP_INIT_REQ init_req;
	KIP_INIT_REP init_rep;
	static const derwalk pack_init_req [] = {
			DER_PACK_KIP_Service_KIP_INIT_REQ,
			DER_PACK_END
	};
	static const derwalk pack_init_rep [] = {
			DER_PACK_KIP_Service_KIP_INIT_REP,
			DER_PACK_END
	};
	log_debug ("daemon _secure_kip_server() invokes  _derio() to handle KIP-INIT-REQ/REP");
	clises->auth_failure = clises->auth_failure || !_derio (clises, process_init_req,
				pack_init_req, &init_req,
				pack_init_rep, &init_rep);
	log_debug ("daemon _secure_kip_server() failure=%d, success=%d at %s:%d", clises->auth_failure, clises->auth_success, __FILE__, __LINE__);
	//
	// Now loop through the SASL server exchange until success or failure
	while ( ! ( clises->auth_success || clises->auth_failure )) {
		log_debug ("daemon _secure_kip_server() invokes  _derio() to handle KIP-SASL-REQ/REP");
		static const derwalk pack_sasl_req [] = {
				DER_PACK_KIP_Service_KIP_SASL_REQ,
				DER_PACK_END
		};
		static const derwalk pack_sasl_rep [] = {
				DER_PACK_KIP_Service_KIP_SASL_REP,
				DER_PACK_END
		};
		clises->auth_failure = clises->auth_failure || !_derio (
					clises,
					process_sasl_req,
					pack_sasl_req, &sasl_req,
					pack_sasl_rep, &sasl_rep);
		log_debug ("daemon _secure_kip_server() failure=%d, success=%d at %s:%d", clises->auth_failure, clises->auth_success, __FILE__, __LINE__);
	}
	log_debug ("daemon _secure_kip_server() failure=%d, success=%d at %s:%d", clises->auth_failure, clises->auth_success, __FILE__, __LINE__);
	//
	// Clone QSASL information into the cnxpool: client_userid, client_domain
	if (!clises->anonymous_user) {
		dercursor cluid;
		dercursor cldom;
		clises->client_userid.derptr = NULL;
		clises->client_domain.derptr = NULL;
		clises->auth_failure = clises->auth_failure || !qsasl_get_client_userid (clises->dersasl, crs2bufp (&cluid));
		clises->auth_failure = clises->auth_failure || !qsasl_get_client_domain (clises->dersasl, crs2bufp (&cldom));
		clises->auth_failure = clises->auth_failure || !dermem_strdup (clises/*IMPLIED->cnxpool*/, crs2buf (cluid), (char **) &clises->client_userid.derptr);
		clises->auth_failure = clises->auth_failure || !dermem_strdup (clises/*IMPLIED->cnxpool*/, crs2buf (cldom), (char **) &clises->client_domain.derptr);
	//
	// Produce a +visitor+...@ANONYMOUS structured client identity
	} else {
		clises->client_domain.derptr = "ANONYMOUS";
		dermem_buffer_open (clises, 80, &clises->client_userid);
		size_t uidlen = 0;
		struct sockaddr_storage ss;
		socklen_t sslen = sizeof (ss);
		if (getpeername (clises->sox, (struct sockaddr *) &ss, &sslen) != 0) {
			ss.ss_family = AF_UNIX;		/* Invalidate */
		}
		uint8_t  *addr8;
		uint16_t *addr16;
		switch (ss.ss_family) {
		case AF_INET6:
			addr8 = (uint8_t *) &((struct sockaddr_in6 *) &ss)->sin6_addr;
			uidlen = snprintf (clises->client_userid.derptr, 65,
					"+visitor+ip6+%02x%02x+%02x%02x+%02x%02x+%02x%02x+%02x%02x+%02x%02x+%02x%02x+%02x%02x",
					addr8 [ 0], addr8 [ 1], addr8 [ 2], addr8 [ 3],
					addr8 [ 4], addr8 [ 5], addr8 [ 6], addr8 [ 7],
					addr8 [ 8], addr8 [ 9], addr8 [10], addr8 [11],
					addr8 [12], addr8 [13], addr8 [14], addr8 [15]);
			break;
		case AF_INET:
			addr8  = (uint8_t  *) &((struct sockaddr_in *) &ss)->sin_addr;
			addr16 = (uint16_t *) &((struct sockaddr_in *) &ss)->sin_port;
			uidlen = snprintf (clises->client_userid.derptr, 65,
					"+visitor+ip4+%d+%d+%d+%d+%d",
					addr8 [0], addr8 [1], addr8 [2], addr8 [3],
					ntohs (*addr16));
			break;
		default:
			uidlen = snprintf (clises->client_userid.derptr, 65, "+visitor");
			break;
		}
		if (uidlen > 64) {
			uidlen = 64;
		}
		clises->client_userid.derptr [uidlen++] = '\0';
		dermem_buffer_close (clises, uidlen, &clises->client_userid);
	}
	//
	// Produce a neat report with the constructed client identity
	if (clises->auth_success && !clises->auth_failure) {
		clises->client_userid.derlen = strlen ((char*)clises->client_userid.derptr);
		clises->client_domain.derlen = strlen ((char*)clises->client_domain.derptr);
		log_info ("daemon _secure_kip_server() ended SASL with client userid %.*s and domain %.*s at %s:%d", (int)clises->client_userid.derlen, clises->client_userid.derptr, (int)clises->client_domain.derlen, clises->client_domain.derptr, __FILE__, __LINE__);
	}
	//
	// Check that we are done; stay cautious, failure overshadows success
	assert ( clises->auth_success || clises->auth_failure );
	if (clises->auth_failure) {
		clises->auth_success = false;
		goto fail;
	}
	//
	// Successfully return the socket
	goto cleanup;
	//
	// Fail and return -1 to say so
fail:
	clises->sox = -1;
cleanup:
	qsasl_close (&clises->dersasl);
	log_info ("daemon recycles request pool after SASL");
	dermem_recycle (~0, clises->reqpool);
	log_debug ("daemon _secure_kip_server() returns socket = %d", clises->sox);
	return (clises->sox >= 0);
}


/* Handle the interaction over a KIP connection, starting with the TLS handshake.
 * The cnx socket is assumed to have (just) been returned from accept(), and
 * kip should not be NULL, and set to a base KIP context.  Return a kipsvc handle.
 *
 * FOR NOW, this routine closes the cnx before it ends, as well as the base KIP context.
 * THIS API MAY CHANGE IF WE LATER DECIDE ABOUT ANOTHER WORK PACKAGE SEPARATION.
 */
bool kipdaemon_handle (kipt_dctx kipd, int sox) {
	kipd->sox = sox;
	//
	// Secure the connection -- TODO: Probably take it out
	except_ifnot (fail, _secure_kip_server (kipd));
	//
	// Load the key for "kip/MASTER@srealm" key from the keytab
	assertxt (kipd->srealm.derptr != NULL, "No KIP Service realm");
	assertxt (kipd->srealm.derptr [kipd->srealm.derlen] == '\0', "Empty KIP Service realm");
	except_ifnot (fail, kipkey_fromvhosttab (kipd->kipctx, "kip", (char*)kipd->srealm.derptr, &kipd->keytab_keyid));
	kipd->keytab_keyno = (uint32_t) kipd->keytab_keyid;	/* trim to wire size */
	kipd->keytab_keyno_dercrs  = der_put_uint32 (kipd->keytab_keyno_derbuf,  kipd->keytab_keyno );
	kipd->keytab_encalg_dercrs = der_put_int32  (kipd->keytab_encalg_derbuf, kipd->keytab_encalg);
	//
	// Now iterate over requests
	while (kipd->sox >= 0) {
		//
		// Clear out the request memory pool on every round
		log_info ("KIP Daemon recycles request pool");
		dermem_recycle (~0, kipd->reqpool);
		//
		// Handle the connection
		log_info ("Handling one KIP Service request on socket %d", kipd->sox);
		static const derwalk pack_cmd_req [] = {
				DER_PACK_KIP_Service_KIP_Service_Request,
				DER_PACK_END
		};
		static const derwalk pack_cmd_rep [] = {
				DER_PACK_KIP_Service_KIP_Service_Response,
				DER_PACK_END
		};
		KIP_Request  cmd_req;
		KIP_Response cmd_rep;
		//TODO// Yes it will build, but not all commands are implemented yet (see _from/toservice)
		if (!_derio (kipd, process_cmd_req, pack_cmd_req, &cmd_req, pack_cmd_rep, &cmd_rep)) {
			break;
		}
		log_info ("Done w/  one KIP Service request on socket %d", kipd->sox);
	}
	//
	// Return success
	log_info ("Done w/  the KIP Service         on socket %d", kipd->sox);
	return true;
	//
	// Return failure
fail:
	dermem_recycle (~0, kipd->reqpool);
	if (kipd->sox >= 0) {
		close (kipd->sox);
		kipd->sox = -1;
	}
	return false;
}


/* Open a KIP Daemon handle for use with kipdaemon_handle().
 * Close it with kipdaemon_close().
 */
bool kipdaemon_open (kipt_ctx kip, kipt_dctx *out_kipd) {
	kipt_dctx kipd = NULL;
	if (!dermem_open (sizeof (struct kipt_daemon_context), (void **) &kipd)) {
		goto fail;
	}
	kipd->kipctx = kip;
	if (!dermem_open (0, &kipd->reqpool)) {
		goto fail;
	}
	//
	// Return success
	*out_kipd = kipd;
	return true;
	//
	// Return failure
fail:
	if (kipd != NULL) {
		dermem_close (          &kipd->reqpool);
		dermem_close ((void **) &kipd         );
	}
	*out_kipd = NULL;
	return false;
}


/* Close the KIP Daemon handle that was previously obtained with
 * a successful call to kipdaemon_handle().
 */
void kipdaemon_close (kipt_dctx kipd) {
	dermem_close (          &kipd->reqpool);
	dermem_close ((void **) &kipd         );
}



/* Initialise the KIP Daemon module.
 *
 * During operation, the following environment variables are used:
 *
 *  - KIP_VARDIR defaults to /var/lib/kip and should hold keymud in
 *    files named by requested client realms.
 *
 *  - KIP_KEYTAB defaults to the system default keytab but may set
 *    a more suitable keytab to use.
 *
 */
bool kipdaemon_init (void) {
	return true;
}


/* Finalise the KIP Daemon module.
 *
 * This call does not fail.
 */
void kipdaemon_fini (void) {
	;
}
