/* Xover SASL -- Simple Authentication with Secure Xover.
 *
 * This is a wrapper around Quick DER that adds the
 * SXOVER mechanism, using the KIP Service for key
 * management.  Only a few API calls are wrapped, but
 * most are defined as aliases in the header file.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include <time.h>

#include <arpa2/socket.h>

#include "arpa2/kip.h"

#include <arpa2/quick-der.h>
#include <quick-der/KIP-Service.h>
#include <quick-der/SASL-RealmCrossover.h>

#include <arpa2/com_err.h>
#include <arpa2/except.h>
#include "arpa2/xover-sasl.h"

#include "kip-dbg.h"


#define SXOVER_NAME  "SXOVER-PLUS"
#define SXOVER_LEN   (strlen (SXOVER_NAME))


/* Type shorthands for DER structures
 */
typedef DER_OVLY_KIP_Service_KIP_Opaque_Data KIP_Opaque_Data;
typedef DER_OVLY_SASL_RealmCrossover_C2S_Init_2 C2S_Init_2;
typedef DER_OVLY_SASL_RealmCrossover_S2C_Init S2C_Init;
typedef DER_OVLY_SASL_RealmCrossover_C2S_Cont C2S_Cont;
typedef DER_OVLY_SASL_RealmCrossover_S2C_Cont S2C_Cont;


/* The fixed dercursor for the mechanism name SXOVER_NAME.
 */
static dercursor sxover = {
	.derlen = SXOVER_LEN,
	.derptr = SXOVER_NAME
};


/* Packing instructions for Quick DER.
 */
static const derwalk pack_opaq [] = {
	DER_PACK_KIP_Service_KIP_Opaque_Data,
	DER_PACK_END
};
static const derwalk pack_c2s_init2 [] = {
	DER_PACK_SASL_RealmCrossover_C2S_Init_2,
	DER_PACK_END
};
static const derwalk pack_s2c_init [] = {
	DER_PACK_SASL_RealmCrossover_S2C_Init,
	DER_PACK_END
};
static const derwalk pack_c2s [] = {
	DER_PACK_SASL_RealmCrossover_C2S_Cont,
	DER_PACK_END
};
static const derwalk pack_s2c [] = {
	DER_PACK_SASL_RealmCrossover_S2C_Cont,
	DER_PACK_END
};


/* Extra information appended to the Quick SASL data
 * to store any SXOVER mechanism specifics.
 */
struct xsaslt_extra {
	//
	// Whether SXOVER is disabled, is wrapping, has a key, passed wrapped mechlist
	bool disabled;
	bool wrapping;
	int  keysetup;
	bool wrapmech;
	//
	// KIP context, with KIP Service -- no KIP Daemon
	kipt_ctx kip;
	//
	// KIP session encryption keys (after keysetup)
	kipt_keyid k0id, k1id, k2id;
	//
	// Recipient for SXOVER (after keysetup)
	dercursor recipient;
	//
	// Next counter value (after keysetup)
	//CTR_GONE// uint32_t nextctr;
};


/* This would be a hack, if its implementation was not
 * so consisely monitored.
 *
 * Given a Quick SASL implementation, the size of the
 * first allocation should be fixed.  This means that we
 * can use that offset to append information.  This is
 * simpler than encapsulating a reference and then pass
 * through all functions.  We may need to fall back to
 * that at some moment, but not yet.
 *
 * Meanwhile, the offset is measured once and then
 * enforced to be the same on every following call.
 * Given the fixed implementation underlying this
 * library, that would be reasonable.
 */
static size_t ofs_skip_qs = 0;


/* Find the offset relative from a QuickSASL pointer.
 */
#define skip_qs(qs) ((struct xsaslt_extra *) (((uint8_t *) qs) + ofs_skip_qs))


/* Allocate our extra data structure, and require that
 * it is at a fixed offset from the original one.
 *
 * See skip_qs() and ofs_skip_qs for more information.
 */
static bool append_xsaslt_extra (QuickSASL qs) {
	//
	// Find the "extra" structure through allocation
	uint8_t *xsaslt_extra = NULL;
	if (!qsasl_alloc (qs, sizeof (struct xsaslt_extra), &xsaslt_extra)) {
		return false;
	}
	//
	// The "extra" data should sit right after qs
	size_t ofs_found_now = ((intptr_t) xsaslt_extra) - ((intptr_t) qs);
	//
	// Ensure fixed distance from qs to "extra"
	if (ofs_skip_qs == 0) {
		/* First use, learn and continue */
		ofs_skip_qs = ofs_found_now;
	} else {
		/* Repeated use, enforce the offset */
		void *xsaslt_expected = skip_qs (qs);
		assert (xsaslt_extra == xsaslt_expected);
	}
	//
	// Return success -- we have the extra data, all is well
	return true;
}


/* Wrap around qsasl_client() by appending extra data.
 */
bool xsasl_client (XoverSASL *out_xs,
		const char *service,
		void (*opt_cb) (void *cbdata, XoverSASL xs, xsaslt_state newstate),
		void *cbdata,
		unsigned flags) {
	if (!qsasl_client (out_xs, service, (void *) opt_cb, cbdata, flags)) {
		return false;
	}
	if (!append_xsaslt_extra (*out_xs)) {
		qsasl_close (out_xs);
		return false;
	}
	return true;
}


/* Wrap around qsasl_server() by appending extra data.
 */
bool xsasl_server (XoverSASL *out_xs,
		const char *service,
		void (*opt_cb) (void *cbdata, XoverSASL xs, xsaslt_state newstate),
		void *cbdata,
		unsigned flags) {
	if (!qsasl_server (out_xs, service, (void *) opt_cb, cbdata, flags)) {
		return false;
	}
	if (!append_xsaslt_extra (*out_xs)) {
		qsasl_close (out_xs);
		return false;
	}
	return true;
}


/* Close the Client/Server Connection Object.
 *
 * This frees the SASL connection handle, plus
 * any data that was allocated in its pool.
 *
 * After this call, *inout_xs will be NULL.
 */
void xsasl_close (XoverSASL *inout_xs) {
	/* Retrieve the extra storage */
	struct xsaslt_extra *extra = skip_qs (*inout_xs);
	/* Cleanup the KIP context, if any */
	if (extra->kip) {
		kipctx_close (extra->kip);
	}
	/* Close the underlying Quick SASL context */
	qsasl_close (inout_xs);
}


/* Check if a dercursor contains the string "SXOVER-PLUS".
 */
static bool mechlist_has_sxover (dercursor mechlist, size_t *where) {
	int ofs = mechlist.derlen - SXOVER_LEN;
	while (ofs >= 0) {
		// Search backwards for a space or start of string
		do {
			ofs--;
			if (ofs < 0) {
				break;
			}
		} while (mechlist.derptr [ofs] != ' ');
		ofs++;
		// We are now at the beginning of a mechanism name
		if (memcmp (mechlist.derptr + ofs, SXOVER_NAME, SXOVER_LEN) == 0) {
			// The beginning looks good; check string ending
			if ((ofs+SXOVER_LEN == mechlist.derlen) || (mechlist.derptr [ofs+SXOVER_LEN] == ' ')) {
				if (where != NULL) {
					*where = ofs;
				}
				return true;
			}
		}
		// Make sure to progress in the next looping
		ofs--;
	}
	return false;
}


/* Get the Mechanism(s) that is/are currently possible.
 *
 * Xover SASL adds the SXOVER mechanism so it is considered
 * available to anyone using this API.  If was are
 * already wrapping Xover SASL, we will not even make the
 * backcall and instead return just SXOVER.
 *
 * Before the client chooses, there may be any number
 * of possible mechanisms; after the choice there is
 * only one.  In both cases, a single string is used to
 * represent the mechanism(s), with spaces to separate
 * multiple options.
 */
bool xsasl_get_mech (XoverSASL xs, dercursor *out_mech) {
	dercursor inner_mech;
	/* Retrieve the extra storage */
	struct xsaslt_extra *extra = skip_qs (xs);
	/* If SXOVER is wrapping, return just that */
	if (extra->wrapping) {
		assert (extra->wrapping && !extra->disabled);
		*out_mech = sxover;
		return true;
	}
	/* Retrieve inner array */
	if (!qsasl_get_mech (xs, crs2bufp (&inner_mech))) {
		return false;
	}
	/* Reject the inner array if it contains SXOVER */
	if (mechlist_has_sxover (inner_mech, NULL)) {
		"Configuration error... SXOVER wraps around SXOVER";
		return false;
	}
	/* Return without SXOVER if it was disabled */
	if (extra->disabled) {
		*out_mech = inner_mech;
		log_debug ("SXOVER disabled, mechlist follows inner: %s", inner_mech);
		return true;
	}
	/* Allocate a new dercursor to return */
	dercursor outer_mech;
	outer_mech.derptr = NULL;
	outer_mech.derlen = SXOVER_LEN + 1 + inner_mech.derlen;
	if (!dermem_alloc ((void *) xs, outer_mech.derlen, (void **) &outer_mech.derptr)) {
		return false;
	}
	/* Fill the new array with SXOVER and more */
	memcpy (outer_mech.derptr, SXOVER_NAME, SXOVER_LEN+1);
	if (inner_mech.derlen > 0) {
		outer_mech.derptr [SXOVER_LEN] = ' ';
		memcpy (outer_mech.derptr + SXOVER_LEN + 1, inner_mech.derptr, inner_mech.derlen);
	} else {
		outer_mech.derlen--;
	}
	/* Return the extended string and signal success */
	log_debug ("SXOVER enabled, mechlist extended: %s", outer_mech);
	*out_mech = outer_mech;
	return true;
}


/* Set the Mechanism(s) to be considered possible.
 *
 * We look into the list to decide whether we may wrap
 * the SXOVER mechanism.  If not, we pass the list on
 * to the wrapped Quick SASL layer; if SXOVER is listed
 * we will defer this and wait for the inner layer's
 * offering.
 *
 * Before the client chooses, there may be any number
 * of possible mechanisms; after the choice there is
 * only one.  In both cases, a single string is used to
 * represent the mechanism(s), with spaces to separate
 * multiple options.
 */
bool xsasl_set_mech (XoverSASL xs, dercursor mech) {
	/* Retrieve the extra storage */
	struct xsaslt_extra *extra = skip_qs (xs);
	/* Disable if the SXOVER mechanism is not offered */
	if (!mechlist_has_sxover (mech, NULL)) {
		extra->disabled = true;
		assert (extra->disabled && !extra->wrapping);
		log_debug ("SXOVER was not offered: %s", mech);
		return qsasl_set_mech (xs, crs2buf (mech));
	}
	/* SXOVER is offered, hope we are enabled */
	if (extra->disabled) {
		size_t where;
		while (mechlist_has_sxover (mech, &where)) {
			if (where > 0) {
				// Replace preceding space, if there is one
				where--;
			}
			mech.derlen -= 1 + SXOVER_LEN;
			if (mech.derlen > where) {
				memcpy (mech.derptr + where, mech.derptr + where + 1 + SXOVER_LEN, mech.derlen - where);
			}
		}
		log_debug ("SXOVER removed: %s", mech);
		return qsasl_set_mech (xs, crs2buf (mech));
	}
	/* If we already knew, we can quietly exit */
	if (extra->wrapping) {
		return true;
	}
	/* Initiate the wrapping of data in SXOVER */
	extra->wrapping = true;
	log_debug ("SXOVER found, will wrap: %s", mech);
	/* Return success */
	return true;
}


/* Use the extra data for decryption.
 */
static bool extra_decrypt (XoverSASL xs, kipt_keyid kid, const dercursor crypt, dercursor *plain) {
	uint32_t decrypt_len = ~0;
	struct xsaslt_extra *extra = skip_qs (xs);
	memset (plain, 0, sizeof (dercursor));
	if (crypt.derptr == NULL) {
		return true;
	}
#if 0
	/* Nobody does this, the routine doesn't even allow it */
	/* TODO: Use a dermem_buffer instead */
	if (kipdata_down (extra->kip, kid,
				crypt.derptr, crypt.derlen,
				NULL, 0, &decrypt_len)
			|| (errno != KIPERR_OUTPUT_SIZE)) {
		return false;
	}
#else
	/* Slightly over-estimate the memory needed for plaintext */
	decrypt_len = crypt.derlen;
#endif
	if (!dermem_alloc (xs, decrypt_len, (void **) &plain->derptr)) {
		return false;
	}
	plain->derlen = decrypt_len;
	print_block ("decrypt from", crypt.derptr, crypt.derlen, false);
	log_debug ("kipdata_down (ctx=%p, keyid=%lx, crypt=%p, cryptlen=%d, plain=%p, plainmaxlen=%d, &plainlen=%p)", extra->kip, (long) kid, crypt.derptr, crypt.derlen, plain->derptr, decrypt_len, &decrypt_len);
	assert (kipdata_down (extra->kip, kid,
				crypt.derptr, crypt.derlen,
				plain->derptr, decrypt_len, &decrypt_len));
	plain->derlen = decrypt_len;
	log_debug ("decrypted to %zd == %d bytes", plain->derlen, decrypt_len);
	print_block ("decrypted to", plain->derptr, plain->derlen, false);
	return true;
}


/* Use the extra data for encryption.
 */
static bool extra_encrypt (XoverSASL xs, kipt_keyid kid, const dercursor plain, dercursor *crypt) {
	uint32_t out_len = ~0;
	struct xsaslt_extra *extra = skip_qs (xs);
	if (plain.derptr == NULL) {
		return true;
	}
	memset (crypt, 0, sizeof (dercursor));
	if (kipdata_up (extra->kip, kid,
				plain.derptr, plain.derlen,
				NULL, 0, &out_len)
			|| (errno != KIPERR_OUTPUT_SIZE)) {
		return false;
	}
	if (!dermem_alloc (xs, out_len, (void **) &crypt->derptr)) {
		return false;
	}
	crypt->derlen = out_len;
	log_debug ("encrypt from %zd bytes", plain.derlen);
	print_block ("encrypt from", plain.derptr, plain.derlen, false);
	log_debug ("kipdata_up(ctx=%p, keyid=%lx, plain=%p, plainlen=%zd, crypt=%p, &cryptlen=%p)", extra->kip, (long) kid, plain.derptr, plain.derlen, crypt->derptr, crypt->derlen);
	assert (kipdata_up (extra->kip, kid,
				plain.derptr, plain.derlen,
				crypt->derptr, out_len, &out_len));
	print_block ("encrypted to", crypt->derptr, crypt->derlen, false);
	assert (crypt->derlen == out_len);
	return true;
}


/* Cleverly concatenate two static buffers.  If they are
 * allocated in immediate succession, it is often possible
 * to return a buffer that overlays these two.  If not, a
 * new buffer must be allocated with copies of the original
 * buffers.
 */
static inline bool clever_concat (XoverSASL xs, dercursor one, dercursor two, dercursor *three) {
	if (two.derlen == 0) {
		*three = one;
	}
	if (one.derlen == 0) {
		*three = two;
	}
	if (one.derptr + one.derlen == two.derptr) {
		three->derptr = one.derptr;
		three->derlen = one.derlen + two.derlen;
		return true;
	}
	memset (three, 0, sizeof (*three));
	if (!dermem_alloc (xs, one.derlen + two.derlen, (void **) &three->derptr)) {
		return false;
	}
	three->derlen = one.derlen + two.derlen;
	memcpy (three->derptr             , one.derptr, one.derlen);
	memcpy (three->derptr + one.derlen, two.derptr, two.derlen);
	return true;
}


/* Extract environment variables and apply them to the KIP Service
 * linked from the XoverSASL extra data.
 */
static void setup_kipservice_clientid (XoverSASL xs) {
	struct xsaslt_extra *extra = skip_qs (xs);
	char *realm  = getenv ("KIPSERVICE_CLIENT_REALM");
	char *login  = getenv ("KIPSERVICE_CLIENTUSER_LOGIN");
	char *access = getenv ("KIPSERVICE_CLIENTUSER_ACL");
	if (realm != NULL) {
		kipservice_set_client_realm (extra->kip, realm);
	}
	if (login != NULL) {
		kipservice_set_clientuser_login (extra->kip, login);
	}
	if (access != NULL) {
		kipservice_set_clientuser_acl (extra->kip, access);
	}
}


/* Make a step as a SASL Client.
 *
 * This is a wrapper around the Quick SASL function that
 * adds SXOVER-PLUS mechanism support.
 *
 * The return value is true for success passing through the
 * programming steps, as an accumulated assertion of the entire
 * process.  It does _not_ signify an authentication result.
 *
 * Authentication results are dissiminated through the
 * success and failure flags, and can be requested with
 * qsasl_get_outcome() or through the callback facility.
 *
 * When neither success or failure is set after this step,
 * then authentication needs to continue another step.
 * The two flags should not ever occur together, but it is
 * cautious to treat failure as the leading one when the
 * flow has ended, so when either-or-both are flagged.
 */
bool xsasl_step_client (XoverSASL xs,
			dercursor s2c,
			dercursor *out_mech, dercursor *out_c2s) {
	bool ok = true;
	log_debug ("Entered xasl_step_client");
	/* Retrieve the extra storage */
	struct xsaslt_extra *extra = skip_qs (xs);
	/* The mechanism must have been selected before the first step */
	assert (extra->disabled || extra->wrapping);
	/* If we are disabled / not wrapping, pass on the the inner layer */
	if (extra->disabled) {
		assert (extra->disabled && !extra->wrapping);
		log_debug ("XoverSASL disabled, xsasl_step_client() -> qsasl_step_client()");
		ok = ok && qsasl_step_client (xs, crs2buf (s2c), crs2bufp (out_mech), crs2bufp (out_c2s));
		if (!ok) log_debug ("ok==%d at %s:%d", ok, __FILE__, __LINE__);
		return ok;
	}
	/* Before wrapping, we need to send the session key */
	if (extra->keysetup <= 0) {
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		ok = ok && kipctx_open (&extra->kip);
		if (!ok) log_debug ("ok==%d at %s:%d", ok, __FILE__, __LINE__);
		log_debug ("Opened client KIP context %p", extra->kip);
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		uint8_t *svckeymud = NULL;
		uint32_t svckeymudlen = ~0;
		time_t now = time (NULL);
		char *realm = getenv ("KIP_REALM");
		assert (realm != NULL);
		char at_realm [1 + strlen (realm) + 1];
		at_realm [0] = '@';
		strcpy (at_realm + 1, realm);
		char *whitelist [] = { at_realm, NULL };
		log_debug ("Found KIP_REALM=\"%s\"", realm);
		log_debug ("Setting up a session key for later wrapping");
		ok = ok && (now > 0) && (now - 120 > 0) && (realm != NULL) && (*realm != '\0');
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		dercursor rlm = {
			.derptr = ok ?         realm  : 0,
			.derlen = ok ? strlen (realm) : 0
		};
		ok = ok && dermem_strdup (xs, rlm, (char **) &extra->recipient.derptr);
		extra->recipient.derlen = rlm.derlen;
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		//CTR_GONE// ok = ok && kipdata_random (extra->kip, 4, (uint8_t *) &extra->nextctr);
		//CTR_GONE// if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		ok = ok && kipkey_generate (extra->kip, 17 /* aes128-cts-hmac-sha1-96 */, 11, &extra->k0id);
		log_debug ("Generated key with keyid=%lx", (long) extra->k0id);
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		//
		// Map the key, svc_ok remembers whether KIP Service opened
		bool svc_ok = ok && kipservice_start (extra->kip);
		ok = ok && svc_ok;
		setup_kipservice_clientid (xs);
		ok = ok && kipservice_tomap (extra->kip,
					realm,
					extra->k0id,
					NULL, whitelist,
					now - 120, now + 180,	/* minus 2min, plus 3min */
					&svckeymud, &svckeymudlen);
		print_block ("kipservice_tomap", svckeymud, svckeymudlen, false);
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		//
		// Decode the DER parts in the svckeymud
		dercursor svckeymudcrs = {
			.derlen = svckeymudlen,
			.derptr = svckeymud
		};
		KIP_Opaque_Data opaq;
		uint32_t keyno;
		int32_t encalg;
		log_debug ("Unpacking %zd bytes starting with tag 0x%02x if %d", svckeymudcrs.derlen, svckeymudcrs.derptr ? *svckeymudcrs.derptr : 0x00, ok);
		ok = ok && (der_unpack (&svckeymudcrs, pack_opaq, (dercursor *) &opaq, 1) == 0);
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		dercursor  c2s_init1;
		memset (&c2s_init1, 0, sizeof (c2s_init1));
		const char *chanbindname = NULL;
		ok = ok && xsasl_get_chanbind_name (xs, &chanbindname);
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		c2s_init1.derlen = 2 + strlen (ok ? chanbindname : "") + 2 + strlen (realm) + 1;
		ok = ok && dermem_alloc (xs, c2s_init1.derlen, (void **) &c2s_init1.derptr);
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		if (ok) {
			// Print a string without trailing '\0'
			char *here = c2s_init1.derptr;
			*here++ = 'p';
			*here++ = '=';
			memcpy (here, chanbindname, strlen (chanbindname));
			here += strlen (chanbindname);
			*here++ = ',';
			*here++ = ',';
			memcpy (here, realm, strlen (realm));
			here += strlen (realm);
			*here++ = ',';
		}
		C2S_Init_2 c2s_init2;
		memset (&c2s_init2, 0, sizeof (c2s_init2));
		//CTR_GONE// der_buf_uint32_t inictrbuf;
		c2s_init2.keyno  = opaq.keyno  ;
		c2s_init2.encalg = opaq.encalg ;
		c2s_init2.keymap = opaq.payload;
#ifdef CLIRND_USELESSLY_ADDED_TO_CLIENT_ORIGINATED_KEYMAP_ENTROPY
		c2s_init2.clirnd.derlen = 32;
		ok = ok && dermem_alloc (xs, 32, (void **) &c2s_init2.clirnd.derptr);
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		ok = ok && kipdata_random (extra->kip, 32, c2s_init2.clirnd.derptr);
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
#else
		c2s_init2.clirnd.derptr = "";
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
#endif
		//CTR_GONE// c2s_init2.inictr       = der_put_uint32 (inictrbuf, extra->nextctr++);
		//
		// Construct the SXOVER token with the initial response
		*out_mech = sxover;
		memset (out_c2s, 0, sizeof (dercursor));
		ok = ok && dermem_pack (xs, pack_c2s_init2, (dercursor *) &c2s_init2, out_c2s);
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		ok = ok && clever_concat (xs, c2s_init1, *out_c2s, out_c2s);
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		print_block ("c2s_init", out_c2s->derptr, out_c2s->derlen, false);
		//
		// Add the complete C2S-Init message as salt to the session key
		const kipt_keyid inkey [1] = { extra->k0id       };
		const uint8_t *salt    [1] = { out_c2s->derptr };
		const uint32_t saltlen [1] = { out_c2s->derlen };
		print_block ("Client.mixer C2S-Init", salt [0], saltlen [0], false);
		ok = ok && kipkey_mixer (extra->kip,
					1, inkey, salt, saltlen,
					extra->k0id + 1, &extra->k1id);
		//
		// Cleanup KIP Service if we opened it, regardless of "ok" (retain KIP Core)
		if (svc_ok) {
			kipservice_stop (extra->kip);
		}
		//
		// Return success
		extra->keysetup = 2;
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		return ok;
	}
	/* Use the session key K1 or K2 to decrypt s2c */
	dercursor dn_s2c;
	kipt_keyid kid = (extra->keysetup <= 2) ? extra->k1id : extra->k2id;
	log_debug ("Decrypting with session key %lx", (long) kid);
	if (!extra_decrypt (xs, kid, s2c, &dn_s2c)) {
		log_debug ("xsasl_step_client() failed s2c decryption");
		return false;
	}
	/* We are wrapping, have keys, but did we get wrapped mechs? */
	dercursor token_s2c;
	if (!extra->wrapmech) {
		/* We did not get wrapped mechanisms, so this is SXOVER-Initial-Challenge */
		S2C_Init s2c_init;
		dercursor s2c_init_der = dn_s2c;
		ok = ok && (der_unpack (&dn_s2c, pack_s2c_init, (dercursor *) &s2c_init, 1) == 0);
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		//CTR_GONE// /* Assure the value of the counter */
		//CTR_GONE// uint32_t ctr;
		//CTR_GONE// ok = ok && (der_get_uint32 (s2c_init.ctr, &ctr) == 0) && (ctr == extra->nextctr++);
		//CTR_GONE// if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		//REALM_GONE// /* Assure the value of the realm/recipient */
		//REALM_GONE// ok = ok && (der_cmp (s2c_init.realm, extra->recipient) == 0);
		//REALM_GONE// log_debug ("Comparing realm \"%.*s\" to \"%.*s\"", s2c_init.realm.derlen, s2c_init.realm.derptr, extra->recipient.derlen, extra->recipient.derptr);
		//REALM_GONE// if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		/* Setup the server-sent mechanism list */
		log_debug ("Setting up wrapped Quick SASL mechanisms %.*s", s2c_init.mechlist.derlen, s2c_init.mechlist.derptr);
		ok = ok && qsasl_set_mech (xs, crs2buf (s2c_init.mechlist));
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		/* Derive the K2 key from K1 */
		dercursor k2salt;
		dercursor cbval;
		ok = ok && xsasl_get_chanbind_value (xs, crs2bufp (&cbval));
		ok = ok && clever_concat (xs, s2c_init_der, cbval, &k2salt);
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		const kipt_keyid inkey [1] = { extra->k1id   };
		const uint8_t *salt    [1] = { k2salt.derptr };
		const uint32_t saltlen [1] = { k2salt.derlen };
		print_block ("Client.mixer S2C-Init + Channel Binding", salt [0], saltlen [0], false);
		ok = ok && kipkey_mixer (extra->kip,
					1, inkey, salt, saltlen,
					extra->k1id + 1, &extra->k2id);
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		if (ok) {
			extra->keysetup++;
		}
		/* This s2c was SXOVER-wrapped, but held a pre-SASL message to set mechanisms */
		memset (&token_s2c, 0, sizeof (token_s2c));
	} else {
		/* We alread got wrapped mechanisms, so this is SXOVER-Challenge */
		S2C_Cont chal;
		ok = ok && (der_unpack (&dn_s2c, pack_s2c, (dercursor *) &chal, 1) == 0);
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		//CTR_GONE// /* Assure the value of the counter */
		//CTR_GONE// uint32_t ctr;
		//CTR_GONE// ok = ok && (der_get_uint32 (chal.ctr, &ctr) == 0) && (ctr == extra->nextctr++);
		//CTR_GONE// if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		/* Copy the challenge's token for further processing */
		token_s2c = chal.s2c.token;
	}
	if (!ok) {
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		return false;
	}
	/* SXOVER wraps a normal internal action, pass it down */
	dercursor up_out_mech = {
		.derptr = NULL,
		.derlen = 0
	};
	dercursor up_out_c2s = {
		.derptr = NULL,
		.derlen = 0
	};
	if (!qsasl_step_client (xs, crs2buf (token_s2c), crs2bufp (&up_out_mech), crs2bufp (&up_out_c2s))) {
		return false;
	}
	/* Continue wrapping mechanisms, if this is the inital response packed into SXOVER */
	if (!extra->wrapmech) {
		assert ((!extra->wrapmech) && (up_out_mech.derptr != NULL));
		extra->wrapmech = ok;
	} else {
		assert (up_out_mech.derptr == NULL);
	}
	log_debug ("extra->wrapmech=%d, up_out_mech.derptr#len=%p#%zd at %s:%d", extra->wrapmech, up_out_mech.derptr, up_out_mech.derlen, __FILE__, __LINE__);
	/* Pack the up_out_c2s into Initial-Response der_out_c2s before encrypting it */
	C2S_Cont c2s;
	memset (&c2s, 0, sizeof (c2s));
	//CTR_GONE// der_buf_uint32_t ctrbuf;
	//CTR_GONE// c2s.ctr = der_put_uint32 (ctrbuf, extra->nextctr++);
	if (up_out_c2s.derptr == NULL) {
		/* Active NULL variant in c2s token */
		c2s.c2s.no_token.derptr = "";
	} else {
		c2s.c2s.token = up_out_c2s;
	}
	c2s.mechsel = up_out_mech;
	dercursor der_out_c2s = {
		.derptr = NULL,
		.derlen = 0
	};
	ok = ok && dermem_pack (xs, pack_c2s, (dercursor *) &c2s, &der_out_c2s);
	if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
	print_block ("SXOVER-Response", der_out_c2s.derptr, der_out_c2s.derlen, false);
	/* Encrypt out_c2s with our wrapping keys -- no out_mech for nested Quick SASL */
	if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
	ok = ok && extra_encrypt (xs, extra->k2id, der_out_c2s,  out_c2s );
	if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
	memset (out_mech, 0, sizeof (dercursor));
	if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
	/* Return success */
	return ok;
}


/* Make a step as a SASL Server.
 *
 * This is a wrapper around the Quick SASL function that
 * adds SXOVER-PLUS mechanism support.
 *
 * The return value is true for success passing through the
 * programming steps, as an accumulated assertion of the entire
 * process.  It does _not_ signify an authentication result.
 *
 * Authentication results are dissiminated through the
 * success and failure flags, and can be requested with
 * qsasl_get_outcome() or through the callback facility.
 *
 * When neither success or failure is set after this step,
 * then authentication needs to continue another step.
 * The two flags should not ever occur together, but it is
 * cautious to treat failure as the leading one when the
 * flow has ended, so when either-or-both are flagged.
 */
bool xsasl_step_server (XoverSASL xs,
			dercursor c2s,
			dercursor *out_s2c) {
	bool ok = true;
	/* Retrieve the extra storage */
	struct xsaslt_extra *extra = skip_qs (xs);
	/* The mechanism must have been selected before the first step */
	assert (extra->disabled || extra->wrapping);
	/* If we are disabled / not wrapping, pass on the the inner layer */
	if (extra->disabled) {
		assert (extra->disabled && !extra->wrapping);
		log_debug ("xsasl_step_server() redirects to qsasl_step_server(): SXOVER is disabled");
		return qsasl_step_server (xs, crs2buf (c2s), crs2bufp (out_s2c));
	}
	/* Before wrapping, we need to receive the keymap */
	char    *realm    = "";
	uint32_t realmlen = 0;
	dercursor c2sgo = c2s;
	if (extra->keysetup <= 0) {
		log_debug ("xsasl_step_server() keysetup (c2s as SXOVER-Initial-Response)");
		const char *name;
		ok = ok && xsasl_get_chanbind_name (xs, &name);
		if ((c2sgo.derlen > 14) && (memcmp (c2sgo.derptr, "p=tls-unique,,", 14) == 0)) {
			ok = ok && (strcmp(name, QSA_TLS_UNIQUE) == 0);
			c2sgo.derptr += 14;
			c2sgo.derlen -= 14;
			log_debug ("xsasl_step_server() channel binding type tls-unique");
		} else if ((c2sgo.derlen > 24) && (memcmp (c2sgo.derptr, "p=tls-server-end-point,,", 24) == 0)) {
			ok = ok && (strcmp(name, QSA_TLS_SERVER_END_POINT) == 0);
			c2sgo.derptr += 24;
			c2sgo.derlen -= 24;
			log_debug ("xsasl_step_server() channel binding type tls-server-end-point");
		} else if ((c2sgo.derlen > 24) && (memcmp (c2sgo.derptr, "p=x-manual-session-key,,", 24) == 0)) {
			ok = ok && (strcmp(name, QSA_X_MANUAL_SESSION_KEY) == 0);
			c2sgo.derptr += 24;
			c2sgo.derlen -= 24;
			log_debug ("xsasl_step_server() channel binding type x-manual-session-key");
		} else {
			ok = false;
			if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		}
		realm = c2sgo.derptr;
		uint8_t *comma = ok ? memchr (c2sgo.derptr, ',', c2sgo.derlen-1) : NULL;
		ok = ok && (comma != NULL);
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		realmlen = ok ? (comma - c2sgo.derptr) : 0;
		c2sgo.derptr += 1 + realmlen;
		c2sgo.derlen -= 1 + realmlen;
		extra->recipient.derptr = realm;
		extra->recipient.derlen = realmlen;
		ok = ok && kipctx_open (&extra->kip);
		log_debug ("Opened server KIP context %p", extra->kip);
		C2S_Init_2 c2s_init2;
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		ok = ok && (der_unpack (&c2sgo, pack_c2s_init2, (dercursor *) &c2s_init2, 1) == 0);
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		//REALM_GONE// ok = ok && dermem_strdup (xs, c2s_init2.realm, (char **) &extra->recipient.derptr);
		//REALM_GONE// if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		//REALM_GONE// extra->recipient.derlen = c2s_init2.realm.derlen;
		//CTR_GONE// ok = ok && (der_get_uint32  (c2s_init2.inictr, &extra->nextctr) == 0);
		//CTR_GONE// if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		if (!ok) {
			return false;
		}
		//CTR_GONE// extra->nextctr++;
		KIP_Opaque_Data opaq;
		opaq.realm   = extra->recipient;
		opaq.keyno   = c2s_init2.keyno;
		opaq.encalg  = c2s_init2.encalg;
		opaq.payload = c2s_init2.keymap;
		dercursor svckeymud;
		memset (&svckeymud, 0, sizeof (svckeymud));
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		ok = ok && dermem_pack (xs, pack_opaq, (dercursor *) &opaq, &svckeymud);
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		kipt_keyid *mappedkeys = NULL;
		uint32_t mappedkeyslen = ~0;
		//
		// Unmap the key, svc_ok remembers whether KIP Service opened
		if (!ok) log_debug ("ok==%d at %s:%d", ok, __FILE__, __LINE__);
		bool svc_ok = ok && kipservice_start (extra->kip);
		ok = ok && svc_ok;
		if (!ok) log_debug ("ok==%d at %s:%d", ok, __FILE__, __LINE__);
		setup_kipservice_clientid (xs);
		print_block ("kipservice_frommap", svckeymud.derptr, svckeymud.derlen, false);
		ok = ok && kipservice_frommap (extra->kip,
					svckeymud.derptr, svckeymud.derlen,
					&mappedkeys, &mappedkeyslen);
		if (!ok) log_debug ("ok==%d at %s:%d", ok, __FILE__, __LINE__);
		//
		// Harvest the keyid before the KIP Service cleans up the memory
		ok = ok && (mappedkeyslen == 1);
		if (!ok) log_debug ("ok==%d at %s:%d", ok, __FILE__, __LINE__);
		extra->k0id = ok ? mappedkeys [0] : -1;
		log_debug ("mapped keyid = 0x%lx", (long) extra->k0id);
		//
		// Add the complete C2S-Init message as salt to the session key
		const kipt_keyid inkey [1] = { extra->k0id       };
		const uint8_t *salt    [1] = { c2s.derptr };
		const uint32_t saltlen [1] = { c2s.derlen };
		//TODO// use extra->kip and extra->keyid or another?
		print_block ("Server.mixer C2S-Init", salt [0], saltlen [0], false);
		ok = ok && kipkey_mixer (extra->kip,
					1, inkey, salt, saltlen,
					extra->k0id + 1, &extra->k1id);
		//
		// Cleanup KIP Service if we opened it, regardless of "ok" (retain KIP Core)
		if (svc_ok) {
			kipservice_stop (extra->kip);
		}
		if (!ok) {
			return false;
		}
		extra->keysetup = 2;
		// continue to wrap mechanisms
	}
	/* We are wrapping, have keys, but did we send wrapped mechs? */
	if (!extra->wrapmech) {
		log_debug ("xsasl_step_server() wrapmech sending");
		/* Initialise the response structure */
		S2C_Init s2c_init;
		memset (&s2c_init, 0, sizeof (s2c_init));
		/* Fill the structure */
		//CTR_GONE// der_buf_uint32_t ctrbuf;
		//CTR_GONE// s2c_init.ctr = der_put_uint32 (ctrbuf, extra->nextctr++);
		//REALM_GONE// s2c_init.realm = extra->recipient;
		ok = ok && qsasl_get_mech (xs, crs2bufp (&s2c_init.mechlist));
		log_debug ("Got mechanism string with %.*s", s2c_init.mechlist.derlen, s2c_init.mechlist.derptr);
		if (!ok) log_debug ("ok==%d at %s:%d", ok, __FILE__, __LINE__);
		s2c_init.srvrnd.derlen = 32;
		ok = ok && dermem_alloc (xs, 32, (void **) &s2c_init.srvrnd.derptr);
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		ok = ok && kipdata_random (extra->kip, 32, s2c_init.srvrnd.derptr);
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		/* Now pack the c2s_init2 structure into pre_c2s */
		dercursor der_s2c_init = {
			.derptr = NULL,
			.derlen = 0
		};
		ok = ok && dermem_pack (xs, pack_s2c_init, (dercursor *) &s2c_init, &der_s2c_init);
		print_block ("s2c_init", der_s2c_init.derptr, der_s2c_init.derlen, false);
		/* Derive the K2 key from K1 */
		dercursor k2salt;
		dercursor cbval;
		ok = ok && xsasl_get_chanbind_value (xs, crs2bufp (&cbval));
		ok = ok && clever_concat (xs, der_s2c_init, cbval, &k2salt);
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		const kipt_keyid inkey [1] = { extra->k1id   };
		const uint8_t *salt    [1] = { k2salt.derptr };
		const uint32_t saltlen [1] = { k2salt.derlen };
		print_block ("Server.mixer S2C-Init + Channel Binding", salt [0], saltlen [0], false);
		ok = ok && kipkey_mixer (extra->kip,
					1, inkey, salt, saltlen,
					extra->k1id + 1, &extra->k2id);
		if (!ok) log_debug ("ok==0 at %s:%d", __FILE__, __LINE__);
		if (ok) {
			extra->keysetup++;
		}
		/* Encrypt the der_s2c_init structure into s2c */
		log_debug ("xsasl_step_server() encrypts DER s2c_init into s2c if %d", ok);
		ok = ok && extra_encrypt (xs, extra->k1id, der_s2c_init, out_s2c);
		/* Report success, if applicable */
		extra->wrapmech = ok;
		return ok;
	}
	/* We are wrapping, have keys, sent mechs; so now decrypt c2s for processing */
	dercursor dn_c2s;
	log_debug ("xsasl_step_server() decrypting c2s with k2id %ld", extra->k2id);
	ok = ok && extra_decrypt (xs, extra->k2id, c2s, &dn_c2s) && (dn_c2s.derlen >= 8);
	if (!ok) {
		return false;
	}
	print_block ("dn_c2s", dn_c2s.derptr, dn_c2s.derlen, false);
	/* Unpack dn_c2s into a SXOVER-Response */
	C2S_Cont resp;
	ok = ok && (der_unpack (&dn_c2s, pack_c2s, (dercursor *) &resp, 1) == 0);
	if (!ok) log_debug ("ok==%d at %s:%d", ok, __FILE__, __LINE__);
	//CTR_GONE// /* Check the counter value */
	//CTR_GONE// uint32_t ctr;
	//CTR_GONE// ok = ok && (der_get_uint32 (resp.ctr, &ctr) == 0) && (ctr == extra->nextctr++);
	//CTR_GONE// if (!ok) log_debug ("ok==%d at %s:%d", ok, __FILE__, __LINE__);
	/* If we got a mechanism, it would be just one, pass it down */
	if (resp.mechsel.derptr != NULL) {
		ok = ok && qsasl_set_mech (xs, crs2buf (resp.mechsel));
		if (!ok) log_debug ("ok==%d at %s:%d", ok, __FILE__, __LINE__);
	}
	if (!ok) {
		return false;
	}
	/* SXOVER wraps a normal internal action, pass it down */
	log_debug ("xsasl_step_server() calls nested qsasl_step_server()");
	S2C_Cont chal;
	memset (&chal, 0, sizeof (chal));
	if (!qsasl_step_server (xs, crs2buf (resp.c2s.token), crs2bufp (&chal.s2c.token))) {
		return false;
	}
	if (chal.s2c.token.derptr == NULL) {
		/* Active NULL variant in s2c token */
		chal.s2c.no_token.derptr = "";
	}
	log_debug ("xsasl_step_server() continues after nested qsasl_step_server()");
	//CTR_GONE// /* Set the new counter value */
	//CTR_GONE// der_buf_uint32_t chal_ctrbuf;
	//CTR_GONE// chal.ctr = der_put_uint32 (chal_ctrbuf, extra->nextctr++);
	/* Pack the result into a DER byte sequence */
	dercursor up_out_s2c;
	memset (&up_out_s2c, 0, sizeof (up_out_s2c));
	ok = ok && dermem_pack (xs, pack_s2c, (dercursor *) &chal, &up_out_s2c);
	/* Encrypt out_s2c with our wrapping keys */
	log_debug ("xsasl_step_server() encrypts s2c");
	ok = ok && extra_encrypt (xs, extra->k2id, up_out_s2c, out_s2c);
	/* Return success */
	return ok;
}
