# Python packages for KIP and DiaSASL
#
# Author: Rick van Rein <rick.vanrein@arpa2.org>
# SPDX-License-Identifier: BSD-2-Clause


import os
from os import path

import setuptools


#
# Warn about deprecation of this location/code
#
print ('\n###\n### WARNING\n### This code is moving to a project of its own and will not be maintained here\n### https://gitlab.com/arpa2/kip-document\n###\n')


#
# Additional SWIG options from a $SWIG_OPTIONS environment variable
#
swigopts = os.environ.get ('SWIG_OPTIONS', None)
if swigopts is None:
	swigopts = []
else:
	swigopts = swigopts.split (' ')
#
swigincl = [ so [2:] for so in swigopts if so [:2] == '-I' ]
#
print ('Including from %r' % swigincl)


#
# Preparation
#
here = path.dirname (path.realpath (__file__))


#
# Extension modules for KIP CORE/SERVICE/DAEMON
#
ext_kipcore = setuptools.Extension (name='_kipcore',
			sources=[ path.join (here, 'python', 'kipcore.i'), ],
			swig_opts=[ '-py3', '-I%s' % (path.join (here, 'include'),), ] + swigopts,
			include_dirs=[ path.join (here, 'include'), ] + swigincl,
			libraries=[ 'kip', ],
)
ext_kipservice = setuptools.Extension (name='_kipservice',
			sources=[ path.join (here, 'python', 'kipservice.i'), ],
			swig_opts=[ '-py3', '-I%s' % (path.join (here, 'include'),), ] + swigopts,
			include_dirs=[ path.join (here, 'include'), ] + swigincl,
			libraries=[ 'kipservice', 'kip', ],
)
ext_kipdaemon = setuptools.Extension (name='_kipdaemon',
			sources=[ path.join (here, 'python', 'kipdaemon.i'), ],
			swig_opts=[ '-py3', '-I%s' % (path.join (here, 'include'),), ] + swigopts,
			include_dirs=[ path.join (here, 'include'), ] + swigincl,
			libraries=[ 'kipdaemon', 'kip', ],
)


#
# Package KIP core library
#

readme = open (path.join (here, 'README.MD')).read ()
setuptools.setup (

	# What?
	name = 'kip',
	version = '0.13.5',
	url = 'https://gitlab.com/arpa2/kip',
	description = 'KIP -- Kipping the Balance on Privacy',
	long_description = readme,
	long_description_content_type = 'text/markdown',

	# Who?
	author = 'Rick van Rein (for the ARPA2 KIP project)',
	author_email = 'rick@openfortress.nl',

	# Where?
	ext_modules = [ ext_kipcore, ext_kipservice ],
	namespace_packages = [
		'arpa2',
	],
	packages = [
		'arpa2',
		'arpa2.kip',
		'arpa2.kip.io',
	],
	package_dir = {
		# Silly nesting for arpa2 nspkg, but harmless
		'arpa2'       : path.join (here, 'python', 'arpa2'),
		'arpa2.kip'   : path.join (here, 'python'),
		'arpa2.kip.io': path.join (here, 'python', 'io'),
	},
	package_data = {
		'arpa2.kip' : [ path.join (here, 'README.MD'),
		],
	},
	entry_points = {
		'console_scripts' : [
			'kip=arpa2.kip.command:main',
		],
		# Protocol plugins register for Source classes
		'arpa2.kip.io.sources' : [
			'file=arpa2.kip.io.file:FileSource',
			# 'imap=arpa2.kip.io.mail:ImapSource',
			# 'web=arpa2.kip.io.http:WebSource',
			# 'amqp=arpa2.kip.io.amqp:MessagingSource',
		],
		# Protocol plugins register for Target classes
		'arpa2.kip.io.targets' : [
			'file=arpa2.kip.io.file:FileTarget',
			# 'mail=arpa2.kip.io.mail:MailTarget',
			# 'web=arpa2.kip.io.http:WebTarget',
			# 'amqp=arpa2.kip.io.amqp:MessagingTarget',
		],
	},

	# Requirements
	install_requires = [ 'six', ],
	# extras_require = {
	# 	'TOOLS' : [ 'arpa2.quickder_tools' ],
	# },

)


