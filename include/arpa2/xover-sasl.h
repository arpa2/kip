/* Xover-SASL -- Simple Authentication with Secure Xover
 * =====================================================
 *
 * This is a wrapper around Quick SASL, adding SXOVER
 * as a wrapper mechanism.  SXOVER is a mechanism for
 * secure realm crossover (Xover means Crossover).
 * Any other mechanisms that SXOVER can pass through.
 *
 * Most of the Quick SASL calls can be used without
 * change, but we provide xsasl_ prefixed functions
 * for such qsasl_ variants.  The names of the
 * QuickSASL and XoverSASL structures are also equivalent.
 * As a result, name substitution is all that is
 * needed to wrap Xover SASL around Quick SASL code.
 *
 * For now, when SXOVER is available, it is always
 * selected by the SASL client.  Since we nest SASL
 * and wish to avoid creating loops of SXOVER in
 * SXOVER, the mechanism is filtered out of the
 * wrapped SASL layer.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#ifndef ARPA2_XOVER_SASL_H
#define ARPA2_XOVER_SASL_H


#include <arpa2/quick-dermem.h>
#include <arpa2/quick-sasl.h>


/* The opaque data for a XoverSASL Connection.
 */
typedef QuickSASL XoverSASL;


/* The state of progress of a SASL Connection.
 */
typedef qsaslt_state xsaslt_state;


/* Global Initialisation of Xover SASL.
 *
 * If you need to provide a driver library hint,
 * this is where to do it.  Not actually used yet.
 *
 * The default appname is "InternetWide_Identity".
 */
static inline bool xsasl_init (const char *_opt_driver, const char *appname) {
	if (appname == NULL) {
		appname = "InternetWide_Identity";
	}
	return qsasl_init (_opt_driver, appname);
}


/* Global Cleanup of Xover SASL.
 */
#define xsasl_fini qsasl_fini


/* Create a Client Connection Object.
 *
 * Optionally provide a callback function that will
 * be called when the state changes to QSA_SUCCESS
 * or QSA_FAILURE.
 *
 * The flags may be set to QSA_CLIENTFLAG_xxx
 *
 * This routine allocates a dermem_pool containing
 * itself.  The dermem_alloc() function can be used
 * to add more data to its pool.
 *
 * Prior to this call, *out_xs must be NULL.
 *
 * This is a wrapper around the Quick SASL function.
 */
bool xsasl_client (XoverSASL *out_xs,
		const char *service,
		void (*opt_cb) (void *cbdata, XoverSASL xs, xsaslt_state newstate),
		void *cbdata,
		unsigned flags);


/* Create a Server Connection Object.
 *
 * Optionally provide a callback function that will
 * be called when the state changes to QSA_SUCCESS
 * or QSA_FAILURE.
 *
 * The flags may be set to QSA_SERVERFLAG_xxx
 *
 * This routine allocates a dermem_pool containing
 * itself.  The dermem_alloc() function can be used
 * to add more data to its pool.
 *
 * Prior to this call, *out_xs must be NULL.
 *
 * This is a wrapper around the Quick SASL function.
 */
bool xsasl_server (XoverSASL *out_xs,
		const char *service,
		void (*opt_cb) (void *cbdata, XoverSASL xs, xsaslt_state newstate),
		void *cbdata,
		unsigned flags);


/* Close the Client/Server Connection Object.
 *
 * This frees the SASL connection handle, plus
 * any data that was allocated in its pool.
 *
 * After this call, *inout_xs will be NULL.
 *
 * This is a wrapper around the Quick SASL function.
 */
void xsasl_close (XoverSASL *inout_xs);


/* Make a step as a SASL Client.
 *
 * This is a wrapper around the Quick SASL function that
 * adds SXOVER-PLUS mechanism support.
 *
 * The return value is true for success passing through the
 * programming steps, as an accumulated assertion of the entire
 * process.  It does _not_ signify an authentication result.
 *
 * Authentication results are dissiminated through the
 * success and failure flags, and can be requested with
 * qsasl_get_outcome() or through the callback facility.
 *
 * When neither success or failure is set after this step,
 * then authentication needs to continue another step.
 * The two flags should not ever occur together, but it is
 * cautious to treat failure as the leading one when the
 * flow has ended, so when either-or-both are flagged.
 */
bool xsasl_step_client (XoverSASL xs,
			dercursor s2c,
			dercursor *out_mech, dercursor *out_c2s);


/* Make a step as a SASL Server.
 *
 * This is a wrapper around the Quick SASL function that
 * adds SXOVER-PLUS mechanism support.
 *
 * The return value is true for success passing through the
 * programming steps, as an accumulated assertion of the entire
 * process.  It does _not_ signify an authentication result.
 *
 * Authentication results are dissiminated through the
 * success and failure flags, and can be requested with
 * qsasl_get_outcome() or through the callback facility.
 *
 * When neither success or failure is set after this step,
 * then authentication needs to continue another step.
 * The two flags should not ever occur together, but it is
 * cautious to treat failure as the leading one when the
 * flow has ended, so when either-or-both are flagged.
 */
bool xsasl_step_server (XoverSASL xs,
			dercursor c2s,
			dercursor *out_s2c);

/* Determine the flags for success or failure.  They may
 * both be reset, in which case the work is not done.
 * The return value indicates success of the call that
 * fills the flags.
 */
#define xsasl_get_outcome qsasl_get_outcome

/* Determine the flags for success or failure.  They may
 * both be reset, in which case the work is not done.
 * The return value indicates success of the call that
 * fills the flags.
 */
#define xsasl_get_outcome qsasl_get_outcome


/* Set the Channel Binding information for a given type.
 */
#define xsasl_set_chanbind qsasl_set_chanbind


/* Get the Channel Binding type.
 */
#define xsasl_get_chanbind_type qsasl_get_chanbind_type


/* Get the Channel Binding value.
 */
#define xsasl_get_chanbind_value qsasl_get_chanbind_value


/* Get the Channel Binding name.
 */
#define xsasl_get_chanbind_name qsasl_get_chanbind_name


/* Set the Server Realm.
 */
#define xsasl_set_server_realm qsasl_set_server_realm


/* Set the Client Realm.
 */
#define xsasl_set_client_realm qsasl_set_client_realm


/* Get the Mechanism(s) that is/are currently possible.
 *
 * Before the client chooses, there may be any number
 * of possible mechanisms; after the choice there is
 * only one.  In both cases, a single string is used to
 * represent the mechanism(s), with spaces to separate
 * multiple options.
 */
bool xsasl_get_mech (XoverSASL xs, dercursor *out_mech);


/* Set the Mechanism(s) to be considered possible.
 *
 * Before the client chooses, there may be any number
 * of possible mechanisms; after the choice there is
 * only one.  In both cases, a single string is used to
 * represent the mechanism(s), with spaces to separate
 * multiple options.
 */
bool xsasl_set_mech (XoverSASL xs, dercursor mech);


/* Set the Client username for authentication (the "login user").
 */
#define xsasl_set_clientuser_login qsasl_set_clientuser_login


/* Set the Client username to act for (the "ACL user").
 */
#define xsasl_set_clientuser_acl qsasl_set_clientuser_acl


/* Set the Client Identity (for SASL EXTERNAL).
 */
#define xsasl_set_clientid qsasl_set_clientid


/* Get the Client UserID, available after success.
 * The result has been subjected to SASL security.
 * For ANONYMOUS, this would return derptr==NULL,
 * because no client identity has been established.
 */
#define xsasl_get_client_userid qsasl_get_client_userid


/* Get the Client Domain, available after success.
 * The result has been subjected to SASL security.
 * For ANONYMOUS, this would return the approving realm.
 * even though no client UserID has been established.
 */
#define xsasl_get_client_domain qsasl_get_client_domain


/* Some mechanisms may delegate a credential to the
 * client.  This can be retrieved with this call.
 * The call sets DER NULL if no credential was
 * presented, but still returns true.
 */
#define xsasl_get_delegated_credential qsasl_get_delegated_credential


/* Get an informal client hint string, usually after success.
 * This may be used to trace them, possibly even shown on
 * their interface, though no formal meaning should ever
 * be assigned to it, as would be possible with _clientid().
 * This is meant to return the unreliable information that
 * the ANONYMOUS mechanism shares.  Other sources of this
 * information may also be used, as long as privacy of
 * other users is not hampered.  Expect to see derptr==NULL
 * if nothing is available.
 */
//NOTIMPL// #define xsasl_get_clienthint qsasl_get_clienthint



/********** ASSORTED UTILITY FUNCTIONS **********/



/* Allocate buffer memory to be held on to until
 * the xsasl_close() for the XoverSASL connection.
 * The memory is zeroed before it is returned.
 *
 * When this fails, it sets errno to ENOMEM.
 *
 * Since Xover SASL allocates a fresh dermem_pool,
 * this is just a wrapper around dermem_alloc().
 */
static inline bool xsasl_alloc (XoverSASL xs, size_t size, uint8_t **out_memptr) {
	return dermem_alloc ((void *) xs, size, (void **) out_memptr);
}


/* Buffer a character string provided as a dercursor,
 * so without trailing NUL char, but make it available
 * in the master's pool with a trailing NUL.
 *
 * Before this call, *out_strbuf must be NULL.
 */
static inline bool xsasl_strdup (XoverSASL xs, dercursor str, char **out_strbuf) {
	return dermem_strdup ((void *) xs, str, out_strbuf);
}


/* Test if support for Quick SASL passphrase entry is
 * available.  This may check for existence of a pinentry
 * program in the local setup.
 */
#define xsasl_haveinteraction qsasl_haveinteraction


/* First usage pattern: passphrase entry.  The return value
 * is NULL on failure (with errno set) or a NUL-terminated
 * string which must be cleared and free()d when done.
 *
 * SETTIMEOUT 30
 * SETDESC Passphrase for https://github.com Realm ARPA2
 * SETPROMPT [user@]pass
 * SETTITLE Login with HTTP-SASL
 * SETKEYINFO s/AABBCCDD
 * SETOK Login
 * SETCANCEL Anonymous
 * GETPIN
 *
 * The parameters should be provided by the context, in
 * desc_for, prompt, title.  The desc_for is printed after
 * "Passphrase for " and since this value is expected to
 * identify the resource to login to, it is turned to hex
 * for use in SETKEYINFO, which may be used to store the
 * key in a system-specific password storage mechanism.
 *
 * The text for Login and Cancel buttons may be replaced.
 * A useful idea is to replace Cancel with something
 * positive, like "Anonymous".  To facilitate this, the
 * Cancel operation returns a fixed string that must not
 * be free()d, holding the pointer fixptr_cancel.
 */
#define xsasl_getphassphrase qsasl_getpassphrase


/* Second usage pattern: Ask for Confirmation.
 *
 * SETTIMEOUT 30
 * SETDESC Login as vanrein@github.com
 * SETTITLE Approval for Client Identity
 * SETOK OK
 * SETNOTOK Change
 * SETCANCEL Cancel
 * CONFIRM
 *
 * The desc and title values should be supplied.  Alternate
 * texts for OK and Cancel buttons may be given.  The return
 * value is:
 *  1 for OK
 *  0 for NOTOK
 * -1 for CANCEL or TIMEOUT
 */
#define xsasl_confirm qsasl_confirm


#endif
