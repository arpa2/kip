/** @defgroup kip Keyful Identity Protocol
 * @{
 * <arpa2/kip.h> -- The API to libkip keying and encryption.
 *
 * The KIP library performs encryption and decryption of data,
 * possibly with the help of an online KIP Service.  There is
 * a fair amount of luxoury, including key maps that enable very
 * powerful key derivation procedures.
 *
 * The cryptographic structures underpinning KIP is taken from
 * Kerberos.  This is not a common choice, but it is the result
 * of the kind of logic that we follow.  There is no need to
 * run a complete Kerberos infrastructure, to use these basic
 * algorithms and data formats.  Kerberos is helpful for its
 * own reasons, but not because of these underlying mechanistic
 * elements.  (Kerberos is useful for administrators as a
 * centralised identity management system, for users as a
 * single sign-on system servicing a plethora of protocols, and
 * to cryptographers as a system ready for Quantum Computers.)
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#ifndef ARPA2_KIP_H
#define ARPA2_KIP_H


#include <stdbool.h>
#include <stdint.h>
#include <sys/types.h>  /* for time_t */

#include <arpa2/quick-der.h>
#include <com_err/kip.h>


/** @defgroup kiptypes Types and definitions for KIP
 * @{
 */



/** @brief KIP algorithm type.
 *
 * KIP algorithms are enctypes from Kerberos.  Note that these
 * are symmetric algorithms.  Their great disadvantage is that
 * we need to contact a KIP Service; their great benefit are
 * that they are proof from Quantum Computing.
 */
typedef int32_t kipt_alg;


/** @brief KIP key number and identity.
 *
 * KIP documents reference keys with a key number.  On the API,
 * this number is considered a mere hint at the key identity.
 * This is helpful when documents or streams of keys are being
 * merged.  Though this could open up an angle of attack, it
 * is easy to remedy that; key numbers can be random generated,
 * and probability theory will tell us what the chances of
 * abuse are for a given number of clashes from a given number
 * of inputs.  It should be relatively easy to separate deceit
 * from proper use of these facilities, and an error may be
 * be produced immediately at the time of document merges.
 * 
 * On the API, we have the luxoury of seeing everything pass
 * through.  That allows us to add bits to the key number and
 * form a key identity.  The extra bits cannot be sent on the
 * wire without repeating the problem; this is because those
 * extra bits are locally generated, and dynamic, but not of
 * impact on the remote key selection process.
 *
 * The API numbers are a superset of the wire numbers.  Inasfar
 * as the ranges overlap, API numbers refer to wire numbers,
 * including their property of being a hint.  Be careful with
 * this; not all API calls appreciate being dealt an upscaled
 * wire key hint as though it was unique.
 *
 * KIP wire format key numbers are stored in kvno field in the
 * Kerberos data formats.
 *
 * KIP key number type, for use on the wire; see also kipt_keyid.
 */
typedef uint32_t kipt_keynr;

/** @brief KIP key identity, for use in the API; see also kipt_keynr.
 */
typedef uint64_t kipt_keyid;


/** @brief Checksum identity type.
 *
 * Checksum identities are created automatically for every
 * key (and they actually have the same value as a keyid)
 * but it is possible to create extra checksums as well,
 * whose handles would of not be usable as keys.
 */
typedef uint64_t kipt_sumid;	/* includes all kipt_keyid */


/** @brief KIP Context type.
 *
 * The KIP context holds the keys and checksums as they
 * evolve during use of the API.  Anything connected to
 * the context will be cleaned up when it is destroyed.
 * keys will be zeroed before free()ing their memory,
 * and so will any checksums be.
 *
 * The KIP context is an opaque type.  Clients handle
 * a pointer only.
 */
struct kipt_context;
typedef struct kipt_context *kipt_ctx;


/** @brief KIP Daemon context type.
 *
 * The KIP Daemon context holds a KIP context and socket
 * handle as well as any administrative content to allow
 * it to handle KIP Service on the server side, dedicated
 * to one client.
 */
struct kipt_daemon_context;
typedef struct kipt_daemon_context *kipt_dctx;


/** @def KIPSUM_MAXSIZE
 * @brief Maximum storage size for a hash.
 *
 * Useful for such
 * things as seed buffers.  This number may change with
 * times, so do not rely on a fixed setting.
 */
#define KIPSUM_MAXSIZE 48


/** @brief Key Usage Numbers to shake one key in different ways.
 *
 * Key Usage values are used in Kerberos to allow the use of keys
 * for various purposes, without the risk that the result of one
 * operation could be meaningful to another.  Call it isolation
 * of key uses if you like.
 *
 * The codes below are not formally allocated in the registry
 * managed by MIT for the krb5 software and its applications.
 * After discussion with Greg Hudson at MIT, we concluded that
 * so many forces need to get together before a clash would
 * occur and be subject of abuse that we follow RFC 4120 and
 * pick random key values.  Their differentiation within the
 * protocol is important.  Abuse would require matches in
 * service name, including the "kip/" part, matching keys and
 * a few other factors that are avoided by common practices.
 */
#define KIP_KEYUSAGE_USERDATA	1863
#define KIP_KEYUSAGE_MAPPING	1864
#define KIP_KEYUSAGE_INTEGRITY	1865
#define KIP_KEYUSAGE_SERVICE	1866


/** @} */


/*
 *****     SECTION START: KIP CORE FUNCTIONS     *****
 */

/** @def SUPPRESS_KIP_CORE Do not show the KIP Core functions.
 */

#ifndef SUPPRESS_KIP_CORE

/** @defgroup kipcore KIP Core functions
 * @{
 */

/** @defgroup kipupdown KIP Up and KIP Down functions
 * @{
 *
 * The basic use case for KIP is to add and remove protection
 * for a chunk of data.  We refer to these operations as
 * "kipping up" and "kipping down" the security.
 */



/** @brief KIP up, or encryption operation.
 *
 * Encrypt or *kip up* a clear text string into a bucket of mud.
 * The routine encrypts a block of clear text in isolation; your
 * application may however develop a system that splits content
 * into blocks of a moderate size and treat those separately.
 *
 * When your application splits data into blocks, the entirety
 * of the transported information is not protected by a sequence
 * of kipdata_up() calls.  The suggested remedy is to have an overall
 * checksum under protection of each key.  You can feed data into
 * the checksum with the routines below.
 *
 * This function returns true on success, or false with errno set
 * on failure.  The errno values are enhanced with codes from the
 * com_err table "KIP" as defined in this package, and those in
 * use with Kerberos.
 *
 * If the max_mudsz is too low, this function sets KIPERR_OUTPUT_SIZE
 * and nothing will get written to mud, not even a NULL value would
 * be noticed.  The out_mudsz value will be filled, though, and you
 * can use this to measure the desired output size.  When you set
 * max_mudsz=0 you can trigger this deliberately.
 */
bool kipdata_up (kipt_ctx ctx, kipt_keyid key,
			uint8_t *clear,   uint32_t clearsz,    /* input  */
			uint8_t *out_mud, uint32_t max_mudsz,  /* outbuf */
			uint32_t *out_mudsz);                  /* usedup */


/** @brief KIP down, or decryption operation.
 *
 * Decrypt or *kip down* a bucket of mud into a clear text string.
 * The routine decrypts a bucket of mud in isolation, and always
 * as a whole; the application may however compose multiple of
 * these buckets in some way.  See kipdata_up() for how this is done,
 * and be sure to validate the checksums it generates to retain
 * consistency.
 *
 * The key may be a unique key identity or the key number hints 
 * that form a subset thereof; hints may require a few passes
 * through the decryption process though, in case of clashes
 * of those key numbers.
 *
 * This function returns true on success, or false with errno set
 * on failure.  The errno values are enhanced with codes from the
 * com_err table "KIP" as defined in this package, and those in
 * use with Kerberos.
 *
 * If the max_clearsz is too low, this function sets
 * KIPERR_OUTPUT_SIZE and nothing will get written to clean, not
 * even a NULL value would be noticed.  The out_clearsz value will
 * be filled though, and you can use this to measure the desired
 * output size.  When you set max_clearsz=0 you can trigger this
 * deliberately.
 */
bool kipdata_down (kipt_ctx ctx, kipt_keyid key,
			uint8_t *mud,       uint32_t mudsz,        /* input  */
			uint8_t *out_clear, uint32_t max_clearsz,  /* outbuf */
			uint32_t *out_clearsz);                    /* usedup */

/** @} */


/** @defgroup kiplib KIP Context and Library operation
 * @{
 *
 * The library is initialised and finalised.  Between theose calls,
 * applications can open and close contexts.
 */


/** @brief Globally initialise the KIP module.
 */
bool kip_init (void);


/** @brief Global cleanup for the KIP module.
 */
void kip_fini (void);


/** @brief Open a context for kipping up and/or down.
 *
 * You can use different
 * contexts for the kip up and kip down processes.  The context is
 * a place in which to mount keys and checksums.  Once done, the
 * contents may be eradicated.
 */
bool kipctx_open (kipt_ctx *out_ctx);


/** @brief Close a context for kip.
 *
 * This will destroy all the resources that were
 * allocated to it.  Any key material is first overwritten with
 * zeroes before it is removed.
 */
void kipctx_close (kipt_ctx ctx);


/** @} */

/** @defgroup kiprnd Key generation and entropy
 * @{
 *
 * KIP does things similar to a public key infrastructure,
 * but it relies on symmetric keys.  Keys will be related
 * to domain names, which can be validated online, and this
 * effectively skips the problem of trust having to be
 * placed in a public key.  This avoids problems of checking
 * fingerprints and needing to ask for a public key from a
 * party that one wants to communicate with.
 *
 * Key generation has a strong similarity to the generation
 * of random bits, but wrapped into a structure.  As with
 * public key crypto, everything starts with a locally
 * generated key.
 */


/** @brief Produce random bytes with the given address and length.
 *
 * This function returns true on success, or false with errno set
 * on failure.  The errno values are enhanced with codes from the
 * com_err table "KIP" as defined in this package, and those in
 * use with Kerberos.
 */
bool kipdata_random (kipt_ctx ctx, uint32_t len_target, uint8_t *out_target);


/** @brief Given a KIP key, determine its algorithm.
 */
bool kipkey_algorithm (kipt_ctx ctx, kipt_keyid keyid,
			kipt_alg *out_alg);


/** @brief Generate a KIP key.
 *
 * There can be a few ways in which we
 * can get hold of the key; we do not define those yet but
 * ought to, at some point -- we need it for decryption.
 *
 * The input provides the key number as it will appear on
 * the wire.  The ideal situation is that this is unique,
 * but it is not required by KIP.  For use in the library,
 * we have stricter needs, so the context adds extra bits
 * to arrange this locally.
 *
 * The key identity is exported, but not the key itself.
 * As long as it resides in the context, the proper clearing
 * of the key material can be arranged by the KIP context.
 */
bool kipkey_generate (kipt_ctx ctx, kipt_alg alg,
			kipt_keynr   in_keynr,
			kipt_keyid *out_keyid);

/** @} */

/** @defgroup kipkeymap Key Mapping for Controlled Sharing
 * @{
 *
 * Keys used in KIP can be used as session keys, so to
 * protect an exchange, or a file in transit.  This kind
 * of key is only useful for the one exchange.
 *
 * To allow others and yourself to derive these keys, they
 * take it out from a "key map" which may be seen to map
 * their/your key to the individual session key.
 *
 * Key maps may actually require multiple keys to be provided
 * for the retrieval of a session key.  This works like an
 * AND requirement on keys to present.  It is also possible to
 * conceal the same session key in multiple key maps, which
 * functions like an OR requirement on keys to present.  The
 * two can be combined to any OR/AND combination, which means
 * that any logic expression without inversion can be expressed.
 * (Inversion is not sensible, one would be proving NOT to have
 * a right to see a key).
 */



/** @brief Conceal a key in a key map.
 *
 * This makes it possible to
 * have alternative paths, like a logical OR on the access
 * paths to a key and its contents.  To also provide an AND
 * compisition mechanism, it is possible to require the use
 * of multiple keys in a key mapping.  The result will be a
 * nested key mapping.
 *
 * This function returns true on success, or false with errno set
 * on failure.  The errno values are enhanced with codes from the
 * com_err table "KIP" as defined in this package, and those in
 * use with Kerberos.  Among the KIP values may be ones that were
 * generated on the KIP Service.
 *
 * If the keymudszmax is too low, this function sets
 * KIPERR_OUTPUT_SIZE and nothing will get written to keymud, not
 * even a NULL value would be noticed.  The out_keymudsz value
 * will be filled though, and you can use this to measure the
 * desired output size.  When you set keymudszmax=0 you can
 * trigger this deliberately.  (Note that out_keymudsz covers
 * all the mappings, even with depth above one.)
 */
bool kipkey_tomap (kipt_ctx ctx, kipt_keyid keyid,
			uint16_t    mappingdepth,                 /* AND count */
			kipt_keyid *mappingkeyids,                /* AND keyid */
			uint8_t *out_keymud, uint32_t max_keymudsz,  /* outbuf */
			uint32_t *out_keymudsz);                     /* usedup */


/** @brief Retrieve a key from a key map.
 *
 * Use already-known keys to extract a key that was hidden in "key
 * mud" by a previous call to kipkey_tomap().  Store the resulting
 * key in the KIP context and provide the customary locally unique
 * keyid for it.  The extension from the wire format of a keynr_t
 * into a handle of a keyid_t means that the latter could differ
 * from the keyid_t originally sent.  The key value is not exported.
 *
 * This function returns true on success, or false with errno set
 * on failure.  The errno values are enhanced with codes from the
 * com_err table "KIP" as defined in this package, and those in
 * use with Kerberos.  Among the KIP values may be ones that were
 * generated on the KIP Service.
 */
bool kipkey_frommap (kipt_ctx ctx,
			uint8_t *keymud, uint32_t keymudsz,
			kipt_keyid *out_mappedkey);


/** @} */


/** @defgroup kipmac KIP and integrity checks.
 * @{
 * Integrity checks may be used to span independently encrypted
 * blocks.  Without these checks, it could be possible to remove
 * blocks, insert or duplicate older ones, or change the order
 * in which they occur.  This is generally undesirable, but it
 * is not a requirement for protocols employing single blocks.
 * 
 * These integrity checks are implemented as secure checksums
 * under protection of a key.  Only those who hold the key
 * could alter the document without breaking the checksum, so
 * that includes you when you can check it, but at least it
 * excludes anyone outside your trusted group.  Under symmetric
 * crypto, there is no difference between reading and writing
 * privileges, but there is a difference between the in-crowd
 * and the out-laws.
 *
 * There is a difference between integrity checks on encrypted
 * or decrypted content.  Note however, that a complex document
 * with sections encrypted to different keys may not be readable
 * in full by everyone.  Such unreadable content is best not
 * included in the integrity check, or it should be included
 * in its encrypted form.  Other applications may choose to
 * represent separate views on a document from each of the keys
 * used in it, since integrity checks are made with keys anyway.
 * This would mean that encrypted content is used where it
 * cannot be decrypted with the integrity-checking key, but use
 * decrypted content where it can be decrypted with that key.
 * This is all pretty wild, but there is room for diversity,
 * and that being a good thing we aim to support it here, while
 * leaving a simple-yet-secure case as a default.
 *
 * By default, every key starts its own checksum and will hash
 * everything that it decrypts.  You can additionally define
 * insertions; either with plain or muddy data, or perhaps you
 * feel a need to note where the checksummed blocks start and
 * end, as that is lost in the default approach.
 *
 * Checksums can be finished and exported at any time.  Their
 * value is a "muddy sum", ready to be inserted into a document
 * or other application flow.
 */


/** @brief Start a new checksum, to be protected with the indicated
 * key identity.
 *
 * This call is implicit for any key added to
 * the context; the kipt_keyid identity serves as the checksum
 * identity for this implied checksum; the reverse is not true.
 */
bool kipsum_start (kipt_ctx ctx, kipt_keyid supporting_key,
			kipt_sumid *out_kipsum);


/** @brief Restart a checksum.
 *
 * This works by flushing its current state and
 * assuming nothing passed through it yet.  Do not loose
 * the coupling with the key; both key identity and, if
 * this is the default sum for a key its future data, will
 * be inserted here.
 */
bool kipsum_restart (kipt_ctx ctx, kipt_sumid sumid_or_keyid);


/** @brief Fork a checksum.
 *
 * This means that its current state will be
 * cloned and the old and new checksum can continue from
 * this same state.  The old one will not show any change
 * and the new one would be able to compute the same
 * output.
 *
 * If the old checksum happened to be a key's coupled
 * checksum, then only the old checksum will continue
 * to receive data automatically; the new checksum will
 * only have what was automatically inserted up to the
 * point of forking.
 *
 * TODO: For now, we require *out_kipsum to be 0 on
 *       calling; we may later reuse existing sums.
 */
bool kipsum_fork (kipt_ctx ctx, kipt_sumid sumid_or_keyid, kipt_sumid *out_kipsum);


/** @brief Update a checksum.
 *
 * This is the typical hash operation of inserting the bytes
 * given into the flow of hashed data.
 *
 * Note that a keyid is always a sumid; you can send data
 * to the checksum for your key by using its keyid in this
 * position.  Also note that this does not extend to
 * key numbers, as these are not sufficiently selective.
 */
bool kipsum_append (kipt_ctx ctx, kipt_sumid sumid_or_keyid,
			uint8_t *data, uint32_t datasz);


/** @brief Insert a marker separating data pieces.
 *
 * This solves
 * problems that may arise when fields just run from one
 * into the next, and a separator cannot be seen; added
 * information to the first field may now be taken from
 * the second field without changing the checksum, which
 * is a possible security risk.
 *
 * As a general rule, try to compose your input to checksum
 * such that they could be parsed unambiguously.  One way
 * of doing this is inserting separators (and be mindful
 * about escapes!) and this is why this function exists.
 * It will insert a marker that forms a chain until the
 * end (where we will insert an extra marker, even if you
 * do that too).
 *
 * To help you write unambiguously parsing input to the
 * checksum, you can even add typing information, in the
 * form of a string.  The string will be inserted too.
 * It is up to you what it covers; the marker, text
 * before or after it, or perhaps both.  You can insert
 * any number of markers that you like.
 *
 * These functions are likely to be incompatible with
 * things done in other software, or hand-crafted
 * procedures such as standardised in RFCs.  For those,
 * you should manually provide your input, byte by byte.
 * For local uses, you may benefit from the simplicity
 * and not-having-to-be-a-cryptographer just to get
 * your security organised.
 */
bool kipsum_mark (kipt_ctx ctx, kipt_sumid sumid_or_keyid,
			char *opt_typing);


/** @brief Merge another checksum into this one.
 *
 * This is done
 * by computing its value and appending its output.  Do
 * keep this separatable from other content, or risk the
 * consequences.  (If you could not parse back what you
 * put into a checksum, you may end up allowing abuse.)
 *
 * The checksums should be compatible, that is, they
 * should use the same key and algorithm.  This may be
 * relaxed at a later time.
 */
bool kipsum_merge (kipt_ctx ctx, kipt_sumid accumulator, kipt_sumid addend);


/** @brief Sign a document.
 *
 * To do this, export the checksum, which is a key-encrypted
 * form of the encryption type's mandatory hash.
 *
 * This operation does not consume the checksum; it can simply
 * continue to add data and/or be verified or signed again.
 */
bool kipsum_sign (kipt_ctx ctx, kipt_sumid sumid_or_keyid, uint32_t max_siglen,
				uint32_t *out_siglen, uint8_t *out_sig);


/** @brief Verify a document.
 *
 * To do this, compare the checksum, which was key-encrypted
 * with the encryption type's mandatory hash.
 *
 * This operation does not consume the checksum; it can simply
 * continue to add data and/or be verified or signed again.
 *
 * In general, those are the outcomes that you are looking for:
 *
 *  - true  for successful verifcation
 *  - false with errno=KIPERR_CHECKSUM_INVALID for signature failure
 *  - false with other errno for various operational problems
 */
bool kipsum_verify (kipt_ctx ctx, kipt_sumid sumid_or_keyid,
				uint32_t siglen, uint8_t *sig);


/** @} */

/** @} */


/** @defgroup kiptable Key table handling (for servers)
 * @{
 */

/** @brief Load a KIP key from a key table.
 *
 * If an opt_hostname is
 * given, the key should be named "kip/opt_hostname@DOMAIN"
 * and otherwise it will be a local name such as
 * "kip/MASTER@DOMAIN" for a master key.
 *
 * Whether the master key is accessible is not a matter of
 * hiding it from this API.  It is a matter of access
 * rights to the keytab containing it.  It is strongly
 * advisable to have "kip/MASTER@DOMAIN" on a keytab of
 * its own, and constrain access to that key.
 *
 * You may specify a service if it is not "kip", but that
 * must mean this is a virtual service reference, so it
 * will require no opt_hostname and domain must be ".".
 *
 * Note: This functionality is separately linked; it is not
 *       part of the usualy libkip library.
 *
 * Note: Consider kipkey_fromvhosttab() instead; it is
 *       easier to use, and integrates with management
 *       commands for service virtual hosts.
 */
bool kipkey_fromkeytab (kipt_ctx ctx,
			const char *opt_ktname, const char *opt_service,
			const char *opt_hostname, const char *domain,
			kipt_keynr kvno, kipt_alg enctype,
			kipt_keyid *out_keyid);


/** @brief Load a KIP key for a service at a virtual host/domain.
 *
 * This bootstraps from a service master key found in a
 * special place.  Where this special place is, depends
 * on the installation setup; it may be a keytab or PKCS #11
 * store, or just a higher-up process with more privileges
 * from which we were just a constrained clone.
 *
 * Using on this service key, a key specific to the virtual
 * host is derived with kipkey_frommap().  The virtual host
 * mapping can be just a file in the local file system under
 * protection of the master key, but again it depends on the
 * installation setup if it is that simple or more advanced.
 *
 * In general, the idea of a vhosttab is a dictionary lookup
 * for a given (service,vhost) name pair.  In the specific
 * form stored on a file system, it stores a directory named
 * "xxx-vhost" for the service name "xxx", with underneath
 * it a single file for every vhost carrying its name and
 * containing the keymud as produced by kipkey_tomap().
 * File systems with inline data may store this in the inode!
 * Take note that vhosts may or may not be case sensitive.
 * 
 * This is a replacement for the simple and implementation-
 * specific kipkey_fromkeytab() function.  It adds a layer
 * of (virtual host) indirection that can be managed with
 * tools.  It also supports key migration, by using info in
 * the keymap as a hint for the key to load, both its number
 * and algorithm.  The result is that a vhost indicates what
 * master key it relies on.  It is possible to upgrade the
 * key setup for a vhost to another master key, which can be
 * helpful during key migrations, including moving of vhost
 * definitions to another host.
 */
bool kipkey_fromvhosttab (kipt_ctx ctx,
			const char *service, const char *vhost,
			kipt_keyid *out_keyid);

/** @} */


/** @defgroup kipextract Key and entropy extraction functions
 * @{
 */


/** @brief Derive key material to be used outside of KIP.
 *
 * This does not actually export the key.
 * An existing KIP key can be used in a PRF, along
 * with an input salt, to produce seemingly random output that
 * can be used outside of KIP as derived key material.  The same
 * key material can be mapped from the same salt and same key in
 * another time and place.
 *
 * This implements the PRF+ function defined in RFC 6113.
 *
 * The length of the input salt must be at least the length of
 * the usermud, but longer is possible if the entropy is not
 * as dense as a full random sequence.  Proze, for instance, is
 * only good for one 1 bit per character and passwords may not
 * be better.  Hashing and encryption produces output that may
 * _seem_ more random, but the entropy does not rise due to
 * these operations; it is just more difficult to see that.
 *
 * The salt may be public material, because the entropy derives
 * from the key and the output cannot be traced back to the key.
 * This means that the salt is not extremely precious, not like
 * a password.  Do note that if its contents could be manipulated
 * before making this call, then replay attacks might be mounted;
 * so do protect the path that delivers the salt here.
 */
bool kipdata_usermud (kipt_ctx ctx, kipt_keyid keyin,
			const uint8_t *salt, uint32_t salt_len,
			uint8_t *usermud, uint32_t usermud_len);


/** @brief Mix keys and salt into a new key.
 *
 * Each of the keys gets
 * its own salt to allow variations of mixtures that depend
 * on (possibly public) key material.  The output key is given
 * the same encryption algorithm as the first key.  To be an
 * equal contributor, the second key and salt must provide at
 * least the entropy that goes into the output key; an easy way
 * of ensuring this is using the same algorithm for both keys.
 * This is not verified however.
 *
 * The two salts must at least be as long as the output key's
 * need for random material.  The same warnings about salt and
 * compression as for kipdata_usermud() apply here.
 *
 * This implements the algorithm KRB-FX-CF2 from RFC 6113, but
 * generalised to any number of key / salt pairs.  You should
 * be careful not to cancel any of these by repeating them, as
 * the combination is made with XOR.
 */
bool kipkey_mixer (kipt_ctx ctx, uint16_t num_keys,
			const kipt_keyid *keysin, const uint8_t **salts, const uint32_t *salt_lens,
			kipt_keynr in_keynr, kipt_keyid *out_keyid);


/** @} */
#endif
/*
 *****     SECTION END: KIP CORE FUNCTIONS     *****
 */


/*
 *****     SECTION START: KIP SERVICE FUNCTIONS     *****
 */

/** @def SUPPRESS_KIP_SERVICE Do not show the KIP Service functions.
 */

#ifndef SUPPRESS_KIP_SERVICE

/** @defgroup kipservice KIP Service functions (client mode functions)
 * @{
 */


/** @brief Initialise the KIP Service module.
 *
 * This mostly installs Unbound configuration:
 *  - optional root trust file (default "/etc/unbound/root.key")
 *  - optional test hosts file (default "/etc/hosts")
 */
bool kipservice_init (char *opt_rootkey, char *opt_hosts);


/** @brief Cleanup the KIP Service module.
 */
void kipservice_fini (void);


/** @brief Start KIP Service on a KIP context.
 *
 * This prepares a kipt_ctx for use with KIP Service.
 * The call should be paired with kipservice_stop().
 */
bool kipservice_start (kipt_ctx ctx);


/** @brief Stop KIP Service on a KIP context after kipservice_start().
 */
void kipservice_stop (kipt_ctx ctx);


/** @brief Restart KIP Service.
 *
 * This is functionally equivalent to
 * kipservice_stop() followed by kipservice_start(), but more
 * efficient.  What it does is recycle the connection pool
 * into which output parameter from kipservice_xxx() piles up.
 * In normal use, such piling-up is just what the user wants.
 */
void kipservice_restart (kipt_ctx ctx);


/** @brief Set the Client Realm.
 *
 * This is useful when addressing a
 * Remote KIP Service, to indicate it is crossing over, but
 * also when addressing one local to the realm, because it
 * still needs to know that the client is realm-local (and,
 * in support of multiple realms, for which realm it is
 * requesting access).
 *
 * The pointer may be NULL.  The string is internally
 * copied, and reset during stop or restart of the
 * KIP Service.
 */
bool kipservice_set_client_realm (kipt_ctx ctx, char *crealm);


/** @brief Set the Client username for authentication (the "login user").
 *
 * This is used for SASL authentication, so any credentials
 * provided must match this.
 *
 * The pointer may be NULL.  The string is internally
 * copied, and reset during stop or restart of the
 * KIP Service.
 */
bool kipservice_set_clientuser_login (kipt_ctx ctx, char *user_login);


/** @brief Set the Client username to act for (the "ACL user").
 *
 * This may be useful when addressing a resource that is
 * made accessible to another user, such as a group.
 *
 * The KIP Service evaluates the transition from the
 * authenticated user to the authorisation user.  When
 * it relies on a backend through SASL Realm Crossover,
 * this is delegated too and the authorisation user is
 * reported as the effective user identity.
 *
 * The value may well depend on the KIP Service realm
 * that is being addressed; for example, in support of
 * aliases or pseudonyms that vary between realms.
 *
 * The pointer may be NULL.  The string is internally
 * copied, and reset during stop or restart of the
 * KIP Service.
 */
bool kipservice_set_clientuser_acl (kipt_ctx ctx, char *user_acl);


/** @brief Find the realm mentioned in the KIP-Opaque-Data value.
 *
 * The KIP-Opaque-Data value can be found in either
 * the service key mud between kipservice_tomap()
 * and kipservice_frommap(), or between kipservice_sign()
 * and kipservice_verify() calls.
 *
 * When realms run their own KIP Service, it is common
 * to desire encryption to be made against that node,
 * but KIP itself can crossover if so desired.  Before
 * calling kipservice_frommap() you will want to test
 * whether the user agrees with crossover.
 *
 * Provide the service mud in svcmud; in case of success,
 * domain will be filled with the realm indicated in it.
 */
bool kipservice_realm (dercursor svcmud, dercursor *realm);


/** @brief Use the KIP Service to map a key into service key mud.
 *
 * The result is opaque, and requires the use of the same
 * KIP Service, identified in the opaque data, to extract
 * the original key.  You may still want to do additional
 * key mapping with kipkey_tomap() locally for better
 * flexibility in key management.
 *
 * You can define a series of identities on a white list
 * and a black list.  The most concrete match with entries
 * on these lists determines whether an attempt by others
 * succeeds when they wish to extract key material from
 * the key mud.
 *
 * You should split the key mud per domain.  The reason is
 * that we need the ACL to be processed by the KIP Service
 * appointed by the recipient's domain, if one is present.
 * This KIP Service has the prerogative to decide about
 * user identities, including potentially creative aliases
 * for them.  Also, you would be expecting your audience
 * to login to a remote KIP Service and authenticate there,
 * which is unreasonable when they run their own.  When
 * they don't, things change and you could choose any
 * domain, where the sender's domain is the first fallback
 * and "general service" domains are not likely to be
 * trusted by all recipients.  You will get a false result
 * and errno set to KIPERR_SERVICE_NOTFOUND when a domain
 * has no KIP Service configured.
 *
 * The request looks like this:
 *
 * KIP-ENCR-REQ ::= [APPLICATION 10] SEQUENCE OF KIP-ACL-Group
 *
 * KIP-ACL-Group ::= SEQUENCE {
 *    acl    [0] KIP-AccessControl,
 *    group  [1] KIP-Group,
 *    from   [2] TimeStamp OPTIONAL,
 *    till   [3] TimeStamp OPTIONAL
 * }
 *
 * KIP-AccessControl ::= CHOICE {
 *    inline-acl  [0] KIP-AccessControl-Inline,
 *    localref    [1] OCTET STRING,
 *    ...
 * }
 *
 * KIP-AccessControl-Inline ::= SEQUENCE {
 *    blacklist  [0] SEQUENCE OF ARPA2Selector,
 *                   -- may be empty
 *    whitelist  [1] SEQUENCE OF ARPA2Selector
 *                   -- may be empty
 * }
 *
 * KIP-Group ::= SEQUENCE {
 *    keyno    [0] KeyNumber,
 *    encalg   [1] EncryptAlg,
 *    entropy  [2] OCTET STRING
 * }
 *
 * The response packs this information into opaque data
 * under KIP Service encryption to hold it together:
 *
 * KIP-ENCR-REP ::= [APPLICATION 11] KIP-Opaque-Data
 *
 * KIP-Opaque-Data ::= SEQUENCE {
 *    realm    [0] IA5String,
 *    keyno    [1] KeyNumber,
 *    encalg   [2] EncryptAlg,
 *    payload  [3] OCTET STRING
 * }
 *
 * TODO: This API is unsatisfactory.  It is complex,
 *       and does not even allow us to send a list
 *       of KIP-ACL-Group.  We may need  more of a
 *       "builder" structure and more functions.
 *       Expect incompatible API and ABI changes...
 */
bool kipservice_tomap (kipt_ctx ctx, char *domain,
			kipt_keyid keytohide,
			char **blacklist_users,  /* NULL terminated */
			char **whitelist_users,  /* NULL terminated */
			time_t from_or_0,
			time_t till_or_0,
			uint8_t **out_svckeymud,          /* outbuf */
			uint32_t *out_svckeymudlen);      /* buflen */


/** @brief Use a KIP Service to retrieve a key from "service
 * key mud".
 *
 * The "service key mud" is output by a previous call to
 * the function kipservice_tomap().
 *
 * This could crossover to another domain, which may not
 * be accepable to the user, especially if they run their
 * own KIP Service.  See kipservice_realm() for more.
 * 
 * The input data is a KIP-Opaque-Data block, and the
 * output is a newly added key, as with kipkey_frommap().
 * Also comparably, the new key is given a unique keyid
 * that has at least the key number part in the lower
 * 32 bits matching the originally mapped key number.
 *
 * Very often, kipservice_frommap() bootstraps a keying
 * scheme and kipkey_frommap() may add its more complex
 * functions for AND/OR logic on key access.  This level
 * of complexity is not needed in kipservice_frommap().
 *
 * The message sent takes the following form, with the
 * KIP-Opaque-Data literally in the service key mud,
 *
 * KIP-DECR-REQ ::= [APPLICATION 12] KIP-Opaque-Data
 * 
 * KIP-Opaque-Data ::= SEQUENCE {
 *    realm    [0] IA5String,
 *    keyno    [1] KeyNumber,
 *    encalg   [2] EncryptAlg,
 *    payload  [3] OCTET STRING
 * }
 * 
 * The output is a service-filtered form of the data,
 * based on ACL and possibly constrained to a certain
 * period of validity.  This is not reported, but the
 * list of accepted KIP-Group values is,
 * 
 * KIP-DECR-REP ::= [APPLICATION 13] SEQUENCE OF KIP-Group
 * 
 * KIP-Group ::= SEQUENCE {
 *    keyno    [0] KeyNumber,
 *    encalg   [1] EncryptAlg,
 *    entropy  [2] OCTET STRING
 * }
 * 
 * It is possible that an empty sequence is delivered;
 * note that this is considered a valid result from this
 * function, as you generally have no idea how many keys
 * are there anyway.  If you want to enforce a number of
 * keys you can inspect the length of the returned array.
 */
bool kipservice_frommap (kipt_ctx ctx,
			uint8_t *svckeymud, uint32_t svckeymudlen,
			kipt_keyid **out_mappedkeys,
			uint32_t *out_mappedkeyslen);


/** @brief Use the KIP Service to sign a message.
 *
 * This combines your authenticated identity with a checksum
 * statement.
 *
 * In its most general form, multiple checksums can be signed
 * at once.  You provide an array, and the single-element
 * form is as simple in C as a pointer to the one element.
 * It is not an error to provide an array of zero length.
 *
 * The checksums are expanded by this routine; you simply
 * point to an array of ones to include and they will be
 * included, in the order given.  It is up to applications
 * to know what checksum goes where.  Note that checksums
 * can only be combined when they use the same key.  We
 * let you specify the key so we may loosen this behaviour
 * in the future (without API change).
 *
 * The network protocol sends, with empty author field:
 *
 *	KIP-SIGN-REQ ::= [APPLICATION 5] KIP-Signature
 *
 *	KIP-Signature ::= SEQUENCE {
 *		author  [0] ARPA2Identity,
 *		keyno   [1] KeyNumber,
 *		encalg  [2] EncryptAlg,
 *		salt    [3] OCTET STRING,
 *		hashes  [4] OCTET STRING,
 *		meta    [5] OCTET STRING OPTIONAL
 *	}
 *
 * Where the concatenated hashes are passed through the
 * signing key, and into the keyed-hash field.  The reply
 * from the KIP Service is:
 *
 *	KIP-SIGN-REP ::= [APPLICATION 6] KIP-Opaque-Data
 *
 *	KIP-Opaque-Data ::= SEQUENCE {
 *		realm    [0] IA5String,
 *		keyno    [1] KeyNumber,
 *		encalg   [2] EncryptAlg,
 *		payload  [3] OCTET STRING
 *	}
 *
 * The metadata is included as-is.  It is not processed
 * by the KIP Service, but cryptographically bound to
 * the identity of the signer, to assure it has been
 * originated there.  A good use of this field would be
 * a signature timestamp, but applications can use this
 * as they please.  Empty data is signified by a zero
 * value for metalen, while absent data is signified
 * with a NULL value for opt_meta.
 *
 * The output from this routine is the KIP-Opaque-Data,
 * stored in the mud and mudlen.
 *
 * Local copies of the checksums are hashed after adding
 * a salt at the end, intended to turn the KIP Service
 * signature into a (symmetric) blind signature.  The
 * salt is incorporated into the service signature mud,
 * so that kipservice_verify() can quietly incorporate
 * it in the same manner.
 *
 * Since this involves network traffic, memory allocation
 * is dynamic.  TODO: memory pool.
 */
bool kipservice_sign (kipt_ctx ctx,
		kipt_keyid keyid,
		uint16_t sumslen, kipt_sumid *sums,
		uint32_t metalen, uint8_t *opt_meta,
		uint8_t **out_mud, uint32_t *out_mudlen);


/** @brief Use the KIP Service to verify a message signed before by
 * that same KIP Service.
 *
 * The actual signature verification
 * is done locally, but the signature passes through the
 * KIP Service to find the attached signer's identity.  As
 * the signer logged in before using kipservice_sign(),
 * their identity is known in relation to the signature.
 *
 * In the simplest form, you supply the routine with the
 * KIP-Opaque-Data and a (reference to a) single checksum
 * to compare under a given key.  If the checksum differs,
 * we return false and errno is KIPERR_CHECKSUM_INVALID.
 * Only if it all works out, would we return true.
 *
 * In its most general form, multiple checksums can be
 * verified.  You provide an array, and the single-element
 * form is as simple in C as a pointer to the one element.
 * It is not an error to provide an array of zero length.
 *
 * This routine iterates over your checksum array and can
 * invoke a callback on the success or failure while
 * checking it, plus the number of server-supplied sums
 * that it had to skip to get there.  The returned value
 * from the checksum is collated into an overall result.
 * If you provide no callback, the default action is to
 * only accept one-by-one matching of everything.  The
 * callback option exists to give you more control.  We
 * will set KIPERR_CHECKSUM_INVALID if a callback or the
 * default cannot verify a checksum, but your callbacks
 * will continue to be invoked until the bitter end, so
 * you can process in whatever way seems fit.  Verfiers
 * may remain after all checksums have had a callback;
 * in that case, an extra callback is made with both
 * sum_pos==0 and vfy_pos==0 but with vfy_skip>0 set to
 * the number of extraneous signatures.  Note that this
 * would not normally combine with vfy_pos==0.  For this
 * callback, the signature will be set to accepted, as
 * is the case with other skips, though your callback
 * may disagree in any way it wants -- our default
 * handler also never accepts skipping verifiers.
 *
 * The checksums are expanded by this routine; you simply
 * point to an array of ones to include and they will be
 * included, in the order given.  It is up to applications
 * to know what checksum goes where.  Note that checksums
 * can only be combined when they use the same key.  We
 * let you specify the key so we may loosen this behaviour
 * in the future (without API change).
 *
 * The network protocol sends, with empty author field:
 *
 *	KIP-VRFY-REQ ::= [APPLICATION 7] KIP-Opaque-Data
 *
 * Where the contained KIP-Opaque-Data should have come
 * from a KIP-SIGN-REP, which you supply to this function.
 * We retrieve the realm from it, contact the KIP Service
 * underneath and send it this request.  Your user may
 * have a fit about this, so ask them about contacting
 * the realm's KIP Service before calling this function,
 * as it may have privacy implications.
 *
 * The response is verify similar to the KIP-SIGN-REQ:
 *
 *	KIP-VRFY-REP ::= [APPLICATION 8] KIP-Signature
 *
 *	KIP-Signature ::= SEQUENCE {
 *		author  [0] ARPA2Identity,
 *		keyno   [1] KeyNumber,
 *		encalg  [2] EncryptAlg,
 *		salt    [3] OCTET STRING,
 *		hashes  [4] OCTET STRING,
 *		meta    [5] OCTET STRING OPTIONAL
 *	}
 *
 * The author field can be trusted as authenticated by
 * the KIP Service at the time of the signature.
 *
 * The keyno and encalg are as they were supplied back
 * then, and if you have most of the corresponding
 * context we should be able to find it and decrypt
 * the keyed-hash with it.  Do realise that we need it,
 * or we shall have to return an error.
 *
 * The mud includes a salt to turn the KIP Service into
 * a blind signing mechanism: it cannot see what data
 * it is signing.  The salt is appended to local copies
 * of the checksums so they compute to the same outcome
 * as during kipservice_sign().
 *
 * The metadata is returned as-is.  It is not processed
 * by the KIP Service, but cryptographically bound to
 * the identity of the signer, to assure it has been
 * originated there.  A good use of this field would be
 * a signature timestamp, but applications can use this
 * as they please.  Empty data is signified by a zero
 * value in *out_metalen, while absent data is signified
 * with a NULL value in *out_opt_meta.
 *
 * Since this involves network traffic, memory allocation
 * is dynamic.  TODO: memory pool.
 */
bool kipservice_verify (kipt_ctx ctx,
		kipt_keyid keyid,
		uint16_t sumslen, kipt_sumid *sums,
		uint8_t *mud, uint32_t mudlen,
		uint32_t *out_metalen, uint8_t **out_opt_meta);



/** @} */
#endif
/*
 *****     SECTION END: KIP SERVICE FUNCTIONS     *****
 */


/*
 *****     SECTION START: KIP DAEMON FUNCTIONS     *****
 */

/**
 * @def SUPPRESS_KIP_DAEMON Do not show the KIP Daemon functions.
 */

#ifndef SUPPRESS_KIP_DAEMON

/** @defgroup kipdaemon KIP Daemon Functions (server mode functions)
 * @{
 */



/** @brief Initialise the KIP Daemon module.
 *
 * Pass in the keytab file or directory where we will look for keys.
 * When set to NULL, it defaults to the "/var/db/kip/" directory.
 *
 * In a directory, we would be looking for a keytab per domain, named
 * like the domain with ".keytab" added.  Domains are recognised by
 * their ending in a slash.
 */
bool kipdaemon_init (void);


/** @brief Finalise the KIP Daemon module.
 *
 * This call does not fail.
 */
void kipdaemon_fini (void);


/** @brief Open a KIP Daemon handle for use with kipdaemon_handle().
 *
 * Close it with kipdaemon_close().
 */
bool kipdaemon_open (kipt_ctx kip, kipt_dctx *out_kipd);


/** @brief Close the KIP Daemon handle.
 *
 * The handle would have been previously obtained with
 * a successful call to kipdaemon_handle().
 */
void kipdaemon_close (kipt_dctx ctx);


/** @brief Handle the interaction over a KIP connection, starting with the TLS handshake.
 *
 * The cnx socket is assumed to have (just) been returned from accept(), and
 * kip should not be NULL, and set to a base KIP context.  Return a kipsvc handle.
 *
 * FOR NOW, this routine closes the cnx before it ends, as well as the base KIP context.
 * THIS API MAY CHANGE IF WE LATER DECIDE ABOUT ANOTHER WORK PACKAGE SEPARATION.
 */
bool kipdaemon_handle (kipt_dctx kipd, int sox);



/** @} */
#endif
/*
 *****     SECTION END: KIP DAEMON FUNCTIONS     *****
 */


#endif
/** @} */
