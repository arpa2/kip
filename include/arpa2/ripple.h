/* Ripple List Management, using spin locks.
 *
 * These are singly-linked lists with references in
 * head and next fields.  The references hold an extra
 * spin lock which protects the reference and the data
 * that is being referenced by it.
 *
 * The rippling routing locks elements in sequence,
 * and can invoke an action when 1 or 2 are locked.
 * This can be used to construct semantics such as:
 *  - at 1: Find and process a single element
 *  - at 1: Add a new element after a locked one
 *  - at 2: Remove an element from the list
 *
 * A newly added element needs no lock; a removed
 * element will be unlocked after its removal.
 *
 * Actions may find NULL in their last reference.
 * Action are called with a callback data pointer.
 * Each routine returns whether the chase continues.
 *
 * When the symbol RIPPLE_WITHOUT_SPINLOCKS exists,
 * the spinlocks are suppressed in resulting code.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdbool.h>



#ifndef RIPPLE_WITHOUT_SPINLOCKS
# include <pthread.h>
# define SPINLOCK_FIELD pthread_spinlock_t lock;
#else
# define SPINLOCK_FIELD
#endif


/* Ripple references are included as head and next
 * fields in a linked list and its starting block.
 */
typedef struct ripple_ref {
	struct ripple_ref *ref;
	SPINLOCK_FIELD
} ripple_ref;


/* Callback functions are used to process spin-locked
 * elements, which is at least the provided element
 * and possibly the next one, depending on where the
 * callback was supplied to ripple_iter().
 */
typedef bool ripple_cb (ripple_ref *element, void *cbdata); 


/* Tests are used when finding an element in a ripple list.
 * The function should return true when the test succeeded.
 * This will cast the victim to a larger structure, so it
 * must not be used on the head; set skip1st to be safe.
 */
typedef bool ripple_test (ripple_ref *victim, void *cbdata);



/* Initialise a list head as an empty list.
 */
void ripple_init (ripple_ref *head);


/* Cleanup a list head.
 */
void ripple_fini (ripple_ref *head);


/* Ripple iterator.  Start at the head and pass through
 * the list for as long as callbacks return true to
 * continue.  At the end, return the result from the last
 * callback.  The head must not be NULL.
 *
 * The callbacks are cb1 and cb2; the former is used with
 * just one element locked and non-NULL, the latter is
 * used when the next element is also locked and non-NULL.
 * The cbdata is supplied to both types of callback and
 * may serve as an accumulator.
 *
 * When skip1st is true, the head is not passed into the
 * cb1 callback.  This is necessary when cb1 interprets
 * the structure of the head; cb2 is assumed to only be
 * interested in the ripple_ref structure of the first.
 *
 * Callbacks may read and write only what they have been
 * given with a lock.  The ripple_ref are free to change
 * within callbacks, but all prior or later elements must
 * remain in the list, in their current positions, to
 * allow other processes to ripple through.
 */
bool ripple_iter (ripple_ref *head, bool skip1st, ripple_cb *cb1, ripple_cb *cb2, void *cbdata);


/* Insert a new item at the start of a ripple list.
 * The new item does not have to be initialised.
 *
 * This function does not fail.
 */
void ripple_insert (ripple_ref *head, ripple_ref *newbe);


/* Append a new item to the end of a ripple list.
 * The new item does not have to be initialised.
 *
 * This function does not fail.
 */
void ripple_append (ripple_ref *head, ripple_ref *newbe);


/* Remove an element from a ripple list.
 *
 * Return whether the element was found and removed.
 */
bool ripple_remove (ripple_ref *head, ripple_ref *goner);


/* Find the first element in a ripple list that returns
 * true from a test function.  The test function may
 * expand the ripple_ref to a larger structure to see
 * the fields surrounding it if it can be sure that the
 * list was filled with such structures.
 */
struct ripple_ref *ripple_find (ripple_ref *head, ripple_test *testfun, void *cbdata);


/* Map a function over a ripple list while a condition holds.
 * The callback function returns true as its while condition.
 *
 * The return value is the last value returned.
 */
bool ripple_mapwhile (ripple_ref *head, ripple_test *maptestfun, void *cbdata);


/* Drop elements from a ripple list while a condition holds.
 * The callback function removes an element and returns
 * true as its while condition.  Supply NULL as a function
 * to drop every element from the list.
 *
 * The return value is the last value returned.
 */
bool ripple_dropwhile (ripple_ref *head, ripple_test *testfun, void *cbdata);

