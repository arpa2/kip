# Python API for KIP

> *This is a Pytonic API to the KIP functions, which are
> imported from the C libraries.*

The C libraries for KIP avoid "direct contact" with keys,
and they clear the memory that stores them before freeing.
This is retained in the Python API; keys are not on the
Python heap.

The API is similarly simple, but it is more Pythonic by
using objects instead of integer handle codes.


## Module arpa2.kip

The core context is always needed for KIP operations.
The corresponding functions are therefore also the
parent package to optional/pluggable packages.

The package has submodules, all of which are discussed
below:

  * `arpa2.kip.service` for KIP Service clients
  * `arpa2.kip.daemon` to provide KIP Daemon service
  * `arpa2.kip.command` for the `kip` shell command
  * `arpa2.kip.document` for KIP Document operations


### Class arpa2.kip.Context

The `Context` class is a starting point for much work.
It may be subclassed for more functionality to either
`service.ServiceContext` or `daemon.DaemonContext`.

Which context is used determines which KIP libraries
are loaded.  When the `arpa2.kip` module loads, it
*tries* to also load `arpa2.kip.service` and/or
`arpa2.kip.daemon` into its context, but this will
fail silently if the local setup lacks the packages.

Your attempt to names that failed to import will
set off alarms, though.  There are also a few
explicit checks to do this less dramatically.

The choice of just `Context` needs less C libraries
and Python packages to packaged along with an application;
the `service` and `daemon` modules are separately distributed.

Construction is through one of the three context classes:

```
core    = arpa2.kip.Context ()
service = arpa2.kip.service.ServiceContext ()
daemon  = arpa2.kip.daemon.DaemonContext ()
```

Or, equivalently,

```
core    = arpa2.kip.Context ()
service = arpa2.kip.ServiceContext ()
daemon  = arpa2.kip.DaemonContext ()
```

A few simple methods in the core context are

```
__init__ ()
random (length) --> bytes
```

In addition, a few flags are available,

```
have_service --> bool
have_daemon  --> bool
```


### Class arpa2.kip.Key

New `Key` objects are made from methods in the context
object.

The core context provides methods

```
key_generate (alg, keynr) --> key
key_frommap (bytes_keymud) --> key
key_fromkeytab (kvno, enctype, domain, opt_hostname, opt_ktname) --> key
```

The `Key` object provides methods

```
algorithm () --> alg
tomap (mapkeys) --> bytes_keymud
up/data_up (bytes_data) --> bytes_mud
down/data_down (bytes_mud) --> bytes_data
```


### Class arpa2.kip.Sum

New `Sum` objects are made from objects in the context
object.

The core context provides methods

```
new_sum/start_sum (keyid) --> sum
```

The `Sum` object provides methods

```
append (bytes)
restart ()
fork () --> sum
mark (opt_typing)
merge (sum)
sign () --> bytes_sigmud
verify (bytes_sigmud)
```


## Module arpa2.kip.service

The optional extension for addressing KIP Service,
so to be a client to a remote KIP Daemon.

### Class arpa2.kip.service.ServiceContext

The service context provides methods

```
__init__ (rootkey=None, hosts=None)
service_start ()
service_stop ()
service_restart ()
service_realm (bytes_svcmud) --> str_realm
service_tomap (list_key, list_black=[], list_white=[], time_from=0, time_till=0) --> bytes_svckeymud
service_frommap (svckeymud) --> list_key
```

The current C API only permits one key in the `list_key` to `service_tomap()`,
so the full extent of this API is not available inthe current version.


### Class arpa2.kip.service.ServiceKey

Keys output by a service context are
of class `arpa2.kip.service.ServiceKey` which inherits
from `arpa2.kip.Key` and add the methods

```
service_tomap (list_black=[], list_white=[], time_from=0, time_till=0) --> bytes_svckeymud
service_sign (list_sum, bytes_meta=None) --> bytes_svcsigmud
service_verify (list_sum, bytes_svcsigmud) --> bytes_meta
```


## Module arpa2.kip.daemon

The optional extension for offering the
services of a KIP Daemon to clients.


### Class arpa.kip.daemon.DaemonContext

The daemon context adds methods

```
daemon_open (keytab_file_or_dir='/var/db/kip/')
daemon_handle/process (accepted_socket)
```

Note that the current C API assumes the `accepted_socket` to
have come from a TCP-level `accept()` call and actually
having completed TLS wrapping too, and that a Daemon
Context lives to handle just one connection.  The inherited
KIP Context is cleaned up at the end of that connection.
Other than the `keytab_file_or_dir`, no state in terms of
keys passes in, and certainly nothing passes out, except
over the socket connection and as requested.


## KIP Document API, Submodule .document

The KIP Document API is used to create, parse and edit
KIP Document objects.  It is not optional, but it also
is not preloaded into the `arpa2.kip` package API.

**TODO:** Define an API for manipulating the content.

**TODO:** Is it useful to have I/O Hooks for KIP Document?
If so, can they be shared with Command I/O Hooks?


## Command with I/O Hooks, Sub-modules .command, .up, .down

The shell command `kip` that services `kip up` and `kip down`
commands is in module `arpa2.kip.command` and runs as the
`main()` command.

Hooks for uploading are in `arpa2.kip.up` and hooks for
downloading are in `arpa2.kip.down` modules.  These are
namespace packages, so anyone who follows the right
protocol can extend them.  The right way to do this is
to include `arpa2.kip.up` and/or `arpa2.kip.down`
packages with the `__init__.py` contents set to

```
__import__('pkg_resources').declare_namespace(__name__)
```

This is "social behaviour 101" for (this flavour of)
namespace packages.

**TODO:** Check if this package nesting is possible:
`arpa2` namespace, `arpa2.kip` plain, `arpa.kip.up`
and `arpa2.kip.down` namespace.

**TODO:** Work out a protocol for the plugin modules
in `arpa2.kip.up` and `arpa2.kip.down`.  They need
not encrypt/decrypt or sign/verify, but would have
regular expression patterns to match remote peers.
There may have to be some configuration, including
the eagerness to comply to each pattern.



