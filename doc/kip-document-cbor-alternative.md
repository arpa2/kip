**This document is not longer authoritative.**
After starting a KIP Document design in ASN.1 and running into practicalities that made little sense, we decided to switch to CBOR.  This document set the tone of the original project.  KIP Document based on CBOR has become a standalone project:

  * [Project page](https://gitlab.com/arpa2/kip-document) for KIP Document, which mixes conceptual explanation with programming examples and CBOR dumps.
  * [API website](https://kip-document.arpa2.net/) holds the API documentation, currently only with Python3 code examples, but the project.

The following text is the original record.


# KIP Document in CBOR

> *ASN.1 is useful as a language for design and
> documentation.  It is also helpful for extensions,
> canonicalisation and containment of binary content.
> But it is not the only option.*

Tags and lengths before data need not be minimally sized; they may also be longer.  Another approach might be [Concise Binary Object Represenation](https://tools.ietf.org/html/rfc7049).

There is such a thing as [COSE](https://tools.ietf.org/html/rfc8152) but that is simpler than KIP; no surprise there, KIP is rather experimental with cryptographic primitives.  We should leave that in tact, and learn about useful concepts for (distributed) documents like these.


## Early Exploration

A few quick checks and ideas.

Elements are written as arrays, whose first element is an integer tag.  The magic header follows another format, and can therefore be an optional prefix to provide information to fit it into a non-MIME-typed context.

### Magic Header

This was a trick with DER encoding and readable content; this is likely possible with CBOR too.

  * `A` is ASCII 0x41, or major type 2 low end 1.  That's a byte string of length 1.
  * We can add `R` and have something that would not start a KIP flow; we know that magic data follows.
  * Then we add `PA2KIP`, of which `P` is ASCII 0x50, or major type 2 low end 16.  A byte string of length 16, of which `A2KIP` is for recognition; we will end in a recognised `\n` too, the rest are fields of 6 and 4 bytes, that we will also fill with ASCII.
  * The first field of 6 bytes is filled with YYYYMM, initially `202003`.
  * The second field of 4 bytes is filled with flags, 6 bits per character, higher first, base64 character table.  In 4 characters we find 24 bits of information.

      - Flag 0x001 is `signed` for being signed
      - Flag 0x002 is `crypto` for being encrypted
      - Flag 0x004 is `origin` for carrying an origin identity

  * MIME flags combine these:

      - Flag `;authentic` maps to 0x005, so `signed` with `origin`
      - Flag `;private` maps to 0x002, so `crypto`
      - Flag `;integrity` maps to 0x001, so `signed` but not neccessarily with identified `origin`

The information should work well with the UNIX command `file`.

The magic header is a requirement if no MIME-type was provided in the context.  It is optional if a MIME-type is provided; in this case the flags mentioned in the MIME-type must all be set in the magic header too, or else an exception is raised.

An example Magic header in `hexdump` with `authentic` and `private` flags is

```
00000000  41 52 50 41 32 4b 49 50  32 30 32 30 30 33 41 41  |ARPA2KIP202003AA|
00000010  41 44 0a                                          |AD.|
00000013
```

This is actual CBOR, its parsing makes sense, it can be distinguished from any possibly following and it relays useful information to tools.

It is on a line of its own for readability and for easy compatibility with command line tools; it is the only thing that could match the regular expression `^ARPA2KIP` on the first line, and you could choose to match the version as well.  You are welcome :)


### Metadata

This is of course a dictionary, so (key,value) pairs.  It may be part of signed or unsigned content and,
to allow variation, may occur in multiple places.  (Could it be encrypted?  Would that be useful?)
See below for the format definition.

### Concern: Hashing CBOR

We need a canonical representation.  As with other formats that initially ignore this constraint,
this may still be a topic of some debate, even though
[a first stab](https://tools.ietf.org/html/rfc7049#section-3.9)
was taken.

## Simplifications

Collections are a central idea, as well as their composition from elements.  But what elements make sense, and what to do with them?

  * *Operators* are probably too much.  Maybe have those nested inside documents held, rather than pull it into the KIP Document layer.
  * *Brackets* and *No-Param* are only required for Operators, and could then also go.
  * The other elements do make sense: *objects* and *references*, with or without *encryption*.
  * *Object syntax* may be a bit too much, and better kept inside the document, which is a byte sequence when we drop operators.  We might however consider *partial document extractions*?  [We'd still mention the total document hash.]  Tags in the document could help to provide start/end tags, perhaps just by counting them in a tagging sequence.  Onced hashed, the contents are static anyway.  Offsets are faster but may lead to abusive extractions.
  * The annotations for *signatures* and *encryption* may be taken from COSE.  We probably have to think harder for *key mapping* and especially for the *KIP Service* aspects that are so vital to the idea of KIP.
  * It may be an idea to introduce the *signing key* anywhere signing starts, and have the *signature* right where it ends.  This means we can have a flow of elements without nesting; nesting may be useful to get to referenced data, but it would not be used to mark signed and/or encrypted portions.  Nested references would be signed as references, not as referenced content, but of course they can still benefit from a *Merkle tree* structure, another vital part of the KIP design.

## Redesigning the Content

Whichever encoding we select, we can certainly cleanup the work by making it a more sequenced notation.

  * The document may start with a *magic header*
  * The document may then include *metadata* as a dictionary with LDAP-ish attributes (mind duplication)
  * The remainder of the document is a *sequence of chunks*

The chunks are also really simple:

  * Signing keys are introduced where they start working for signing, or where they are made available for encryption
  * Signing keys stop working where they introduce a signature
  * References are signed as references, but not as the referenced document; the reference may however contain hash values which are part of the signature and therefore extend security to nested layers
  * Encrypted objects and encrypted references may apply a signature on the encrypted form.  Signatures on the encrypted form can also be validated by someone who cannot see the content.  Encryption keys also trace content, and may be used to add a closing signature t the end of data that passed through them.

## Applications

Just to try if this could work in anticipated use cases.

**Text Processing.** We can create documents, even from parts authored by others.  To freeze content, we add signatures and/or sign references.  Note that parts may still "opt out" from this protection by making a reference without a hash on it.  This is deliberate and should be marked as content-under-review or non-authentic content.  Similarly, encryption will be broken by links; this may be deliberate to address different audiences in different document parts.

**Web Links.** To have a `data:` URI, we skip header, maybe the metadata, and immediately turn to the reference.  The reference may well be signed by its origin.

**Reservoir.** To reference partial content in Reservoir, we use the `documentHash` content to locate it, and possibly UUID values or just a URI.  It makes sense to have a Document Collection holding multiple document roots, possibly referencing between Resources in the Collection, even allowing overlap.

**P2P.** To reference partial content on a peer-to-peer network, we just mention the hash value.  It does not matter what network we use, as long as it indexes by document content; if not, an adaptation may be required for using that network.

**Git.** To reference partial content in Git, we mention its hash value and possibly one or more locations.

## Introducing Ranges

Documents have a sequence of chunks in them; these might be counted with the exclusion of magic and meta.  Within a chunk, the actual content may be counted.  In objects, that would be the byte numbering (for plain content) and in references, the story starts from the beginning with chunk numbering and its contained numbering system.  Not mentioning a level means to include it completely.

  * Having no range set means that everything is counted.  Having an open end means that it has no start or no end (in which case the key might be the actual range limit).
  * The range from 3 to 3 includes chunk 3 (the fourth since we count from 0) in its entirety.  It is equivalent to saying from 3.0 to 3.X if X is the number of counted elements in the chunk.
  * The range from 3.1 to 3 includes chunk 3 but for its first element.
  * The range from 3.1 includes chunk 3 but for its first element, plus chunks 4, 5, 6, ... as long as supply lasts (and the key is not terminated).
  * The range from 3.7.8.1 to 3.7.8.1 dives into chunk 3, follows its reference, finds its chunk 7, follows its reference, finds its chunk 8, and includes the second element (with number 1) under the local counting system (byte or referenced element).

It should be noted that references may be inconsistent until a reference is hashed.  It is generally wise to warn or bail out of other situations.

We can combine ranges to include multiple parts, if it is not possible to just name them in different chunks.

Ranges might be used for signing and encryption, but may then lead to pretty strong confusion.  Let's not do that.

Ranges are downright useful in references, to constrain the part being copied.  It saves on storage to list ranges, but it adds to the complexity and may also reduce clarity of the concept.  And then again, it may not be such a bad idea.  Not sure what to do.

Ranges are represented as two arrays of integers; an open end is made explicit as an empty list, but not `null`.  The first list of a range must not end with a zero value, as that can always be taken off.  Such rules cannot be given for the end of a range; at least not without looking up the data.

As with the other parts encoded in CBOR, the range structure is part of signing mechanics.

Signatures in referenced data only stay in tact when their complete range is quoted.  You cannot have correct signatures on data taken out of context!

## References

We can continue to have multiple kinds of reference/identity, just for fewer uses.

  * `uri` is very useful; it must be absent instead if it would otherwise be an empty list
  * `uuid` is probably useful in many contexts; it must be absent if it would otherwise be an empty list
  * hashes are not references but constraints; they are part of references for the second reason
  * `(domain,colluuid,resuuid)` for Reservoir?

References take the shape:
```
# type.v0:int, { ?uri, ?uuid, ?hash, ?range }
[ TAG0_REF, {
  'uri' : ['http://example.com/xyz'],
  'uuid': [ ],
  'hash': {'sha256':b'\xe3\xb0\xc4\x42', '_size':123},
  'range': [ null, [3,0], [4,0], null ],
} ]
```

## Object Types

Objects inline data literally.  They carry them as bytes or as UTF-8 string data.  Both are processed by signatures and encryption in that form.

```
# type.v0:int, object
[ TAG0_OBJ, b'...' ]
```

## Encrypted Objects and References

Encryption always yields byte strings.  The type is `mud`, and unpacking it reveals whether it conceals a reference or an object.  Signing ranges over the encrypted form, except for a final signed hash by the encryption key which ranges over the encrypted content.

```
# type.v0:int, mud
[ TAG0_MUD, b'...' ]
```

## Key Material

Public keys are `pubkey0`, keys from KIP Service are `svckeymud0`, Key maps are `keymud0`.  The hashes expected by the listed keys are mentioned, and should be updated from here on.

```
# type.v0:int, keynr, pkitype.v0, hashes, binkey
[ TAG0_PUBKEY, 123, 'pgp0', 'sha256', b'...' ]
```

The encryption type and key number are part of `svckeymud` and `keymud`, and so is the hash algorithm to use; their CBOR structures are therefore simpler.

Although KIP Service requires a realm name, it can be extracted from `svckeymud0` with `kipservice_realm()` and, if trusted, subsequently tested by `kipservice_frommap()` reaching out to the realm.  The reason that the realm is part of `svckeymud` is that this enables application of integrity checks involving the (user-acceptable) realm.

```
# type.v0:int, svckeymud
[ TAG0_SVCKEYMUD, b'...' ]
```

```
# type.v0:int, [int,int,...], int, int, keymud
[ TAG0_KEYMUD, keynr_alg_pairs, keynr, alg, b'...' ]
```

This is the suggested order, but anything else is possible.

## Signatures

Signatures are on the content as it appears in these files, including such things as references.  This imposes certain difficulties with hashing (see above) but it does not apply to referenced content; for referential integrity, just add hashes to the references.

Signatures start in the CBOR element after the one introducing a key, and they end before the CBOR element holding the signature.  After the signature, the key is considered "used up" and will no longer be usable.  **TODO:** Consider skipping certain elements completely; notably, keys and signatures may not be desirable to sign as well, perhaps even for legal reasons.

Signatures need some grounds for validity, so they are encrypted with their related key.  This indicates that anyone with access to the signing key could have modified it; with a group-shared symmetric key that applies to all those in the group.  Add personal signatures to remedy that.

```
# type.v0:int, keynr, pubsig
[ TAG0_PUBSIG, 123, b'...' ]
```

The encryption type and key number are included in the `sigmud` message portion, so unlike with public key signatures it is not required for symmetric signatures.  Note that public keys are in plain view, but this really is `sigmud`, encrypted with the signing key.

```
# type.v0:int, keynr, sigmud
[ TAG0_SIGMUD, b'...' ]
```


## Metadata, once more

Some metadata may be good to sign.  This means that it must be part of the sequence of chunks, which is easily managed with

```
# type.v0:int, metadict
[ TAG0_META,
  { 'documentAuthor': ['Rick','Henri'],
    'documentLocation': ['Enschede'],
  }
]
```

Now we can put it in anywhere.  We can also have multiple if we wanted to.  Perhaps local annotations could make sense, for example.


## Signature-skipping Content

Do we want content that skips signatures?  For example for notes made on the document?  Probably not; anything along those lines alters the hash of the document, and so its storage location in a hash-based index.  It is better to manage such metadata separately from authoring content, quite possible in an annotation document referencing this one, possibly as a whole or just ranges of it!


## Advanced Trickery

We have a few clever tricks with hashes and keys that may be added as we go.  The API to KIP explains what they are.

