# KIP Service and Key Tables

> *Every KIP Service needs a long-term key to protect the keys
> once concealed by it, and later uncovered by it.  Proper
> dissection of keys between realms is a good idea.  And how
> about Kerberos server principal keys?*

There are a few secrets that a KIP Service needs to manage.
They can be stored in Kerberos key tables, or `keytab` for short.


## Long-term Keys per Realm

There is a need for long-term keys.  Since these may later have
to be exported, backed up, split over hosts and so on, it is good
operational practice to have no overlap of such keys between realms.
This allows the removal of a single realm at a time, too.

Within a realm, there is an option to further split the keys,
perhaps for regular rotation of the entropy used.

A keytab would hold entries such as `kip/MASTER@REALM.NAME` to represent
these keys for a realm, or possibly another name under a local policy.
The `MASTER` part must not have a dot in it, so it can be clearly
distinguished from a host name.  It may be stored on a keytab that is
dedicated for use as this master key to further avoid confusion,
clutter and assaults.  The `name-type` should be set to `NT-SRV-INST`
or the numerical value 2.


## Service Keys per Host and Realm

The second kind of key needed is to be usable as a service.  These
look like `kip/host.name@REALM.NAME` and are setup with the KDC,
and so users can access the KIP Service under that name.  They
would find the `host.name` by looking for SRV records in DNS, and
they might find the `REALM.NAME` by looking for TLSA records or
with `_kerberos TXT` records.

Usually the host and realm change together, but in general there
is a separate key for each combination of these two values.  These
have `name-type` set to `NT-SRV-HST` or numerically to 3 like for
all host-based services.


## Crossover Keys between Realms

It is possible for any client to connect to a KIP service in
another realm.  This may use KXOVER to construct a ticket that
crosses over to the another realm.  The criteria for selecting
a remote service would be based on the recipient's domain and
on the availability of KIP Service underneath it.  Clients would
connect with their own client name, usually `user@CLIENT.REALM`.

KIP Services may find it useful to connect when a representative
of the sender and recipient want to exchange pleasantries.  This
can be useful because each represents the prerogative over the
assignment of user names.  The KIP Services would just validate
their mutual host names are being in SRV records under the
domain name that occurs in the realm for KIP.  In this scenario,
each KIP Service identifies as `kip/host.name@DOMAIN.NAME`,
using their customary key.  There is probably a need for KXOVER
between the realms as well, to be arranged by the KDC; the
best SASL authentication form is therefore GSS-API.


## Keytabs for Users?

It may be dangerous to allow users to also load keys from keytabs.
Since keytabs are stored without protection, they might end up with
"make belief" security.  This would be downright harmful.

We cannot say yet if we should withhold keytabs from users.  It is
certainly not a facility to build into initial versions of `kip up`
and `kip down`.  The facilities for keytabs are initially just for
the KIP Service, nowhere else.

