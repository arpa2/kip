# HAAN: Helm Arbitrary Access Node

> *HAAN is to authentication what KIP is to encryption.
> Both systems store a long-term static key that is always
> mixed with session-specific entropy to derive a session
> key.  KIP requires no storage for encryption keys, and
> HAAN requires no storage for credentials.*

Most online resources are assigned on the basis of an
email address or other form of communication.  When a
password is lost this communication channel is used, in
spite of its low-grade security, to change the access
to the online resource.  This is not very satisfying in
terms of security.


## Bootstrapping Credentials

Even when online resources are protected from direct
abuse, there is still the problem of the procedures used
when someone (hopefully the actual owner) claims to have
lost the credentials to access the resource.  Being unable
to verify that this is the case, a password reset link is
usually sent to an email address.  This is an opportunity
that attackers might take advantage of, and should be
avoided if possible.

An added problem is that the credentials to an underlying
email address might also be lost.  And so on.  This may
leave users completely locked out of the resources that
they do actually own.  Nothing helps, but calling an
administrator and pulling a magic trick (usually after a
feeble check of the owner being who they say they are).

The real problem to solve here is one of credential
bootstrapping.  How can we allow anyone to register
online resources with proper fallback programs, but
without the staircase of online resources that ultimately
must end with a higher authority's administrator?

Intestingly, credentials are very easy to create,
*especially when they have no value at all.*  Think
of opening a new back account; nobody is interested
in its protection while nothing has been deposited
into it; what needs to be protected is the money in
it.  Similarly, completely worthless accounts can be
created at a whim, and only gain importance when their
owner uses them to set up online resources.  This is
what we mean with bootstrapping of credentials.


## Have a Plan B (and C)

People being who they are, they will loose credentials.
They also have a tendency to die at some point, or are
fired from their jobs, or they might go mad; in all these
cases it is good to have a Plan B to gain control over
their online resources.  An employer may want to relieve
an administrator from their duties, a notary official may
unlock an online resource for a deceased next of kin, or
a collaborative group may decide to replace a member from
their group who has gone gaga.  Oh, and in case you lost
your own credentials a friend may kick in with an extra
credential to help you out.

Nowhere in this would you need to rely on overruling
powers by service providers.  All you need is to assign
more than one credential to your online resources and
distribute these credentials in such a way that others can
fulfil the agreed-upon control over the resources.  So,
you hand out a credential to your friend under a friendly
agreement, or to a the notary official for use under
contractual obligations, and so on.  There also is no
reason why an owner could not have multiple credentials
of their own.


## Credentials have a Public and Secret part

Although founded on symmetric keys, credentials for HAAN
come as a public and private part.  One might think of
them as a username and password, both long codes of
considerable strength.  The public/username part may be
shown to others, so they can set them up in their online
resources.  The secret/password part should be kept in
safe keeping, for instance in a sealed envelope in a
locked cabinet or a bolted safe.  Again, you choose the
security level that you desire.

Because these credentials are truly worthless, you can
create many of them.  So, you can have a separate
envelope dedicated solely to your neighbour's domain name
portfolio, but not to yours.  He might do the same towards
you, or if he chooses, use one envelope for both his and
your domains.  As long as the secret/password part is never
shown to others it should be no problem.

The public part looks like a (very long) email address.
There is a long code, then an AT sign and a realm.
The realm is the domain name of a place where a validating
HAAN server is running.  Anytime you want to show your
ownership of the credential by entering its secret part
you are facing this HAAN service.

Anyone can run a HAAN service, because it is incredibly
light-weight.  This allows you to choose a party that you
trust; perhaps your own home connection, a friend's and
a domain hosting provider's would do.  Since you can
setup multiple bootstrapping credentials in your
online resources, you can mix such services in any way
you like, and thereby protect yourself from one of them
ever retracting their service.


## Everyday Authentication through HAAN

Although HAAN is designed to be a fallback system, you
might choose to have a (limited-power) credential setup
in your browser, and use it for day-to-day interactions
with your online resources.  To have the fastest response
times you should use a HAAN service from your service
provider for such purposes.  Do keep in mind that it is
still valuable to have fallbacks installed, especially
because a day-to-day key is easier to loose or to have
stolen; you want to be able to remove it using fallback
credentials.

The HAAN technology is perhaps a bit too specific for
such uses, however.  You probably want to create a nicely
named `admin` account, and store a long and difficult
secret for it.  The derivation of a secret part from the
(originally random) public part is not of any use here.
The protocols can be the same, however.


## HAAN as a Protocol

To create a fresh and worthless pair of public identity
and secret, the HAAN protocol first generates a long
random number as the public identity.  It attaches the
realm of a validating HAAN service, which identifies a
fixed key to use; this key is used with the public
identity as a salt, to produce a random sequence from
which the secret part is a prefix of secure length.

The public and secret part are offered together, and
then forgotten completely by the HAAN server.  You
may download it once as a PDF or other document, but
a reload will get you another pair.  The HAAN service
is not a password oracle; you cannot enter the public
identity and expect to receive the password.  Since
the derivation of a the password is fixed for any
given HAAN service realm, there cannot be a change
of password either, only of the pair of public and
secret parts.

The HAAN validation service can access the same key as
used during generation, to derive the secret part for
a given public identity.  This secret part is not ever
handed out, but it is possible to validate against
this secret.  This now behaves like a password, and
though it might be matched, it cannot be guessed.
The high entropy and lack of any pattern or cat name
in the secret make it safe from cracking or rainbow
tables.

Given this, relatively modest password schemes can be
used.  The vital concern would be to avoid replay,
as well as recording of the session, so the scheme
used should incorporate server challenges and contain
the traffic in a cloak between a user and the HAAN
service.

SASL can be used as a password scheme, and because the
secret is derived in the same form as it is available
to the user, any challenge/response system can be
computed from it.  SASL can be run in a backend over
Diameter, and this is precisely the service that the
HAAN service offers.  This backend may be used behind
the system of an online resource provider and, as long
as you use it over SXOVER encryption, you should feel
free to have the traffic relayed to the backend whose
realm is attached to the public identity.


## HAAN to place You at the Helm

The interface that we call ARPA2 Helm is really just
a list to which you can add and remove resources, or
in Helm-speak take-off and landing, and you might
reconfigure them in-flight.

The Helm is accessed by a person called a Pilot, or
a Co-Pilot when they need to get the approval of
others.  Co-Pilots are in fact also Pilots, but a
Pilot has the full 100 points assigned to make
changes in the Helm, while a Co-Pilot is a Pilot
with less than 100 points and needs to collaborate
to get to 100 points on any action.

Very often when you register an online resource, a
first HAAN identity will be created for you, but
you are strongly encouraged to add ones that you
prepared yourself.  You should do this immediately,
even before the online resource is assigned to you,
so you never risk loosing it.  Imagine registering
a wonderful domain name and not being to access it
because you accidentally removed a credential that
was assigned to you during the registration process!

