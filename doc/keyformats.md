# KIP and Key Formats

> *KIP is the Keyful Identity Protocol -- it makes keys easy to use to
> anyone with an identity.  There are several forms of wrapping and
> unwrapping keys in KIP, described here.*

In cryptography, most problems are essentially key handling problems.
KIP reduces the problem of encryption (*how do I get the key to use*)
to one of authentication/authorisation and wrapping/unwrapping of keys.


## Key Numbers and Key Identity

On the wire, key numbers are 32-bit unsigned integer values.
This provides a large-enough space for unique key numbering,
though uniqueness is not a requirement; the KIP routines will
happily accept multiple keys with the same number, and treat
them as alternatives while decrypting.  It does not matter
if the same key number appears with the same, or different
key material.

This simplifies a few practices, namely random key generation
and merging of documents.  You can use these mechanisms without
being concerned for the results.  Furthermore, it is possible
for the same key number to appear with various algorithms,
which is indeed also true for the underlying Kerberos formalism.
If this happens under KIP, the various algorithms would each
end up with a unique key identity.

Key identities are used behind the API, but never sent over
the wire.  The idea is that a handle in a computer program
can be used to manipulate an exact instance of a key, and
this is indeed necessary in a few circumstances, such as
when encrypting to the third key in a document, or the second
generated.

Key identities may be unique, but because they are never
stored they are ephemeral.  They are constructed by interpreting
the information being read, and there may be some order to this,
but the code runs are completely independent and so the actual
identities found may differ between runs.

Key identities can be any locally unique format, but in practice
they usually end up being unsigned 64-bit integer values
whose lower half holds a key number.  The upper half is non-zero
to indicate the unique key identity.  This enables us to
merge the number spaces of key identity and key number, and
have several routines that can accept either.


## Checksums and their Identity

Checksums in KIP are secure hashes, encrytped with a key to
provide it some protection.  The purpose is to ensure that
the content was not manipulated by anyone who did not hold
the encryption key, for instance by shuffling the order of
encrypted blocks, or by inserting or removing some.

Checksums use the same message digest that is incorporated
in the algorithm of the key, and a signature made with it
is basically an encrypted block holding that message digest.

Checksums have a unique identity, and they share the space
that holds the key identities too.  This is useful, because
every key will be created with an implicit checksum that
records plain text data as it occurs before successful
encryption or after successful decryption.  This means that
the same checksum is derived when protecting a document as
when recovering it.  As a result, the checksum can be
exported and saved at the end of a document.  There is no
need for negotiating a hash, because this follows from the
algorithms of the keys being used.

Note that every key identity serves as a checksum identity,
but that the same does not hold for a key number; you need
to know what key to use before you can ask for a checksum.
This is also required because a key number may appear with
several algorithms, so it would not lead to a decision
about the message digest algorithm.


## Key maps

A key map is a translation from one key to another.  If you have key
*K*, you might be able to produce key *J*, for instance.  It is a
form of indirection.  This in itself adds little value.

The complete story is a bit more complex.  You could have multiple
paths leading to the same key, such as *K* to *J* and *L* to *J*.
Now, anyone with key *K* or key *L* can get hold of key *J*, making
the key maps work together in an "or" logic fashion.  We can now
produce text encrypted just to *J*, but rest assured that both *K*
and *L* suffice to access it.

To allow a complete positive logic (not including negation), we also
need to add "and" logic.  This is done by requiring multiple keys
for a key map, each of which are needed before the key map can be
unwrapped.  So, key *J* may be produced when *M* and *N* are both
known.  The complete logic calculation is now *K or L or (M and N)*.

**Wire format.**
On the wire and in a file, we use pairs *(keynr,alg)* in which *keynr* is an
unsigned 32-bit integer representing the key number, and *alg*
is a signed 32-bit integer with the encryption algorithm as they
are specified for use with Kerberos.  As a prefix to the binary
content of a key map, there is one such pair for every required
key, followed by another for the key to be generated with it.
**TODO:** Or the latter could be part of the wrapped content.
The binary content is encrypted with each of the key pairs, in
their order of appearance in the prelude, but otherwise it just
holds the key's raw contents.


## KIP Service requests

A service request is similar to a key map, in that it results
in keys.  The origin however, is not another key but an exchange
with the KIP Service involving authentication.  This is the
essential procedure for key retrieval after authentication (is it
you?) and authorisation (are you on the ACL?).

**Wire format.**
The prelude for KIP Service requests are more extensive than
for a key map, and there may be multiple keys involved.  This
is best understood by looking at the ASN.1 specifications, but
it involves the authenticated identity of the originator, the
access control list with permissable key retrievers, and some
other things like validity times.  The key format is simply
the raw key data, without key identification information; there
is enough naming information in the context to locate a key.
The raw key data is not encrypted; instead, a much larger bit
of the KIP Service request is encrypted, as described in the
ASN.1 specification.  This larger encryption chunk is needed
to allow a strong binding between the ACL and keys.


## Key tables

A "keytab" is a construct from Kerberos, used to store keys in an
unencrypted form.  This makes it a highly undesired form in the
frivolous context of a client, but it may be useful in the more
constrained environment of a server.  Indeed, this is initially
the format used used by the KIP Service to store its key(s) that
form the beginning of the trust chain.  Access to a key table
should be pinned down as much as possible.

Key tables usually hold names `kip/MASTER@DOMAIN` for a given
`DOMAIN` to be serviced.  This allows virtual hosting, where
multiple domains (or security realms) can be serviced on one
server, while keeping the possibility open to add or remove
realms and their unique keys.

**Wire format.**
Key tables are not transported over the wire, but they are stored
on disk.  Their format is determined by the underlying Kerberos
library.


## Key Usages

We distinguish four key usage numbers, which Kerberos uses to
scatter key material and thereby separate the binary spaces
and avoid abusing the result of one usage with the input to
another.

  * **USERDATA** is used for encryption of application data;
  * **MAPPING** is used during key mapping;
  * **INTEGRITY** is used for checksum signatures;
  * **SERVICE** is used by the KIP Service.

These numbers are different from the uses in Kerberos libraries
as well, so there is no possible mixup, accidental or purposeful,
between messages from there either.


