# Anonymous Users in KIP Service

> *Authentication is obligatory in the KIP Service protocol,
> but several actions are considered public.  This means that
> SASL authentication with `ANONYMOUS` is deliberately used.*

We do not want to split sessions into authenticated and
unauthenticated session halves.  Rather, we prefer to provide
less service to those who deliberately choose to not login.
This is what `ANONYMOUS` authentication expresses.


## Temporary Identity

Users will be somewhat traceable.  Their IPv4 address and port
or their IPv6 address or /64 prefix may be set as their userid,
for instance

```
+visitor+ip4+123+45+67+89+49594@example.com
+visitor+ip6+fdaa+bbcc+ddee@example.com
+visitor+ip6+fdaa+bbcc+ddee+ff00+11ff+fe22+3344@example.com
```

Such names may be reproduced for some time, they can be logged
and they may be used in ACL settings.  When such internally
generated (service) identities are not explicitly put on the
whitelist, they will be blocked.  Note how the ARPA2 ACL also
easily allows banning the whole `+visitor@example.com` branch,
regardless of the following refinement.

More importantly, the internal signaling of the KIP Daemon will
indicate that `ANONYMOUS` was used, so that only public services
are accessible.


## Local Authentication

The customary authentication relies on a SASL layer below `kipd`,
notably our wrapper Quick-SASL.  For `ANONYMOUS` authentication
`kipd` will bypass with its own logic.  This logic makes a point
of signaling the public-only rights internally.


## Connection Pooling

The KIP Client may pool connections.  It distinguishes them on
the basis of the domain name *and* whether authentication was
performed by the user, or skipped with `ANONYMOUS`.  So the
KIP Client is also aware of the distinction.

When in need of KIP Service, the internal API will indicate
whether authentication with an identity is needed, or that an
`ANONYMOUS` login suffices because public services are used.


## Privacy by Minimal Login

When such public services are used, it is usually better to
not reveal identity information at all.  The KIP Client will
basically avoid login when it is not required, while it is no
concern for the KIP Daemon if login was done without needing it.

It is therefore possible, but not likely, to start a connection
with identity-based login, then using a public service and then
a service that requires login.


## Tests and Demos

This change is long overdue.  There was a need to login for a
few operations that would not have required so with public-key
crypto, which raises questions, and rightly so.  Tests and demos
will be easier, and make more sense.

