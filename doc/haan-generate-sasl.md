# Generating Helm Identities

> *It is easy to generate new Helm Identities.  Can it be
> incorporated into SASL?*

A new identity is worthless, so granting its identity as soon
as it is generated is not really a problem.  That means it
may be used in SASL.

**Note:** This is presently just an idea!


## GENERATE

To generate a new identity, either SASL or GSSAPI can be used.
The protocol is straightforward:

```
C: mech="GENERATE", c2s="unicorn.demo.arpa2.lab"
S: success,s2c="pub-lic-id sec-ret-part"
```

This may be used as part of any SASL exchange to create a new
secret.  The public part and realm are communicated to an
application that may store it in a password maneger for day-to-day
use, or produce a PDF to print.

This protocol may even be run over SXOVER-PLUS, so that the
backend is known to be reliable and thus the identity supplied
comes from the targeted realm.  In this case, the `c2s` token
must match that requested by the surrounding Diameter exchange.
When returning, the username can be supplied to the foreign
server, which may continue to use it.

Is there a usecase for direct use of this connection?  If so,
it might call for a GENERATE-PLUS mechanism and the
requirement to run it over a secure channel like TLS.
This does have the benefit of avoiding the browser, which is
quite attractive for PDF printing.  Even in such cases, it
may be an idea to store the username for field completion.


