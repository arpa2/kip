.TH KIPSUM_SIGN/VERIFY 3 "March 2019" "ARPA2.net" "Library Calls"
.SH NAME
kipsum_sign, kipsum_verify \- KIP signing and verification
.SH SYNOPSIS
.B #include <stdbool.h>
.br
.B #include <stdint.h>
.sp
.B #include <arpa2/kip.h>
.sp
.B bool kipsum_sign (kipt_ctx \fIctx\fB,
.RS
.B kipt_sumid \fIsumid_or_keyid\fB, uint32_t \fImax_siglen\fB,
.br
.B uint32_t *\fIout_siglen\fB, uint8_t *\fIout_sig\fB);
.RE
.sp
.B bool kipsum_verify (kipt_ctx \fIctx\fB,
.RS
.B kipt_sumid \fIsumid_or_keyid\fB,
.br
.B uint32_t \fIsiglen\fB, uint8_t *\fIsig\fB);
.RE
.br
.SH DESCRIPTION
.PP
These functions sign a checksum under a
\fIsumid\fR or \fIkeyid\fR, or validate it.
For any key introduced in the KIP context, an
implicit checksum is started.  Every successful
call to
.BR kipdata_up (3)
or
.BR kipdata_down (3)
appends the plaintext to the integrity check.
The signature is protected with a key, and this is
considered during verification.  The use of the key
is the protection of integrity, because no party
can change the data without invalidating the
checksum, unless they hold the key.
.PP
Checksums are secure hashes, more specifically the
hash algorithm implied by the key used to protect
it.  Since this key is already known at the beginning
of the checksum, there is no need to be explicit
about the checksum algorithm.  Unlike a MAC, the
entire hash value is included.
.PP
If an output buffer must be allocated but no size
is available, it is possible to invoke these
functions with the \fImax_xxxsz\fR set to zero.
The functions then return an error code
\fBKIPERR_OUTPUT_SIZE\fR and store the required
size in the \fIout_xxxsz\fR, which is also used
on successful return.
.SH RETURN VALUE
These functions return \fBtrue\fR on success, or
\fBfalse\fR with \fBerrno\fR set.  Note that the
functions use the \fIcom_err\fR system and define
their own \fBKIPERR_xxx\fR error codes.
.PP
.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl, for the ARPA2.net project.
.SH "REPORTING BUGS"
.PP
Please use our issue track for reporting bugs.  It can be
found online, at
.IR https://gitlab.com/arpa2/kip/issues .
.SH COPYRIGHT
.PP
Copyright \(co 2019 Rick van Rein, ARPA2.net.
.PP
ARPA2 is funded from InternetWide.org, which in turns receives donations
from various funding sources with an interest in a private and secure
Internet that gives users control over their online presence.  This particular
project is part of ARPA2 Mail & Reservoir, funded by SIDNfonds.
.SH "SEE ALSO"
.IR kip "(1), " kipkey_generate "(3), "
.IR kipkey_frommap "(3), " kipservice_frommap "(3)."
.PP
Online resources may be found on the project home page,
.IR http://https://gitlab.com/arpa2/kip .
