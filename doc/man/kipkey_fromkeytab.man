.TH "KIPKEY_FROMKEYTAB" 3 "April 2019" "ARPA2.net" "Library Calls"
.SH NAME
kipkey_fromkeytab \- KIP keys loaded from Kerberos keytabs.
.SH SYNOPSIS
.B #include <stdbool.h>
.br
.B #include <stdint.h>
.sp
.B #include <arpa2/kip.h>
.sp
.B bool kipkey_fromkeytab (kipt_ctx \fIctx\fB, .B char *\fIopt_ktname\fB,
.RS
.B char *\fIopt_hostname\fB, char *\fIdomain\fB,
.br
.B kipt_keynr \fIkvno\fB, kipt_alg \fIenctype\fB,
.br
.B kipt_keyid *\fIout_keyid\fB);
.RE
.br
.SH DESCRIPTION
.PP
This function loads a KIP key from file in the
keytab format used by MIT krb5 and Heimdal, two
flavours of Kerberos5.  These keytabs are used
for storing unencrypted keys as they are used
by services.
.PP
The underlying assumption for keytabs is that
their storage is somehow protected, presumably
by their isolated installment as a server.  This
is perhaps not completely reliable given the
virtualisation that we use these days, but some
effort can be made for encrypting file systems
or perhaps exporting secrets from a host OS.
Anyhow, the assumption is that keytabs are
sufficiently protected by infrastructure.
.PP
This means that it is not a good idea to use
this call on clients.  We therefore may choose
to include it just in daemon libraries in the
future, so that is another reason for clients
to evade this function.  It is however a way
for KIP daemons to bootstrap their keying
services.  Note that more advanced mechanisms,
such as PKCS #11, are probably better, but in
return for possibly higher investments.
.PP
The function
.BR kipkey_fromkeytab (3)
takes in a \fIopt_ktname\fR but will fallback
as the underlying Kerberos library does, to
environment variables and default files.
The \fIopt_hostname\fR is used to specify a
service named
.B kip/\fIopt_hostname\fB@\fIDOMAIN.NAME\fB
instead of the default name
.BR kip/MASTER@\fIDOMAIN.NAME\fB .
The \fIDOMAIN.NAME\fR mentioned here is just
the uppercase version of the parameter
\fIdomain\fR, as is common with Kerberos; it
is provided as a parameter in support of
"virtual hosting" of KIP Service to any
number of realms.  The
.I kvno
and
.I enctype
parameters serve their usual purposes
of key identification
in Kerberos, but may be set to 0 when unknown.
The result is, as with
.BR kipkey_frommap (3)
a key identity to use in the current content
.IR ctx .
.SH RETURN VALUE
.PP
This function returns \fBtrue\fR on success, or
\fBfalse\fR with \fBerrno\fR set.  Note that the
functions use the \fIcom_err\fR system and define
their own \fBKIPERR_xxx\fR error codes.
.PP
.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl, for the ARPA2.net project.
.SH "REPORTING BUGS"
.PP
Please use our issue track for reporting bugs.  It can be
found online, at
.IR https://gitlab.com/arpa2/kip/issues .
.SH COPYRIGHT
.PP
Copyright \(co 2019 Rick van Rein, ARPA2.net.
.PP
ARPA2 is funded from InternetWide.org, which in turns receives donations
from various funding sources with an interest in a private and secure
Internet that gives users control over their online presence.  This particular
project is part of ARPA2 Mail & Reservoir, funded by SIDNfonds.
.SH "SEE ALSO"
.IR kip "(1), " kipctx_open "(3), " kipctx_close "(3),
.IR kipkey_frommap "(3),
.IR kipkey_fromkeytab "(3)."
.PP
Online resources may be found on the project home page,
.IR http://https://gitlab.com/arpa2/kip .
