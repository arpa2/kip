.TH KIPSUM_APPEND/FORK 3 "March 2019" "ARPA2.net" "Library Calls"
.SH NAME
kipsum_append, kipsum_fork \- KIP checksum changes
.SH SYNOPSIS
.B #include <stdbool.h>
.br
.B #include <stdint.h>
.sp
.B #include <arpa2/kip.h>
.sp
.B bool kipsum_append (kipt_ctx \fIctx\fB, kipt_sumid \fIsumid_or_keyid\fB,
.RS
.B uint8_t *\fIdata\fB, uint32_t \fIdatasz\fB);
.RE
.sp
.B bool kipsum_mark (kipt_ctx \fIctx\fB, kipt_sumid \fIsumid_or_keyid\fB,
.RS
.B char *\fIopt_typing\fB);
.RE
.sp
.B bool kipsum_fork (kipt_ctx \fIctx\fB, kipt_sumid \fIsumid_or_keyid\fB,
.RS
.B kipt_sumid *\fIout_kipsum\fB);
.RE
.br
.SH DESCRIPTION
.PP
These functions work on checksums while they
are in flight.
.BR kipsum_append (3)
concatenates data to the string that is collected
in the checksum,
.BR kipsum_mark (3)
inserts a cryptographically sensible marker that
helps to separate appended portions, and
.BR kipsum_fork (3)
makes a copy of the checksum with its
thus-far collected state from such prior operations.
Note that an implicit call to
.BR kipsum_append (3)
will be made for the plaintext data during
.BR kipsum_up "(3) and " kipsum_down "(3)"
operations, to update the checksum that is
implicitly created with every newly made
key.  Those checksums have the same
\fIkipt_sumid\fR value as the \fIkipt_keyid\fR,
which is why the above mentions several
parameters named \fIsumid_or_keyid\fR.
.PP
Of these operations,
.BR kipsum_update (3)
is the most common one.  It just appends
data as-is, but it comes with a risk: clients
may consider separately appended chunks of
data as distinguishable, but for the checksum
a sequence of calls forms one concatenation
of bytes and the distinction cannot be seen.
Appending "AB" followed by "BA" is the same
as appending "A" and then "BBA", for example.
This can be the source of security problems,
for which
.BR kipsum_mark (3)
offers a general solution, without asking
the client to implement cryptographic
measures on checksums themselves.
.PP
A call to
.BR kipsum_mark (3)
inserts a distinguishing marker between
preceding and following calls to
.BR kipsum_append (3)
and it chains them to ensure that there
can be no misinterpretation about them.
The resulting checksum output will be
different than without making the marks,
and this is not the best way to achieve
compatibility with a given protocol, but
it is suitable for exchanges made
between KIP clients.
.PP
The operation
.BR kipsum_fork (3)
clones the current state of a checksum,
including its collected appending calls
with any implicit ones, and including
any marks that may have been made.
After the call, the two checksums are
indistinguishable but for their
\fIkipt_sumid\fR, though that changes
as soon as marks or (implicit) appends
are made.  Forking itself does not change
the state of the checksum, it merely
duplicates the state.  It is often used
to allow "backtracking" to a previous state
from which to continue, or try again.
.PP
Checksums can be computed over various
data structures, due to forking or due
to syntactic or typing variations in the data stream.
To accommodate explicit distinctions between
such variations, the parameter
.I opt_typing
may be set to an application-fixed string
that describes the variation in the course
of a 
.BR kipsum_mark (3)
operation.  It is the
client's choice whether this applies to
preceding or following content, or
both.  To name one example, it could be used
to distinguish whether the byte sequence
0x41 0x42 0x42 0x41 represents the integer
value 1094861377 in palindrome-endian notation
or the UTF-8 name of a Swedish pop/rock band
from the ASCII era.
Take note that absense of
.I opt_typing
as indicated by a NULL value differs from
an empty string.
.PP
.SH RETURN VALUE
These functions return \fBtrue\fR on success, or
\fBfalse\fR with \fBerrno\fR set.  Note that the
functions use the \fIcom_err\fR system and define
their own \fBKIPERR_xxx\fR error codes.
.PP
.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl, for the ARPA2.net project.
.SH "REPORTING BUGS"
.PP
Please use our issue track for reporting bugs.  It can be
found online, at
.IR https://gitlab.com/arpa2/kip/issues .
.SH COPYRIGHT
.PP
Copyright \(co 2019 Rick van Rein, ARPA2.net.
.PP
ARPA2 is funded from InternetWide.org, which in turns receives donations
from various funding sources with an interest in a private and secure
Internet that gives users control over their online presence.  This particular
project is part of ARPA2 Mail & Reservoir, funded by SIDNfonds.
.SH "SEE ALSO"
.IR kip "(1), " kipsum_start "(3), "
.IR kipsum_restart "(3), " kipsum_sign "(3),"
.IR kipsum_verify "(3), "
.IR kipservice_sign "(3), " kipservice_verify "(3)."
.PP
Online resources may be found on the project home page,
.IR http://https://gitlab.com/arpa2/kip .
