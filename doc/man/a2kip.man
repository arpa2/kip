.TH A2RULE 8 "July 2021" "InternetWide.org" "System Management Commands"
.SH NAME
a2kip \- Manage KIP and HAAN services
.SH SYNOPSIS
.PP
.B a2kip
master create|destroy [service \fIservice\fR]
.RS
[vardir \fIvardir\fR | keytab \fIktname\fR] [enctype \fIenctype\fR]
.RE
.PP
.B a2kip
virtual add|del domain \fIdomain\fR
.RS
[service \fIservice\fR] [vardir \fIvardir\fR]
.br
[keytab \fIktname\fR] [enctype \fIenctype\fR]
.RE
.PP
.SE DESCRIPTION
The command
.BR a2kip
is used to make changes to underlying files for hosting of the
ARPA2 Kip daemon, or the HAAN authentication service.  These
two have completely different functions, but they hinge on
the same general structures, namely those of KIP.
.PP
The purpose of KIP is to provide encryption and signatures,
where the ability to authenticate is used as proof of identity
and translated to cryptographic abilities.  Instead of relying on
a trust network to bind identity to a public key, KIP ties the
cryptographic capabilies directly to identity through an online
service.  This can be beneficial for communication with people
who do not publish a public key.
.PP
The purpose of HAAN is to provide a fallback authentication
option that does not rely on unencrypted communication such as
an email.  HAAN hands out random accounts with derived passwords,
and can derive the password again when presented with the same
account.  Based on this, it supports authentication.  This is
especially powerful when combined with Realm Crossover, as it
allows (multiple) external parties to serve as backup access
mechanisms.
.PP
Both KIP and HAAN build on a key derivation scheme that starts
from a service-specific master key, which is used to encrypt
a key specific to a service/domain combination.  The purpose
of
.B a2kip
is to manage this key derivation scheme, by creating and
destroying master keys for services with the subcommand
.B a2kip master
and, given these master keys for services, to use the subcommand
.B a2kip virtual
to add and delete virtual domains under these services.
.SH PARAMETERS
.TP
.BI "service " service
specifies the service to which changes are being made.  The
default service is
.B kip
and the other service discussed here is
.BR haan ,
but both are handled like strings, and made part of a
file name.  There is no restriction to using
.B a2kip
for other service names.
.TP
.BI "vardir " vardir
is the variable storage directory for services.  Normally, no
data is stored by either KIP or HAAN, but merely generated or
derived and forgotten once a session closes.  The only thing
that is stored is the key hierarchy.  These go under the
\fIvardir\fR which may also be set in environment variable
.BR KIP_VARDIR ,
or otherwise its default value
.B /var/lib/arpa2/kip
is used.
.TP
.BI "keytab " ktname
is the name of a keytab that stores the master keys, in a
form understandable to the underlying Kerberos library.
Files with constrained access rights may be used, but other
key storage forms are possible.
.PP
.RS
When not specified, the environment variable
.B KIP_KEYTAB
is consulted.  The fallbacks of the generic Kerberos library
are not tried.  Instead, the fallback option is the \fIvardir\fR,
regardless of its source, followed by the
.B master.keytab
filename.
.RE
.TP
.BI "enctype " enctype
is the Kerberos Encryption Type in a string form, used as the
encryption key.  Since the logic of KIP and HAAN hinges on the
longevity of their keys, it is strongly advised to choose a
key that is future-proof.  The current default is
.BR aes256-cts-hmac-sha384-192 ,
which is thought to be strong enough to withstand
Quantum Computer attacks.  This parameter is only used when
constructing new keys.
.TP
.BI "domain " domain
is the virtual domain for which a service is setup or torn down.
This should be supplied as a fully qualified domain name, and
must not end in a dot.
.SH KEY FILES
The file structure under the KIP variable directory is as
follows:
.TP
.B master.keytab
is the filename holding the master keys.  It is a keytab in
the true Kerberos sense, and could be read over the KIP library
API by anyone with sufficient permissions.  Keytab utilities
may be used to inspect or edit the contents of this file.
.TP
.BR kip-vhost ", " haan-vhost
are names of directories for the virtual hosts of services,
in this case for \fBkip\fR and \fBhaan\fR.  These sit next
to the \fBmaster.keytab\fR file.
.PP
.RS
Underneath vhost directories are files named like virtual hosts
and containing keymaps as made by KIP.  These keymaps are
encrypted with the master key for a service, that decrypt
to the service key for a virtual host whose name is used
as the filename.
.RE
.SH ENVIRONMENT VARIABLES
A few environment variables impact the behaviour of not just
.BR a2kip ,
but also the daemon
.B kipd
and applications that load the KIP libraries.
.TP
.B KIP_VARDIR
is the starting point for variable storage, as described
in the KEY FILES section.  The contents of this directory
may be managed with
.BR a2kip .
.TP
.B KIP_KEYTAB
is the name of the keytab holding the master keys.  Its
default is a file named
.B master.keytab
residing under the
.B KIP_VARIDR
directory.
.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl, for the InternetWide project.
.SH "REPORTING BUGS"
For any discussion, including about bugs, please use the
.IR https://gitlab.com/arpa2/kip/
project page for KIP.  We use its issue tracker
to track bugs and feature requests.
Documentation for the KIP library API can be found on
.IR http://kip.arpa2.net/ .
.SH COPYRIGHT
.PP
Copyright \(co 2021 Rick van Rein, ARPA2.net.
.PP
ARPA2 is funded from InternetWide.org, which in turn receives
funding from various sources.  Though not directly sponsored,
this work has been inspired by the problems of secure exchange
in the SURF FileSender application and ARPA2 Reservoir.
.SH "SEE ALSO"
The KIP library API is documented on
.IR http://kip.arpa2.net/
and its code is on
.IR https://gitlab.com/arpa2/kip/ .
