.TH "KIPKEY_TO/FROMMAP" 3 "March 2019" "ARPA2.net" "Library Calls"
.SH NAME
kipkey_tomap, kipkey_frommap \- KIP keys mapping through other keys.
.SH SYNOPSIS
.B #include <stdbool.h>
.br
.B #include <stdint.h>
.sp
.B #include <arpa2/kip.h>
.sp
.B bool kipkey_tomap (kipt_ctx \fIctx\fB, kipt_keyid \fIkeyid\fB,
.RS
.B uint16_t    \fImappingdepth\fB,
.B kipt_keyid *\fImappingkeyids\fB,
.br
.B uint8_t *\fIout_keymud, uint32_t \fImax_keymudsz\fB,
.br
.B uint32_t *\fIout_keymudsz\fB);
.RE
.sp
.B bool kipkey_frommap (kipt_ctx \fIctx\fB,
.RS
.B uint8_t *\fIkeymud\fB, uint32_t \fIkeymudsz\fB,
.B kipt_keyid *\fIout_mappedkey\fB);
.RE
.br
.SH DESCRIPTION
.PP
These functions allow translation from one key
into another.  Even when a kipped-up portion of
a document is encrypted to a single key, it is
possible to construct multiple key maps that lead
to that key.  This enables a logic "OR" function
in terms of keys that may read that portion of
the document.
.PP
For perfect balance, a logic "AND" function can also be
constructed, by mapping from multiple keys, which
are applied in sequence and which must therefore all
be available to permit access to the key map.
.PP
The function
.BR kipkey_tomap (3)
therefore requires not just a \fIkeyid\fR to map,
but also one or more \fImappingkeyids\fR, whose
number is provided as \fImappingdepth\fR.  (In the
simple case where the depth is one, the sequence
of key identities can be a mere pointer to the one
key identity, so no arrays needs to be constructed.)
.PP
The \fImud\fR parameters follow the usual protocol,
including the error code \fBKIPERR_OUTPUT_SIZE\fR
with the output length stored, as always, in the
\fIout_keymudsz\fR to aid in allocation.
.PP
The \fIkeymud\fR should be considered as secure as
the mapping key(s), and can normally be sent over
unprotected wires as far as encryption goes; there
is an issue of authenticity that may be remedied
with other tools.
.PP
If the
.BR kipkey_tomap (3)
exists to "kip up" a key, the mirror function of
"kipping down" this \fIkeymud\fR is the
.BR kipkey_frommap (3)
function.
Take note that key mapping is entirely a local
operation; keys used for mapping must already
be available in the KIP context \fIctx\fR if it
is to work.
.PP
Keys are not destroyed until the entire KIP context
is destroyed in a call to
.BR kipctx_close (3).
.SH RETURN VALUE
.PP
These functions return \fBtrue\fR on success, or
\fBfalse\fR with \fBerrno\fR set.  Note that the
functions use the \fIcom_err\fR system and define
their own \fBKIPERR_xxx\fR error codes.
.PP
.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl, for the ARPA2.net project.
.SH "REPORTING BUGS"
.PP
Please use our issue track for reporting bugs.  It can be
found online, at
.IR https://gitlab.com/arpa2/kip/issues .
.SH COPYRIGHT
.PP
Copyright \(co 2019 Rick van Rein, ARPA2.net.
.PP
ARPA2 is funded from InternetWide.org, which in turns receives donations
from various funding sources with an interest in a private and secure
Internet that gives users control over their online presence.  This particular
project is part of ARPA2 Mail & Reservoir, funded by SIDNfonds.
.SH "SEE ALSO"
.IR kip "(1), " kipctx_open "(3), " kipctx_close "(3),
.IR kipkey_generate "(3),
.IR kipservice_tomap "(3), " kipservice_frommap "(3), "
.IR kipkey_fromkeytab "(3)."
.PP
Online resources may be found on the project home page,
.IR http://https://gitlab.com/arpa2/kip .
