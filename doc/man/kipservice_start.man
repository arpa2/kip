.TH KIPSERVICE_START/STOP/RESTART 3 "May 2019" "ARPA2.net" "Library Calls"
.SH NAME
kipservice_start, kipservice_stop, kipservice_restart \- Start and stop using KIP Service
.SH SYNOPSIS
.B #include <stdbool.h>
.sp
.B #include <arpa2/kip.h>
.sp
.B bool kipservice_start (kipt_ctx \fIctx\fB);
.sp
.B void kipservice_stop (kipt_ctx \fIctx\fB);
.sp
.B void kipservice_restart (kipt_ctx \fIctx\fB);
.br
.SH DESCRIPTION
.PP
Given an existing KIP context, these functions start
and stop its use for KIP Service.  Calls to the
other KIP Service functions depend on these.
.PP
After
.BR kipservice_start (3),
connection state piles up, holding all the data output
by other KIP Service calls, inasfar as it requires dynamic
allocation.  This is freed again with
.BR kipservice_stop (3)
but an intermediate call to
.BR kipservice_restart (3)
has the same effect of recycling memory, albeit more
efficiciently.
.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl, for the ARPA2.net project.
.SH "REPORTING BUGS"
.PP
Please use our issue track for reporting bugs.  It can be
found online, at
.IR https://gitlab.com/arpa2/kip/issues .
.SH COPYRIGHT
.PP
Copyright \(co 2019 Rick van Rein, ARPA2.net.
.PP
ARPA2 is funded from InternetWide.org, which in turns receives donations
from various funding sources with an interest in a private and secure
Internet that gives users control over their online presence.  This particular
project is part of ARPA2 Mail & Reservoir, funded by SIDNfonds.
.SH "SEE ALSO"
.IR kip "(1), " kipctx_open "(3), " kipctx_close "(3)."
.PP
Online resources may be found on the project home page,
.IR http://https://gitlab.com/arpa2/kip .
