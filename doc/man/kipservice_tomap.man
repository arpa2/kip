.TH "KIPSERVICE_TO/FROMMAP" 3 "April 2019" "ARPA2.net" "Library Calls"
.SH NAME
kipservice_tomap, kipservice_frommap \- Map keys through KIP Service.
.SH SYNOPSIS
.B #include <stdbool.h>
.br
.B #include <stdint.h>
.sp
.B #include <arpa2/kip.h>
.sp
.B bool kipservice_tomap (kipt_ctx \fIctx\fB, char *\fIdomain\fB,
.RS
.B kipt_keyid \fIkeytohide\fB,
.br
.B char **\fIblacklist_users\fB,
.br
.B char **\fIwhitelist_users\fB,
.br
.B time_t \fIfrom_or_0\fB,
.br
.B time_t \fItill_or_0\fB,
.br
.B uint8_t **\fIout_svckeymud\fB,
.br
.B uint32_t *\fIout_svckeymudlen\fB);
.RE
.sp
.B bool kipservice_frommap (kipt_ctx \fIctx\fB,
.RS
.B uint8_t *\fIsvckeymud\fB, uint32_t \fIsvckeymudlen\fB,
.br
.B kipt_keyid **\fIout_mappedkeys\fB,
.br
.B uint32_t *\fIout_mappedkeyslen\fB);
.RE
.RE
.br
.SH DESCRIPTION
.PP
These functions allow the protection of keys
via a KIP Service.  The idea is that keys are
only available to those that are approved
under an acces control list, which takes the
form of a black list and white list combination.
.PP
By using an online service, we can avoid the
need to store keys on client machines, which
is increasingly attractive as these machines
get more and more mobile (and easy to grab).
One might call the KIP Service a part of the
cloud, if so desired.  Its specific service
is to do things as a service that cannot be
done reliably by a mere data structure that
is processed locally.
.PP
With
.BR kipservice_tomap "(),"
a client can upload a key and access control
lists to a given domain.  The output will
be what we call "service key mud", and it is
a form of opaque data that must be sent to
the same KIP Service, whose domain name can
be retrieved from the key mud with
.BR kipservice_realm "()"
at any time.
The function
.BR kipservice_frommap "()"
then proceeds to uncover the keys that were
uploaded, inasfar as they are visible.
.PP
The call to
.BR kipservice_tomap "()"
is generally considered a public service, and
so it requires only SASL ANONYMOUS from the
client.  This makes it easy for senders to
contact a KIP Service in their recipient's
domain and send the call there.
The call to
.BR kipservice_frommap "()"
then does require SASL authentication, which
explains why it is most logically done under
the recipient's domain.  Clients are advised
to frown upon foreign realms if they run a
KIP Service under their own domain and the
.BR kipservice_realm "()"
function exists to make them preview any
such attempts before they are undertaken.
It is not harmful by itself if other realms
are mentioned; they are however most likely
added to service other recipients.  The
general advise for recipients is to use
only their own KIP Service or, lacking
that, fallback to the sender's.  The sender
is advised to act accordingly, to improve
the chances of content actually getting
through.
.PP
The
.IR blacklist " and " whitelist
parameters define a NULL-terminated array
of entries formatted as
.B user@domain.name
with lowercase domain names and user names under
the domain's local conventions, as with email.
Entries on the blacklist trump entries on the
whitelist when they are more concrete in terms of
.BR a2id_generalize "(3)."
Entries on both lists with the same level of
concreteness have an undefined behaviour.
A shorthand notation of NULL is permitted
for an empty list, which is a
list with only a NULL entry.
It is the responsibility of the KIP Service to
enforce access control, which is why it should
normally be the KIP Service assigned by a
recipient's domain, even if that happens to be
a virtually hosted service: it is the domain's
prerogative to recognise user names under the
realm, and this form of delegation is the
proper way of doing realm crossover, where the
domain can be validated but its user identities
are delegated to services validated to function
on behalf of the domain.
.PP
The paramaters
.IR from_or_0 " and " till_or_0
allow restrictions to the time of release of
key information.  The former enforces an
earliest time if it is not 0, the latter
enforces a last time if it is not 0.  When
set to 0 however, there are no constraints on
the respective side of the time interval.
This is another factor that can influence the
number of keys released.
.PP
As a general mechanism, a list of keys may be
made available through this mechanism, and so
a list of keys may come out.  It is not an error
if such a list is empty.  Be sure to inspect the
\fIout_mappedkeyslen\fR to learn about the size
of the produced \fIout_mappedkeys\fR array.
We may need a slightly different API to actually
make the most proper use of the array concept,
so please allow for changes to the signature
of these functions in future versions.
.PP
These calls can only be made between calls to
.BR kipservice_start "(3) and " kipservice_stop (3).
.PP
TODO: dermem uses allocation in a memory pool of the service.  To be built and documented.
.PP
.SH RETURN VALUE
.PP
These functions return \fBtrue\fR on success, or
\fBfalse\fR with \fBerrno\fR set.  Note that the
functions use the \fIcom_err\fR system and define
their own \fBKIPERR_xxx\fR error codes.
.PP
.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl, for the ARPA2.net project.
.SH "REPORTING BUGS"
.PP
Please use our issue track for reporting bugs.  It can be
found online, at
.IR https://gitlab.com/arpa2/kip/issues .
.SH COPYRIGHT
.PP
Copyright \(co 2019 Rick van Rein, ARPA2.net.
.PP
ARPA2 is funded from InternetWide.org, which in turns receives donations
from various funding sources with an interest in a private and secure
Internet that gives users control over their online presence.  This particular
project is part of ARPA2 Mail & Reservoir, funded by SIDNfonds.
.SH "SEE ALSO"
.IR kip "(1), " kipctx_open "(3), " kipctx_close "(3), "
.IR kipservice_start "(3), " kipservice_stop "(3), "
.IR kipkey_tomap "(3), " kipkey_frommap "(3), "
.IR kipservice_realm "(3), "
.IR kipservice_sign "(3), " kipservice_verify "(3), "
.IR a2id "(3), " a2id_generalize "(3)."
.PP
Online resources may be found on the project home page,
.IR http://https://gitlab.com/arpa2/kip .
