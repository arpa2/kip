.TH KIP 1 "March 2019" "ARPA2.net" "System Management Commands"
.SH NAME
kip \- Encryption through the Keyful Identity Protocol
.SH SYNOPSIS
.B kip
.BR up | down
.I infile outfile
.SH DESCRIPTION
.PP
To protect data, they must be encrypted to a key.
Recipients then recover the key and, using that,
recover the protected content.  With public-key
systems such as
.BR gpg (1)
there is a need to know the recipients' keys before
data can be protected.  With
.BR kip "(1)"
however, a fresh key can be generated and made available
to the intended recipients when they ask for it.  This
simplifies the work involved in encryption, at the expense
of a little extra work during decryption.
.PP
The method used is founded on authentication.  The
fresh key is packaged together with an access control list
(ACL), and encrypted using a KIP Service.  Only this KIP Service
is then able to recover the key, which it will only reveal
the key to authenticated identities on the ACL.
.PP
This is a trade-off.  One needs to rely on the KIP Service
to be online, and when the key is discarded it is needed
again during later decryption attempts.  On the other hand,
no more credentials are needed than authentication credentials.
Particularly pleasant to use is a single-signon system such
as Kerberos.
.PP
The command
.B kip up
is used to "kip up" the content to a level fit for distribution,
and
.B kip down
is used to reverse the process after retrieval.
In both cases, the
.I infile
is read and the
.I outfile
is written.  We generally use the
.B .kip
extension on documents that have been "kipped up".
Such a document is between 2% and 10% larger
than the original, because no compression is performed.  The
decrypted content of the KIP document is integrity protected
with a secure hash signed under the encryption key.  This means
that only parties holding this key are able to alter the
document without distorting the integrity check.
.SH KIP SERVICE
The KIP Service runs as a stateless protocol, except for a
few aspects:
.IP *
The KIP Service has access to a key table, holding its
master key.  A separate master key is usually created for
every domain that is supported.
.IP *
The KIP Service performs authentication through SASL, and
to do that it needs access to a SASL backend.
.PP
There is no technical need for a KIP Service to ever store the
keys that pass through it.  The KIP protocol will never pass
the encrypted content through the KIP Service, only the keys
are handled by it.  These two aspects make the KIP Service a
very light-weight service.
.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl, for the ARPA2.net project.
.SH "REPORTING BUGS"
.PP
Please use our issue track for reporting bugs.  It can be
found online, at
.IR https://gitlab.com/arpa2/kip/issues .
.SH COPYRIGHT
.PP
Copyright \(co 2019 Rick van Rein, ARPA2.net.
.PP
ARPA2 is funded from InternetWide.org, which in turns receives donations
from various funding sources with an interest in a private and secure
Internet that gives users control over their online presence.  This particular
project is part of ARPA2 Mail & Reservoir, funded by SIDNfonds.
.SH "SEE ALSO"
.IR gpg "(1), " smime "(1ssl), " openssl "(1ssl), " ktutil "(1)."
.PP
Online resources may be found on the project home page,
.IR http://https://gitlab.com/arpa2/kip .
