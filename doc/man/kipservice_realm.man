.TH "KIPSERVICE_REALM" 3 "April 2019" "ARPA2.net" "Library Calls"
.SH NAME
kipservice_realm \- The realm that handles KIP Service mud.
.SH SYNOPSIS
.B #include <stdbool.h>
.br
.B #include <stdint.h>
.sp
.B #include <arpa2/kip.h>
.sp
.B bool kipservice_realm (dercursor \fIsvcmud\fB, dercursor *\fIrealm\fB);
.RE
.br
.SH DESCRIPTION
.PP
This simple function retrieves the
.I realm
mentioned in the "service mud" obtained from
either of the functions
.BR kipservice_sign "(3) and " kipservice_tomap "(3)."
The output from these functions must be fed into
.BR kipservice_verify "(3) and " kipservice_frommap "(3),"
respectively which would address the same KIP Service.
Since the paired functions may be run on different
systems and at different times, the information of
which realm to contact may have been lost, or it
may not be authoritative.  This function returns
the \fIrealm\fR that is used by the routines.
.PP
The reason why this is useful is to prepare the
user for contacting an external KIP Service.
It is reasonable to expect the KIP Service for
.BR kipservice_frommap "(3)"
to be the local KIP Service if one is setup for
the recipient's domain, because authentication
will be needed.  This can be verified ahead of
time, and domains at which one would prefer not
to authenticate, or just use certain SASL mechanisms
that are not safe from middle men might be used.
As for
.BR kipservice_verify "(3),"
this is usually run against the domain of the
sender of a document, but using SASL ANONYMOUS
authentication.  Even though this does not leak
any identity information there is still a notion
of reading a document that travels back to a
sender, which the user may want to filter on for
reasons of privacy.  Tools are therefore likely
to maintain lists of trusted realms, both as a
safe contact basis and as a place from which the
signatures can actually be trusted.
.PP
As this is a mere data retrieval function, there
is no need to have an active context for use of
the KIP library.
.SH RETURN VALUE
.PP
This function returns \fBtrue\fR on success, or
\fBfalse\fR with \fBerrno\fR set.  Note that the
functions use the \fIcom_err\fR system and define
their own \fBKIPERR_xxx\fR error codes.
.PP
.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl, for the ARPA2.net project.
.SH "REPORTING BUGS"
.PP
Please use our issue track for reporting bugs.  It can be
found online, at
.IR https://gitlab.com/arpa2/kip/issues .
.SH COPYRIGHT
.PP
Copyright \(co 2019 Rick van Rein, ARPA2.net.
.PP
ARPA2 is funded from InternetWide.org, which in turns receives donations
from various funding sources with an interest in a private and secure
Internet that gives users control over their online presence.  This particular
project is part of ARPA2 Mail & Reservoir, funded by SIDNfonds.
.SH "SEE ALSO"
.IR kip "(1), "
.IR kipservice_tomap "(3), " kipservice_frommap "(3),
.IR kipservice_sign "(3), " kipservice_verify "(3)."
.PP
Online resources may be found on the project home page,
.IR http://https://gitlab.com/arpa2/kip .
