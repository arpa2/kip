.TH KIPSUM_START/RESTART 3 "March 2019" "ARPA2.net" "Library Calls"
.SH NAME
kipsum_start, kipsum_restart \- KIP checksum (re)start
.SH SYNOPSIS
.B #include <stdbool.h>
.br
.B #include <stdint.h>
.sp
.B #include <arpa2/kip.h>
.sp
.B bool kipsum_start (kipt_ctx \fIctx\fB, kipt_keyid \fIsupporting_key\fB,
.RS
.B kipt_sumid *\fIout_kipsum\fB);
.RE
.sp
.B bool kipsum_restart (kipt_ctx \fIctx\fB, kipt_sumid \fIsumid_or_keyid\fB);
.br
.SH DESCRIPTION
.PP
These functions start a new checksum computation
in a number of ways.  In both cases, the result
is a checksum that has not processed any
.BR kipsum_append (3)
operations yet.
Note that every new \fIkipt_keyid\fR implicitly
doubles as a \fIkipt_sumid\fR that automatically
appends any plaintext data passing through
.BR kipdata_up "(3) and " kipdata_down "(3)"
operations based on the identified key.
.PP
With
.BR kipsum_start "(3),"
a new checksum is started in connection to
a key, but without any automatic updates
during
.BR kipdata_up "(3) and " kipdata_down "(3)"
operations.
A call to
.BR kipsum_restart "(3)"
drops any data supplied to an existing
checksum through calls to
.BR kipsum_update "(3) and " kipsum_mark "(3),"
including those calls made automically on
the checksum that is implicit for each key.
.PP
.SH RETURN VALUE
These functions return \fBtrue\fR on success, or
\fBfalse\fR with \fBerrno\fR set.  Note that the
functions use the \fIcom_err\fR system and define
their own \fBKIPERR_xxx\fR error codes.
.PP
.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl, for the ARPA2.net project.
.SH "REPORTING BUGS"
.PP
Please use our issue track for reporting bugs.  It can be
found online, at
.IR https://gitlab.com/arpa2/kip/issues .
.SH COPYRIGHT
.PP
Copyright \(co 2019 Rick van Rein, ARPA2.net.
.PP
ARPA2 is funded from InternetWide.org, which in turns receives donations
from various funding sources with an interest in a private and secure
Internet that gives users control over their online presence.  This particular
project is part of ARPA2 Mail & Reservoir, funded by SIDNfonds.
.SH "SEE ALSO"
.IR kip "(1), " kipkey_generate "(3), "
.IR kipsum_append "(3), " kipsum_fork "(3), "
.IR kipdata_up "(3), " kipdata_down "(3)."
.PP
Online resources may be found on the project home page,
.IR http://https://gitlab.com/arpa2/kip .
