.TH KIPKEY_GENERATE/_ALGORITHM/KIPDATA_RANDOM 3 "March 2019" "ARPA2.net" "Library Calls"
.SH NAME
kipkey_generate, kipkey_algorithm \- KIP key management.
.SH SYNOPSIS
.B #include <stdbool.h>
.br
.B #include <stdint.h>
.sp
.B #include <arpa2/kip.h>
.sp
.B bool kipkey_generate (kipt_ctx \fIctx\fB, kipt_alg \fIalg\fB,
.RS
.B kipt_keynr   \fIin_keynr\fB,
.B kipt_keyid *\fIout_keyid\fB);
.RE
.sp
.B bool kipkey_algorithm (kipt_ctx \fIctx\fB, kipt_keyid \fIkeyid\fB,
.RS
.B kipt_alg *\fIout_alg\fB);
.RE
.sp
.SH DESCRIPTION
.PP
These functions can be used to generate key material
and data within a KIP context.  The function
.BR kipkey_generate (3)
creates a fresh KIP key and stores it under the
context.  The algorithm is selected with the
\fIalg\fR argument, which is a code of a Kerberos5
encryption type as registered by IANA.  The function
.B kipkey_algorithm (3)
retrieves this algorithm for a given \fIkeyid\fR.
.PP
Keys are identified on the wire a unsigned 32-bit
integer values.  These are mostly unique, but KIP does
not enforce this property.  Thanks to this leniency,
it is relatively straightforward to merge data sources,
even when keys have the same 32-bit number.  KIP uses
the integrity check contained in kipped-up data to
select a key if multiple options exist.  The context
however, holds a unique handle for a key in the form
of a \fIkeyid\fR.  Key identities are like upgraded
forms of a key number, with a unique value added.
This unique portion is removed when flattening a
key description for use in the wire format.
.PP
Keys are not destroyed until the entire KIP context
is destroyed in a call to
.BR kipctx_close (3).
.SH RETURN VALUE
.PP
These functions return \fBtrue\fR on success, or
\fBfalse\fR with \fBerrno\fR set.  Note that the
functions use the \fIcom_err\fR system and define
their own \fBKIPERR_xxx\fR error codes.
.PP
.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl, for the ARPA2.net project.
.SH "REPORTING BUGS"
.PP
Please use our issue track for reporting bugs.  It can be
found online, at
.IR https://gitlab.com/arpa2/kip/issues .
.SH COPYRIGHT
.PP
Copyright \(co 2019 Rick van Rein, ARPA2.net.
.PP
ARPA2 is funded from InternetWide.org, which in turns receives donations
from various funding sources with an interest in a private and secure
Internet that gives users control over their online presence.  This particular
project is part of ARPA2 Mail & Reservoir, funded by SIDNfonds.
.SH "SEE ALSO"
.IR kip "(1), " kipctx_open "(3), " kipctx_close "(3),
.IR kipkey_tomap "(3), " kipkey_frommap "(3), "
.IR kipservice_tomap "(3), " kipservice_frommap "(3), "
.IR kipkey_fromkeytab "(3)."
.PP
Online resources may be found on the project home page,
.IR http://https://gitlab.com/arpa2/kip .
