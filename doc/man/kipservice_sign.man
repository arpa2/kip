.TH "KIPSERVICE_SIGN/VERIFY" 3 "April 2019" "ARPA2.net" "Library Calls"
.SH NAME
kipservice_sign, kipservice_verify \- Signing data via KIP Service.
.SH SYNOPSIS
.B #include <stdbool.h>
.br
.B #include <stdint.h>
.sp
.B #include <arpa2/kip.h>
.sp
.B bool kipservice_sign (kipt_ctx \fIctx\fB,
.RS
.B kipt_keyid \fIkeyid\fB,
.br
.B uint16_t \fIsumslen\fB, kipt_sumid *\fIsums\fB,
.br
.B uint32_t \fImetalen\fB, uint8_t *\fIopt_meta\fB,
.br
.B uint8_t **\fIout_mud\fB, uint32_t *\fIout_mudlen\fB);
.RE
.sp
.B bool kipservice_verify (kipt_ctx \fIctx\fB,
.RS
.B kipt_keyid \fIkeyid\fB,
.br
.B uint16_t \fIsumslen\fB, kipt_sumid *\fIsums\fB,
.br
.B uint8_t *\fImud\fB, uint32_t \fImudlen\fB,
.br
.B uint32_t *\fIout_metalen\fB, uint8_t **\fIout_opt_meta\fB);
.RE
.br
.SH DESCRIPTION
.PP
These functions are used to sign data with confirmation
by a KIP Service.  Although symmetric keys can be used
to sign data too, they cannot reveal a verified identity
for a signer.  Signatures passed through the KIP Service
do attach the signer's identity to a document, rather
than merely saying "only those with access to the given
key could have altered this data".  This distinction is
the reason to use the serviced couple
.BR kipservice_sign "(3) and " kipservice_verify "(3)"
instead of the local couple
.BR kipsum_sign "(3) and " kipsum_verify "(3)."
.PP
Both signature mechanisms protect the signature by
encrypting it with a symmetric key; the local couple
uses a key to which you must have access, while the
serviced couple is available to anyone.  This gives
another use case for signing through the KIP Service,
namely for publicly verifiable signatures on content
that may not even be encrypted.  In other use cases
it would be considered an advantage that the
signature has limited views.  The two can be combined
by first passing a checksum through the KIP Service
for identification, followed by local encryption
to protect the identity held inside it.  To uncover
this, a client must first be able to decrypt locally
and then the KIP Service is willing to reveal the
identity of the signer.  This use case could shield
the privacy of the signer to a group with access
to a key.
.PP
The validation of the signer's identity by the
KIP Service allows it to bind information that it
knows was sent by the signer.  This can be used
to incorporate any kind of metadata, if so desired.
This information will not be inspected by the
KIP Service, but it will be reproduced from the
.BR kipservice_sign "(3) operation to the" kipservice_verify "(3)"
operation.  One use that an application may make
of this field is for a signature time stamp, but
in general it is open to the application.
.PP
The KIP Service can be applied to a sequence of
checksums at once.  This is not just more efficient,
but it also helps to protect multiple document
aspects at once, and it constructs a kind of
validated Merkle tree structure.  Merkle trees
are trees of hashes, where each node hashes the
concetenation of hashes of subtrees; the slight
change of flavour with KIP would be that not a
hash but a signature could be computed over the
concetenated hashes of subtrees.  A signature
is essentially a hash bound to an identity.
.PP
All checksums used should follow the keyid
and algorithm of a given \fIkeyid\fR.
The \fIsumslen\fR indicates how many checksums
are to be concetanated, and the \fIsums\fR
provides the array of checksum identifiers,
representing checksum objects in the current
KIP context \fIctx\fR.  These checksum objects
are used both during signing and validation.
The signing function produces what we call
"service signature mud" from the checksums,
and the validation function compares the
"service signature mud" against the checksums.
.PP
KIP shields document data by never sending
it to a KIP Service.  However, when sending
document hashes instead, a magnet URI lookup
might still reveal the document.  For this
reason, an extra salt is produced during
.BR kipservice_sign (3)
and transported along the
data as part of the 
.IR svcmud .
While finishing the
checksums, this data is incorporated into the
hash to drop any possible link to the original
data.  The salt is designed to be
cryptographically secure, by having the length
of the checksum outcome, and having binary
random content so maximum entropy is available.
.PP
The metadata is not protected by salt or encryption
like is the case with the document data hashes.
.PP
These calls can only be made between calls to
.BR kipservice_start "(3) and " kipservice_stop (3).
.PP
.SH RETURN VALUE
.PP
These functions return \fBtrue\fR on success, or
\fBfalse\fR with \fBerrno\fR set.  Note that the
functions use the \fIcom_err\fR system and define
their own \fBKIPERR_xxx\fR error codes.
.PP
.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl, for the ARPA2.net project.
.SH "REPORTING BUGS"
.PP
Please use our issue track for reporting bugs.  It can be
found online, at
.IR https://gitlab.com/arpa2/kip/issues .
.SH COPYRIGHT
.PP
Copyright \(co 2019 Rick van Rein, ARPA2.net.
.PP
ARPA2 is funded from InternetWide.org, which in turns receives donations
from various funding sources with an interest in a private and secure
Internet that gives users control over their online presence.  This particular
project is part of ARPA2 Mail & Reservoir, funded by SIDNfonds.
.SH "SEE ALSO"
.IR kip "(1), " kipctx_open "(3), " kipctx_close "(3), "
.IR kipservice_start "(3), " kipservice_stop "(3), "
.IR kipsum_sign "(3), " kipsum_verify "(3), "
.IR kipservice_realm "(3), "
.IR kipservice_tomap "(3), " kipservice_frommap "(3)."
.PP
Online resources may be found on the project home page,
.IR http://https://gitlab.com/arpa2/kip .
