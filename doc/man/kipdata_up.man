.TH KIPDATA_UP/DOWN/RANDOM 3 "March 2019" "ARPA2.net" "Library Calls"
.SH NAME
kipdata_up, kipdata_down, kipdata_random \- KIP data security.
.SH SYNOPSIS
.B #include <stdbool.h>
.br
.B #include <stdint.h>
.sp
.B #include <arpa2/kip.h>
.sp
.B bool kipdata_up (kipt_ctx ctx, kipt_keyid key,
.RS
.B uint8_t *\fIclear\fB,   uint32_t \fIclearsz\fB,
.B uint8_t *\fIout_mud\fB, uint32_t \fImax_mudsz\fB,
.B uint32_t *\fIout_mudsz\fB);
.RE
.sp
.B bool kipdata_down (kipt_ctx ctx, kipt_keyid key,
.RS
.B uint8_t *\fImud\fB,       uint32_t \fImudsz\fB,
.B uint8_t *\fIout_clear\fB, uint32_t \fImax_clearsz\fB,
.B uint32_t *\fIout_clearsz\fB);
.RE
.sp
.B bool kipdata_random (kipt_ctx \fIctx\fB,
.RS
.B uint32_t \fIlen_target\fB, uint8_t *\fIout_target\fB);
.RE
.br
.SH DESCRIPTION
.PP
The function "kip up" and "kip down" a block
of data.  Kipping up comes down to encryption
with a checksum, kipping down comes down to
decryption and an integrity check.
.PP
The \fIclear\fR arguments indicate the plaintext
information, the \fImud\fR arguments represent
the unreadable binary data.  The latter is safe
to transport, or at least as safe as the protection
of the \fIkey\fR that was used.
.PP
The \fIkey\fR is obtained from other function calls,
and generally several build up in a KIP context.
.PP
If an output buffer must be allocated but no size
is available, it is possible to invoke these
functions with the \fImax_xxxsz\fR set to zero.
The functions then return an error code
\fBKIPERR_OUTPUT_SIZE\fR and store the required
size in the \fIout_xxxsz\fR, which is also used
on successful return.
.PP
The function
.B kipdata_random (3)
may be useful around the same time, to generate a continuous
sequence of random bytes with a pseudo-random generator.
.SH RETURN VALUE
These functions return \fBtrue\fR on success, or
\fBfalse\fR with \fBerrno\fR set.  Note that the
functions use the \fIcom_err\fR system and define
their own \fBKIPERR_xxx\fR error codes.
.PP
.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl, for the ARPA2.net project.
.SH "REPORTING BUGS"
.PP
Please use our issue track for reporting bugs.  It can be
found online, at
.IR https://gitlab.com/arpa2/kip/issues .
.SH COPYRIGHT
.PP
Copyright \(co 2019 Rick van Rein, ARPA2.net.
.PP
ARPA2 is funded from InternetWide.org, which in turns receives donations
from various funding sources with an interest in a private and secure
Internet that gives users control over their online presence.  This particular
project is part of ARPA2 Mail & Reservoir, funded by SIDNfonds.
.SH "SEE ALSO"
.IR kip "(1), " kipctx_open "(3), " kipctx_close "(3),
.IR " kipkey_generate "(3), " kipdata_random "(3)."
.PP
Online resources may be found on the project home page,
.IR http://https://gitlab.com/arpa2/kip .
