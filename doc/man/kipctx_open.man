.TH KIPCTX_OPEN/CLOSE 3 "March 2019" "ARPA2.net" "Library Calls"
.SH NAME
kipctx_open, kipctx_close \- Contexts for the Keyful Identity Protocol
.SH SYNOPSIS
.B #include <stdbool.h>
.sp
.B #include <arpa2/kip.h>
.sp
.B bool kipctx_open (kipt_ctx *\fIout_ctx\fB);
.sp
.B void kipctx_close (kipt_ctx \fIctx\fB);
.br
.SH DESCRIPTION
.PP
These functions open and close a context handle for
working with KIP.  The context handle is needed for
all other KIP functions, and is used as a storage
structure.  All collected storage is cleaned up
when the context is closed.
.PP
Do not use a KIP context simultaneously in multiple
threads.  They are designed to be owned by one thread
at a time.  Functions exist to move keying material
between contexts in binary form.
.SH RETURN VALUE
The open function returns \fBtrue\fR on success, or
\fBfalse\fR with \fBerrno\fR set.  Note that the
functions use the \fIcom_err\fR system and define
their own \fBKIPERR_xxx\fR error codes.
.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl, for the ARPA2.net project.
.SH "REPORTING BUGS"
.PP
Please use our issue track for reporting bugs.  It can be
found online, at
.IR https://gitlab.com/arpa2/kip/issues .
.SH COPYRIGHT
.PP
Copyright \(co 2019 Rick van Rein, ARPA2.net.
.PP
ARPA2 is funded from InternetWide.org, which in turns receives donations
from various funding sources with an interest in a private and secure
Internet that gives users control over their online presence.  This particular
project is part of ARPA2 Mail & Reservoir, funded by SIDNfonds.
.SH "SEE ALSO"
.IR kip "(1), " kipdata_up "(3), " kipdata_down "(3),
.IR " kipdata_random "(3), " kipkey_generate "(3)."
.PP
Online resources may be found on the project home page,
.IR http://https://gitlab.com/arpa2/kip .
