# Securing Remote Desktops

> *Desktops need other abstractions than X11.  Wayland
> is moving in for the management of drawing regions.
> Pango renders text, Cairo renders graphics.
> Still missing: Distribution and security.*

This stack seems to be possible, or at least close, and it integrates with many applications:

  * Applications may use a desktop toolkit such as [Qt](https://www.qt.io/) or [GTK](https://www.gtk.org/).  These in turn use Pango and Cairo.
  * Text is often rendered with [Pango](https://pango.gnome.org/) on Cairo.
  * Graphics are often rendered with [Cairo](https://www.cairographics.org/) on a frame buffer.
  * [Wayland protocol](https://wayland.freedesktop.org/docs/html/) connects applications to compositors that swap frame buffers to actual screens and send back input events.

**Distribution.**
Wayland focusses on efficient framebuffer access, and mostly relies on memory mapping and file descriptor passing, so it is locally oriented.  Any attempt for a remote version would be as wasteful as VNC.  The X11 idea of rendering instructions being sent over the wire is more helpful, because it can reach better encryption.  This is one of the reasons why X11 is expected to be around for a while (another reason being old applications).

Perhaps compression techniques like those in [DjVu](http://www.djvu.org/) can help to improve the transfer of bitmaps.  This specifically requires separation of sharp (textual) content and vague (photo) content, which is where knowledge of the drawn information enters the scene.  Toolkits like Qt and GTK might do this.  A more general approach could be to make this mapping at the level of Pango and Cairo.

This involves a stub/skeleton generator for Pango and Cairo, and perhaps a bit of negotiation to determine whether the application can offload these instructions to the compositor.  If not, then a fallback to pixel rendering or a video stream would be possible.

The best transport is SCTP, and may support a layer of RTP to accommodate integration with protocols such as SIP or WebRTC; applications could then dial into a screen.  For audio, dropouts are less harmful than delays, so UDP is perfect.  For tty traffic, a stream of bytes should be processed in the proper order.  But for desktop rendering, SCTP is quite useful if changes make it possible to already draw the content, perhaps with the intention of redrawing when intermediate content repeats.  When drawing instructions are passed around, they can simply be run again on a backup of content.  Such a mechanism for out-of-order rendering can be used deliberately, allowing high-priority menu text to be drawn before a low-priority backdrop is placed underneath it.

**Security.**
A popular packet-based encryption scheme is DTLS, which is defined for SCTP, currently with user message size constraints, but that is being worked on.  Another approach might use KIP, because push-mode encryption basically allows the sending of content to an intended recipient, but only when they can authenticate properly.  This means that screen content and typed passwords might be shown publicly, and left to the recipient to decode.  The mechanisms of Realm Crossover would be of great use here, to connect a sender and recipient under different domains.  KIP uses SASL, for which Realm Crossover has been defined.

The start of a session would involve sending session key that the recipient can only retrieve through authentication.  As discussed for Streaming KIP, this can be done in the SIP/SDP setup of a stream.  The target of authentication would normally be a KIP server in the recipient's domain.  The actual traffic then consists of encrypted frames, each of which relays Pango and/or Cairo instructions from application to compositor.  The reverse follows the same idea; events from mouse and keyboard are protected with push-mode encryption to the recipient; the compositor ensures that only the traffic intended for the application is actually relayed to the remote application.  In all cases, it is useful to sign content regularly.  This helps to ensure that content comes from a trusted source, and may be trusted on account of their identity.

**Media.**
Streaming KIP is defined at the RTP level.  This means that it may be useful for audio and video streams as well as for desktop streams.  Do not try to insert live streaming media into a Pango/Cairo stream though; stick with the existing protocols, only with KIP to protect them while in transit.

Note how this allows media and applications to be seemlessly integrated into a desktop, in a "rootless" mode.  The limited options offered by many conference tools can now be considered part of a SIP/KIP (or SoepKIP) application that mingles with the desktop.
