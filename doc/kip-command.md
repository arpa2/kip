# KIP on the Command Line

> *KIP makes it incredibly simple to exchange documents with encryption and integrity.*

Basic commands are `kip up` for sending with encrypting and signing, and
`kip down` for receiving with decryption and signature verification`.
It also tries to be clever about commandline parsing, by recognising patterns
of use and doing what makes sense.  Without ever becoming fuzzy, of course.

When `kip` gives no output messages, it is happy.  It will always report an
error of some kind if it exits with a non-zero result.  This will not
be questionable, but absolutely clear an error message.  Warnings may
be reported while still exiting with a zero result.


## Using KIP UP

The basic `kip up` forms are:

  * `kip up` to read from `stdin` and send off via the default uplink.
  * `kip up <source>` to read the `<source>` and send it off via the default uplink.
  * `kip up <source> <target>...` to read the `<source>` and send it off to one or more `<target>` uplinks.

Uplinks are plugin modules that allow sending a KIP-protected document to some target location, such as a recipient's inbox.  The normal form for a `<target>` is a URI, often in a `scheme:[user@]domain.name` notation, but variations exist.  Each `<target>` is matched individually against possible uplinks, and a weight assigned.  You can configure this to behave to your wishes.

The default uplink is normally configured explicitly, and may well be a home collection such as a directory under your local home directory or a share on an object store.  When not configured, the `<target>` used will be `stdout` so the command `kip up` defaults to a pipeline operation.

The defaults (reading from `stdin` as `<source>` and the default uplink as a `<target>`) can also be written explicitly as `.` so a single dot.

The default is to provide encryption as well as integrity.  You can also select this explicitly with two flags, or you can select one of the two and leave out the other:

  * `-e` or `--encrypt` for encryption to the specified `<target>` users
  * `-i` or `--integrity` for integrity protection through a digital signature

So, the forms are

  * `kip -e` for just encryption;
  * `kip -i` for just integrity protection;
  * `kip -ei` to do both;
  * `kip` with neither flag to do both as its clever default.

Integrity requires your authentication, for which KIP uses SASL.  If you use a single-signon system such as Kerberos, this should not result in any interaction; otherwise you may have to direct the process.

The uplinks are various, and are generally pluggable.  Only uplinks that you configured are normally used, with the exception of the local file system and email.

  * `stdio` supports pipeline operation in spite of, or on top of, any configuration
  * `filesystem` supports local file system paths
  * `smtp` sends documents over SMTP email, usually with file size constraints
  * `amqp` sends documents using AMQP 1.0; unlike email, the documents may be of arbitrary size
  * `reservoir` sends documents to the ARPA2 Reservoir, and produces a link that may be sent over another uplink
  * `rest` uploads documents to a RESTful web server API
  * `webdav` uploads documents via WebDAV to a web server
  * `filesender` sends documents of arbitrary size through FileSender using its RESTful interface

Note that some of these may require you to authenticate once more.


## Using KIP DOWN

The basic `kip down` forms are:

  * `kip down` to fetch from the default downlink and output on `stdout`.
  * `kip down <source>` to fetch from the `<source>` and output on `stdout`.
  * `kip down <source>... <target>` to fetch one or more `<source>` items and output each separately to the `<target>`.

Downlinks are plugin modules that allow fetching a KIP-protected document from some source location, such as a URI or your inbox.  The normal form for a `<source>` is a URI, often in a `scheme:[user@]domain.name` notation, but variations exist.  Each `<source>` is matched individually against possible downlinks, and a weight assigned.  You can configure this to behave to your wishes.

The default downlink is normally configured explicitly, and may well be a home collection such as a directory under your local home directory or a share on an object store.  When not configured, `stdin` will be the default `<source>` to fetch from, so the command `kip down` defaults to a pipeline operation.

The defaults (reading from the default downlink as `<source>` and sending output to `stdout`) can also be written explicitly as `.` so a single dot.

The default is to provide encryption as well as integrity.  You can also select this explicitly with two flags, or you can select one of the two and leave out the other:

  * `-e` or `--encrypt` to undo encryption with credentials that you may acquire
  * `-i` or `--integrity` for integrity assurance through a digital signature

So, the forms are

  * `kip -e` for just encryption;
  * `kip -i` for just integrity protection;
  * `kip -ei` to do both;
  * `kip` with neither flag to do both as its clever default.

Encryption requires your authentication, for which KIP uses SASL.  If you use a single-signon system such as Kerberos, this should not result in any interaction; otherwise you may have to direct the process.

The downlinks are various, and are generally pluggable.  Only downlinks that you configured are normally used, with the exception of the MIME type, local file system and email.

  * `stdio` supports pipeline operation in spite of, or on top of, any configuration
  * `filesystem` supports local file system paths
  * `mimetype` works on documents of the MIME type for KIP Documents, as well as `data:` URIs for that MIME type
  * `smtp` retrieves documents from your mailbox, possibly for `cid:` or `mid:` URIs
  * `amqp` receives documents using AMQP 1.0; unlike email, the documents may be of arbitrary size
  * `reservoir` receives documents from the ARPA2 Reservoir, and produces a link that may be sent over another uplink
  * `rest` retrieves documents from a RESTful web server API
  * `webdav` retrieves documents via WebDAV from a web server
  * `filesender` retrieves documents of arbitrary size through FileSender using its RESTful interface

Note that some of these may require you to authenticate once more.


## Further Commands (TODO)

There will be more, I'm sure.  Ideas are:

  * Composing a KIP Document from a JSON recipe, or exporting its description as such
  * Live streaming, with partial KIP Documents and perhaps key rollover (old key signs for new key)
  * Plugins for IPFS and/or IPNS
  * ...your-idea-here...
