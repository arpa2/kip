# KIP for Media Streaming

> *It seems that KIP is quite suitable for media streaming.
> How would this work?*

The KIP Document library loads an entire document into memory.
That is useful for editing, but media streaming is about
processing bits and then forgetting about it.  A different
bit of software may be able to help here (and it may land
alongside the `kip up` and `kip down` in the commandline).


## Easy integration with Secure RTP

There already exists a framework for secure media, namely
[SRTP]()https://tools.ietf.org/html/rfc3711,
which mostly lacks functionality for key exchange.  It is
very easy to use KIP just for key exchange, and
leave it to the existing infrastructure to handle the
packet-level encryption.

The
[SDP](https://tools.ietf.org/html/rfc2327)
specfication defines

```
Encryption Keys

k=<method>
k=<method>:<encryption key>

   k=uri:<URI to obtain key>
     A Universal Resource Identifier as used by WWW clients is
     included in this key field.  The URI refers to the data
     containing the key, and may require additional authentication
     before the key can be returned.  When a request is made to the
     given URI, the MIME content-type of the reply specifies the
     encoding for the key in the reply.  The key should not be
     obtained until the user wishes to join the session to reduce
     synchronisation of requests to the WWW server(s).
```

The provisioning of key material with the MIME content-type for
KIP is an excellent mechanism to share keys before the
stream starts.  A link between SRTP software and KIP is then
all that is required.  Especially the use of KIP Service
can be very useful to constrain those who have access to a
fixed key being exchanged.  Finally, the key included can be
a `data:` URI with the media type for KIP.

One interesting use case is for Kerberos, as this is a
single sign-on mechanism.  Kerberos keys change from day to
day, which makes them less likely candidates for encryption.
This means that an indirection is needed, using Kerberos to
authenticate to some Kerberos service.  Since KIP Service
employs SASL for authentication, including support for
Kerberos, this is as good as any other path (none of which
I know of) to encrypt a media session based on a daily Kerberos
login.

In a legal sense, it is possibly of interest that media keys
are amalgamated with general-purpose applications.  Legal
tapping requirements may not apply to a general, stateless
KIP Service.  Also note that the service is sufficiently
straightforward that it might be run on any node.  Especially
with Realm Crossover for SASL through SXOVER, it would not
even require evaluation of access.

We should make a separate profile for this usage pattern.
It sill seems useful to consider the KIP Document format for
streaming data securely over other protocols, however.  This
is developed below.


## Streaming profile for the KIP Document format

The format of a KIP Document is suited for streaming:

  * Content may be cut up in separately handled chunks

  * Both MAC and Signature can be verified at arbitrary
    points in the flow

  * Key material is sent before anything else, and may be
    repeated for any new media user

  * Limited access to keys could be used to control visibility
    of data

To limit the amount of buffer space required, smaller chunks
of data may be used.  This is particularly important for live
interactions, such as a SIP phone call.

When signatures are included, they can also be made often,
again limiting the amount of buffering time.  In many media
usage cases however, the impact of a single bit of data is
modest and the MAC included in a Mud chunk would suffice.

It is *not* a good idea to repeat key material constantly.
This would cause the KIP Context to gradually expand and
possibly slow down as a result of it.  Rolling between
KIP Contexts may however be used to recover, for instance
in a satellite stream shown to many.  Another solution
could be to explicitly delete keys (an OffKey chunk might
indicate that a Key is not used anymore and this might
be *interpreted* by an application as removal).

When starting streaming, the first thing sent should
be the chunks that indicate keying.  If this is kept
as a rule, then replaying that bit might suffice to
update the listener if it missed it; but resending
at the transport level could do this to.  Once keys are
setup, the KIP Stream can commence.  A client could
store, skip or pause a stream until it has obtained
the key material used.

Repeated chunks can even combine with online encryption,
even when they may be played repeatedly, but this would
destroy signatures.  If signatures are to be combined
with repeat playing, then a requirement is to precalculate
the encryption (and store it on disk, say).  Alternatively,
the entropy used may be precomputed, or derived somehowe.




