# Web enabling: HTTP and JSON

> *KIP was designed to be efficient and minimalistic, and downright
> secure.  These properties would all suffer from a web-based design.
> But this does not mean that KIP could not support applications with
> lower expectations, especially in terms of security.  Here's how.*

KIP consists of a few "classical" protocol layers that each translate
easily to a web infrastructure:

  * The application layer is designed in ASN.1 for reasons of precision
    and extensibility.  The standard protocol is a binary representation,
    mostly DER.  For a web application, there are JSON Encoding Rules
    in X.697, especially with the BASE64 annotation for binary content.
    The pleasure of the abstract syntax notation of ASN.1 is that
    representations can be varied, and it is possible to translate between
    these formats in a protocol tunnel.

  * The authentication protocol used is SASL.  This is like generic piping
    through which messages flow, adhering to a specific mechanism that is
    not of importance to the containing protocol.  A translation layer can
    take SASL from one protocol and forward it in another.  HTTP has no
    [generic support for SASL](https://datatracker.ietf.org/doc/draft-vanrein-httpauth-sasl/)
    yet, but many HTTP authentication methods follow the same idea and can be
    ported into a SASL backend.

  * The encryption protocol is TLS, with server-side authentication as a
    minimum.  (Clients might authenticate during the TLS handshake and
    reap its benefits with the SASL EXTERNAL method.)  HTTPS is simply
    HTTP over TLS, and the intermediate HTTP layer does not add much.


## Why HTTP/JSON is not the default

The main reason for not making HTTP/JSON the default model is that this
is a security protocol.  The general environment of a web browser is
insecure, and it is the most common platform that would use this form
of KIP.  It would degrade the security level available through KIP if
one endpoint runs the protocol in an environment together with adverse
advertisements.  This is possible, but not a suitable default.

A second reason is that we want to support really small devices.  The
world of Internet-of-Things has a serious problem bootstrapping security,
and SIP phones also lack security.  Both may benefit from KIP if it is
simple to implement and has small (additional) code requirements.


## Authentication in HTTP, not JavaScript

The suggestion to link SASL to HTTP headers is not the common approach
of web application development.  The tendency is more towards controlling
exchanges from JavaScript.  But this is also the space in which advertisements
from dynamically loaded third parties run, and that makes this context less
than perfect.  A `kip.js` library might be plugged into such a context and
suffer from it.

The browser is quite capable of serving authentication at the HTTP
level.  We actually have a nice
[demonstration of HTTP SASL](https://github.com/arpa2/docker-demo/tree/master/demo-ffsasl)
that previews how upcoming technology in the web world can be used
to do this in a generic manner.  This puts the user in the cockpit,
rather than the application logic.  Given the simplicity of the
authentication exchange, this is preferrable.  And it helps to make
the security level easier to trust for the users of HTTP/JSON
translations with a matching `kip.js` library.


## Service Localisation

The web platform offers more choice, notably the location of the
translation service is an extra parameter to configure.  This may
be done for local configurations, but is not practical for remote
connections.  Looking up data like this in DNS also is not the
kind of thing a web browser can do.

This implies another requirement for a HTTP/JSON proxy: lookup
the desired domain and contact its underlying server.  Just being
in contact with one's local KIP service suffices for creating
signatures and decrypting traffic; but the functions of verification
of signatures and encryption of traffic require access to remote
KIP services.  So, when the intention is to allow crossover between
domains, that a proxy finds those remote realms.

This means, of course, that the HTTP/JSON proxy can read the target
domain and the method in the request, which is not difficult to
do.  Both the domain and requested method are contained in the
application protocol defined in ASN.1, so it automatically pops
up in JSON.


## RESTful methods and Access Control

It is possible to relate RESTful methods to the operations that
KIP supports.  Since the information is also available in the JSON
encoded form of the ASN.1 syntax, there may be a need to check
consistency.  Given this, any access control mechanism that can
filter generally at the HTTP level will give the desired results
for the KIP service proxy.

The following mapping is proposed:

  * Encryption is like `PUT` and decryption is like `GET`.
  * Signing is like `PUT` and verification is like `GET`.
  * These two methods would need their own paths.
  * Alternatively, there could be deciated methods, perhaps
    `ENCRYPT`, `DECRYPT`, `SIGN`, `VERIFY`.

In terms of access control, consider the following:

  * `DECRYPT` and `SIGN` are usually local methods, but may
    have to link back to the sender domain if no `_tcp._kip`
    SRV record is declared for the local domain;
  * `ENCRYPT` and `VERIFY` are usually remote methods, run on
    the recipient domain, so they must be open to the public;
    this openness however, applies to the KIP Service
    at the `_tcp._kip` server but not to an HTTP/JSON
    wrapper;

So, generally put, the KIP Service may have to be open to the
public while an HTTP/JSON wrapper is local.
