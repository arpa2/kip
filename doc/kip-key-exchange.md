# KIP for Key Exchange

> *KIP can be used in a Quantum Proof manner.  This means
> that it may be interesting to add Key Exchange services.*

Under symmetric encryption, Perfect Forward Secrecy is not possible.
This generally requires Diffie-Hellman key exchange.  That mechanism
however, is under attack by Quantum Computers and this already has
effects; traffic being stored today may be cracked in the future
on account of this key exchange.

Mixtures of the protocols provide the combined benefits; it is both
Quantum Proof and yields Perfect Forward Secrecy.  All that we need
to do is mix a shared symmetric secret into the Diffie-Hellman
protocol; see TLS-KDH for an example.


## API calls for Key Exchange

As per [issue #37](https://gitlab.com/arpa2/kip/-/issues/37),
KIP intends to support key export.  The keys setup in KIP Core
are deliberately never exported, but that can be used to map
input random material to an output key, if that output key may
be just a random blob.  So we define a new call for KIP Core,

```
bool kipdata_userkey (ctx, indata..., outuserkey...)
```

This processes input data, which may be composed of publicly
available data, applies a shared secret key to it and comes
up with the output that can serve as a user key.  That is, a
key that is fully available to the user.

The entropy in this example comes entirely from the shared
key.  Relying on the input data as a secret source of entropy
would mean that the originating protocol had to be trusted,
which is just what we are trying to avoid.  If the context
is protected with Diffie-Hellman, this might otherwise make
this call just a stepping stone; it is therefore paramount
that the shared key is well-protected, and given that this
is the case, no further secrecy is required.

The KIP mechanism to bootstrap keys with remote parties is
KIP Service, and that is the likely call to preceed the
derivation of the user key.  A sending KIP context can use
`kipservice_tomap()` to share a key and a recipient uses
`kipservice_frommap()` to share a key.  Each then calls
`kipdata_userkey()` with data that they previously agreed
to incorporate from their context.


## Data Profile for KIP Key Exchange

The data to transmit for KIP Key Exchange is limited, and
should readily fit in a `data:` URI with the KIP MIME-type
for most transports:

  * There is no need to use a magic header when the context
    defines the document content; this is the case for the
    `data:` URI because it mentions the MIME-type.

  * The first element is a `SvcKeyMud` block, to be used
    in a `kipservice_tomap()` call.

  * To invoke `kipdata_userkey()` there is no need to add
    any further data, as its contents are taken from the
    protocol context.

  * The one element is simple to wrap in CBOR, especially
    because there is no requirement for Canonical CBOR.
    We can prefix a bytes header with 2-byte size, then
    the fixed integer value `TAG0_SVCKEYMUD` and before
    that an array type with 2 elements.  In total a prefix
    of 5 bytes.


## Protocol Embedding of KIP Key Exchange

Protocols need to define input data for use with the
`kipdata_userkey()` protocol, by grabbing identifying parts
from their content.  This will result in cryptographically
binding this information to the encryption scheme.  For this
reason, it is attractive to use material from both sides.

### KIP Key Exchange in TLS

When used with TLS, the input data consists of the
ClientHello random followed by the ServerHello random.
They are represented as one long sequence of binary bytes.

This mixes with the Diffie-Hellman Key Exchange with the
Quantum Relief method of TLS-KDH, but using another form
of key information.

### KIP Key Exchange in SIP/SDP

The
[SDP](https://tools.ietf.org/html/rfc2327)
specfication defines

```
Encryption Keys

k=<method>
k=<method>:<encryption key>

   k=uri:<URI to obtain key>
     A Universal Resource Identifier as used by WWW clients is
     included in this key field.  The URI refers to the data
     containing the key, and may require additional authentication
     before the key can be returned.  When a request is made to the
     given URI, the MIME content-type of the reply specifies the
     encoding for the key in the reply.  The key should not be
     obtained until the user wishes to join the session to reduce
     synchronisation of requests to the WWW server(s).
```

This allows embedding of KIP Key Exchange in a `data:`
URI, including the MIME-type for KIP and the content
in base64 encoding.

SDP alone defines one end point for a media exchange;
when matched with a reverse there is a unique channel
definition defined by IP number, protocol and port on
each end (along with their time of use).

SIP around SDP adds session identity information,
including the identities in `From:` and `To:` headers
that are hopefully not altered in transit.  It also
adds identities for the call and its end points.

