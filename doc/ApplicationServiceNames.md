# Application and Service Names in SASL

> *SASL uses application names and service names.  These are used in here.*

  * **Application Names** are used in CyrusSASL2 to locate its `.conf` files.

  * **Service Name** are used in CyrusSASL2 to locate user account info.


## Application Names

Application names are only meaningful on the server side.  The
Quick SASL interface does however set it in its general API call
`qsasl_init()` or `xsasl_init()`.

  * `Hosted_Identity` is the default name for QuickSASL.
    It will normally be used for protocol-independent identities,
    but is not secure enough for realm crossover; most mechanisms
    should not be observed by intermediate foreign servers.
    QuickSASL may also choose other, software-related names.

  * `InternetWide_Identity` is the default name for XoverSASL.
    This offers the mechanism `SXOVER-PLUS` that offers an
    end-to-end encrypted/authenticated tunnel that wraps another
    SASL mechanism.  This setup is secure for realm crossover.
    For the inner layer, so for the SASL exchange that passes
    through the tunnel, it is better to use another name, such
    as a software-related name or perhaps `Hosted_Identity` to
    stay general.

  * `KIP` is used for the specific protocol KIP, but it may be
    a link to one of the above.

  * `HAAN` is used for the specific protocol HAAN, but it may be
    a link to `InternetWide_Identity`.


## Service Names

We need to be more careful about service names, because they are
set on both client and server.  They may be used as part of the
SASL mechanism, and could end up distorting hash outcomes.

  * `RealmCrossover` is the mechanism of Realm Crossover for SASL,
    which means that it may be passed over Diameter to another
    realm.  Note that this is a generic name, unlike dedicated
    services such as `imap` for a given protocol.  The intention
    is to support Realm Crossover across all protocols.

  * `qsasl-xsasl-demo` is used by our demonstrations for QuickSASL
    and XoverSASL as well as mixes between them.

  * `haan-demo` is used by our demonstrations tools for HAAN, so
    a number of HAAN-specific SASL mechanisms over Diameter.

