# Entanglement of KIP and Diameter

> *Looking at the examples, KIP and Diameter seem to mutually depend.
> The official story is more subtle, as explained herein.*


## Algorithms from KIP Core

The algorithms used in Realm Crossover for SASL are founded on
existing mechanisms for Kerberos.  The specification references
RFC 3961 as a general framework for keying and encryption.

These algorithms may be used from any Kerberos implementation;
the KIP Core facility happens to add a few other conveniences,
notably key usage numbers and a mechanism for mixing entropy
into a key to derive another key, which adheres to the algorithm
KRB-FX-CF2 from RFC 6113 as its formal definition.


## Keying the Realm Crossover Client

Clients need a pre-arranged secret for end-to-end encryption of
the tunnel used to protect Realm Crossover.  This is the primary
value added by the SASL mechanism SXOVER-PLUS.

The secret is not as good as the entire exchange however; it is
like in most TLS use cases, where the server is authenticated
but the client remains anonymous.  This explains why SASL is
necessary as a method for client authentication.

The specification is abstract about the mechanism of key agreement,
and KIP Service is just one way of establishing such a key.
Another might be founded on TLS.  The one concern is that the
server can understand the key setup for the client.

The timing is also free; a single key may be agreed beforehand
through a manual procedure, or one involving automated prior
registration.  The tests generally produce a key on the fly,
as part of the test, which explains why the client needs to
call KIP Service.  This is doable in a test, but situations
involving SASL for online bootstrapping may not be flexible
enough and require pre-agreed secrets.  Still, any such
secrets may be replaced at any time, once online.

The server needs to prove its identity to the SASL client
requesting Realm Crossover, and this is achieved by passing
the secret specific to it.  When KIP Service is the method
setup for the server, then it will call upon the daemon
for decryption.  Maximum flexibility is achieved when this
is done dynamically, as part of the flow, so in this respect
the tests are not overzealous.


## KIP Service may use Realm Crossover

When KIP Service requires SASL authentication, it may in turn
use Realm Crossover.

Early versions of KIP Service were subjected to
[an issue](https://gitlab.com/arpa2/kip/-/issues/63)
that every connection is authenticated, so that even the
client to Realm Crossover had to authenticate in the
KIP Service phase.  This is not generally the idea of
encryption of session keys with KIP Service however, and is
just a form of laziness in the initial coding.

With KIP Service using Realm Crossover for SASL, along with
Realm Crossover for  SASL using KIP Service, there appears
to be a danger of inifinite nesting.  There is not, in
practice, as every nesting depends on the SXOVER-PLUS
mechanism, which is never going to be infinite.  In fact,
the specification does not even allow SXOVER-PLUS to
pass SXOVER-PLUS as its own inner mechanism, because
extra indirections could interfere with traceability of
clients to administrators and would benefit abuse but not
proper use.


