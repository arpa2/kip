# API documentation for KIP Document

**This document has moved!**  You can find it in a standalone project,

  * [Project page](https://gitlab.com/arpa2/kip-document) for KIP Document, which mixes conceptual explanation with programming examples and CBOR dumps.
  * [API website](https://kip-document.arpa2.net/) holds the API documentation, currently only with Python3 code examples, but the project.

**No longer authoritative.**  Visit the links above for the final word.  When the old copy here starts feeling like a fossil (or giving rise to confusion), it will be removed.

**Old words:**

This is a basic website structure.  It can be built with

```
mkdocs build
```
