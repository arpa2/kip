# Input and Output of a KIP Document

> *KIP Document processing often works like a pipeline, taking in one
> form and outputting another.  By default, both are files.  But it is
> possible to use other exchange mechanisms.  New I/O modules can be
> added independently from the KIP handling itself.*

The default handler for File I/O is a module `arpa2.kip.io.file`, which
inherits from general classes in `arpa2.kip.io`.

By way of example, the File I/O module consists of:

 1. A definition `regexes` defining the URI forms that it can handle.
    It uses a literal dot `.` as an abbreviation for `stdin` or `stdout`,
    `/xxx` file names and the URI format `file:///xxx` or
    `file://localhost/xxx` to match the general URI idea.

 2. A class `FileSource` that reads from file.  This inherits from `io.Source`.

 3. A class `FileTarget` that writes to file.  This inherits from `io.Target`.

The superclasses in `arpa2.kip.io` are the following:

 1. `Connector` holds the code and abstractions shared by the
    `Source` and `Target` classes in the `arpa2.kip.io` package.
    Do not inherit directly from this class, but use its
    code and definitions through `Source` or `Target`.

 2. `Source` subclasses can be used as entry_point
     for `arpa2.kip.io.sources` so they are made available
     to users (who may subsequently configure them).

     For every source URI in commands like `kip up`, `kip down`
     and so on, a configured sequence of these classes is
     tried.  First, an exact match is tried on all configured
     plugins; when all fail, then more liberal matching is tried,
     for which they will be sorted by a score.  This allows both
     easy-intuitive uses and accurate, well-established patterns.

     To read data, a connection is made that should return a
     file-like object, possibly with metadata.  Connection failure
     causes the next pattern to be tried, but an exact match that
     fails to connect will never fall back to the more liberal
     matches.

 3. `Target` subclasses can be used as entry_point
     for `arpa2.kip.io.targets` so they are made available
     to users (who may subsequently configure them).

     For every target URI in commands like `kip up`, `kip down`
     and so on, a configured sequence of these classes is
     tried.  First, an exact match is tried on all configured
     plugins; when all fail, then more liberal matching is tried,
     for which they will be sorted by a score.  This allows both
     easy-intuitive uses and accurate, well-established patterns.

     To write data, a connection is made, possibly with metadata,
     that should return a file-like object.  Connection failure
     causes the next pattern to be tried, but an exact match that
     fails to connect will never fall back to the more liberal
     matches.

