# Python3 API

> *The API offers a playground for in-memory KIP Documents.*

We will not go into the details of the API here.  The Python3
inline documentation is a much easier way to get to this.  What
we will do here, is give a few examples, with identifiers to
get you started.

To build the API, the customary incarnations should work:

```
cd kip
python3 setup.py build
python3 setup.py install
```
