# Header of a KIP Document

> *KIP Documents can be recognised in a few manners, among which
> a magic code at the start.*

The start of a KIP Document is an ASCII string of 18 characters, with a
trailing newline `U+000a` but neither carriage return nor NUL character.

  * The start is literally `ARPA2KIP`
  * Following that is the `YYYYMM` formated specification version time
  * The following 4 characters use base64 notation for 4x6 = 24 flags,
    MSB sextet first and LSB sextet last.

This header can be parsed with a normal CBOR parser, which sees it as:

 1. A byte string of length one, `b'R'`
 2. A byte string of length 15, `b'A2KIP202003===='` when the specification
    date is 2020-03 and the flags are zero.
 3. The letters `A` and `P` dropped out; their highest 3 bits encode for
    a byte string and their lower 5 bits encode a length of 1 and 16.

It is up to you whether to scan for the literal start bytes and parse
the rest, or to use the CBOR parser that is already needed for the job.
In the latter case, be sure to validate the string lenghts.
