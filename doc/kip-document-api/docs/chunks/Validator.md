# Abstract concept `Validator`

> *Validators form the end of a signed are in a KIP Document.*

**Subclasses:** [`SigMud`](../SigMud), [`SvcSigMud`](../SvcSigMud), [`PubSig`](../PubSig)

Signatures range from a [`KeyIntro`](../KeyIntro) chunk to a `Validator`
with an overlapping key number.  It is possible to span an entire KIP Document
with one signature, but also portions may be addressed.  Finally, there is a
time-out option by placing some keys [`OffKey`](../OffKey) for a part of a
range in the KIP Document.

Validators define a number of useful methods:

  * `is_signed()` returns `True` if this document was signed, or `False` otherwise.

  * `get_keynrs()` returns the key numbers that are to be used in the signature.

  * `set_signers (validkeys)` is called by subclasses to set the keys that formed
    correct signatures.

  * `got_signers()` returns `True` if at least one key was set as `validkeys`,
    or `False` otherwise.

  * `get_signers()` returns the `validkeys` as setup with `set_signers()`.

  * `make_signature()` is an internal method called to create a fresh signature.
    Subclasses should override this method and locally set the signature value such
    that it may be exported to CBOR.  It will probably have to obtain a list of
    internal Key objects for a given keynr from kipctx, in light of the non-unique
    key numbering system in KIP Documents. 

  * `validate_signature()` invokes the signature validation process.  Subclasses
     should override this method and call `set_signers()` when successful.  It
     will probably have to obtain a list of internal Key objects for a given
     key number from the internal KIP context, in light of the non-unique key
     numbering system in KIP Documents.
