# Suspension and Resumption of signatures in `OffKey` chunks

> *In a dynamic (editing) environment, not all chunks of a KIP Document
> may be part of a signature.  The `OffKey` chunk helps to switch the
> collection of hash input on and off for certain keys.*

While editing a KIP Document, signatures get invalidated.  One's own
signatures can easily be remade, but that does not apply to signing by
others.  What is needed, is to allow some signers to be off-key
while others stay on key and continue signing, eh, singing, eh
not signing it is.  Think of a melody where not everyone is in
tune all the time.

The purpose of this is to allow edits to be made on shared work.
New insertions would make all other signers go off-key, whereas
removals may be proposed by a signer removing his signature and
going off-key on the part that he wants to see gone.

The mechanism works by listing key numbers that will be off-key
for the upcoming chunks, until this is replaced by a future
`OffKey` chunk.  It is valid to set an empty list as an indication
that no signer will henceforth be off-key.

**Code:**
When editing a document, it is useful to remove any keys beforehand,
and recover them afterwards.  The only exempt keys should be those
that are readily available in the editing context, basically the ones
owned by the editor.

```python
insertion = Lit ('Hello Hot World')
pre  = OffKey (123, 456)
post = OffKey ()
doc.append (pre)
doc.append (insertion)
doc.append (post)
```

**CBOR.**
The `OffKey` chunk does play a role in the KIP Document's CBOR form,
because other parties need to see what has changed, and which keys
do not agree with that (yet), as well as what they used to agree with.
The representation is as an array with the tag 7 for `OffKey` chunks,
followed by the (variable-sized, and possibly empty) list of key numbers
that will be excluded from cryptographic hold on the content from the
`OffKey` point onwards:

```json
[ 7, 123, 456 ]
```

This introduces an edit that will not yet be supported by keys 123 and
456; after the edit, their influence should be heard again, so the set
of off-keys is emptied:

```json
[ 7 ]
```

