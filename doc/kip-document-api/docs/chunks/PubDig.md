# Starting Public-Key Signatures in `PubDig` chunks

> *Public Signatures end a sequence that starts with a PubDig marker.
> This is the point where a hash algorithm starts its action.*
