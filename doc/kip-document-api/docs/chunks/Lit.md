# Literal content in `Lit` chunks

> *Literal chunks contain user data, in plaintext form or as bytes.*

Applications need to store data into a KIP Document.  It is impartial
to the KIP Document what that data is; as long as it can be incorporated
into signatures and encrypion schemes in a consistent manner.

To facilitate applications under these requirements, applications can
store either byte sequences (binary content) or UTF-8 strings.

**Rich data.**
Applications
are likely to need semantically richer stores.  They can do this by
subclassing the `arpa2.kip.document.Lit` class and translate any
other type into a string or byte sequence and back.

**Code.**
Create a `Lit` instance while passing it the initial content to store.
There is no structure, and so only one object (string or byte sequence)
can be stored.  Later changes to the content are possible.

```python
litobj = Lit (b'\xaa\xbb\xcc\xdd\xee\xff')

doc.append (litobj)

litobj.set_content (None)
assert not litobj.got_content ()

litobj.set_content ('Hello Hot World')

if litobj.got_content ():
	print (litobj.get_content ())

```

**CBOR.** The tag for `Lit` is 1, and in CBOR it is followed by
the literal value.

```json
[ 1, "Hello Hot World" ]
```
