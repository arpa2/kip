# Opaque symmetric keys in `KeyMud` chunks

> *Key Mud is an opaque symmetric key.  The chunk is used for "key mapping",
> that is, to use one key to obtain another.  In fact, logical combinations
> can be made with a flexibility that is quite uncommon, and shows the
> strength of the KIP Document approach.*

Cryptography can enable flexible tricks and ideas, mostly dependent on new
logical combinations of properties or requirements.  This usually comes down
to playing around with keys.  And so, a KIP Document has a flexible tool for
such key juggling.  The flexibility can be a little confusing, precisely
because it is so simple yet powerful.  Try it in a few simple examples first,
like in the code examples below.

For highly flexible key handling, it is useful to have a kind of "switchboard"
that allows you to group keys into one, sometimes with AND and sometimes
with OR combinations.  General logic dictates that any positive logic
combination can be written in a form with two levels:

```sql
(a AND b AND ...) OR (k AND m AND ...) OR ...
```

This is the kind of structure that can be made with key mapping; you can
require that keys `a` and `b` (and...) are present to gain access to a new one;
alternatively, having `k` and `m` (and...) would suffice.  You can ask for
complex combinations, all to derive access to a new key, which is then
usable in things like [`Mud`](../Mud) for content encryption, or
[`SigMud`](../SigMud) for content signing.

The task of `KeyMud` is to make one of those alternatives available; it is
then up to other `KeyMud` to make the same key available to other combinations.
So, in terms of the above, one `KeyMud` handles `a AND b AND ...` and another
`KeyMud` handles `k AND m AND ...`, both possibly mapping to the same keynumber.


**Keys.**
Keys are described as a number and an algorithm identifier (also a number).
The input keys to the map (such as `a AND b AND c`) deliver twice as many
numbers as the keys (namely the keynr of `a`, the algid of `a`, the keynr
of `b`, the algid of `b`, the keynr of `c`, the algid of `c`).

An additional key is used as output.  This key may have been mentioned before.
The `KeyMud` represents an alternative path to get to that key, for those
who have hitherto not found a way of reconstructing it in their environment.
This is how a KIP Document can be flexible in access rights.  Add to that
the ability to encrypt/decrypt or sign/validate only parts of a KIP Document
and the toolkit offers rather powerful controls to applications.

Think of it like this, a number of rows with alternative paths (OR) where
each requires any number of keys (AND) to get to the desirable output of
the mapped key:

```
   |
   +--> got a --> got b --> ... -->|
   |                               |
   +--> got k --> got m --> ... -->+
   :                               :
     ....    ....   ....  ...   -->+--> get the mapped key
```


**CBOR.**
The `KeyMud` chunk is represented in in a CBOR array containing:

 1. The `KeyMud` tag value 4
 2. map_pairs holds q sequence of integers consisting of a key number and algorithm
 3. The key number provided after applying key mapping
 4. The key algorithm for that key number
 5. The mapping blob used by the KIP library

This is by far the most complicated object in a KIP Document.  You are not
likely to use it much, but it is your ticket to expressive freedom where
access rights to parts of the KIP Document is concerned.

**Code.**
To create `KeyMud`, you basically need to supply the mapping with the
AND-ed keynr/algid numbers for one of the OR branches:

```python
key_a = 123
alg_a = 380
key_b = 456
alg_b = 380

key_out = 789
alg_out = 380

map = KeyMud ( [ key_a,alg_a, key_b,alg_b ], key_out, alg_out, b'')
```

TODO: The last parameter can be set to the body as found in the CBOR chunk.

You do this only once, then insert it into the KIP Document and the chunks
following that will have (extra) ways of getting to `key_out` under `alg_out`.
Of course this is the conceptual story; wrapped into a nice GUI it would be
powerful and wholly usable.

Complex?  Yes.  This basically means thinking about the intended security
of your KIP Document, ahead of time.  Afterwards, choosing the desired
key/alg will help you control access to cryptographic features and the
content that it may unleash.  Again, a GUI can make you happier than this
conceptual explanation.
