# Opaque Signatures in `SigMud` chunks

> *Signature Mud is opaque, but holds a secure hash to validate the
> preceding range of chunks in this KIP Document.*

Signatures are an opaque object that can be accessed under a key to validate
the signing party.  The signature itself is merely a secure hash over the
preceding parts of a document.

In a KIP Document, the parts that are signed may vary.  There is a point of
introduction, namely where the key gets announced, and from here signing is
assumed to start hashing.  The hasing continues until the `SigMud` chunk.

A special point worth noting is that [`Ref`erences](../Ref) are not included
as the content of any documents referenced, but as the factual references.
This means that signatures can be validated without retrieving that content.
It also means that security breaks, unless the `Ref` object is somehow also
verified by a secure hash.  That secure hash would be signed to weld a chain
of trust that makes external content verifiably the same as at the time of
signing.

More subtleties are needed because users may go editing a document after it
was signed.  This basically invalidates a signature, in a black-or-white
rejection.  For a subtler alternative, it is possible to use
[`OffKey`](../OffKey) to bypass signing on new content (and keep older
content around for validations).

All this is particularly useful for multi-party documents.  Each can sign
the parts and versions that they approved of, and later changes will not
invalidate the signature on the original content.  During a review of the
new text, only the changes need to be taken into account, and a new
signature can more easily be made.  Gradually, keys update their part in
the `OffKey` modifiers until they have no further use and can be taken
out.

**Code.**
Create `SigMud` by naming the signing key and the bytes of the signature.
When a signature needs to be constructed, invoke `make_signature()` and
when it needs validation, invoke `validate_signature()`.

Multi-party signatures may be constructed using an intermediate key,
available through a ["key map"](../KeyMud), where each participant to
the multi-party signature must unleash their keys to apply the keymap.

**CBOR.**
In a KIP Document, the signature is stored as a CBOR array starting with
the `SigMud` tag 5, the key number and the bytes holding the signature blob.
