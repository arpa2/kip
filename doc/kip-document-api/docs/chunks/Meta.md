# Metadata in `Meta` chunks

> *Meta data chunks contain a key with one or more values.*

The `Meta` class stores one set of values for a key string.
When multiple metadata attributes are needed, then they go
into separate `Meta` objects.


**Code.**
The constructor for `Meta` objects takes a UTF-8 name and one or
more values, each of which is a UTF-8 string or a byte sequence.

```python
attr = Meta ('grtx', 'Hello', 'Hot', 'World')
doc.append (attr)
```

**CBOR.**
The stored form for `Meta` is an array starting with the `Meta` tag
(value 0), the string name and the (variable) values.

```json
[ 0, "grtx", "Hello", "Hot", "World" ]
```


The key name is an UTF-8 string, and each value is a byte sequence
or UTF-8 string.  The class is part of the signed content of a KIP Document.

Since different people may inject different bits of metadata,
the overall metadata for a KIP Document requires combining
these chunks.  Since the signers for a chunk may vary across
a KIP Document, the question "who says so" is valid for each 
of the values, especially because the same name may be met
with multiple metadata chunks.
