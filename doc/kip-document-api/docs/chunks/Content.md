# Abstract concept `Content`

> *An abstract class below which all user data (or content) falls.
> Effectively, it groups the chunks that are subjected to signing.*

**Subclasses:** [`Meta`data](../Meta), [`Lit`erals](../Lit),
[`Ref`erences](../Ref), [`Mud`dled](../Mud)

This is the superclass for objects to be digested for signing in a
KIP Document.  It defines the following methods:

  * `reset_trust()` resets any trust in this content.  This is useful
    while making a forward pass of signing, while results are pending.

  * `set_trust (checked)` provides the set of keys that tried to
    validate signatures, with success for a range of the KIP Document
    that includes the present content.  The set of keys will be
    reduced to the on-key subset, which is assumed to have been set
    before.

  * `get_trust()` returns the keys that express trust in this content
    through a later [`Validator`](../Validator) incorporating this.

  * `is_trusted()` returns the conclusion whether trust was reached.
    This means that at least one key validated the signature while
    none failed to validate it.  This limits the scope to the keys
    available in this KIP Document and its internal KIP context.

  * `set_on_off_key()` configures for this object which keys are to be
    considered off-key or on-key, that is which will not, and which will
    partake in signatures.  This is automatically derived from
    [`OffKey`](../OffKey) chunks dispersed around the KIP Document.

  * `get_on_key()` returns the set of keys that partake in signing for
    this content.

  * `get_off_key()` returns the set of keys that do not participate in
    the signing of this content.

Perhaps it is surprising the [`Mud`](../Mud) is a subclass of `Content`.
The reason is that it holds content, no matter if it is visible or not.

Similarly, a [`Ref`](../Ref) is a subclass of `Content` because it does
represent user data, whether it is actually loaded is irrelevant to
the desire to incorporate it into signatures.
