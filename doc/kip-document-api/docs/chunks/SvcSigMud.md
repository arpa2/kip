# Domain-verified Signatures with `SvcSigMud` chunks

> *Service Signature Mud is opaque, but holds a secure hash to
> validate the preceding range of chunks in this KIP Document.*

As with [SigMud](../SigMud), referenced content is not signed, but the
references themselves are, possibly including secure hashes.

Since an interaction over the KIP Service is required, which
may incur contacting a remote KIP Daemon, some privacy is
built in with an added salt in the signature, but for a given
document this is still fixed.  Choose beforehand what realm
you intend to trust with this; but mostly it will be the
realm of a sender that you would be likely to trust if known.

Unlike the interaction for [`SvcKeyMud`](../SvcKeyMud), this is
a public operation, and no credentials are required.  The KIP Service
login is done with the SASL `ANONYMOUS` mechanism.

**Code:**
Creation of a `SvcSigMud` uses the C-API format, and is simply

```python
svcsigmud = SvcSigMud (b'\x2b...\xb2')
```

**CBOR:**
The format in KIP Document for this chunk is simply an array with
tag 9 for `SvcSigMud` and the binary content from the C-API:

```json
[ 9, "\x2b..\xb2" ]
```
