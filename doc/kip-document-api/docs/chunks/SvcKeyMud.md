# Opaque Service Key mapping with `SvcKeyMud` chunks

> *Service Key Mud is an opaque service key.*

This is a key introduction chunk that requires an interaction with
the KIP Service to retrieve the key.  That interaction involves
the local user's authentication.  Effectively, the user identity
is keyed by this interaction, hence the name KIP which means
*Keyful Identity Protocol*.

Authentication is done over SASL, and it may cause popups, or rely
on agents that provide the requested keys.  This element of desktop
integration is not part of the KIP Document design, but is delegated
to the underlying KIP Service client libaries.

**Code:**
Creation of a `SvcKeyMud` uses the C-API format, and is simply

```python
svckeymud = SvcKeyMud (b'\x1a...\xa1')
```

**CBOR:**
The format in KIP Document for this chunk is simply an array with
tag 8 for `SvcKeyMud` and the binary content from the C-API:

```json
[ 8, "\x1a..\xa1" ]
```
