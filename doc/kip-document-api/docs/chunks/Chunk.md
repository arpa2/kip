# Abstract concept `Chunk`

> *All chunks in a KIP Document derive from base class `Chunk`.*

This abstract concept defines a number of methods:

  * `to_cbor()` to produce Canonical CBOR to represent the `Chunk` in a KIP Document.
    The same call is used for saving to file, but also for streaming, and so on.

  * `from_cbor_array()` is a class method that functions as a factory for `Chunk`
    subclasses, taking the chunk's CBOR array contents, starting with the tag of
    the chunk to be created.  This raises an exception on syntax errors.

  * `from_cbor()` is a class method that functions as a factory for Chunk
    subclasses, taking the chunk's CBOR byte representation, starting with the tag
    of the chunk to be created.  This raises an exception on syntax errors.

