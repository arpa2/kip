# Abstract concept `KeyIntro`

> *An abstract class for chunks that introduce key objects into a KIP Document.*

**Subclasses:** [`KeyMud`](../KeyMud), [`TabKey`](../TabKey), [`SvcKeyMud`](../SvcKeyMud), [`PubKey`](../PubKey), [`PubDig`](../PubDig)

One object can introduce multiple keys, which is especially the case
for KIP Service, which may yield alternate KIP-Group elements for the
various black/white list and from/till timer settings.

The following methods are defined for `KeyIntro` classes:

  * `get_keynrs()` returns the key numbers introduced in this `KeyIntro`
    object.

  * `got_keys()` returns a boolean to indicate the presence of an internal
    Key object.

  * `set_keys( [123,456] )` is called by subclasses to set one or more
    internal Key objects managed by the KIP Document's internal KIP Context.

  * `get_keys()` returns the set of (extracted) internal Key objects, or
    None if none is present.  This would be a security problem if we were
    not among friends.

  * `extract_keys()` is an internal method that retrieves key information
    from the C-API and sets them up in the Python3 object through `set_keys()`.

  * `get_keynrs()` is called by subclasses to return key numbers introduced
    by this `KeyIntro` object.
