# Opaque content in `Mud` chunks

> *Mud is opaque (like mud), so the information in the chunk cannot be read.*

These chunks represent encrypted user data, such as [`Lit`](../Lit) or
[`Ref`](../Ref) or [`Meta`](../Meta).  Depending on key material and
previous actions the readable contents may be available, and/or the opaque
bytes that represent its online appearance.  In terms of the class, there is
(readable) content and muddled (or opaque) byte sequences.

**CBOR.**
When muddled content is flattened out into CBOR for a KIP Document, only
the muddled bytes are included.  The optional content is meant for in-memmory
user operations on the data.

The file holds the following CBOR array for the `Mud` chunk with tag 3 and
a key number like 123:

```json
[ 3, 123, b'\xa6\xbb...\x08' ]
```

**Keys.**
Throughout the KIP Document, key numbers play an important role.  They may
or may not be usable in the present context, but their numbers are still
used everywhere.

Keys represent the potential to encrypt/decrypt and sign/verify.  Without
keys, these operations will fail.  The idea of KIP Document is to do this
elegantly, and try for example if readable content is available after
loading a `Mud` chunk and running `kip_down` over the entire KIP Document.
Note that you may still work on the remaining content if you like.

Keys and their numbers are introduced by preceding chunks of type
[`KeyMud`](../KeyMud), [`TabKey`](../TabKey), [`SvcKeyMud`](../SvcKeyMud)
or [`PubKey`](../PubKey).  They vary in the work that needs to be done,
but invariably the output is a new key with a new key number.  The example
used below is a key number 123, which is assumed introduced beforehand
by one of these operations.

Note that only one key is permitted to encode and decode a `Mud` chunk.
That may sound minimalistic, but it is enough, thanks to the
[`KeyMud`](../KeyMud) chunk which can take in various keys and map them
to another one.  This allows "translations" of key identity, including
the possibility of multiple other keys zooming in on one intermediate
key.  Since this idea of "key mapping" is available as a generic facility
in KIP (and in KIP Documents), there is not use in also doing it in the
`Mud` chunk.

**Code.**
New instances can be created from content or from muddled content.  Since
the content is encased in a `Let`, `Ref` or `Meta` object, the difference
with muddled content from byte strings is very clear.

```python
keynr = 123
lit1 = Lit ('Hello Hot World')
mud1 = Mud (keynr, lit1)
assert mud1.got_clarity ()
assert not mud1.got_muddled ()
```

The `mud1` object has no muddled content yet.  This is created during `kip_up`
or it may be ordered for this individual object:

```python
mud1.muddle_content ()
doc.append (mud1)
assert got_clarity ()
assert mud1.got_muddled ()
```

The opposite operation would be to `clarify_content()`.  There are operations
to change one of the values, as a way of editing, and thereby remove the other.
Just like testing for either value is possible, so is it possible to fetch it:

```python
mud2 = Mud (keynr, mud1.get_muddled ())
mud2.clarify_content ()
```

This last portion raises an exception: `Please perform a Kip up first`.
If you're a good athlete, you know what to do.  Otherwise, just continue
along with us.

```python
doc.kip_up (VerboseInteractor ())
```

The cause of the exception is that we have no encrypted content in this
KIP Document `doc` yet.  By invoking `doc.kip_up()` the encryption would
be triggers.

_Except..._

We have not yet provided a key to `doc`, nor did we indicate the `Mud` should
use it.  And so we may do a Kip Up, and get a reponse with no failures, but
also with no successes.  _It needs keys do do key-ish things!_

So we need to insert a key in the document before our `mud1` object, _then_
run a Kip Up, and only then can we continue.

How to insert a key is explained on the [`TabKey`](../TabKey) page, it is not
helpful to iterate that here, not without explanation.
