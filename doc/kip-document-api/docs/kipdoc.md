# KIP Documents

> *The content of a KIP Document falls apart in ranges that may each have
> their own security status.  Some parts may be signed, others newly added,
> and parts may be shown while others remain opaque to you, but perhaps not
> to others.  KIP Documents can include references securely.  KIP Documents
> may be used as secure wrappers around a URL.*

**Interface.**
KIP Documents form a sequence of `Lit`eral chunks, optional `Meta`data and
`Ref`erences.  These generally hold application content, with which KIP
will not interfere.  All that is required is that it maps to byte sequences
which may be signed and/or encrypted.

Other chunks can sit between these parts.  Perhaps to mark areas that are
to be signed, or areas that are exempted because they have been edited since
the last signing opportunity.  And perhaps areas of the document that could
not be decrypted with the locally available keys.  And finally there is a
fair bit of key juggling to make various users find those keys under a
logical AND/OR structure to be able to flexibly meet access requirements.

**Abstraction.**
So, as far as the KIP Document is concerned, application content is just a
funny way of coming to sequences of bytes.  And to the application, a
KIP Document is similar to a file system, which is also an abstract store
of byte sequences.  The trick is that these abstractions can be interwoven
to come to exciting new opportunities.

**Surprising.**
Some ideas around KIP Documents are as mind-blowing as they are eye-opening.
The intention is to allow cryptographic strengths in new areas, with better
support for dynamic work while barely complicating simpler use cases.

**Kipping.**
At any point during the lifetime of a KIP Document, you can invoke the
`kip_up()` method to update signatures and encryption inasfar as possible
with the keys available locally.  Output made with other keys will not
be harmed.

Likewise, it is possible to invoke `kip_down()` to decrypt document parts
and verify signatures, again inasfar as possible with locally available
keys.

Both calls take in an `Interactor` object, which collects errors or other
messages.  Failures will render the intention broken.

**CBOR.**
It is possible to extract CBOR codes for the entire document, individual
chunks or anything in between.  The content can be written out to a CBOR
file, usually with a filename ending in `.kip` to hold the KIP Document.

To facilitate such mappings, it may suffice to attract focus.
