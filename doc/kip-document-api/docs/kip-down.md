# KIP Document Processing: `kip_down`

> *The general idea of `kip_down()` is to take away security barriers.
> A lot of algorithmic cleverness makes this look easy.
> That is the point, of course, making crypto accessible.*

The purpose of Kip Down is to remove security barriers in a KIP Document.

**KIP Down on security.**  This makes a pass over the document
to validate its security and perform the separate `kip_down`
invocations on [`Chunks`](chunks/Chunk) of [`Mud`](chunks/Mud)
in the KIP Document.  This is done in a few phases, each of which
reports success and failure to the interactor.

This is an idempotent call: Repeated calls on the same data
have no effect.  When something has been changed however,
you will be able to give that another try.  This is extremely
useful for interactive documents; after any number of changes
you can call `kip_down()` again, and the KIP Document will
not be confused by any repeated portions.

Effectively, `kip_up()` and `kip_down()` are reverse operations,
just like the C library calls do for individual Chunks.

Internally, this work is split into phases:

 1. **Gather keys:** Make a pass over the KIP Document to gather keys that
    are available, and either on-key or off-key at each point.

    This is an idempotent call: Repeated calls on the same data
    have no effect.  When something has been changed however,
    you will be able to give that another try.

 2. **Validate Signatures:**  Make a pass checking signatures used in
    this KIP Document.  This involves detecting introduction of each
    internal Key by a [`KeyIntro`](chunks/KeyIntro) and resetting its hash,
    then updating its Sum with [`Content`](chunks/Content) material inasfar
    as the various internal Keys are on-key.  Note that nothing is done
    with encryption in this phase, as that would alter the Sum.

    Signatures mention a key number, which is not unique.  There may be
    document merges, leading to overlapping numbers.  This is not a problem;
    a successful signature can be traced back with full certainty to the
    exact internal Key that matched, and any associated identity of that Key.
    For this reason, it is safe to consider a signature successful if only
    one internal Key for a given key number matches.

    This is an idempotent call: Repeated calls on the same data have no
    effect.  When something has been changed however, you will be able to
    give that another try.

 3. **Windback Trust:**  Make a pass in backward direction to relay trust
    information from signatures in [`Validator`](chunks/Validator) to the
    signed info in [`Content`](chunks/Content].  The `Content` may have its
    trust set while signatures are being collected in a forward pass, and
    then set by this reverse pass.

    This is an idempotent call: Repeated calls on the same data have no
    effect.  When something has been changed however, you will be able to
    give that another try.

 4. **Clarify Content:**  Make a pass decrypting data in this KIP Document.
    This applies to [`Mud`](chunks/Mud), which may then result in
    [`Lit`](chunks/Lit), [`Ref`](chunks/Ref) or [`Meta`](chunks/Meta)
    to be exposed.  Data extraction is orthogonal to signature checks,
    because signatures are made on the encrypted format.

    This is an idempotent call: Repeated calls on the same data have no
    effect.  When something has been changed however, you will be able to
    give that another try.

