# API documentation for KIP Document

> *KIP Documents enrich documents with encryption and signatures,
> in the most flexible way possible.  When they are edited, security
> would only remain for the original parts of the document, but the
> new content can be woven in to guide later change inspection.
> Key management allows very flexible permissions to users over
> parts of a KIP Document.  And no author needs to handle keys.*

This is the API documentation for manipulating a KIP Document in
general terms.  An application would be mostly concerned with `Lit`eral
data chunks and `Ref`erence inserts, but these are part of a larger
CBOR document structure that can be manipulated as KIP Documents, and
subsequently signed, encrypted and access-controlled.

The system is built around the Keyful Identity Protocol, or ARPA2 KIP
for short.  This system allows people to use their identity (and login)
in places where they would normally work with a key.  They never need
to handle keys, but leave that to an online server that runs under
their domain.  A rather small and dumb server, we should add.

Mobile users can benefit greatly from not having keys on their devices.
When such devices would be lost, their keys would enable the new owner
great control over their work.  Under KIP Document, this only reaches
so far as login still works.

