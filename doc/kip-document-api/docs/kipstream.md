# KIP Streams

> *Nothing about KIP Documents makes them batch-mode objects.
> You can stream contents with security if you like.*

The chunks of a KIP Document are simply placed in sequence, and the computation
of signatures and encryption works in the reading direction.  This may also be
the time direction.  It is possible to apply this on streaming media, for
instance.  As always with KIP, there is no need to hold on to key material before
this can start; each recipient is responsible for obtaining their own key material.

One note worth making is that a signature cannot be validated on parts of a
streaming document.  Only when a signature is hit, can the preceding part be
validated.  When playback commences before this point, it is vital that this
is made aware to the receivers.  It is possible to sign the data at places
that make most sense to the application and its framing properties, of course.

The flexible play with keys, notably through key maps, can be helpful in doing
clever things, such as splitting off new keys from time to time, in a rolling
motion (think of Signal's key ratchet concept).  During setup, such as with
SIP, the key exchange can be initiated and the keys can then be used on RTP
elements, where the frame numbering in RTP may be used in the mix.

Specifically note that, although CBOR has an open-ended form for its data
structures, such is not required in a normal use of KIP Document, just to
achieve KIP Streams.  This is one reason why the definition of a KIP Document
is just a _sequence of chunks_ (after a header).
