# MIME type for KIP Document

> *MIME types can be used for email attachments, web downloads and SIP content.*

We use `message/x.kip` as the MIME type, because `x.` is for unregistered use
and experiments.  We hope to drop that part and later end with an officially
registered MIME type `message/kip`.  The present advise is to send the former
and to accept both.

One interesting use for MIME types is in
[`data:` URIs](https://www.rfc-editor.org/rfc/rfc2397).  These start with a MIME
type, and may have additional flags appended after a semicolon, until a comma
prefixes the actual content in base64 encoding.

As options, we define analogs to the flags, such as `;private`.  We always need
to add `;base64` because we are passing binary content in a textual URI.

Such URIs can be used to trigger a MIME-type-specific
[KIP handler](https://www.freedesktop.org/wiki/Specifications/AddingMIMETutor/)
that can validate signatures and possibly decrypt concealed links before
relaying the link to another application.  This makes it a secure reference,
and the security status may well be acknowledged by the tools.

Note that a URL can also be formatted as a QR-code.  In this case, the same
quality assurances may be made available to mobile devices.

![Demonstrating a KIP Document in a QR-code](kip-document-demo.png)
