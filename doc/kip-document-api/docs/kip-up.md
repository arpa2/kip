# KIP Document Processing: `kip_up`

> *The general idea of `kip_up()` is to improve security.  As with the physical
> exercise, it looks simple to an outside observer, but is a lot of hard work
> to do well.  That is the point, of course, making crypto accessible.*

The purpose of Kip Up is to improve the security of a KIP Document.

**KIP Up on security.**  This makes a pass over the document
to make kip_up invocations on `Chunks` of `Mud` and then
(re)produce signatures.  This is done in a few phases,
each of which reports success and failure to the
interactor.  Where possible, existing signatures by others
will be retained.  The same applies to existing encryption
with others' keys to unchanged data.

This is an idempotent call: Repeated calls on the same data
have no effect.  When something has been changed however,
you will be able to give that another try.  This is an invaluable
property; it means that you can call it again and again, after
any change if you want, and you will not upset the KIP Document.

Effectively, `kip_up()` and `kip_down()` are reverse operations,
just like the C library calls do for individual Chunks.

Internally, this work is split into phases:

 1. **Gather keys:** Make a pass over the KIP Document to gather keys that
    are available, and either on-key or off-key at each point.

    This is an idempotent call: Repeated calls on the same data
    have no effect.  When something has been changed however,
    you will be able to give that another try.

 2. **Muddle content:** Make a pass encrypting data in this KIP Document.
    This applies to [`Mud`](chunks/Mud), which may then result in
    [`Lit`](chunks/Lit), [`Ref`](chunks/Ref) or [`Meta`](chunks/Meta)
    user data to be concealed.  Data muddling is orthogonal to
    signature checks, because signatures are made on the encrypted format.
                   
    This is an idempotent call: Repeated calls on the same data
    have no effect.  When something has been changed however,
    you will be able to give that another try.

 3. **Make signatures:** Make a pass making signatures in this KIP Document.
    This involves detecting introduction of each internal Key by a
    [`KeyIntro`](chunks/KeyIntro) and resetting its hash, then updating its Sum
    with [`Content`](chunks/Content) material inasfar as the various internal
    Keys are on-key.  Note that nothing is done with encryption in this phase,
    as that would alter the Sum.  Encryption is assumed to have been applied
    before this run.
                    
    Signatures mention a key number, which is not unique.  There may be document
    merges, leading to overlapping numbers.  This is not a problem; a successful
    signature can be traced back with full certainty to the exact internal Key
    that matched, and any associated identity of that Key.  For this reason, it
    is safe to consider a signature successful if only one Key for a given
    key number matches.
                   
    This is an idempotent call: Repeated calls on the same data have no effect.
    When something has been changed however, you will be able to give that
    another try.
