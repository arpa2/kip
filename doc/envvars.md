# KIP environment

> *We use a lot of envvars in KIP testing...*

**Note:** When we use the term `REALM` it always means `DOMAIN` in an ARPA2 context.  More formally however, security speaks of realms and might assign a more general meaning to the term.  We follow that habit in our naming.

## SASL

  * `SASL_PATH` comes [from Cyrus](https://www.cyrusimap.org/sasl/sasl/installation.html#unix-package-installation) and is used to locate mechanism plugins
  * `SASL_CONF_PATH` comes [from Cyrus](https://www.cyrusimap.org/sasl/sasl/reference/manpages/library/sasl_getconfpath_t.html) and is used to locate SASL configuration files

## Quick-SASL

  * `QUICKSASL_PASSPHRASE` is used for headless testing of login dialogs that require a passphrase.  Without this variable, a popup ensues to ask for one.
  * `KIPSERVICE_CLIENT_REALM` and
  * `KIPSERVICE_CLIENTUSER_LOGIN` and
  * `KIPSERVICE_CLIENTUSER_ACL` are defined by KIP.

## KIP

  * `KIP_REALM` decides which domain's KIP daemon to connect to.  This falls back to the NIS domain via `getdomainname()` which may be unset or improper.
  * `KIP_VARDIR` is used on the KIP daemon to locate keys and configuration data
    **TODO:** Currently defaults to `/var/lib/kip`, make that `/var/lib/arpa2/kip`
  * `KIP_KEYTAB` is used on the KIP daemon to locate the keytab with master keys and defaults to the system's default keytab
    **TODO:** We need this setting to run tests on `localhost`, but should not use it anywhere else.  Maybe we should aim to exclude it from all our code.
  * `KIPSERVICE_CLIENT_REALM` is the realm, and
  * `KIPSERVICE_CLIENTUSER_LOGIN` is the authentication user, and
  * `KIPSERVICE_CLIENTUSER_ACL` is the authorisation user, together used to login to the KIP daemon from the KIP client and XoverSASL.
  * `UNBOUND_HOSTS` is used in the KIP Service client to setup an alternative for `/etc/hosts`.
  * `UNBOUND_CONFIG` is used in the KIP Service client to setup an Unbound configuration file.
  * `UNBOUND_FORWARD` is used in the KIP Service client to specify a forwarding DNS resolver.

## HAAN

  * `HAAN_REALM` is used on the server to determine the hosted domain name; this is reachable for a SASL plugin and can be locally overruled; it is a lazy alternative to a conffile with this one setting

## Unbound

  * `UNBOUND_HOSTS` are defined by KIP.
