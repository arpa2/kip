# KIP: Keyful Identity Protocol

> *KIP solves the problem of needing keys for
> document encryption, but not having one for all
> recipients.  The technology is reminiscent of
> Kerberos, without requiring its infrastructure.*

Whenever documents need to be encrypted, keys are used.  The management of keys is always the difficult part of cryptography, but KIP can make it really simple.  Instead of expecting to find key material before sending content to a recipient, KIP allows recipients to retrieve keys when they are faced with encryption.

KIP reduces the problem of key sharing to authentication.  Instead of enumerating target keys, a random key is generated and annotated with an ACL to decide which authenticated user may have it.

## Decryption as a Service

KIP enforces an ACL to the visibility of data through online interaction with a key derivation service (but it does not require a KDC) to obtain a document's decryption key(s).  Such decryption keys do not have to be stored, although the may be cached.  This can be useful in terms of privacy, especially in mobile use cases.

The ACL is of course encrypted to this KIP service, so it cannot be seen by anyone.  In fact, there may be a reference in the KIP service backbone, such as an LDAP object reference, to allow a dynamic ACL.  The KIP service will hold the requester's authenticated identity against the ACL and decide whether or not to release a requested key.  It is possible for one party to receive multiple keys, even for different parts of one document.

Even the decryption keys are contained in the document, again protected so only the KIP service can decrypt them.  Based on the ACL, a decision is made which keys will be shared and which will not.

There are two ways of convincing the keying service of one's identity, namely with Kerberos or with KXOVER.  The latter may be based on locally spread certificates that are acknowledged through DANE.

## General Protocol

The general protocol followed by the KIP service is to make keys available to authenticated users that are acknowledged by each key's ACL.

The flow always runs over TLS and starts with client authentication.

### Relation to Kerberos

It is possible to run KIP service without any Kerberos infrastructure; specifically, a KDC is not required.  It is however possible to use Kerberos as part of the SASL authentication phase, and that gives the usual benefit of a single sign-on service.  In addition, our KXOVER technology can be added for impromptu realm crossover.

Parts of documents are encrypted by a key recognised by a `keyno` value.  The encryption technology is taken from Kerberos, which specialises in this kind of scheme.  The dependency however, does not progress beyond shared ciphers and a few shared data structures.  Specifically, there is no need to run a KDC to be able to use KIP.

## Authentication with SASL

To support authentication in the most adaptive manner possible, the KIP service uses SASL for authentication.  The welcome message involves not just the protocol name and version, but also the required list of SASL mechanisms that it can handle.  After this, a SASL exchange is run to completion.  Once this is satisfyingly finished, further KIP protocol messages can be exchanged.

Once the TLS connection is established, the client starts the protocol with its requests, and the service responds while deciding on the most appropriate protocol version and supported flags:

```
KIP-INIT-REQ ::= [PRIVATE 3] SEQUENCE {
   protocol  [0] IA5String,   -- set to "KIP"
   version   [1] INTEGER,     -- currently 0
   software  [2] IA5String OPTIONAL,
   flags     [3] SEQUENCE OF OBJECT IDENTIFIER,
   crealm    [4] KerberosString,
   srealm    [5] KerberosString
}

KIP-INIT-REP ::= [PRIVATE 4] SEQUENCE {
   protocol  [0] IA5String,  -- set to "KIP"
   version   [1] INTEGER,    -- currently 0
   software  [2] IA5String OPTIONAL,
   flags     [3] SEQUENCE OF OBJECT IDENTIFIER,
   saslmech  [4] SEQUENCE OF IA5string
}
```

This exchange is initiated by the client as soon as the TLS connection is established.  Then follows a sequence of tic-toc messages holding the SASL content.  Client and service messages are:

```
KIP-SASL-REQ ::= [PRIVATE 5] SEQUENCE {
   mech  [0] IA5String OPTIONAL,
   c2s   [1] OCTET STRING OPTIONAL
}

KIP-SASL-REP ::= [PRIVATE 6] SEQUENCE {
   s2c    [0] OCTET STRING OPTIONAL,
   extra  [1] OCTET STRING OPTIONAL
}
```

The `mech` holds the string for the SASL mechanism, and marks a fresh SASL authentication attempt.  It must be used in the first `KIP-SASL-REQ` and must not be used in any further such messages that are intended to be part of the same SASL exchange.  A later `KIP-SASL-REQ` with a `mech` field would start a fresh SASL exchange, which the client can use if it finds that the original mechanism selection was problematic.

The `c2s` and `s2c` messages form the core of the SASL negotiation.  In line with the requirements, there is support for arbitrary binary content (including NUL characters, and not constrained by formats such as UTF-8) and there is a difference between empty and absent content.  The interpretation of all this is specific to the mechanism used.  The `extra` field is an optional SASL feature that is rarely used, but mechanisms may use it to carry additional data as part of a successful end to the authentication.

When an authorization identity is provided by the client and accepted by the authentication mechanism, then it shall replace the authentication identity.  All identities should look like `user@domain` but when no `domain` part is given, the KIP service may substitute its realm, taken from the `srealm` in the last `KIP-INIT-REQ` message.

## KIP Protocol Messages

The KIP exchange consists of sending a pair of an ACL with keys, protected to the KIP service, and receiving back a set of encryption keys with an `etype` and `keyno` to identify them for the containing document.  The response may also identify the originator of the message.

No content is exchanged as part of the KIP protocol, so there is no assault on privacy if the encrypted document is not revealed through another channel.

It is possible to make more than one KIP exchange in the same TLS connection.  Once the TLS connection has closed, the authentication information has been lost, and no further use is possible.

A request for KIP decryption service is referred to as a KUNZIP request for a group (of documents), and the data structures for request `KIP-DECR-REQ` and response `KIP-DECR-REP` are:

```
KIP-DECR-REQ ::= [PRIVATE 12] KI-Shield

KIP-DECR-REP ::= [PRIVATE 13] KI-Groups

KI-Groups ::= SEQUENCE {
   cryptor  [0] ARPA2Identity OPTIONAL,
                -- removed when not validated;
                -- KIP service should set this field
                -- to authenticated user identity
                -- (which implicitly whitelists him)
   keytime  [1] KerberosTime OPTIONAL,
                -- not accepted from the client;
                -- KIP service may set this field
                -- to the then-current time
   groups   [2] SEQUENCE OF KI-Group
}

KI-Group ::= SEQUENCE {
   keyno   [0] KeyVersionNumber,
               -- "identifies" this KI-Group, though
               -- clashes are possible and require
               -- multiplicity support to choose
               -- from with crypto checksums
   enckey  [1] EncryptionKey OPTIONAL,
               -- may be absent outside time window
   from    [2] KerberosTime OPTIONAL,
               -- may be added or removed subject to
               -- local policy by the KIP service
   till    [3] KerberosTime OPTIONAL
               -- may be added or removed subject to
               -- local policy by the KIP service
}

KI-Shield ::= EncryptedData
              -- holds KI-Groups
              -- encrypted to the KIP service
              -- identified as kip/host.name@REALM

KI-Recipe ::= SEQUENCE OF KI-Ingredient

KI-Ingredient ::= SEQUENCE {
   access  [0] KI-AccessControl,
               -- not returned as part of KI-UNZIP
               -- for reasons of privacy and security
   metoo   [1] BOOLEAN DEFAULT FALSE,
               -- check the ACL to include the cryptor;
               -- add if not provided for
   group   [2] KI-Group
               -- info to be shared after KI-UNZIP
}

KI-AccessControl ::= CHOICE
   inline    [0] KI-AccessControl-Inline,
   localref  [1] OCTET STRING,
   ...
}

KI-AccessControl-Inline ::= SEQUENCE {
   blacklist  [0] SEQUENCE OF ARPA2Selector,
                  -- may be empty
   whitelist  [1] SEQUENCE OF ARPA2Selector,
                  -- may be empty
   graylist   [2] SEQUENCE OF ARPA2Selector
                  -- may be empty
}

ARPA2Identity ::= OCTET STRING
                  -- forms: "user" or "user@realm"
                  -- realm default from context, or
                  -- otherwise KIP service realm,
                  -- realm is lowercase domain name

ARPA2Selector ::= OCTET STRING
                  -- like UserIdentity, but "user" may
                  -- be absent before @ to match all,
                  -- or "realm" may start with a "."
                  -- to wildcard lower domains; there
                  -- may be additional local semantics
                  -- for "user" patterns;
                  -- do not assume a default realm
```

**TODO:** Consider inclusion of OpenPGP and PKIX keys.  They would derive a session key and subsequently be assigned a `keyno`.  Algorithm choices might interfere, however.

Note that the response is not encrypted, as it is only sent as part of a TLS connection whose sender has been authenticated.  Effectively, the KIP service comes down to decrypting the `KI-Groups` message for the parties on the access control list.

The lists in the `KI-Inline-ACL` are always present, though empty lists are possible.  Gray listing may be implemented, but if may be ignored or rounded off to white or black under a local policy.  The idea of gray listing is to explicitly list members that need to go through extra validation before their request is accepted.  When a member is not specified in any list, then the member is treated as if he is black-listed.  This is how ignoring of gray-listed entries can be safe.

Client software should be cautious about not forgetting to include the `cryptor` in the `whitelist`.  The KIP service has no facilities to interact with users; on the other hand, the KIP service evaluates ACLs and can see if the `cryptor` is part of, say, a group.  The `metoo` flag can be sent to assure the the `cryptor` is somehow supported.  Inclusion as a group member is semantically different from listing the `cryptor` in the ACL, notably when a member is later removed from the group.  **TODO:** Really do it this way?

The `localref` variant of finding an ACL is quite different in operational terms.  By referencing a dynamic source under local policy, it is possible to remove members from an ACL.  The reference could for example be an LDAP DN, and the object found could then be an `AccessControlledObject` as defined by ARPA2.  Members who once obtained a key will continue to be able to decrypt documents using it, but as long as new documents use fresh keys they will not be able to decrypt those.

As a general principle, the same member should not occur on multiple lists, to avoid local or uncertain interpretation about one list trumping another.  The lists can however contain patterns, and for those it is important to specify that more concrete information trumps over less specific.

Patterns are really straightforward; an empty string serves as a wild card.  In terms of realms and domains, an initial dot indicates that anything underneath is matched (TODO: as in arpa2id).  When the user and the start of the realm are both wildcarded, (TODO: as in arpa2id).

The KIP encryption service is separate, and it must not be mixed in the same session.  It may however be implemented on the same port, thank to the use of another service identity.  Again, we have a request `KIP-ENCR-REQ` and response `KIP-ENCR-REP` with a simple format:

```
KIP-ENCR-REQ ::= [PRIVATE 10] KI-Recipe

KIP-ENCR-REP ::= [PRIVATE 11] KI-Shield
                 -- holds KI-Access-Groups
                 -- encrypted to the KIP service
                 -- identified as kip/host.name@REALM
```

Note how the `[PRIVATE xx]` numbering is such that the messages form a fixed sequence `10`, `11`, `12`, `13`, the latter of which is numbered to reflect its dangerous state.

### Encoding the Protocol

The most common encoding for ASN.1 is DER.  It is compact, deals well with binary content and represents the subtleties of the protocol rather directly in a condensed binary format.

Another option might be the JSON encoding rules or JER, as specified in ITU recommendation X.697 and much more in line with popular use.  The verbose nature of these messages does not matter for the modest use of key exchange without content; the binary format of `EncryptedData` messages could be encoded, but they could hold anything that the KIP service likes, be it JER or DER.

It would even be possible to run JSON over en protocol such as CoAP or HTTP.  The addition of TLS is not special in these protocols, and SASL authentication could be incorporated using HTTP SASL.  Realm crossover for SASL can be achieved with Diameter SASL, regardless of the nature of the protocol holding the SASL piping.  The initial greeting messages could be incorporated into various HTTP headers.  Effectively, the only messages remaining would be the `KIP` and `KUNZIP` messages, each of which could be mapped to an HTTP method, possibly even with these precise names.

## Distributing Keys

Various methods can be used to distribute the keys held in the `KIP-Shield`:

  * We can store a `KIP-Shield` in LDAP alongside a certificate to indicate that it may be decrypted, and by whom.
  * We can include the `KIP-Shield` in a document or document stream, and encrypt (parts of) the remainder.  Applications my include RSync, files in tar balls and Content-Encoding for email, SIP or HTTP.
  * Documents can even allow the selection of parts of the content for various readers, to change the document viewing permissions.
  * Various files in a tar ball could share the same `KI-Shield`.
  * It is possible to store keys centrally, perhaps for a group or email list, and simply use a reference or knowledge of the sender and/or recipient to find it back.  The document itself would not hold the `KIP-Shield` anymore.

**TODO:** It could be useful to store keys in the Kerberos credentials store, where it will be cached for hours, flushed automatically and protected like all the other credentials.  We may need a Ticket to do that.  Note that each `KI-Groups` would need its own Ticket, so the listings might get tedious.  Alternatively, we might store a list of `KI-Groups` in a Ticket's AuthorizationData?  *This seems to be a misfit.*

## Document Format

The following document format is rather permissive, allowing parts to be encrypted independently, and concatenated to a document.  Parts that cannot be decrypted are silently dropped, allowing for different documents, and even different partial documents, to be shown to different recipients!

Each document part is encrypted to one `keyno` but this may be setup in a `keymap` that maps one or more already-known `keyno` values to a newly added `keyno` for a document.  This allows multiple `keyno` to view the same document part.

Documents *may* be merged, which would mean that their `keyno` name spaces could clash.  In those situations, the decryption might fail even if we have a key with the requested `keyno` and `etype` -- we should be willing to try another key with the same coordinates at that time.  Again, failure to decrypt leads to skipping, rather than calling it an error.  Worst case, you end up with an empty document.

It is probably useful to reserve `keyno` ranges for various purposes:

  * 0-19999 for keys facilitated by the KIP service, and intended for local use within one document or just a small series; manual picks from 0-3615; random picks from 3616-19999 (a 14-bit range)
  * 20000-32767 for keys related to a day within a year: MMDDS where S is a serial number digit or picked at random; there is no requirement to validate date consistency
  * 32768-98303 (a 16-bit range) for random key numbers allocated for long term use; there will be some clashes
  * 98304-99999 for internal document use in the `keymap`, not facilitated by the KIP service, where they are used as the newly added `keyno` values
  * 100000-4294967295 for explicitly allocated key identities under a realm as serviced by KIP, intended for long-term use practically without clashes, though looping around may not be helpful

The `realm` can be specified to help Kerberos find the KIP service, which may involve realm crossover.  The normal method of detecting the host name is to do an SRV query for `_kip._tcp`.  When no `realm` is specified, the customary assumption would be that the document must be located under the home realm; it may be interesting in terms of privacy to avoid mentioning it and rely on context to provide it.

```
KI-Document ::= SEQUENCE {
   hint    [0] UTF8String OPTIONAL,
               -- a hint to a human who lacks KIP
   realm   [1] KerberosString OPTIONAL,
               -- default realm for the UserIdentity;
               -- when absent, use receiver realm when
               -- available, otherwise sender realm
   keys    [2] SEQUENCE OF KIP-Cipher,
               -- entries to reveal with KIP service;
               -- introduces document-local keyno;
               -- multiple for partial key management
   keymap  [3] SEQUENCE of EncryptedData,
               -- each locally derives extra KI-Map
               -- encrypted to an assumed-known keyno
               -- (as a result of KI-Document order)
               -- in support of key indirections; any
               -- unavailable keyno is skipped silently
   type    [4] KI-Type OPTIONAL,
   data    [5] SEQUENCE OF KI-Block,
   ...
   sig    [30] SEQUENCE OF EncryptedData
               -- each holding the full binary output,
               -- without ASN.1 wrapper, as produced
               -- by the hash algorithm underlying the
               -- signer's etype; computed over the
               -- fields type, data and extensions;
               -- note that hint, realm, keys, keymap
               -- and sig are not incorporated
}

KI-Map ::= CHOICE {
   keyor   [0] KI-Group,
               -- extends the key set available for
               -- decryption of upcoming sections;
               -- the key may be new or it may overlap
               -- with previously seen keys, which
               -- implement an OR functionality
   keyand  [1] EncryptedData
               -- locally derives an extra KI-Map
               -- encrypted to an assumed-known kvno
               -- (as a result of KI-Document order_
               -- in support of key indirections; any
               -- unavailable keyno is skipped silently
               -- and the intention of nested keying is
               -- to allow conjugations on keys, to
               -- complement the use of keymap as an
               -- OR mechanism on keys; together they
               -- express all positive logic formulae
}

KI-Type ::= CHOICE {
   mime   [0] IA5String,
   oid    [1] OBJECT IDENTIFIER,
   uuid   [2] OCTET STRING SIZE(16),
   uri    [3] IA5String,
   local  [4] OCTET STRING,
   ...
}

KI-Block ::= CHOICE {
   plain  [0] OCTET STRING,
              -- a readable part of the document
   crypt  [1] EncryptedData
              -- results in an OCTET STRING
              -- that is part of the document
              -- which concatenates of KI-Block
              -- inasfar as it can decrypt; any
              -- unavailable keyno is skipped silently
}
```

An interesting use of this format, but certainly not the only possible use, is to encrypt separately for different authors.  They may even do this while editing the document, and could even produce encrypted content.  This would support others to review the document while it is being written, but only inasfar as they mey see it.

There are also time constraints in the format, through `from` and `till` fields.  These will be enforced by the KIP service inasfar as they pass through it, but these can only be voluntarily implemented for `keymap` entries of a `KI-Document` structure, unless enforced by law or contract.  The same holds for expired keys that once passed through the KIP service.  **TODO:** Is it a good idea to have time-dependent behaviour?  It can be useful from a privacy perspective, but also makes documents highly unreliable and it may lead to polling of the KIP service.

## Diameter SASL Protection

We are considering to use Diameter as a backend that can connect to a user's home realm.  The result would be that a user can crossover to a service realm and engage in a SASL interaction to authenticate; the service can validate the interaction by looking at the Diameter result.  This is only possible when:

  * Diameter does not grant any rights to the service; this is certain if Diameter only provides a verifiable accept or reject;
  * Other services are protected from being abused as SASL validators with unintended resource access; this can be assured with TLS channel binding (tls-unique is best but even tls-secure-end-point would do);
  * SASL mechanisms cannot be tapped; this can be resolved by wrapping the SASL data in KIP, to be decrypted in the Diameter backend.

The latter is not how SASL mechanisms are commonly designed.  Exceptions exist (notably Kerberos5), but even a rather strong mechanism like SCRAM-*-PLUS advises against this usage pattern.  KIP can present a generic solution, however.  All 

A service simply lists `KIP` among its offered mechanisms, but this means `KIP` wrapped around any of the other mechanisms, your choice.  So, a list holding `KIP` and `SCRAM-SHA256-PLUS` can be interpreted as `SCRAM-SHA256-PLUS` under a `KIP` cloak.  The `-PLUS` reference to channel binding does not indicate the cloak but the TLS connection around the outer SASL exchange.  Similar for GSS-API, of course; `KIP` can be a GS2 mechanism if we also pick an OID.  Without a need for nested mechanism negotiation, it is hardly less efficient to add `KIP`, as long as a key is available.  This can easily be had using the online protocol.

## Privacy during Decryption

The act of decryption has a number of privacy implications, especially when applied cross-realm.  The content itself is not shown, but keys are long and random, so they serve as identities for document access.  We even authenticate to get to the document.  This enables tracing of document developments by the KIP service.

The privacy problems do not escalate to the origin or submitter of a document, because nothing is sent back when the KIP service is not part of a privacy assault.  This means that users should trust the KIP service, but there is no reason for the KIP service to constrain access.  In other words, users can agree on a trusted KIP service, either in one of their home realms or a trusted third party.

It may sound like we are inventing something like PGP's web of trust, but it is much simpler in practice because trust relations may be decided on based on just the realm name.  A good habit might be to discover and use a recipient's KIP service when it is available, and always include the sender's as a fallback.  For known peers, this should not lead to surprises.

KIP clients should just be mindful of the realms trusted for KIP service, and probably need to keep a white list of those, which may be as simple as a list of steady contacts, especially those to whom they are already communicating.  A communication ACL such as designed for ARPA2 is an efficient implementation of this kind of facility.  When such an ACL is used to constrain access to a form of communication, then the free use of a KIP service need not be problematic.  This would mean that the targeted identity/alias can also be of influence on the use of the KIP service.

## Use as a Transfer-Encoding

Various transport protocols allow an encoding to be applied to content while it is in transit.  This is described in meta-data, using an attribute such as a `Content-Transfer-Encoding`.  This can be used to automatically wrap or unwrap documents so they cannot be observed in transit.

A sending MTA may attempt to find a KIP service in a receiving realm, and apply a transit encoding before passing it over a wire, under the assumption that the receiving MTA will be able to remove this.  This will continue to work when the email is forwarded internally, as long as the KIP service is not incorporated into a forwarding service.

Mail lists or groups can use this mechanism too; they would rely on the KIP service for the realm hosting the list or group.

An advantage of a transfer encoding is that the original MIME-type is kept.  In other words, it is possible to filter on the kind of application that would run after unpacking a KIP document.

Email is just one possible application for KIP.  A highly interesting use case might be SIP, where the SDP attachment might be subjected while in transit, thus providing a mechanism for private communication.

