# KIP on a local machine

> *KIP is meant for use across the Internet, but it may also be
> interesting as a local service to the users of a computers.*

On the Internet, we always assume a fair amount of infrastructure,
but a few uses make it interesting to assume less:

  * **peer-to-peer** networks often use keys to identify a host,
    but make no distinction between local users.

  * **time-sharing** systems are no longer the norm, but the
    practice still lives in super computing and dedicated clusters.

  * **role differentiation** could restrict access to files
    based on a user's current role.  Work and play could be
    separately kept, even when stored in the same user account.

Given authenticated local users, KIP could help each user with
their own encryption facilities.


## Mechanisms of Authentication

There are a few possible authentication mechanisms that KIP could use
in such local settings.  One could be a private socket that only one
user can use to reach their local KIP instance.  Alternatively, one
KIP daemon might service users (and groups) over an individual
UNIX domain socket.

Similar but different is SysV InterProcess Communication (IPC),
with controllable access to message queues.  Through `msgctl(2)` it is
possible to see the sending PID, but there is no standard mechanism
to derive the corresponding user or group.  Access control is the same
as for files, including UNIX domain sockets.

Users might simply enter a password and store it under KIP, to be
able to dig it up later.  It is possible to do this as part of the
session setup during login, perhaps with PAM.  This would bring a
master secret that is not derived from the login password, and that
continues to be usable when that password changes.  The downside
is that the key material is available to the system administrator
unless it is mixed with user-entered entropy (such as their password).


## The link between KIP and KXOVER

KIP is useful for the creation of keys, and mapping them as well
as preparing them for sending.  KIP however, requires a basic
key to be shared between the sender and recipient.  This is trivial
when these parties are the same local user.  For remotely connected
users, such as on a peer-to-peer network, it is possible to use
KXOVER with a realm-crossing method of authentication to share an
initial key, after which KIP can add more refined key handling.

KXOVER is then a way of establishing a master secret, and KIP is
a way of managing key material and using it for encryption.


## Local use of KIP

KIP can be used to `kip up` or `kip down` any file to conceal
its contents.  When multiple keys are available locally, it
might be possible to derive a key from file access rights.
Note that symmetric techniques cannot distinguish readers
from writers, but integrity checks can impose additional
requirements.

It is quite possible to have a file system based on the
KIP mechanism.  A change of access rights might lead to a
change in key mapping, without a need to re-encrypt the
entire file, as that would be based on a fresh key for
the file.

It would be feasible to have a file look differently to
different people.  This would however be more useful if
content could be inserted anywhere inside a file,
rather than merely overwritten.  This requires higher-level
concepts such as a cursor to distinguish the editing
positions of various users, however, and that goes
beyond file system service.

