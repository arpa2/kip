# RealmMania

> *SASL has client and service realms, so does Diameter
> and then there are the ideas of the client, their
> identity provider and the foreign server that sits
> between them... what is the sense here?*

A longer version of these points is
[Support Levels for Realm Crossover](http://internetwide.org/blog/2020/09/16/id-16-xover-levels.html).
This article defines a growing path, from the current
practice at level 0, through a pretty nice start with
realm crossover at level 1/2, to full-blown dynamic
crossover at level 1.

**Conceptual Model:**

  * Realm crossover connects a client with an identity
    under a client realm to a service offering it from
    under a service realm.

  * Client and service realm may be the same, but that
    is a kind of coincidence.  The general idea with
    realm crossover is that they might differ, and so
    we always consider them separately.  Note that
    the "coincidence" is current common practice :-)

  * We always use a realm as part of any identity,
    except perhaps to save a few bytes on storage.
    Conceptually however, identities always have a
    realm.

**Services can Blissfully Ignorant:**

  * Server programs implement protocols and face the
    network, so the less authentication is implemented
    in them the better.  They basically relay SASL
    traffic over a simple standard protocol, routed
    over plain TCP/IP in the form of DER messages.
    The complexity of Diameter is not assumed.

  * The result is a very low bar of entry for servers.
    Not only does this simplify administrator's lives,
    but it also improves consistency to handle SASL
    in a site-central node.  It also simplifies the
    use of authentication in the lighest of systems,
    like a containerised service or a simple device.

**Authentication for Plugin Services:**

  * The current common practice is to have an account
    under a service realm.  This is what we assume by
    default.

  * Authentication is directed to a node on the
    trusted/local network of the service.  This uses
    a simple TCP/IP connection with the DiaSASL
    shaped into DER messages.

  * The trusted/local node may have been setup with
    fixed relations to realms that it happens to
    serve, basically our plugin service model.  These
    "static" forms of realm crossover are explicitly
    configured to connect between realms.

  * Not all SASL mechanisms are safe to reveal to
    third parties; the usual solution of running it
    over TLS does not work because it is not end-to-end
    and the relaying party would have to be trusted.
    The SXOVER mechanism was designed to remedy that
    where trust is less suitabled, such as when
    crossover between administrative realms.

**Authentication for Power Users:**

  * Full use of realm crossover involves the client
    presenting their own realm.  This is common in
    some models (Kerberos, GSS-API) but not in all.
    SXOVER explicitly introduces a client realm in
    the first SASL token for precisely this purpose.

  * The use of a client realm causes authentication
    to be under that realm, and the authentication
    server sends it back.  When not provided by the
    SASL mechanism that the client chooses, the
    client realm matches the service realm, as setup
    in the server configuration.

  * Servers may configure as a default service realm
    the empty string; the authentication node will
    use only mechanisms fit for realm crossover, like
    SXOVER.

