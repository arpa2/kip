# Using Public Keys in KIP Documents

> *KIP is designed around on symmetric key crypto.  In fact,
> the KIP Service protocol is an authentication-based alternative
> to public keys and their identity mechanisms.  Though simper
> to use, integration with public key crypto is useful.*

We now describe how KIP Documents can use public key crypto.
The design supports pluggable mechanisms, so it is possible to
allow forms for X.509, OpenPGP but also for any other mechanism
desired, including mechanisms based on raw keys.


## Encryption or Signing

Public key crypto can do two things: Encryption and Signing.

  * For encryption, the customary mechanism is to derive a
    symmetric session key, and use that for bulk data work.
    This easily aligns with KIP Document processing, as long
    as we insert a blob that can be used by public-key crypto
    to derive the symmetric key.

  * For signing, the customary mechanism is to start hashing
    at some point, and passing bulk data through it and then
    apply public key crypto to the hash output to prove the
    agreement with the text passed.

  * Encryption with signing keys is often founded in key
    exchange, for which Diffie-Hellman is the common choice.
    This requires an exchange of keys between parties, where
    agreeing parties sign for their side of the agreement.


## Session Encryption Keys

The introduction of a new encryption key from a public-key
operation is trivial.  The slight challenge imposed by KIP
is that the key should be usable for the KIP mechanisms,
which are Kerberos mechanisms.

The `PubKey` element is designed for this purpose.  It
consists of a CBOR array with the following elements:

  * The tag `TAG0_PUBKEY` to mark the array for this use
  * The tag for the public-key crypto system
  * The key number to use in Kerberos
  * The encryption type to use in Kerberos
  * The blob that feeds the public-key system

The key number and encryption type are generally used by
KIP to find keys.  Various other fields use these values.
The encryption type implies a "key-generation seed length"
needed from the public-key crypto system, which can then
be used to
[construct the key](https://tools.ietf.org/html/rfc3961)
with the encryption scheme's "random-to-key" function.


## Public Signatures

Signing with a public key is independent from encryption.
In fact, some mechanisms can do one but not the other.
Signatures are useful and require no password entry or
even a local key; what is required is an infrastructure
to manage the trust in a key.

The `PubSig` element is designed to create signatures
with any such mechanism, but it requires the prior start
of a hashing (or digest) algorithm, for which a previous
`PubDig` element serves as a marker.  Think of them as
a pair `PubDig`/`PubSig`, which sounds a bit like DigSig,
short for digital signature.

But there would be no need to use `PubDig` and `PubSig`
strictly in pairs; multiple `PubSig` may mark points for
one `PubDig` value.  In fact, a `PubSig` may just as
well be made for any other key introduced by KIP, and
that means that `TabKey`, `KeyMud`, `SvcKeyMud` and
even `PubKey` could be used.  The algorithms do not
check for the combination of tags, but whether a key
is available in the document position where `PubSig`
is placed.

The `PubDig` element consists of:

  * The tag `TAG0_PUBDIG` to mark the start of digesting
  * The key number to match against
  * The encryption type for Kerberos, implying a hash

There is no support for AEAD in this element.  The
general structure of a KIP Document makes it easy to
inline anything desirable, even signatures referring
back to elements before the creation of the key.

The `PubSig` element consists of:

  * The tag `TAG0_PUBSIG` to mark a signed digest
  * The tag for the public-key crypto system
  * The key number to match against
  * The encryption type for Kerberos
  * The blob understood as a pub-key signature

One of the preceding `PubDig` keys with the same key number
and encryption type will be used to feed `PubSig` and upon
verification, only one of those needs to verify.  A proper
document controls the key numbers to avoid a clash and any
resulting uncertainty on the proven claim.


## Signed Key Agreement

**TODO:** Under design.  Likely to support multiple parties.


## Integration of Plugins

**TODO:** Balance dynamics against security.

The second tag is used to locate a public-key crypto system
as a KIP plugin.  In Python, this is possible through
[Dynamic Discovery of Plugins](https://setuptools.readthedocs.io/en/latest/setuptools.html#dynamic-discovery-of-services-and-plugins)
where we use the name `arpa2.kip.pubkey` as an entry name.
We will need some user interaction to permit the use of new
public-key systems, because these are likely to ask you
for a password.  Placing them under "our" name space
package `arpa2.kip.pubkey` may not offer much protection,
so we might skip that.  Perhaps we can use a hash of the
software and maintain a register.


## Warning about Quantum Computing

Unlike Kerberos, current-day public key crypto systems
are not protected from Quantum Computer attacks.  Please
be very careful when using such systems to protect your
information with a longevity beyond, say, 2030.

It is vitally important to understand that what we store
with encryption today may be deciphered by the time that
Quantum Computers hit the playing field.  They will
[break our backs and bones](http://internetwide.org/blog/2018/02/10/quantum-crypto-1.html)
and algorithms and protocols need to be replaced
[to get over Quantum Computers](http://internetwide.org/blog/2019/02/11/quantum-crypto-2.html).

KIP, founded on symmetric keys, can be a useful force
in combating this problem.  The use of public-key crypto
can weaken this, though algorithms are beginning to
(re)appear that are safe from these predictable future
assaults.

