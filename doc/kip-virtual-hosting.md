# KIP and Service Virtual Hosting

> *KIP can provide virtually hosted services, making use of its own
> clever key mapping techniques to do it safely-yet-flexibly.*

Both KIP and HAAN Service are services that can be provided to
various virtual hosts, each with their own keys.  The keys are
encrypted with a concealed master key, and it future extensions
are possible to remap those keys to other master keys.


## Structures

There is a KIP variable directory, which may be set in environment
variable `KIP_VARDIR`.  Underneath, a subdirectory for services
`kip` and `haan` are called `kip-vhost` and `haan-vhost`; the pattern
may be extended at will.

In each of these service-specific directories, there can be any
number of files named exactly as the service name (or realm) in
lowercase, and its contents are a key mud (as produced by the
`kipkey_tomap()` function).  These very small files may fit in
an inode in file systems that support inline data (like ext4 with
the right setting).

Another environment variable `KIP_KEYTAB` points to a Kerberos5
`.keytab` file, in which each service must be represented by a
service master key `service/MASTER@.` where `service` is the
service name in lowercase, for now `kip` or `haan`.  Note the
domain which reflects the large scope being covered; it is not
welcome in normal online activity.


## Test Data

There is some test data present; in `test/bin/unicorn.keytab` two
fixed keys named `haan/MASTER@.` and `kip/MASTER@.` are available.
You should not use this, of course, it is there to have predictable
credentials which is not your goal.

The test realm `unicorn.demo.arpa2.org` is present for both the
services, as `test/vardir/kip-vhost` and `test/vardir/haan-vhost`.
These are wrapped by the master keys in `test/bin/unicorn.keytab`.


## Management

To setup a keytab from scratch, use the krb5 utility `ktutil`.
These statements should do the kind of thing you are looking for:

```
ktutil: addent -key -p kip/MASTER@. -k 2020 -e aes256-cts-hmac-sha384-192
Key for kip/MASTER@. (hex): <your secret here>

ktutil: wkt /var/lib/kip/kip.keytab

ktutil: list -e

ktutil: quit
```

The path to the keytab file, written with `wkt`, should be setup
in `KIP_KEYTAB` so it can be found.

There is no need to memorise the hex secret.  You can generate it with
a statement such as

```
shell$ cat /dev/urandom | hexdump | head | openssl sha256
```

To add a serivce, you need to have a directory under the KIP variable
directory.  For the two standard services,

```
shell# mkdir -p /var/lib/kip/kip-vhost
shell# mkdir -p /var/lib/kip/haan-vhost
```

Once this is setup, you can add virtual hosts, for instance

```
shell# kipvhs add kip  unicorn.demo.arpa2.org
shell# kipvhs add haan unicorn.demo.arpa2.org
```

This creates similar structures as in the `test/vardir` directory,
but installed in your system and with fresh keys.  It barely makes
sense, because these are mere testing domains that you cannot direct
to your servers; so use your own virtual host name as the last
commandline argument.  And repeat if you need multiple.

There is no support for removal (yet), but you can manually
remove the file, for instance the two files at
`/var/lib/kip/*-vhost/unicorn.demo.arpa2.org` may be removed
in a single command.  Be careful; once gone, all the encrypted
data is gone.


## Security

The `.keytab` file is your most precious data to protect.  It holds
service master keys without any form of protection.  For this reason,
you should consider storing it on a protected device.  Future options
may include PKCS \#11 support and perhaps other mechanisms.

The virtual host key map directories are far less of a concern.
Their contents are encrypted with the respective service master keys,
and not of direct use once picked up.

The intention of this construct is to provide future support for
mechanisms that load secrets from environment variables, or perhaps
from a pre-`chroot` context before running a public-facing service
that has access to the key map directory for a service.


## Resilience

You should backup the `.keytab` files, presumably in encrypted form,
as well as the virtual host mapping directories.  Future versions
may incorporate key splitting options to allow distrubution of the
control over service master keys.

