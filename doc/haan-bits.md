# Bitsize Calculations for Helm Identities

> *How many bits of entropy make sense for HAAN?  Can we add redundant bits
> to compensate for typo's?  Can we detect errors during entry?*

There are basically two levels of trust for HAAN -- either at a Quantum Proof
level or below.

**Note:** These ideas may be running ahead on the code.


## Impact of Quantum Computers

The entropy for the key material should be twice as much for symmetric
algorithms to make the Quantum Proof.  What used to be 128-bit security
would become 256-bit security.  This is completely senseles (and a lot
of extra typing, and spreading a false sense of security) to do when the
secrets travel over 128-bit secure channels and/or channels that are
not Quantum Proof.

So we make two levels of entropy requirement:

  * Quantum Proof, 256-bit level and secrets travel over 256-bit
    secure channels that use public-key algorithms that are also
    Quantum Proof.

  * Not Quantum Proof, 128-bit level and secrets travel over
    128-bit secure channels that may use public-key algorithms
    that need not be Quantum Proof.

The selection is automatically derived from the TLS stack in use during
the generation of the secret.  It will be imposed on the TLS stack when
the user name is presented.

Mechanisms must never be used that present the password in plain text.
Mechanisms permitted at the Quantum Proof level must be 256-bit proof.


## Provisionary Checksums

It is easy enough to compute checksums for all identities, and present
them to the user for checking.  There might be two forms, one for just
the username and another for the realm.  In fact, the latter might be
a combination of the username and realm, so even more opportunities to
detect typo's is found.  The checksum would be printed along with the
public string, of course, so it is available during use.  For Helm
Identities that are not HAAN Identities the checksum is not a problem
and may in fact be incorporated elsewhere.

A nice size is 6 digits.  The first 3 digits may represent the username
alone, the last 3 digits could be the username and realm combined.
Even if we do not document it, chances are that this is interpreted as
a hunch to first check the indicated part of the name, especially when
a space is printed in between.  For Quantum Proof security we need
more bits, and might choose to use 3 groups of 3 digits of checksum;
3 digits for the first 32 characters of the username, 3 digits for the
full username, and 3 digits for the entire username and realm.

The check can be a simple MD5 hash, whose initial 32 bits are seen
as a big endian unsigned integer and whose module-1000 checksum is
displayed in 3 decimal digits.  This wraps 4 million times before a
shorter sequence occurs, so the loss of accuracy is negligable.

When the realm contains uppercase characters, an error is shown, and
a hint given that it matters for the username part what case is used.
The computation is done on both parts as they are entered, and the
result for uppercase realms is likely to differ from the checksum.
To incorporate both username and realm in the second checksum, a
separator character `@` is inserted.  No UTF-8 is used; the username
is in ASCII and the realm is in Punycode.


## Addition of Redundant Bits

The manual entry of usernames and passwords can be difficult.  Rather
than requiring a check when entering it, which would require the use
of the password before it is actually needed, a bit of redundancy is
added.  This also relays the message quite clearly that the user is
on their own, but provided with the ability to register multiple Helm
Identities.

When a realm is entered wrongly, it can easily be corrected.  Its
cryptographic meaning is to select a key at the HAAN service for the
realm, but there is no wrong in changing that.  All the entropy is
in the public and secret parts anyway.

When a password is entered wrongly, it can be retyped.  Detection
of a wrongly entered name is friendly, so a few bits extra to detect
that may help.  Unfortunately, SASL does not support this kind of
feedback so we cannot do that.

More importantly however, the username is registered to empower the
Helm, and that cannot be changed lightly.  Still, added bits on top
of the required entropy level would allow the change of that number
of bits without degrading the security level, taking into account
that the freedom to position the bits also involves a degree of choice
to compensate for (like error detection/correction codes do).  In a
SASL implementation, it would be possible to supply a thusly mangled
username as an authorisation username (reported back to the Helm) for
a corrected authentication username.

We do not have to design an error correction code, because we are
using random bit strings.  We do need to know the upper limit to the
number of bits that may be corrected with a code, however, where a
theoretic value suffices.  We do have the ability to correct and
validate the result, after all.  The maximum number of bits that
may be altered match the maximum number of bit errors that can be
detected with the additional bits.

Since characters are likely to be mistyped, several bits may be off
in the same position.  It is useful to detect such situations for
each bit separately and limit the maximum changes to a number of
characters that match the error detection limit for individual bits.


## Encoding in Strings

The public/username and secret/password involved in HAAN is supplied
as a string, and that is the form that defines equivalence.
The public part is created as a random bit string, then mapped to a
textual form which is fed into the `kipdata_usermud()` function.
The output is once again a seemingly random bit string, to be mapped
to a textual form (perhaps the same).

The distinction between uppercase and lowercase is complicating for
a user, so may be better to avoid.  We might use a form like
BASE32, with 5 bits per character.  Following SMTP guidelines for
local-parts, we can have 64 characters, so at most 320 bits.  This
can indeed capture a 256-bit security level, with 64 bits left to
waste on marker characters and/or redundancy.

The entry of passwords is difficult, because it is not usually shown.
The use of separators may be helpful to detect typing errors, and
it certainly helps a human to focus.  So we can group 5 characters
at a time, with a separator like a dash, to be typed by the user.
There are no size constraints on passwords in SASL, and they are
not stored in places that matter about the size.

Perhaps more bits can be supplied.  The 26 letters and 8 digits
(dropping 0 and 1 as aliases for O and I) leaves us with 34 values,
but the result is a disappointing 325 bits, barely anything gained
but a lot of bug-inspiring work.  This is not desirable.

There is a remainder of 320-256=64 bits, which can encode the
positions of characters that are wrong, along with the bits that
are off. The character positions use about 5.678 bits each,
followed by 5 bits of corrective data; so 10.678 bits each.  We can
fit that 6 times in the remaining 64 bits.  The slight overflow is
not problematic and can be remedied by thinking that later errors
require less bits to store.  So, this approach allows us to store
as much as 6 errors; we can be quite permissive to the user.  We
have no model for dropped or inserted characters, however.  This
can be remedied by looking at the length (extra data!) and using
the error-correcting bits for pointing at a missing position.
There are many ways of saying "nothing is wrong" by having 0 bit
changes in arbitrary positions; this may be a bit wasteful of
code space.

For 128 bit security, we should then support for up to 3 character
mistakes.  Mapped to BASE32 we see 25.6 characters and positions
therein occupy 4.678 bits.  Correction takes 5 bits, so a total of
9.678 bits are needed per correction.  Times 3 adds 29 bits and
rounding up to a multiple of 5 bits makes 160 bits or 32 characters
in the username and password.  We can insert 6 dashes to separate
groups of 5 characters, ending with a final part of 2 characters.


In summary:

  * The 128-bit security level uses BASE32 to encode 160 bits,
    leading to 32 characters.  These are separated by dashes
    every 5 characters, with a final part of 2 characters; the
    total username and password occupy 38 characters each.
    Up to 3 character mistakes may be corrected.

  * The 256-bit security level has no separators in the
    username, but it uses them to separate every 5 characters
    in the password.  The separators could be shown, even when
    the actual password is not (a nice innovation).  The
    generated code occupies 320 bits of entropy in 64 characters
    in BASE32-encoding.  Up to 6 character mistakes may be
    corrected.

  * Realms can be changed at will when they were falsely
    entered.  User names that are stored with errors can only
    be changed subject to the maximum number of changes.
    Passwords are not stored and can simply be retyped when
    a problem occurs.

  * Adding a character counts as a single character change;
    removing a character counts as a single character change;
    changing a character counts as a single character change;
    after changes, the length must be as intended.

