# SIP with KIP

> *VoIP telephony is mostly setup with SIP.  One of the big
> problems with this system is encrypting the media flow.
> With KIP, this can be simple.*

The proposal described here is very similar to other protocols,
but it does show a very good example of use of KIP.  The Dutch
may like to refer to this profile as *soepkip* to reuse a word
that may soon
[lose its original meaning](https://recepten.blub.net/recept.php?recno=352).


## Encryption assumes IPv6

SIP connects endpoints by defining their media capabilities in an
SDP attachment.  These attachments include not only IP addresses,
ports and media codecs but also a possible encryption key.  When
this key is sent in the plain, it is meaningless, so we need to
encrypt the SDP content.

The same SDP content also holds IP addresses, so these must not
change.  Though this is often safe with IPv4, notably when the
ICE standard is followed, it is still not generally safe.  The
annoying results of one-directional media flow still happen.

IPv6 uses transparent addresses, and is much easier on this kind
of problems.  So, the premise for encryption is to use IPv6.  This
is not a new realisation; the 6bed4 tunnel was designed to remedy
that, and allow all people (especially your telephony peers) to
use SIP clients over IPv6.  The SIPproxy64 program was designed
to accomodate translation between an IPv6 reality and the mundane
habits of old IPv4-only SIP phone devices.


## KIP for SDP: Introducing Keys

Using SIP, we either dial a URI like `sip:bakker@orvelte.nep` or
even `sips:bakker@orvelte.nep`, the latter of which adds far less
security than one would assume, due to the hopping nature of SIP
and the leg-specificity of a TLS wrapper.  End-to-end encryption,
inasfar as possible, is much better.  SDP encryption is one such
technique.

SIP attachments are MIME-typed.  They could be of a different
format, but we might also use KIP as a transfer-encoding.  An
example SDP message is 153 bytes, the best gzip would only reduce
it to 142 bytes -- useless.  A verbose mapping to KIP yielded
337 bytes, mostly adding key mapping and an integrity check; there
is no reason why SDP could not travel in binary form.  SIP can
handle these sizes without much effort.

The trick is now to find the key to use.  For that, we need to
look into DNS.  One place is the domain of a `sip:bakker@orvelte.nep`
URI, so the SRV record named `_kip._tcp.orvelte.nep` in this case.
For a strictly numeric system, we usually have an e.164 scheme
to connect to the classical phone system.  These numbers can be
mapped into ENUM domains, which would provide yet another place
to look for the `_kip._tcp` labels to an SRV record.

Most VoIP providers use the domain name for their service, usually
with a localised form of the e.164 number as the user part.  These
vendors could add support for KIP under their domain like any
user domain might, but users could take control and redirect the
predictable number patterns as they dial them to ENUM paths.


## KIP for SIP: Juggling Keys

KIP is extremely versatile in juggling keys.  In this case, we
would perform the following to match the flexibility of SIP:

  * Generate a random key.  This will serve as a session key
    for the entire Call, regardless of its number of peers
    regardless of any dynamicity in adding or removing peers.

  * For each peer, generate a mapping of this same random key
    through their KIP Service or, lacking that, through our
    own or, lacking even that, using another peer's KIP Service.

  * Map the Call's session key to each peer through the selected
    KIP Service.  The peer is expected to contact that KIP Service
    to recover the Call's session key.

  * Each peer now has the same session key, and can use it to
    gain access to SDP attachments and possibly other content.
    It may be used for encryption when no key is provided in SDP
    or, if a key is present, then it could be used on top of the
    KIP protection for the Call Leg in the direction that it
    describes.

  * When transferring a call, and also when inviting a new peer
    into a conference, there is a need to share the session key
    for the Call.  This can be done by any peer if they all use
    the same KIP key.

  * Calls are strictly separated by their independent keys.
    Peers that never received an INVITE for a Call would not have
    been sent the session key mapped to a key they can decrypt.

KIP relies on authentication with SASL.  This is relatively easy
for a phone, since it already holds a username and account.  The
same data might be used, or:

  * Upon registration, the phone may authenticate with a key.

  * During later KIP Service calls, a cookie or handle gotten
    during that registration is used, possibly while referencing
    the table of registrations from the KIP Service.


## Using SIPproxy64 as Encoder

The handy little tool
[SIPproxy64](http://devel.0cpm.org/sipproxy64/)
translates SIP/SDP calls over IPv4 into ones
over IPv6.  This is tremendously useful to allow calls between
domain-bound SIP URIs to actually get a call going.  It does not
even need to resort to NAT traversal, because the only thing that
it needs to pierce through is a firewall (which is easy).

The SDP attachments hold IPv6 addresses, at least on the public side
of SIPproxy64.  Once these SDP attachments are constructed, they are
in a perfect situation for incorporating KIP and, in the other
direction, removing this "transfer encoding".  The SIP header
`Content-Transfer-Encoding` allows this easily.

TODO: MIME-type?  NOTE: RFC 4289 is weary of zeal in defining new ones.
Maybe `Content-Encoding` is better?  It might be `x-kip` or similar.
This is certainly better than `Content-Type`, which is semantically
oriented.


