# LDAP definitions for KIP, the Keyful Identity Protocol
#
# LDAP attributes to describe:
#  - kipDataMud: inline data encrypted with KIP
#  - kipKeyMud: inline key map
#  - kipServiceKeyMud: inline data for KIP-DECR-REQ
#  - kipX509KeyMud: inline session key encrypted to X.509
#  - kipOpenPGPKeyMud: inline session key encrypted to OpenPGP
#  - kipSignatureMud: inline signature encrypted with KIP
#  - kipServiceSignatureMud: inline signature for KIP-VRFY-REQ
#  - kipX509SignatureMud: inline X.509 signature
#  - kipOpenPGPSignatureMud: inline OpenPGP signature
#  - kipRequiredKeys: sets of required pairs of keynr/encalg
#
# LDAP classes to describe:
#  - kipDocument: an object with content encrypted with KIP
#  - kipCollection: a set of kipDocument and kipCollection

attributetype ( 1.3.6.1.4.1.44469.666.389.4.1
	NAME 'kipDataMud'
	ALIASES ( 'kipMud' )
	DESC 'Keyful Identity Protocol, data mud'
)

attributetype ( 1.3.6.1.4.1.44469.666.389.4.2.1
	NAME 'kipKeyMud'
	DESC 'Keyful Identity Protocol, key mud'
)

attributetype ( 1.3.6.1.4.1.44469.666.389.4.2.2
	NAME 'kipServiceKeyMud'
	DESC 'Keyful Identity Protocol, service key mud'
)

attributetype ( 1.3.6.1.4.1.44469.666.389.4.2.3
	NAME 'kipX509KeyMud'
	DESC 'Keyful Identity Protocol, session key import from X.509'
)

attributetype ( 1.3.6.1.4.1.44469.666.389.4.2.4
	NAME 'kipOpenPGPKeyMud'
	DESC 'Keyful Identity Protocol, session key import from OpenPGP'
)

attributetype ( 1.3.6.1.4.1.44469.666.389.4.3.1
	NAME 'kipSignatureMud'
	DESC 'Keyful Identity Protocol, signature mud'
)

attributetype ( 1.3.6.1.4.1.44469.666.389.4.3.2
	NAME 'kipServiceSignatureMud'
	DESC 'Keyful Identity Protocol, service signature mud'
)

attributetype ( 1.3.6.1.4.1.44469.666.389.4.3.3
	NAME 'kipX509SignatureMud'
	DESC 'Keyful Identity Protocol, signature from X.509'
)

attributetype ( 1.3.6.1.4.1.44469.666.389.4.3.4
	NAME 'kipOpenPGPSignatureMud'
	DESC 'Keyful Identity Protocol, signature from OpenPGP'
)

attributetype ( 1.3.6.1.4.1.44469.666.389.4.4
	NAME 'kipRequiredKeys'
	DESC 'Keyful Identity Protocol, keynr/encalg pairs, 8 bytes each, that are critical in combination for the present object; other attribute values may present an alternative requirement set'
)


objectclass ( 1.3.6.1.4.1.44469.666.389.4.8
	NAME 'kipDocument'
	DESCR 'Keyful Identity Protocol, protected object'
	AUXILIARY
	MAY  ( kipDataMud $ kipKeyMud $ kipServiceKeyMud $ kipX509KeyMud $ kipOpenPGPKeyMud $ kipSignatureMud $ kipServiceSignatureMud $ kipX509SignatureMud $ kipOpenPGPSignatureMud $ kipRequiredKeys )
)

objectclass ( 1.3.6.1.4.1.44469.666.389.4.9
	NAME 'kipCollection'
	DESCR 'Keyful Identity Protocol, collection of objects'
	AUXILIARY
	MAY  ( kipKeyMud $ kipServiceKeyMud $ kipX509KeyMud $ kipOpenPGPKeyMud $ kipRequiredKeys )
)

