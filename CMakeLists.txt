# This is KIP, which is the chicken *and* the egg for encrypted document exchange

#
#   SPDX-License-Identifier: BSD-2-Clause
#   SPDX-FileCopyrightText: Copyright 2020, Rick van Rein, OpenFortress.nl

cmake_minimum_required (VERSION 3.13 FATAL_ERROR)
project ("KIP" VERSION 0.15.0 LANGUAGES C)

# Pretty reporting of packages
include (FeatureSummary)

# Look for the module itself. Set properties here, because ARPA2CM
# might not be installed to set them itself.
find_package (ARPA2CM 0.9.2 QUIET NO_MODULE)
set_package_properties (ARPA2CM PROPERTIES
    DESCRIPTION "CMake modules for ARPA2 projects"
    TYPE REQUIRED
    URL "https://gitlab.com/arpa2/arpa2cm/"
    PURPOSE "Required for the CMake build system for ${PROJECT}"
)

# If found, use it, otherwise report error and stop CMake.
if (ARPA2CM_FOUND)
    set (CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_SOURCE_DIR}/cmake ${ARPA2CM_MODULE_PATH})
else()
    feature_summary (WHAT ALL)
    message (FATAL_ERROR "ARPA2CM is required.")
endif()

include (MacroEnsureOutOfSourceBuild)
include (MacroAddUninstallTarget)
include (MacroGitVersionInfo)
include (MacroCreateConfigFiles)
include (MacroLibraryPair)
include (MacroStripDestDir)
include (GNUInstallDirs)
include (ExceptionHandling)
include (UseInstallRPATH)

#
# Options / Build Settings
#

option (DEBUG
	"Switch on output and flags that aid developpers in debugging"
	OFF)

option (CUT_THROAT_TESTING
	"Include cut-through tests: under development, still made to work, known to fail"
	OFF)

if (DEBUG)
	add_compile_options (-DDEBUG -ggdb3 -O0)
endif()

if (WIN32)
	set (NO_DAEMONS_DEFAULT ON)
else ()
	set (NO_DAEMONS_DEFAULT OFF)
endif ()

option (NO_DAEMONS
	"Do not build daemons and server management programs"
	${NO_DAEMONS_DEFAULT})

if (NO_DAEMONS)
	set (DAEMON_EXCLUDE_FROM_ALL "EXCLUDE_FROM_ALL")
else ()
	set (DAEMON_EXCLUDE_FROM_ALL)
endif ()

macro_ensure_out_of_source_build("Do not build KIP in the source directory.")
get_project_git_version()

enable_testing ()


#
# Configuration Settings (Runtime)
#

set (KIP_VARDIR
	"/var/lib/kip"
	CACHE STRING "Installed KIP data directory (holds service virtual key mappings)")

set (KIP_KEYTAB
	"/var/lib/kip/kip.keytab"
	CACHE STRING "Installed KIP keytab (may be stored separately to benefit chroot)")


#
# Dependencies from ARPA2
#

find_package (ARPA2Common)
set_package_properties (ARPA2Common PROPERTIES
	TYPE REQUIRED
	URL "https://gitlab.com/arpa2/arpa2common/"
	PURPOSE "Common ARPA2 Utensils")

find_package (Quick-MEM)
set_package_properties (Quick-MEM PROPERTIES
	TYPE REQUIRED
	URL "https://gitlab.com/arpa2/Quick-MEM/"
	PURPOSE "Memory management (extended by Quick-DERMEM)")

find_package (Quick-DER    1.5.0)
set_package_properties (Quick-DER PROPERTIES
	TYPE REQUIRED
	URL "https://gitlab.com/arpa2/quick-der/"
	PURPOSE "Handling ASN.1 data structures in DER blobs")

find_package (Quick-DERMEM       REQUIRED)  # From Quick-DER
set_package_properties (Quick-DERMEM PROPERTIES
	TYPE REQUIRED
	URL "https://gitlab.com/arpa2/quick-der/"
	PURPOSE "Memory management extended with send/recv of ASN.1 in DER blobs")

find_package (Quick-SASL 0.11.0)
set_package_properties (Quick-SASL PROPERTIES
	TYPE REQUIRED
	URL "https://gitlab.com/arpa2/Quick-SASL/"
	PURPOSE "Simple API for local SASL handling")

find_package (Quick-DiaSASL)  # From Quick-SASL
set_package_properties (Quick-DiaSASL PROPERTIES
	TYPE REQUIRED
	URL "https://gitlab.com/arpa2/Quick-SASL/"
	PURPOSE "SASL passthrough to a Diameter backend for Realm Crossover")

#
# Dependencies from third-parties
#
find_package (Doxygen
    COMPONENTS dot)
set_package_properties (
    Doxygen PROPERTIES
    PURPOSE "Produce browsable documentation for KIP and HAAN")

find_package (Unbound)
set_package_properties (Unbound PROPERTIES
    TYPE REQUIRED
)

# Keep Cyrus SASL, even though Quick SASL moved out -- it is used in HAAN
# for fuzzing. We need only the include- and plugins-directories, though.
find_package (Cyrus-SASL2)
set_package_properties (Cyrus-SASL2 PROPERTIES TYPE REQUIRED)

find_package (OpenSSL)
set_package_properties (OpenSSL PROPERTIES TYPE REQUIRED)

find_package (MIT-krb5)
set_package_properties (MIT-krb5 PROPERTIES TYPE REQUIRED)

find_package (com_err)
set_package_properties (com_err PROPERTIES TYPE REQUIRED)

# Additional, for Diameter SASL
if (NOT ${NO_DAEMONS})
	find_package (freeDiameter 1.2.0)
	set_package_properties (freeDiameter PROPERTIES TYPE REQUIRED)

	find_package (BISON)
	set_package_properties (BISON PROPERTIES TYPE REQUIRED)

	find_package (FLEX)
	set_package_properties (FLEX PROPERTIES TYPE REQUIRED)
endif ()

# Additional, for threaded example(s)
find_package (Threads)  #OPTIONAL

# Additional, for event loop example(s)
find_package (LibEv)  #OPTIONAL

# Additional, for browser-client example
find_package (PkgConfig)
set_package_properties (PkgConfig PROPERTIES TYPE REQUIRED)

# TODO: do this in a "normal" Find module so it can be find-package'd
pkg_check_modules(json-c REQUIRED IMPORTED_TARGET json-c)

feature_summary (WHAT ALL FATAL_ON_MISSING_REQUIRED_PACKAGES)


### BUILD
#
#
include_directories (
	${CMAKE_CURRENT_SOURCE_DIR}/include
	#DEPRECATION# ${CMAKE_CURRENT_BINARY_DIR}/asn1
	${KERBEROS_INCLUDE_DIRS}
	${freeDiameter_INCLUDE_DIRS}
)

#DEPRECATION# add_subdirectory (asn1)
add_subdirectory (lib)
add_subdirectory (test)
add_subdirectory (src)
#LOCAL OPTION# add_subdirectory (src/windows-cpp)
add_subdirectory (doc)
add_subdirectory (contrib)

install (
    DIRECTORY include/arpa2
    DESTINATION include
    FILES_MATCHING PATTERN "*.h"
)

create_config_files(KIP EXPORT)


#
# Building
#


#
# Testing
#
