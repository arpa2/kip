/* This is the logical opposite of haangen.c -- to test user/pass.
 *
 * The pretty-printed user name (after possible canonicalisation) is again
 * used to generate the password, which can then be checked.
 *
 * In the code below, the check is a simple comparison of strings.  In the
 * anticipated future form however, it integrates with SASL mechanisms as
 * a supplied password that can use any mechanism.  That includes simple
 * comparisons of PLAIN forms as well as more complicated forms of juggling
 * codes that conceal the actual password from intermediates.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <unistd.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/stat.h>

#include <krb5.h>

#include <arpa2/kip.h>


char BASE32 [] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";


void polish (char *pwd) {
	while (*pwd != '\0') {
		if (*pwd == '0') {
			*pwd = 'O';
		} else if (*pwd == '1') {
			*pwd = 'I';
		} else if ((*pwd >= 'a') && (*pwd <= 'z')) {
			*pwd ^= 'a' ^ 'A';
		}
		pwd++;
	}
}


int main (int argc, char *argv []) {
	//
	// Read the username from the keyboard
	char username [500];
	printf ("Helm Key: ");
	fflush (stdout);
	if (fgets (username, sizeof (username)-1, stdin) == NULL) {
		fprintf (stderr, "Failure reading the user name\n");
		exit (1);
	}
	if ((strlen (username) < 33) || (username [32] != '@')) {
		fprintf (stderr, "Syntax error in Helm Key (user@)\n");
		exit (1);
	}
	username [32] = '\0';
	char *vhostname = username + 33;
	int vhostnamelen = strlen (vhostname);
	if ((vhostnamelen <= 1) || (vhostname [vhostnamelen-1] != '\n')) {
		fprintf (stderr, "Syntax error in Helm Key (realm)\n");
		exit (1);
	}
	vhostname [--vhostnamelen] = '\0';
	//
	// Read the provided password TODO:interact.c
	char *provided = getpass ("Password: ");
	if (provided == NULL) {
		fprintf (stderr, "Failure reading the password\n");
		exit (1);
	}
	//
	// Polish username and password; make it uppercase and map 0-->O, 1-->I
	polish (username);
	polish (provided);
	//
	// Initialise the KIP library and open a context
	kipt_ctx kip;
	assert (kip_init ());
	assert (kipctx_open (&kip));
	//
	// Load the virtual host keymap
	kipt_keyid vhostkey;
	assert (kipkey_fromvhosttab (kip,
				"haan", vhostname,
				&vhostkey));
	//
	// Produce the expected password from the readable username
	// We've been lazy, ignoring 3 bits out of 8 when mapping to BASE32
	uint8_t exp128 [32];
	assert (kipdata_usermud (kip, vhostkey, (uint8_t*)username, strlen (username), exp128, sizeof (exp128)));
	//
	// Map password to a readable format
	char expected [66];
	for (int i = 0; i < sizeof (exp128); i++) {
		expected [i] = BASE32 [ exp128 [i] & 31 ];
	}
	expected [sizeof(exp128)] = '\0';
	//
	// Cleanup the KIP environment
	kipctx_close (kip);
	kip_fini ();
	//
	// Compare expected and provided password and exit accordingly
	if (strcmp (expected, provided) == 0) {
		printf ("Password is correct\n");
		exit (0);
	} else {
		printf ("Password is wrong\n");
		exit (1);
	}
}
