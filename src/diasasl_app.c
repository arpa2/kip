/* diasasl_app.c -- freeDiameter Extension app for Diameter-SASL
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Henri Manson <info@mansoft.nl>
 * SPDX-FileCopyrightText: Sebastien Decugis <sdecugis@freediameter.net>
 */

 
/*********************************************************************************************************
* Software License Agreement (BSD License)                                                               *
* Author: Sebastien Decugis <sdecugis@freediameter.net>							 *
*													 *
* Copyright (c) 2013, WIDE Project and NICT								 *
* All rights reserved.											 *
* 													 *
* Redistribution and use of this software in source and binary forms, with or without modification, are  *
* permitted provided that the following conditions are met:						 *
* 													 *
* * Redistributions of source code must retain the above 						 *
*   copyright notice, this list of conditions and the 							 *
*   following disclaimer.										 *
*    													 *
* * Redistributions in binary form must reproduce the above 						 *
*   copyright notice, this list of conditions and the 							 *
*   following disclaimer in the documentation and/or other						 *
*   materials provided with the distribution.								 *
* 													 *
* * Neither the name of the WIDE Project or NICT nor the 						 *
*   names of its contributors may be used to endorse or 						 *
*   promote products derived from this software without 						 *
*   specific prior written permission of WIDE Project and 						 *
*   NICT.												 *
* 													 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED *
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A *
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR *
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 	 *
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 	 *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR *
* TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF   *
* ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.								 *
*********************************************************************************************************/

/*
 * diasasl application for freeDiameter.
 */

#include "diasasl_app.h"

/* Initialize the configuration */
struct diasasl_conf *diasasl_conf = NULL;
static struct diasasl_conf _conf;
static pthread_t diasasl_thread;

static int diasasl_conf_init(void)
{
	diasasl_conf = &_conf;
	memset(diasasl_conf, 0, sizeof(struct diasasl_conf));

	/* Set the default values */
	diasasl_conf->vendor_id  = 44469;    /* ARPA2 */
	diasasl_conf->appli_id   = 1;        /* Network Access */
	diasasl_conf->cmd_id     = 265;      /* AAR/AAA */
	diasasl_conf->avp_id     = 0xffffff; /* dummy value */
	diasasl_conf->long_avp_len = 5000;
	diasasl_conf->mode       = MODE_SERV | MODE_CLI;
	diasasl_conf->dest_realm = strdup(fd_g_config->cnf_diamrlm);
	diasasl_conf->dest_host  = NULL;
	diasasl_conf->diasasl_ip = strdup("::");
	diasasl_conf->diasasl_port = 12346;
	diasasl_conf->sasl_app = strdup("InternetWide_Identity");
	return 0;
}

static void diasasl_conf_dump(void)
{
	if (!TRACE_BOOL(INFO))
		return;
	fd_log_debug( "------- diasasl configuration dump: ---------");
	fd_log_debug( " Vendor Id .......... : %u", diasasl_conf->vendor_id);
	fd_log_debug( " Application Id ..... : %u", diasasl_conf->appli_id);
	fd_log_debug( " Command Id ......... : %u", diasasl_conf->cmd_id);
	fd_log_debug( " AVP Id ............. : %u", diasasl_conf->avp_id);
	fd_log_debug( " Long AVP Id ........ : %u", diasasl_conf->long_avp_id);
	fd_log_debug( " Long AVP len ....... : %zu", diasasl_conf->long_avp_len);
	fd_log_debug( " Mode ............... : %s%s%s", diasasl_conf->mode & MODE_SERV ? "Serv" : "", diasasl_conf->mode & MODE_CLI ? "Cli" : "");
	fd_log_debug( " Destination Realm .. : %s", diasasl_conf->dest_realm ?: "- none -");
	fd_log_debug( " Destination Host ... : %s", diasasl_conf->dest_host ?: "- none -");
	fd_log_debug( " Diameter SASL ip.... : %s", diasasl_conf->diasasl_ip);
	fd_log_debug( " Diameter SASL port.. : %i", diasasl_conf->diasasl_port);
	fd_log_debug( " SASL Application ... : %s", diasasl_conf->sasl_app);
	fd_log_debug( "------- /diasasl configuration dump ---------");
}

static struct fd_hook_hdl * hookhdl[2] = { NULL, NULL };
static void diasasl_hook_cb_silent(enum fd_hook_type type, struct msg * msg, struct peer_hdr * peer, void * other, struct fd_hook_permsgdata *pmd, void * regdata) {
}
static void diasasl_hook_cb_oneline(enum fd_hook_type type, struct msg * msg, struct peer_hdr * peer, void * other, struct fd_hook_permsgdata *pmd, void * regdata) {
	char * buf = NULL;
	size_t len;

	CHECK_MALLOC_DO( fd_msg_dump_summary(&buf, &len, NULL, msg, NULL, 0, 0),
		{ LOG_E("Error while dumping a message"); return; } );

	LOG_N("{%d} %s: %s", type, (char *)other ?:"<nil>", buf ?:"<nil>");

	free(buf);
}


/* entry point */
static int diasasl_entry(char * conffile)
{
	TRACE_ENTRY("%p", conffile);

	/* Initialize configuration */
	CHECK_FCT( diasasl_conf_init() );

	/* Parse configuration file */
	if (conffile != NULL) {
		CHECK_FCT( diasasl_conf_handle(conffile) );
	}

	TRACE_DEBUG(INFO, "Extension diasasl initialized with configuration: '%s'", conffile);
	diasasl_conf_dump();

	/* Install objects definitions for this diasasl application */
	CHECK_FCT( diasasl_dict_init() );

	/* Install the handlers for incoming messages */
	if (diasasl_conf->mode & MODE_SERV) {
		CHECK_FCT( diasasl_serv_init() );
	}

	if (diasasl_conf->mode & MODE_CLI) {
		CHECK_FCT( diasasl_cli_init() );
		CHECK_POSIX( pthread_create(&diasasl_thread, NULL, diasasl_socket_der, NULL) );
	}
	/* Advertise the support for the diasasl application in the peer */
	CHECK_FCT( fd_disp_app_support ( diasasl_appli, diasasl_vendor, 1, 0 ) );

	return 0;
}

/* Unload */
void fd_ext_fini(void)
{
	if (diasasl_conf->mode & MODE_CLI)
		diasasl_cli_fini();
	if (diasasl_conf->mode & MODE_SERV)
		diasasl_serv_fini();
	if (hookhdl[0])
		fd_hook_unregister( hookhdl[0] );
	if (hookhdl[1])
		fd_hook_unregister( hookhdl[1] );
	CHECK_FCT_DO( fd_thr_term(&diasasl_thread), );
}

EXTENSION_ENTRY("diasasl_app", diasasl_entry);
