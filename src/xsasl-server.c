/* A SASL server based on Xover SASL.
 *
 * It differs only on slight details from the qsasl-server, and that is
 * deliberate -- both as a demonstration of the difference, and to be
 * able to test plain Quick SASL against Xover SASL.
 *
 * This is similar to the sasl-sample-server provided with Cyrus SASL2,
 * but we use different prompts and switched from BASE64 to HEX for
 * reasons of human readability (the term "human" is debatable, grinn).
 * When translating, also note the different mechanism lists; these are
 * not standardised as part of SASL, so each makes up their own.
 *
 * The server can listen to an IP and port combination, where a client
 * would be connecting.  When this is not requested, it will instead assume
 * traffic on stdin in a somewhat standard format.
 *
 * Lines are prefixed with either "c2s> " or "s2c> " to indicate the kind
 * of information and the traffic passed.  Plus, initially there is a
 * "mech> " prefix from server to client with the mechanism, and one from
 * the client back to the server.  When returning data with success, the
 * prompt "extra> " might also be used.  Any lines not recognised are
 * quietly dropped, except for a message about misrecognised prompts.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include <unistd.h>

#include <arpa2/socket.h>

#include <arpa2/kip.h>
#include <arpa2/quick-der.h>
#include <arpa2/xover-sasl.h>


#define DEFAULT_PORT "16832"


#include "sasl-common.c"



/* Main program.
 */
int main (int argc, char *argv []) {
	//
	// Harvest commandline arguments
	assert (argc > 0);
	if (argc > 4) {
		fprintf (stderr, "Usage: %s [serverIP [serverPort [chanbindvalue]]]\n"
				"Default serverPort is " DEFAULT_PORT " but without serverIP, the user is the network\n",
				argv [0]);
		exit (1);
	}
	socket_init();
	const char *service = "qsasl-xsasl-demo";
	bool tty_user_networking = (argc < 2);
	char *server_ip   = (argc >= 2) ? argv [1] : NULL        ;
	char *server_port = (argc >= 3) ? argv [2] : DEFAULT_PORT;
	char *chan_bind   = (argc >= 4) ? argv [3] : NULL        ;
	//
	// Have streams for networking
	int sox = -1;
	FILE *c2s_stream = stdin;
	FILE *s2c_stream = stdout;
	if (tty_user_networking) {
		printf ("\n*\n* You did not supply a network.  No problem.\n* Please copy/paste lines to the qsasl-client or xsasl-client.\n*\n\n");
	} else {
		struct sockaddr_storage ss;
		memset (&ss, 0, sizeof (ss));
		if (!socket_parse (server_ip, server_port, &ss)) {
			fprintf (stderr, "Syntax error in host ip:port setting %s:%s\n", server_ip, server_port);
			exit (1);
		}
		if (!socket_server (&ss, SOCK_STREAM, &sox)) {
			perror ("Failed to serve network");
			exit (1);
		}
		printf ("--\n");
		fflush (stdout);
		int cnx = accept (sox, NULL, NULL);
		if (cnx < 0) {
			perror ("Failed to accept connection");
			exit (1);
		}
		c2s_stream = fdopen (dup (cnx), "r");
		s2c_stream = fdopen (     cnx , "w");
	}
	//
	// Start the SASL session as a client.
	kip_init ();
	kipservice_init (NULL, NULL);
	xsasl_init (NULL, "InternetWide_Identity");
	XoverSASL sasl = NULL;
	xsaslt_state curstate = QSA_UNDECIDED;
	assert (xsasl_server (&sasl, service, cb_statechange, &curstate, QSA_SERVERFLAG_ALLOW_FINAL_S2C));
	//
	// Be someone -- including step down to the ACL user
	char *str_realm = getenv ("KIPSERVICE_CLIENT_REALM");
	char *str_servr = getenv ("KIP_REALM"              );
	membuf crs_realm = { .bufptr=(uint8_t*)str_realm, .buflen=strlen (str_realm) };
	membuf crs_servr = { .bufptr=(uint8_t*)str_servr, .buflen=strlen (str_servr) };
	assert (xsasl_set_client_realm     (sasl, crs_realm));
	assert (xsasl_set_server_realm     (sasl, crs_servr));
	if (chan_bind != NULL) {
		uint8_t *cb = NULL;
		assertxt (qsasl_alloc (sasl, strlen(QSA_X_MANUAL_SESSION_KEY) + strlen(chan_bind) + 2, &cb), "Failed to allocate channel binding value");
		sprintf (cb, "%s:%s", QSA_X_MANUAL_SESSION_KEY, chan_bind);
		membuf mem_chanbind = {
			.bufptr = cb,
			.buflen = strlen (cb)
		};
		assert (xsasl_set_chanbind (sasl, false, mem_chanbind));
	}
	//
	// Now provide a list of mechanisms for the client
	// Mechanism lists are not standardised but these demos pass them as
	// a string with spaces between the standardised mechanism names.
	dercursor mechs;
	assert (xsasl_get_mech (sasl, &mechs));
	prompted_hex_send (sasl, s2c_stream, "mech", mechs);
	//
	// Await the response from the client, in the form of a "mech> " prompt
	static const char *only_mech [] = { "mech", NULL };
	const char *_lineprompt;
	dercursor mech_used;
	dercursor s2c;
	prompted_hex_recv (sasl, c2s_stream, only_mech, &_lineprompt, &mech_used);
	assert (xsasl_set_mech (sasl, mech_used));
	//
	// Start looping as a server
	while (curstate == QSA_UNDECIDED) {
		//
		// Receive a response from the client
		static const char *only_c2s []  = { "c2s" , NULL };
		dercursor c2s;
		prompted_hex_recv (sasl, c2s_stream, only_c2s, &_lineprompt, &c2s);
		//
		// Make a server step: c2s -> s2c
		assert (xsasl_step_server (sasl, c2s, &s2c));
		//
		// Do not send a new challenge when the last response was final
		if (curstate != QSA_UNDECIDED) {
			break;
		}
		//
		// Send output to the client; initially "mech> " and later "s2c> "
		prompted_hex_send (sasl, s2c_stream, "s2c", s2c);
	}
	//
	// Optionally send "extra>" with an addtional token upon success
	if ((curstate == QSA_SUCCESS) && (s2c.derptr != NULL)) {
		prompted_hex_send (sasl, s2c_stream, "extra", s2c);
	}
	//
	// Determine the mechanism used, if it was one, and the clientid
	membuf clientuid;
	if (xsasl_get_client_userid (sasl, &clientuid)) {
		if (clientuid.bufptr == NULL) {
			printf ("No client userid\n");
		} else {
			printf ("Client userid is %.*s\n", (int)clientuid.buflen, clientuid.bufptr);
		}
	}
	membuf clientdom;
	if (xsasl_get_client_domain (sasl, &clientdom)) {
		if (clientdom.bufptr == NULL) {
			printf ("No client domain\n");
		} else {
			printf ("Client domain is %.*s\n", (int)clientdom.buflen, clientdom.bufptr);
		}
	}
	if (mech_used.derptr != NULL) {
		printf ("Mechanism used was %.*s\n", (int)mech_used.derlen, mech_used.derptr);
	}
	//
	// Report whether the exchange was successful
	printf ("\n*\n* The SASL exchange was a %s.  Application %s the client.\n*\n\n",
			(curstate == QSA_SUCCESS) ? "SUCCESS" : "FAILURE",
			(s2c.derptr == NULL) ? "will tell" : "has told");
	//
	// Send an explicit EOF signal to be detected as failure
	dercursor eof_null = { .derptr = NULL, .derlen = 0 };
	prompted_hex_send (sasl, s2c_stream, "eof", eof_null);
	//
	// Wait for the client to close the connection
	fclose (s2c_stream);
	while (!feof (c2s_stream)) {
		char buf [100];
		fread (buf, 1, 100, c2s_stream);
	}
	fclose (c2s_stream);
	if (sox >= 0) {
		close (sox);
	}
	//
	// Cleanup & Closedown
	xsasl_close (&sasl);
	xsasl_fini ();
	kipservice_fini ();
	kip_fini ();
	socket_fini ();
	exit ((curstate == QSA_SUCCESS) ? 0 : 1);
}
