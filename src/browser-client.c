/* browser-client.c -- HTTP-SASL client, mimics browser behaviour.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Henri Manson <info@mansoft.nl>
 */

#include <stdio.h>
#include <signal.h>
#include <getopt.h>
#include <locale.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include <sasl/sasl.h>
#include <sasl/saslplug.h>
#include <sasl/saslutil.h>

#include <json-c/json.h>

#include <arpa2/socket.h>

#include <arpa2/quick-der.h>
#include "arpa2/xover-sasl.h"

#include "arpa2/kip.h"

#include "sasl-common.c"

#define SAMPLE_SEC_BUF_SIZE (2048)
#define HTTP_SERVICE_NAME "HTTP"

static char *conffile = NULL;
static const char service[] = "HTTP";
static unsigned char chanbind[128];

int read_int32()
{
	int byte1 = getchar();
	if (byte1 == EOF) {
		return -1;
	}
	int byte2 = getchar();
	if (byte2 == EOF) {
		return -1;
	}
	int byte3 = getchar();
	if (byte3 == EOF) {
		return -1;
	}
	int byte4 = getchar();
	if (byte4 == EOF) {
		return -1;
	}
	return (byte1) | (byte2 << 8) | (byte3 << 16) | (byte4 << 24);
}

void write_int32(int length)
{
	putchar(length);
	putchar(length >> 8);
	putchar(length >> 16);
	putchar(length >> 24);
}

void copy_json_object(json_object *http_sasl_in, json_object *http_sasl_out, const char *property)
{
	json_object *json;
	if (json_object_object_get_ex(http_sasl_in, property, &json)) {
		json_object *copy = NULL;
#if JSON_C_VERSION_NUM < 0x000d00 /* < 0.13.0 */
		copy = json_tokener_parse(json_object_get_string(json));
#else
		json_object_deep_copy(json, &copy, NULL);
#endif
		json_object_object_add(http_sasl_out, property, copy);
	}
}

void copy_json_string(json_object *http_sasl_in, json_object *http_sasl_out, const char *property)
{
	json_object *json;
	if (json_object_object_get_ex(http_sasl_in, property, &json)) {
		const char* value = json_object_get_string(json);
		fprintf(stderr, "%s: %s\n", property, value);
		fflush(stderr);
		json_object_object_add(http_sasl_out, property, json_object_new_string(value));
	}
}

static void printchan(unsigned char* chan, int len)
{
    fprintf(stderr, "0x");
    for(size_t count = 0; count < len; count++)
        fprintf(stderr, "%02x", chan[count]);
    fprintf(stderr, "\n");
}

int main()
{
	char buf[SAMPLE_SEC_BUF_SIZE];
	XoverSASL sasl;

	//
	// Start the SASL session as a client.
	kip_init ();
	kipservice_init (NULL, NULL);
	xsasl_init (NULL, "InternetWide_Identity");
	//
	// Be someone -- including step down to the ACL user
	char *str_login = getenv ("KIPSERVICE_CLIENTUSER_LOGIN");
	char *str_acl   = getenv ("KIPSERVICE_CLIENTUSER_ACL"  );
	char *str_realm = getenv ("KIPSERVICE_CLIENT_REALM"    );
	char *str_servr = getenv ("KIP_REALM"                  );
	membuf crs_login = { .bufptr=str_login, .buflen=strlen (str_login) };
	membuf crs_acl   = { .bufptr=str_acl  , .buflen=strlen (str_acl  ) };
	membuf crs_realm = { .bufptr=str_realm, .buflen=strlen (str_realm) };
	membuf crs_servr = { .bufptr=str_servr, .buflen=strlen (str_servr) };
	for (;;) {
		sasl = NULL;

		dercursor c2s;
		dercursor mech;
		int len = read_int32();
		int result;
		fprintf(stderr, "len: %d\n", len);
		fflush(stderr);
		if (len == -1) {
			break;
		}
		char buf[4096];
		fread(buf, 1, len, stdin);
		buf[len] = '\0';
		fprintf(stderr, "read: %s\n", buf);
		fflush(stderr);
		json_object *http_sasl_in = json_tokener_parse(buf);
		json_object *json_mech;
		const char* str_mech = NULL;


		json_object *json_c2c;
		if (json_object_object_get_ex(http_sasl_in, "c2c", &json_c2c)) {
			fprintf(stderr, "Using existing sasl\n");
			fflush(stderr);
			const char* value = json_object_get_string(json_c2c);
			fprintf(stderr, "c2c: %s\n", value);
			fflush(stderr);
			result = sasl_decode64(value, strlen(value), buf, SAMPLE_SEC_BUF_SIZE, &len);
			if (result == SASL_OK) {
				buf[len] = '\0';
				sscanf(buf, "%p", &sasl);
			}
		} else {
			char *service = getenv("HTTP_SERVICE_NAME");
			if (!service) {
				service = HTTP_SERVICE_NAME;
			}
			fprintf(stderr, "HTTP service name: %s\n", service);
			fprintf(stderr, "Creating new sasl\n");

			fflush(stderr);
			assert (xsasl_client (&sasl, service, NULL, NULL, 0));
			assert (xsasl_set_clientuser_login (sasl, crs_login));
			assert (xsasl_set_clientuser_acl   (sasl, crs_acl  ));
			assert (xsasl_set_client_realm     (sasl, crs_realm));
			assert (xsasl_set_server_realm     (sasl, crs_servr));
		}

		if (sasl != NULL) {
			fprintf(stderr, "sasl: %p\n", sasl);
			fflush(stderr);

			if (json_object_object_get_ex(http_sasl_in, "mech", &json_mech)) {
				str_mech = json_object_get_string(json_mech);
				fprintf(stderr, "mech: %s\n", str_mech);
				fflush(stderr);
				dercursor mech_used = {
					.derptr = (uint8_t *) str_mech,
					.derlen = strlen(str_mech)
				};
				assert (xsasl_set_mech (sasl, mech_used));
				json_object *json_channelbinding;
				if (json_object_object_get_ex(http_sasl_in, "channelBinding", &json_channelbinding)) {
					const char* str_channelBinding = json_object_get_string(json_channelbinding);
					fprintf(stderr, "channelBinding: %s\n", str_channelBinding);
					result = sasl_decode64(str_channelBinding, strlen(str_channelBinding), chanbind, sizeof(chanbind), &len);
					if (result == SASL_OK) {
						printchan(chanbind, len);
						membuf chanbindbuf = {
							.bufptr = chanbind,
							.buflen = len
						};
						assert(xsasl_set_chanbind(sasl, false, chanbindbuf));
					}
				}
			}
			json_object *json_s2c;
			dercursor s2c = {
				.derptr = NULL,
				.derlen = 0
			};
			if (json_object_object_get_ex(http_sasl_in, "s2c", &json_s2c)) {
				const char* value = json_object_get_string(json_s2c);
				fprintf(stderr, "s2c: %s\n", value);
				fflush(stderr);
				result = sasl_decode64(value, strlen(value), buf, SAMPLE_SEC_BUF_SIZE, &len);
				if (result == SASL_OK) {
					s2c.derptr = buf;
					s2c.derlen = len;
				}
			}
			assert (xsasl_step_client (sasl, s2c, &mech, &c2s));
			result = sasl_encode64(c2s.derptr, c2s.derlen, buf, SAMPLE_SEC_BUF_SIZE, &len);
			if (result == SASL_OK) {
				json_object *http_sasl_out;

				http_sasl_out = json_object_new_object();
				fprintf(stderr, "s2c.derptr: %p\n", s2c.derptr);
				fflush(stderr);
				if (mech.derptr != NULL) {
					char chosen_mech[128];
					memcpy(chosen_mech, mech.derptr, mech.derlen);
					chosen_mech[mech.derlen] = '\0';
					fprintf(stderr, "chosen mechanism: %s\n", chosen_mech);
					fflush(stderr);
					json_object_object_add(http_sasl_out, "mech", json_object_new_string(chosen_mech));
				}
				json_object_object_add(http_sasl_out, "realm", json_object_new_string("kipsvc.pixie.demo.arpa2.lab"));
				json_object_object_add(http_sasl_out, "c2s", json_object_new_string(buf));
				fprintf(stderr, "c2s: %s\n", buf);
				copy_json_string(http_sasl_in, http_sasl_out, "s2s");
				copy_json_string(http_sasl_in, http_sasl_out, "requestId");
				copy_json_object(http_sasl_in, http_sasl_out, "extraInfoSpec");
				char buf_c2c[32];
				snprintf(buf_c2c, sizeof(buf_c2c), "%p", sasl);
				result = sasl_encode64(buf_c2c, strlen(buf_c2c), buf, SAMPLE_SEC_BUF_SIZE, &len);
				if (result == SASL_OK) {
					json_object_object_add(http_sasl_out, "c2c", json_object_new_string(buf));
				}
				const char *str_http_sasl_out = json_object_to_json_string_ext(http_sasl_out, JSON_C_TO_STRING_PLAIN);
				fprintf(stderr, "sasl***: %p\n", sasl);
				fprintf(stderr, "http_sasl_out: %s\n\n", str_http_sasl_out);
				fflush(stderr);
				len = strlen(str_http_sasl_out);
				write_int32(len);
				printf("%s", str_http_sasl_out);
				fflush(stdout);
			} else {
				fprintf(stderr, "error in sasl_encode64\n");
			}
			bool success;
			bool failure;
			if (xsasl_get_outcome(sasl, &success, &failure) && (success || failure)) {
				fprintf(stderr, "closing sasl: %p, success: %d, failure: %d\n", sasl, success, failure);
				fflush(stderr);
				xsasl_close(&sasl);
			}
		} else {
			fprintf(stderr, "sasl is NULL\n");
		}
	}
	return 0;
}
