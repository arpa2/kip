/* KIP Service daemon, main program.
 *
 * The KIP Service is very much a "remote keytab" that unpacks keys
 * as directed by the ACL that is packed along with them.  When the
 * KIP library calls kipkey_fromservice() or kipkey_toservice() are
 * called they will connect to a KIP Service to get some work done,
 * but always just on the keys and never on the data
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <unistd.h>
#include <signal.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>

#include <krb5.h>

#include <arpa2/socket.h>

#include <arpa2/quick-der.h>
#include <arpa2/quick-dermem.h>

#include <arpa2/quick-sasl.h>
#include <quick-der/KIP-Service.h>

#include <arpa2/kip.h>


uint32_t forks = 0;


void child_exit (int sigchld) {
	/* quick check if child processes need cleanup */
	int wstatus;
	while (waitpid (-1, &wstatus, WNOHANG) > 0) {
		if (WIFEXITED (wstatus)) {
			forks--;
		}
		//TODO// Might process WEXITSTATUS (wstatus)
	}
}


void please_stop (int stopsignal) {
	printf ("Exiting gently on stop signal\n");
	exit (0);
}


int main (int argc, char *argv []) {
	//
	// Check the commandline
	if (argc != 4) {
		fprintf (stderr, "Usage: %s <address> <port> <stopsig>\nSetup: KIP_VARDIR, KIP_KEYTAB", argv [0]);
		exit (1);
	}
	char *progname =       argv [0] ;
	char *hostname =       argv [1] ;
	char *portname =       argv [2] ;
	int stopsignal = atoi (argv [3]);
	//
	// Install the stop signal handler
	signal (stopsignal, please_stop);
	//
	// Initialias KIP and KIP Daemon libraries
	assert (kip_init  ());
	assert (kipdaemon_init ());
	//
	// Initialise Quick-SASL
	assert (qsasl_init (NULL, "Hosted_Identity"));
	//
	// Get a server socket
	struct sockaddr_storage sa;
	if (!socket_parse (hostname, portname, &sa)) {
		com_err (progname, errno, "Failed to parse host:name from %s:%s\n", hostname, portname);
		exit (1);
	}
	int sox = -1;
reconnect:
	if (!socket_server (&sa, SOCK_STREAM, &sox)) {
		com_err (progname, errno, "Failed to serve on %s port %s\n", hostname, portname);
#if 0
		if (errno == EADDRINUSE) {
			sleep (5);
			goto reconnect;
		}
#endif
		exit (1);
	}
	//
	// Report to Pypeline that we are ready for action
	printf ("--\n");
	fflush (stdout);
	//
	// Setup to clean zombie processes immediately
	signal (SIGCHLD, child_exit);
	//
	// Main "loop", waiting for a connection (for now: one at a time)
	struct sockaddr_storage peer;
	socklen_t peerlen = sizeof (peer);
	int cnx = -1;
	while ( (cnx = accept (sox, (struct sockaddr *) &peer, &peerlen)) ) {
		if (cnx < 0) {
			fprintf (stderr, "Failed to pickup incoming connection\n");
			continue;
		}
		//
		// Fork a handler process
		pid_t fork_out = fork ();
		switch (fork_out) {
		case -1:
			/* error forking */
			perror ("fork()");
			close (cnx);
			break;
		case 0:
			/* child returns */
			forks = 0;
			break;
		default:
			/* parent got child id */
			forks++;
			close (cnx);
			/* parent loops back to accept() */
			continue;
		}
		//
		// Child continues and will exit
		int exitval = 0;
		//
		// Have a kipservice structure with builtin memory pools
		kipt_ctx  kip;
		kipt_dctx kipsvc;
		if (!kipctx_open (&kip)) {
			perror ("!kipctx_open()");
			exitval = 1;
			goto endgame;
		}
		printf ("Opened KIP context\n");
		if (!kipdaemon_open (kip, &kipsvc)) {
			perror ("!kipdaemon_open()");
			exitval = 1;
			goto ctx_endgame;
		}
		printf ("Opened KIP Daemon context\n");
		//
		// Consume both cnx and kip into the handler call
		if (!kipdaemon_handle (kipsvc, cnx)) {
			perror ("!kipdaemon_handle()");
			close (cnx);
			exitval = 1;
			goto ctx_endgame;
		}
		printf ("Handled KIP Daemon connection\n");
		//
		// Close down the current service connection
		kipdaemon_close (kipsvc);
ctx_endgame:
		kipctx_close (kip);
endgame:
		close (cnx);
		//
		// Child ends with exit()
		exit (exitval);
	}
	//TODO// Might setsid() to allow immediate restart
	//
	// Wait until all child processes have closed
	printf ("Waiting for child processes to close down\n");
	signal (SIGCHLD, SIG_DFL);
	while (forks > 0) {
		int wstatus;
		waitpid (-1, &wstatus, 0);
		if (WIFEXITED (wstatus)) {
			forks--;
		}
	}
	//
	// Cleanup and close without error
	qsasl_fini ();
	kipdaemon_fini ();
	kip_fini ();
	close (sox);
	return 0;
}
