/* A SASL identity maker based on Quick SASL.
 *
 * This is a variation on the plain client, which specifically runs the
 * mechanism GS2-HAAN128-GENERATE to retrieve a new identity.
 *
 * The client can connect to an IP and port combination, where a server
 * would be listening.  When this is not requested, it will instead assume
 * traffic on stdin in a somewhat standard format.
 *
 * Lines are prefixed with either "c2s> " or "s2c> " to indicate the kind
 * of information and the traffic passed.  Plus, initially there is a
 * "mech> " prefix from server to client with the mechanism, and one from
 * the client back to the server.  When returning data with success, the
 * prompt "extra> " might also be used.  Any lines not recognised are
 * quietly dropped, except for a message about misrecognised prompts.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include <unistd.h>

#include <arpa2/socket.h>

#include <arpa2/quick-der.h>
#include <arpa2/quick-sasl.h>


#define DEFAULT_PORT "16832"


#include "sasl-common.c"



/* Main program.
 */
int main (int argc, char *argv []) {
	//
	// Harvest commandline arguments
	assert (argc > 0);
	if (argc > 3) {
		fprintf (stderr, "Usage: %s [serverIP [serverPort]]\n"
				"Default serverPort is " DEFAULT_PORT " but without serverIP, the user is the network\n",
				argv [0]);
		exit (1);
	}
	char *service = "haan-demo";
	bool tty_user_networking = (argc < 2);
	char *server_ip   = (argc >= 2) ? argv [1] : NULL        ;
	char *server_port = (argc >= 3) ? argv [2] : DEFAULT_PORT;
	//
	// Have streams for networking
	socket_init ();
	FILE *s2c_stream = stdin;
	FILE *c2s_stream = stdout;
	if (tty_user_networking) {
		printf ("\n*\n* You did not supply a network.  No problem.\n* Please copy/paste lines to the hsasl-server.\n*\n\n");
	} else {
		struct sockaddr_storage ss;
		memset (&ss, 0, sizeof (ss));
		if (!socket_parse (server_ip, server_port, &ss)) {
			fprintf (stderr, "Syntax error in host ip:port setting %s:%s\n", server_ip, server_port);
			exit (1);
		}
		int sox;
		if (!socket_client (&ss, SOCK_STREAM, &sox)) {
			perror ("Failed to connect");
			exit (1);
		}
		s2c_stream = fdopen (dup (sox), "r");
		c2s_stream = fdopen (     sox , "w");
	}
	printf ("--\n");
	fflush (stdout);
	//
	// Start the SASL session as a client.
	qsasl_init (NULL, "HAAN");
	QuickSASL sasl = NULL;
	qsaslt_state curstate = QSA_UNDECIDED;
	assert (qsasl_client (&sasl, service, cb_statechange, &curstate, 0));
	//
	// Be someone -- but then step down to the "demo1" alias
	membuf crs_user  = { .bufptr=(uint8_t*)"377e18219c7c406a0550ac0131bfc63a", .buflen=32  };
	//TODO// membuf crs_realm = { .bufptr="unicorn.demo.arpa2.org", .buflen=22 };
	membuf crs_realm;
	crs_realm.bufptr = (uint8_t*)getenv ("HAAN_REALM");
	crs_realm.buflen = (crs_realm.bufptr != NULL) ? strlen ((char*)crs_realm.bufptr) : 0;
	assert (qsasl_set_clientuser_login (sasl, crs_user ));
	assert (qsasl_set_clientuser_acl   (sasl, crs_user ));  //TODO//???//
	assert (qsasl_set_client_realm     (sasl, crs_realm));
	assert (qsasl_set_server_realm     (sasl, crs_realm));
	assert (0 == putenv ("QUICKSASL_PASSPHRASE=e7c9074165d74b7ff0a82d14131b4711"));
	//
	// Now wait for a list of mechanisms from the server
	static const char *only_mech [] = { "mech", NULL };
	const char *lineprompt;
	dercursor mechs;
	prompted_hex_recv (sasl, s2c_stream, only_mech, &lineprompt, &mechs);
	//
	// Insist on GS2-HAAN128-GENERATE as a mechanism
#if 0
	char *mth = memmem (mechs.derptr, mechs.derlen, "GS2-HAAN128-GENERATE", 20);
	if (((mth != mechs.derptr) && (mth [-1] != ' ')) ||
	    ((mth [17] != '\0') && ( mth [17] != ' '))) {
		printf ("Mechanism not found: GS2-HAAN128-GENERATE\n");
		exit (1);
	}
#endif
	membuf haanmech = { .bufptr = (uint8_t*)"GS2-HAAN128-GENERATE", .buflen = 20 };
	assert (qsasl_set_mech (sasl, haanmech));
	//
	// Start looping as a client
	dercursor s2c = {
		.derptr = NULL,
		.derlen = 0
	};
	dercursor mech_used = {
		.derptr = NULL,
		.derlen = 0
	};
	while (curstate == QSA_UNDECIDED) {
		//
		// Make a client step: s2c --> mech, c2s
		dercursor c2s;
		dercursor mech;
		assert (qsasl_step_client (sasl, crs2buf (s2c), crs2bufp (&mech), crs2bufp (&c2s)));
		//
		// If we received "extra" then we can check proper ending
		if (strcmp (lineprompt, "extra") == 0) {
			assert (mech.derptr == NULL);
			assert (curstate != QSA_UNDECIDED);
			break;
		}
		//
		// We optionally send "mech> " before "c2s> "
		if (mech.derptr != NULL) {
			prompted_hex_send (sasl, c2s_stream, "mech", mech);
			mech_used = mech;
		}
		//
		// Send c2s in HEX to the server
		prompted_hex_send (sasl, c2s_stream, "c2s", c2s);
		//
		// TODO: what if curstate != QSA_UNDECIDED
		if (curstate != QSA_UNDECIDED) {
			break;
		}
		//
		// Retrieve the HEX result from the server
		static const char *either_s2c_or_extra []  = { "s2c", "extra", NULL };
		prompted_hex_recv (sasl, s2c_stream, either_s2c_or_extra, &lineprompt, &s2c);
	}
	//
	// Print the mechanism used, if it was decided on
	if (mech_used.derptr != NULL) {
		printf ("Mechanism used was %.*s\n", (int)mech_used.derlen, mech_used.derptr);
	}
	//
	// Harvest username and password
	membuf got_userid  ;
	// membuf got_domain  ;
	membuf got_password;
	bool got = true;
	got = got && qsasl_get_client_userid        (sasl, &got_userid  );
	// got = got && qsasl_get_client_domain        (sasl, &got_domain  );
	got = got && qsasl_get_delegated_credential (sasl, &got_password);
	got = got && !mem_isnull (&got_userid);
	// got = got && !mem_isnull (&got_domain);
	got = got && !mem_isnull (&got_password);
	if (got) {
		printf ("Username: %.*s\nDomain:   %.*s\nPassword: %.*s\n",
				(int)got_userid  .buflen, got_userid  .bufptr,
				(int)crs_realm   .buflen, crs_realm   .bufptr,
				(int)got_password.buflen, got_password.bufptr);
	} else {
		printf ("No userid@domain and password pair was delegated\n");
	}
	//
	// Assuming GS2-HAAN128-GENERATE, retrieve user_login and password
	//
	// Cleanup & Closedown
	qsasl_close (&sasl);
	qsasl_fini ();
	socket_fini ();
	//
	// Report whether the exchange was successful
	printf ("\n*\n* The SASL exchange was a %s.  Application %s.\n*\n\n",
			(curstate == QSA_SUCCESS) ? "SUCCESS" : "FAILURE",
			(strcmp (lineprompt, "extra") == 0) ? "on server succeeded" : "will pass server verdict" );
	exit ((curstate == QSA_SUCCESS) ? 0 : 1);
}
