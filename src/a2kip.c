/* a2kip.c -- Management of the KIP Daemon.
 *
 * Use this progrm to manage a KIP Service in terms of the host names
 * that it can provide for.  Host names or realms are FQDNs, and must
 * contain a dot.  Service names are simple strings.  They should be
 * in lowercase.
 *
 * Virtual hosts are stored as keymaps stored as host-named files in a
 * directory reserved for a service.  The entries are very small, and
 * on file systems that can store inline data, they may well fit in the
 * inode entry.  For example on ext4, there can be 128 bytes, which is
 * enough for the longer encryption types in use today.
 *
 * Every service stores a master secret in a keytab.  This program needs
 * to access that same keytab.
 *
 * This program allows the creation and removal of virtual hosts for
 * KIP services.  Future versions may also support the import and export,
 * by passing the key in unmapped form through KIP encryption.  This would
 * benefit the longevity of service keys even under transfers of control.
 * Another angle to this might be to split and reconstruct the initial
 * private key, and/or virtual-host mapped keys.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2021 Rick van Rein <rick@openfortress.nl>
 */


#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <limits.h>
#include <time.h>
#include <fcntl.h>

#include <krb5.h>

#include <arpa2/kip.h>

#include <arpa2/util/cmdparse.h>


/* Though opcode_t is intended to carry an enum opcode
 * value, it is mapped to short for compatibility with
 * "cmdparse.h" generic definitions.
 */
typedef short opcode_t;

#define OPCODE_ADD     1
#define OPCODE_DEL     2
#define OPCODE_CREATE  1
#define OPCODE_DESTROY 2


/* Symbolic names for parsed variables; to be named in this order
 */
enum parse_variables {
	/* Location of the master key */
	VAR_KEYTAB,	/* KIP_KEYTAB, a file or some Kerberos-cache */
	VAR_VARDIR,	/* KIP_VARDIR, default "/var/lib/arpa2/kip" */
	VAR_SERVICE,	/* Service name to work for, default "kip" */
	VAR_ENCTYPE,	/* Kerberos Encryption Type for the master key */
	VAR_DOMAIN,	/* Domain name to host a service for */
	/* End marker */
	VAR_ENDMARKER
};


/* Values to be quickly referenced in the array
 */
#define VAL_KEYTAB	values [VAR_KEYTAB ]
#define VAL_VARDIR	values [VAR_VARDIR ]
#define VAL_SERVICE	values [VAR_SERVICE]
#define VAL_ENCTYPE	values [VAR_ENCTYPE]
#define VAL_DOMAIN	values [VAR_DOMAIN ]


/* Flags to compose in the combinations
 */
#define FLAG_KEYTAB	(1 << VAR_KEYTAB )
#define FLAG_VARDIR	(1 << VAR_VARDIR )
#define FLAG_SERVICE	(1 << VAR_SERVICE)
#define FLAG_ENCTYPE	(1 << VAR_ENCTYPE)
#define FLAG_DOMAIN	(1 << VAR_DOMAIN )



/* Ask for confirmation, return true when it was given.
 */
bool confirm (char *warning) {
	printf ("###\n### DANGEROUS OPERATION REQUIRES CONFIRMATION\n###\n\n%s\n", warning);
	if ((!isatty (0)) || (!isatty (1))) {
		fprintf (stderr, "This action cannot be confirmed without interaction; bailing out\n");
		return false;
	}
	char *conFIRM = getpass ("\n### Enter the word \"conFIRM\": ");
	return (strcmp (conFIRM, "conFIRM") == 0);
}


/* Compute a nice kvno value based on YYYYMMDD
 */
uint32_t kvno_YYYYMMDD (void) {
	time_t now;
	struct tm now_tm;
	time (&now);
	if (gmtime_r (&now, &now_tm) == NULL) {
		fprintf (stderr, "Failed to construct a kvno form YYYYMMDD\n");
		exit (1);
	}
	return (19000100 + now_tm.tm_year * 10000 + now_tm.tm_mon * 100 + now_tm.tm_mday);
}


/* Create or destroy a master key for this host -- with great care
 *
 * Use the "keytab" parameter or fallback to $KIP_KEYTAB.
 * With neither set, parameter "vardir" is tried, or else $KIP_VARDIR.
 * Do not fall back to Kerberos' default.
 *
 * Use the "service" parameter which defaults to "kip".
 *
 * Return success.
 */
bool master_ops (opcode_t opcode, struct cmdparser *prs, const char *usage, int argc, char *argv []) {
	krb5_context k5ctx;
	krb5_keytab k5kt;
	krb5_error_code k5err;
	krb5_enctype k5enc;
	//
	// We only look at the opcode to distinguish create from destroy
	(void) usage;
	(void) argc;
	(void) argv;
	//
	// Fill defaults for missing parameters
	char ktbuf [MAX_KEYTAB_NAME_LEN + 3];
	if (prs->VAL_KEYTAB == NULL) {
		prs->VAL_KEYTAB = getenv ("KIP_KEYTAB");
	}
	if (prs->VAL_KEYTAB == NULL) {
		if (prs->VAL_VARDIR == NULL) {
			prs->VAL_VARDIR = getenv ("KIP_VARDIR");
		}
		if (prs->VAL_VARDIR == NULL) {
			prs->VAL_VARDIR = "/var/lib/arpa2/kip";
		}
		if (prs->VAL_VARDIR != NULL) {
			snprintf (ktbuf, MAX_KEYTAB_NAME_LEN + 1,
				"FILE:%s/master.keytab", prs->VAL_VARDIR);
		}
		prs->VAL_KEYTAB = ktbuf;
	}
	if (prs->VAL_SERVICE == NULL) {
		prs->VAL_SERVICE = "kip";
	}
	char *string_k5enc = "aes256-cts-hmac-sha384-192";
	if (prs->VAL_ENCTYPE == NULL) {
		prs->VAL_ENCTYPE = string_k5enc;
	}
	//
	// Be sure to have the Kerberos Encryption Type
	k5err = krb5_string_to_enctype (prs->VAL_ENCTYPE, &k5enc);
	if (k5err != 0) {
		fprintf (stderr, "Failure resolving Kerberos Encryption Type %s\nDefault is %s, other names are IANA-registered at\nhttps://www.iana.org/assignments/kerberos-parameters/kerberos-parameters.xhtml\n", prs->VAL_ENCTYPE, string_k5enc);
		goto fail;
	}
	//
	// Be sure to have the KIP_VARDIR
	if (prs->VAL_VARDIR != NULL) {
		if (access (prs->VAL_VARDIR, X_OK | W_OK | R_OK) != 0) {
			fprintf (stderr, "Failed to access KIP variable directory %s\n", prs->VAL_VARDIR);
			goto fail;
		}
	}
	//
	// Construct the vhost directory name
	char vhostkeydirname [PATH_MAX + 3];
	ssize_t vhkdnlen = snprintf (vhostkeydirname, PATH_MAX + 2,
				"%s/%s-vhost",
				prs->VAL_VARDIR,
				prs->VAL_SERVICE);
	if ((vhkdnlen > PATH_MAX) || (vhkdnlen <= 0)) {
		fprintf (stderr, "Path name to service directory too long\n");
		goto fail;
	}
	//
	// Open the Kerberos context
	k5err = krb5_init_context (&k5ctx);
	if (k5err != 0) {
		fprintf (stderr, "Failed to initialise Kerberos context\n");
		goto fail;
	}
	//
	// Test if the master key currently exists (same logic as in kipd)
	k5err = krb5_kt_resolve (k5ctx, prs->VAL_KEYTAB, &k5kt);
	if (k5err != 0) {
		fprintf (stderr, "Failed opening keytab %s\n", prs->VAL_KEYTAB);
		goto fail_k5;
	}
	//
	// Build a PrincipalName for the master key, "kip/MASTER@." for kip
	krb5_principal k5name;
	k5err = krb5_build_principal (k5ctx, &k5name,
			1, ".",
			prs->VAL_SERVICE,
			"MASTER",
			NULL);
	if (k5err != 0) {
		goto fail_k5_kt;
	}
	//
	// Test if the master key is already known
	krb5_keytab_entry k5oldent;
	k5err = krb5_kt_get_entry (k5ctx, k5kt, k5name, 0, 0, &k5oldent);
	switch (opcode) {
	case OPCODE_CREATE:
		if (k5err == 0) {
			fprintf (stderr, "There already is a master key for %s\n", prs->VAL_SERVICE);
			goto fail_k5_kt_princ;
		}
		break;
	case OPCODE_DESTROY:
		if (k5err != 0) {
			fprintf (stderr, "Failed to locate the master key for %s\n", prs->VAL_SERVICE);
			goto fail_k5_kt_princ;
		}
		break;
	}
	//
	// Add or delete the master key
	krb5_keytab_entry k5newent;
	memset (&k5newent, 0, sizeof (k5newent));
	switch (opcode) {
	case OPCODE_CREATE:
		k5newent.principal = k5name;
		k5newent.vno = kvno_YYYYMMDD ();
		k5err = krb5_c_make_random_key (k5ctx, k5enc, &k5newent.key);
		if (k5err != 0) {
			fprintf (stderr, "Failed to generate master key for %s\n", prs->VAL_SERVICE);
			goto fail_k5_kt_princ_newent;
		}
		k5err = krb5_kt_add_entry (k5ctx, k5kt, &k5newent);
		if (k5err != 0) {
			fprintf (stderr, "Failed to add master key for %s to keytab\n", prs->VAL_SERVICE);
			goto fail_k5_kt_princ_newent;
		}
		break;
	case OPCODE_DESTROY:
		if (confirm ("You are about to remove a master key.\nAfter that the service will no longer work.\n")) {
			k5err = krb5_kt_remove_entry (k5ctx, k5kt, &k5oldent);
			if (k5err != 0) {
				fprintf (stderr, "Failed to remove the master key for %s\n", prs->VAL_SERVICE);
				goto fail_k5_kt_princ;
			}
			printf ("The master key for %s was removed from keytab %s\n", prs->VAL_SERVICE, prs->VAL_KEYTAB);
		} else {
			printf ("The action was cancelled\n");
		}
		break;
	}
	//
	// Make an effort at key administration
	char rmcmd [20 + PATH_MAX];
	switch (opcode) {
	case OPCODE_CREATE:
		if ((mkdir (vhostkeydirname, S_IRUSR | S_IWUSR | S_IXUSR) != 0) && (errno != EEXIST)) {
			perror ("Virtual host directory not created");
			fprintf (stderr, "Not fatal.  Please create it at %s\n", vhostkeydirname);
		}
		break;
	case OPCODE_DESTROY:
		sprintf (rmcmd, "rm -rf '%s'", vhostkeydirname);
		if (system (rmcmd) != 0) {
			fprintf (stderr, "Failed to remove the (now useless) key material.  Not fatal.\nPlease destroy it at %s\n", vhostkeydirname);
		}
		printf ("Virtual host keys were rendered useless, and have been deleted\n");
		break;
	}
	//
	// Discard the principal name
	krb5_free_principal (k5ctx, k5name);
	//
	// Close the changed keytab
	k5err = krb5_kt_close (k5ctx, k5kt);
	memset (&k5newent, 0, sizeof (k5newent));
	if (k5err != 0) {
		fprintf (stderr, "Failed while closing keytab %s\n", prs->VAL_KEYTAB);
		goto fail_k5;
	}
	//
	// Close Kerberos
	krb5_free_context (k5ctx);
	//
	// Report success
	return true;
	//
	// Report failure
fail_k5_kt_princ_newent:
	memset (&k5newent, 0, sizeof (k5newent));
fail_k5_kt_princ_ent:
	krb5_kt_free_entry (k5ctx, &k5oldent);
fail_k5_kt_princ:
	krb5_free_principal (k5ctx, k5name);
fail_k5_kt:
	krb5_kt_close (k5ctx, k5kt);
fail_k5:
	krb5_free_context (k5ctx);
fail:
	return false;
}


/* Add or delete a virtual domain for a service.
 *
 * Locate the "keytab" as in the "master" command.
 *
 * Use the "vardir" with $KIP_VARDIR or "/var/lib/arpa2/kip" as fallback
 * to decide where the SERVICE-vhost files are stored.
 *
 * The service defaults to "kip".
 *
 * Return success.
 */
bool virtual_ops (opcode_t opcode, struct cmdparser *prs, const char *usage, int argc, char *argv []) {
	krb5_error_code k5err;
	krb5_enctype k5enc;
	//
	// We only look at the opcode to distinguish create from destroy
	(void) usage;
	(void) argc;
	(void) argv;
	//
	// Fill defaults for missing parameters
	char ktbuf [MAX_KEYTAB_NAME_LEN + 3];
	if (prs->VAL_KEYTAB == NULL) {
		prs->VAL_KEYTAB = getenv ("KIP_KEYTAB");
	}
	if (prs->VAL_VARDIR == NULL) {
		prs->VAL_VARDIR = getenv ("KIP_VARDIR");
	}
	if (prs->VAL_VARDIR == NULL) {
		prs->VAL_VARDIR = "/var/lib/arpa2/kip";
	}
	if (prs->VAL_KEYTAB == NULL) {
		if (prs->VAL_VARDIR != NULL) {
			snprintf (ktbuf, MAX_KEYTAB_NAME_LEN + 1,
				"FILE:%s/master.keytab", prs->VAL_VARDIR);
		}
		prs->VAL_KEYTAB = ktbuf;
	}
	if (prs->VAL_SERVICE == NULL) {
		prs->VAL_SERVICE = "kip";
	}
	char *string_k5enc = "aes256-cts-hmac-sha384-192";
	if (prs->VAL_ENCTYPE == NULL) {
		prs->VAL_ENCTYPE = string_k5enc;
	}
	//
	// Be sure to have the Kerberos Encryption Type
	k5err = krb5_string_to_enctype (prs->VAL_ENCTYPE, &k5enc);
	if (k5err != 0) {
		fprintf (stderr, "Failure resolving Kerberos Encryption Type %s\nDefault is %s, other names are IANA-registered at\nhttps://www.iana.org/assignments/kerberos-parameters/kerberos-parameters.xhtml\n", prs->VAL_ENCTYPE, string_k5enc);
		goto fail;
	}
	//
	// Checks on DOMAIN grammar
	if (strchr (prs->VAL_DOMAIN, '/') != NULL) {
		fprintf (stderr, "Domain names cannot contain slashes: %s\n", prs->VAL_DOMAIN);
		goto fail;
	}
	if (prs->VAL_DOMAIN [0] == '.') {
		fprintf (stderr, "Domain names cannot start with a dot: %s\n", prs->VAL_DOMAIN);
		goto fail;
	}
	if (strchr (prs->VAL_DOMAIN, '.') == NULL) {
		fprintf (stderr, "Domain names need to be fully qualified: %s\n", prs->VAL_DOMAIN);
		goto fail;
	}
	if (prs->VAL_DOMAIN [strlen (prs->VAL_DOMAIN) - 1] == '.') {
		fprintf (stderr, "Domain names cannot be specified with a trailing dot: %s\n", prs->VAL_DOMAIN);
		goto fail;
	}
	//
	// Maybe later: Check if the hostname has an SRV record
	;
	//
	// Setup envvar KIP_VARDIR for KIP, from prs->VAL_VARDIR
	if (prs->VAL_VARDIR != NULL) {
		setenv ("KIP_VARDIR", prs->VAL_VARDIR, 1);
	}
	//
	// Construct the vhostkeyfilename
	char vhostkeyfilename [PATH_MAX + 3];
	ssize_t vhkfnlen = snprintf (vhostkeyfilename, PATH_MAX + 2,
				"%s/%s-vhost/%s",
				prs->VAL_VARDIR,
				prs->VAL_SERVICE,
				prs->VAL_DOMAIN);
	if ((vhkfnlen > PATH_MAX) || (vhkfnlen <= 0)) {
		fprintf (stderr, "Path name to keymap for virtual domain too long\n");
		goto fail;
	}
	//
	// Check if the presence of the vhostkeyfilename is as expected
	bool accessible = (access (vhostkeyfilename, F_OK) == 0);
	switch (opcode) {
	case OPCODE_ADD:
		if (accessible) {
			fprintf (stderr, "Virtual domain %s already has a key that would be destroyed\n", prs->VAL_DOMAIN);
			goto fail;
		}
		break;
	case OPCODE_DEL:
		if (!accessible) {
			fprintf (stderr, "Virtual domain %s has no key to remove\n", prs->VAL_DOMAIN);
			goto fail;
		}
		break;
	}
	//
	// We can now complete deletion
	if (opcode == OPCODE_DEL) {
		if (confirm ("You are about to remove all services for the virtual domain.\nAfter that, the service is no longer usable for that virtual domain.\n")) {
			if (unlink (vhostkeyfilename) != 0) {
				perror ("Failed to remove the virtual domain");
				fprintf (stderr, "Please remove the following file: %s\n", vhostkeyfilename);
				goto fail;
			}
		} else {
			printf ("The action was cancelled\n");
		}
		return true;
	}
	//
	// Continue into addition code
	;
	//
	// Initialise KIP
	kipt_ctx kip;
	kip_init ();
	if (!kipctx_open (&kip)) {
		fprintf (stderr, "Failed to open KIP context\n");
		goto fail;
	}
	//
	// Load the master key from the keytab
	kipt_keyid master;
	if (!kipkey_fromkeytab (kip,
			prs->VAL_KEYTAB, prs->VAL_SERVICE,
			NULL, ".",
			0, 0,
			&master)) {
		fprintf (stderr, "Failed to load keytab entry for %s\n", prs->VAL_SERVICE);
		goto fail_kip;
	}
	//
	// Construct a random key
	kipt_keyid vhk;
	if (!kipkey_generate (kip, k5enc, kvno_YYYYMMDD (), &vhk)) {
		fprintf (stderr, "Failed to generate a virtual host key for %s\n", prs->VAL_DOMAIN);
		goto fail_kip;
	}
	//
	// Map the random key
	uint8_t mapbuf [2048+2];
	uint32_t mapbufsz = sizeof (mapbuf) - 2;
	if (!kipkey_tomap (kip, vhk, 1, &master, mapbuf, mapbufsz, &mapbufsz)) {
		fprintf (stderr, "Failed to map virtual host key for %s\n", prs->VAL_DOMAIN);
		goto fail_kip;
	}
	//
	// Write the virtual hostname key file
printf ("Writing to %s\n", vhostkeyfilename);
	int mapfd = open (vhostkeyfilename, O_CREAT | O_WRONLY, S_IRUSR);
	if (mapfd < 0) {
		perror ("Failed to write vritual host keymap");
		goto fail_kip;
	}
	if (write (mapfd, mapbuf, mapbufsz) != mapbufsz) {
		perror ("Incomplete write for virtual host keymap");
		close (mapfd);
		unlink (vhostkeyfilename);
		goto fail_kip;
	}
	close (mapfd);
	//
	// Close the KIP Context
	kipctx_close (kip);
	//
	// Finish the KIP library
	kip_fini ();
	//
	// Return success
	return true;
	//
	// Report failure
fail_kip:
	kipctx_close (kip);
fail:
	return false;
}



/*
 * Usage & Grammar for a2kip master create|destroy
 */

static const char master_usage [] = "... Master key management for services\n"
	"master create  ...  To create    a master key for a new service\n"
	"master destroy ...  To destroy the master key for a     service\n";

static const char master_creadel_usage [] = "[service SERVICE]\n"
	"[vardir VARDIR|keytab KEYTAB] [enctype ENCTYPE]";

static const uint32_t master_creadel_combis [] = {
	/* Allow VARDIR _or_ KEYTAB _or_ neither */
	FLAG_VARDIR | FLAG_KEYTAB, FLAG_VARDIR,
	FLAG_VARDIR | FLAG_KEYTAB, FLAG_KEYTAB,
	FLAG_VARDIR | FLAG_KEYTAB, 0,
	/* end marker */
	0
};

static const struct cmdparse_grammar master_creadel_grammar = {
	.keywords = {
		"keytab",	// keytab
		"vardir",	// vardir
		"service",	// service
		"enctype",	// enctype
		NULL,		// domain
	},
	.listwords = 0,
	.combinations = master_creadel_combis,
};

static const struct cmdparse_action master_grammar [] = {
	{ "create",  OPCODE_CREATE,  &master_creadel_grammar, master_creadel_usage, master_ops },
	{ "destroy", OPCODE_DESTROY, &master_creadel_grammar, master_creadel_usage, master_ops },
		/* end marker */
	{ NULL, 0, NULL, NULL, NULL }
};



/*
 * Usage & Grammar for a2kip virtual add|del
 */

static const char virtual_usage [] = "... Master key management for services\n"
	"virtual add  ...  To add    a virtual domain to   a service\n"
	"virtual del  ...  To remove a virtual domain from a service\n";

static const char virtual_adddel_usage [] = "domain DOMAIN\n"
	"[service SERVICE] [vardir VARDIR] [keytab KEYTAB]";

static const uint32_t virtual_adddel_combis [] = {
	/* Require DOMAIN */
	FLAG_DOMAIN, FLAG_DOMAIN,
	/* end marker */
	0
};

static const struct cmdparse_grammar virtual_adddel_grammar = {
	.keywords = {
		"keytab",	// keytab
		"vardir",	// vardir
		"service",	// service
		"enctype",	// enctype
		"domain",	// domain
	},
	.listwords = 0,
	.combinations = virtual_adddel_combis,
};

static const struct cmdparse_action virtual_grammar [] = {
	{ "add", OPCODE_ADD, &virtual_adddel_grammar, virtual_adddel_usage, virtual_ops },
	{ "del", OPCODE_DEL, &virtual_adddel_grammar, virtual_adddel_usage, virtual_ops },
		/* end marker */
	{ NULL, 0, NULL, NULL, NULL }
};



/*
 * Usage & Grammar for a2kip
 */

static const char *a2kip_usage = "...  Manages KIP and derived services (like HAAN)\n"
	"master  ...  Manages master  keys  for services like KIP or HAAN\n"
	"virtual ...  Manages virtual hosts for services like KIP or HAAN\n";

static const struct cmdparse_class a2kip_grammar [] = {
	{ "master",   master_usage,  master_grammar },
	{ "virtual", virtual_usage, virtual_grammar },
		/* end marker */
	{ NULL, NULL, NULL }
};


/*
 * Main program, mostly activation of parser and subcommands.
 */
int main (int argc, char *argv []) {
	//
	// Invoke the cmdparser for <class> <action> [<keyword> <value>]...
	bool ok = cmdparse_class_action (argc, argv, a2kip_usage, a2kip_grammar);
	//
	// Return 1 for failure or 0 for success
	return (ok ? 0 : 1);
}

