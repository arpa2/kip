/* A SASL client based on Quick SASL.
 *
 * This is similar to the sasl-sample-client provided with Cyrus SASL2,
 * but we use different prompts and switched from BASE64 to HEX for
 * reasons of human readability (the term "human" is debatable, grinn).
 * When translating, also note the different mechanism lists; these are
 * not standardised as part of SASL, so each makes up their own.
 *
 * The client can connect to an IP and port combination, where a server
 * would be listening.  When this is not requested, it will instead assume
 * traffic on stdin in a somewhat standard format.
 *
 * Lines are prefixed with either "c2s> " or "s2c> " to indicate the kind
 * of information and the traffic passed.  Plus, initially there is a
 * "mech> " prefix from server to client with the mechanism, and one from
 * the client back to the server.  When returning data with success, the
 * prompt "extra> " might also be used.  Any lines not recognised are
 * quietly dropped, except for a message about misrecognised prompts.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 * SPDX-FileCopyrightText: Copyright 2020 Henri Manson <info@mansoft.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include <unistd.h>

extern "C"
{
#include <arpa2/quick-der.h>
#include <arpa2/quick-sasl.h>
}


#define DEFAULT_PORT "16832"


#include "sasl-common.cpp"



/* Main program.
 */
int main (int argc, char *argv []) {
	//
	// Harvest commandline arguments
	assert (argc > 0);
	if (argc > 3) {
		fprintf (stderr, "Usage: %s [serverIP [serverPort]]\n"
				"Default serverPort is " DEFAULT_PORT " but without serverIP, the user is the network\n",
				argv [0]);
		exit (1);
	}
	const char *service = "qsasl-xsasl-demo";
	bool tty_user_networking = (argc < 2);
	const char *server_ip   = (argc >= 2) ? argv [1] : NULL        ;
	const char *server_port = (argc >= 3) ? argv [2] : DEFAULT_PORT;
	//
	// Have streams for networking
	std::iostream* stream = NULL;
	std::istream* s2c_stream = &std::cin;
	std::ostream* c2s_stream = &std::cout;
	if (tty_user_networking) {
		printf ("\n*\n* You did not supply a network.  No problem.\n* Please copy/paste lines to the qsasl-server or xsasl-server.\n*\n\n");
	} else {
		stream = get_socket_stream(server_ip, server_port);
		
		s2c_stream = stream;
		c2s_stream = stream;
	}
	printf ("--\n");
	fflush (stdout);
	//
	// Start the SASL session as a client.
	qsasl_init (NULL, "Hosted_Identity");
	QuickSASL sasl = NULL;
	qsaslt_state curstate = QSA_UNDECIDED;
	assert (qsasl_client (&sasl, service, cb_statechange, &curstate, 0));
	//
	// Be someone -- including step down to the ACL user
	char *str_login = getenv ("KIPSERVICE_CLIENTUSER_LOGIN");
	char *str_acl   = getenv ("KIPSERVICE_CLIENTUSER_ACL"  );
	char *str_realm = getenv ("KIPSERVICE_CLIENT_REALM"    );
	char *str_servr = getenv ("KIP_REALM"                  );
	dercursor crs_login = { .derptr=(uint8_t*) str_login, .derlen=strlen (str_login) };
	dercursor crs_acl   = { .derptr=(uint8_t*) str_acl  , .derlen=strlen (str_acl  ) };
	dercursor crs_realm = { .derptr=(uint8_t*) str_realm, .derlen=strlen (str_realm) };
	dercursor crs_servr = { .derptr=(uint8_t*) str_servr, .derlen=strlen (str_servr) };
	assert (qsasl_set_clientuser_login (sasl, crs_login));
	assert (qsasl_set_clientuser_acl   (sasl, crs_acl  ));
	assert (qsasl_set_client_realm     (sasl, crs_realm));
	assert (qsasl_set_server_realm     (sasl, crs_servr));
	//
	// Now wait for a list of mechanisms from the server
	static const char *only_mech [] = { "mech", NULL };
	const char *lineprompt;
	dercursor mechs;
	prompted_hex_recv (sasl, s2c_stream, only_mech, &lineprompt, &mechs);
	assert (qsasl_set_mech (sasl, mechs));
	//
	// Start looping as a client
	dercursor s2c = {
		.derptr = NULL,
		.derlen = 0
	};
	dercursor mech_used = {
		.derptr = NULL,
		.derlen = 0
	};
	while (curstate == QSA_UNDECIDED) {
		//
		// Make a client step: s2c --> mech, c2s
		dercursor c2s;
		dercursor mech;
		assert (qsasl_step_client (sasl, s2c, &mech, &c2s));
		//
		// If we received "extra" then we can check proper ending
		if (strcmp (lineprompt, "extra") == 0) {
			assert (mech.derptr == NULL);
			assert (curstate != QSA_UNDECIDED);
			break;
		}
		//
		// We optionally send "mech> " before "c2s> "
		if (mech.derptr != NULL) {
			prompted_hex_send (sasl, c2s_stream, "mech", mech);
			mech_used = mech;
		}
		//
		// Send c2s in HEX to the server
		prompted_hex_send (sasl, c2s_stream, "c2s", c2s);
		//
		// TODO: what if curstate != QSA_UNDECIDED
		if (curstate != QSA_UNDECIDED) {
			break;
		}
		//
		// Retrieve the HEX result from the server
		static const char *either_s2c_or_extra []  = { "s2c", "extra", NULL };
		prompted_hex_recv (sasl, s2c_stream, either_s2c_or_extra, &lineprompt, &s2c);
	}
	//
	// Print the mechanism used, if it was decided on
	if (mech_used.derptr != NULL) {
		printf ("Mechanism used was %.*s\n", mech_used.derlen, mech_used.derptr);
	}
	//
	// Cleanup & Closedown
	qsasl_close (&sasl);
	qsasl_fini ();
	//
	// Report whether the exchange was successful
	printf ("\n*\n* The SASL exchange was a %s.  Application %s.\n*\n\n",
			(curstate == QSA_SUCCESS) ? "SUCCESS" : "FAILURE",
			(strcmp (lineprompt, "extra") == 0) ? "on server succeeded" : "will pass server verdict" );
	if (stream != 0) {
		delete stream;
	}
	exit ((curstate == QSA_SUCCESS) ? 0 : 1);
}
