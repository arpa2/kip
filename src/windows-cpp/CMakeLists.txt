PROJECT(src-cpp C CXX)

find_package (Boost COMPONENTS system)

add_executable (qsasl-client-cpp
	qsasl-client.cpp
)
add_executable (qsasl-server-cpp
	qsasl-server.cpp
)
target_link_libraries (qsasl-client-cpp pthread ${Boost_LIBRARIES} quicksasl_shared ARPA2::Quick-DER)
target_link_libraries (qsasl-server-cpp pthread ${Boost_LIBRARIES} quicksasl_shared ARPA2::Quick-DER)
use_com_err_table(kip TARGETS qsasl-client-cpp qsasl-server-cpp)
