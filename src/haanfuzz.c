/* HAAN fuzz tester.  Takes in two strings and computes their
 * Damerau-Levenshtein distance -- in terms of entropy consumed.
 * See contrib/cyrus-sasl2/haan_fuzz.c for details.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int haanfuzz_distance (const char *user_login, unsigned userlen_login,
                              const char *user_acl  , unsigned userlen_acl);

int main (int argc, char *argv []) {
	for (int i = 1; i < argc; i++) {
		for (int j = i + 1; j < argc; j++) {
			int dist = haanfuzz_distance (
					argv [i], strlen (argv [i]),
					argv [j], strlen (argv [j]));
			printf ("%5d from %15s to %15s\n",
					dist,
					argv [i], argv [j]);
		}
	}
}

