#!/usr/bin/env python3
#
# Test program for DiaSASL library for Python
#
# Called with an IP address, TCP port and optional fake chanbind value:
# ${diasasl-api-client} IP:DIASASL TCP:DIASASL [RNDHEX:CHANBIND]
#
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>


import os
import sys

# Relative path expansion, so we can import DiaSASL
#
here = os.path.dirname (os.path.realpath (__file__))
sys.path.append (os.path.abspath (here + '/../python'))

import diasasl


# Test commandline arguments
#
if len (sys.argv) not in [3,4]:
	sys.stderr.write ('Usage: %s host port [chanbindfake]\n' % sys.argv [0])
	sys.exit (1)
host =      sys.argv [1]
port = int (sys.argv [2])
chanbind = sys.argv [3] if len (sys.argv) >= 4 else None


# Subclass to override methods
#
class AuthnSession (diasasl.Session):

	def recv_success (self, extra):
		sys.stdout.write ('Authentication success!\n')

	def recv_failure (self, errno):
		sys.stdout.write ('Error: %s\n' % str (errno))

	def recv_mechlist (self, mech_list):
		sys.stdout.write ('Mechanism list: %r\n' % mech_list)
		# self.mech_choice = mech_list.split (' ') [0]
		self.mech_choice = 'ANONYMOUS'
		# self.mech_choice = 'PLAIN'
		sys.stdout.write ('Selected %r\n' % self.mech_choice)

	def recv_challenge (self, s2c):
		sys.stdout.write ('S2C Token: %r\n' % s2c)
		# sys.stdout.write ('Boldly responding with "Hello World"\n')
		sys.stdout.write ('Current success = %r, failure = %r\n' % (self.success, self.failure))
		# self.send_response (b'Hello World')
		self.send_response (b'%s\x00%s\x00%s' %
					(b'demo+ali',b'demo',b'sekreet'))



# Connect and create the first Session
#
n = diasasl.Node ( (host,port) )
s = AuthnSession (n, 'unicorn.demo.arpa2.org')

# Test gear
#
print ('Node = %r\nSession = %r' % (n,s) )
#
# n.recv (b'00123456789abcdef0123456789abcdef0123456789abcdef0')
# n.recv ()
while not (s.success or s.failure):
	n.poll (justone=True)
	print ('Polling ended...')

print ('Done')
