/* diasasl-api-client.c -- Client program for DiaSASL protocol
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Henri Manson <info@mansoft.nl>
 * SPDX-FileCopyrightText: Rick van Rein <rick@openfortress.nl>
 */

#include <stdio.h>
#include <signal.h>
#include <getopt.h>
#include <locale.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include <sasl/sasl.h>
#include <sasl/saslplug.h>
#include <sasl/saslutil.h>

#include <arpa2/socket.h>

#include <arpa2/quick-der.h>
#include <quick-der/Quick-SASL.h>

#include <arpa2/quick-diasasl.h>

#include <arpa2/xover-sasl.h>

#include "arpa2/kip.h"

#include "sasl-common.c"

#define DEFAULT_PORT "16832"

static char *conffile = NULL;

static char *program;

static diasasl_node_t dianode;

struct diasasl_data {
	struct diasasl_session diasasl;
	XoverSASL sasl;
	dercursor mechlist;
	dercursor s2c_token;
	bool success;
	bool failure;
};

/* Callback from diasasl.
 */
void diasasl_cb (struct diasasl_session *session,
		const int32_t *com_errno,
		char *opt_mechanism_list,
		uint8_t *s2c_token, uint32_t s2c_token_len) {
	struct diasasl_data *s = (struct diasasl_data *) session;
	assert (!s->success);
	assert (!s->failure);
	s->s2c_token.derptr = NULL;
	s->s2c_token.derlen = 0;
	if (com_errno == NULL ? false : *com_errno != 0) {
		s->failure = true;
		com_err (program, *com_errno, "Diameter SASL failure");
		diasasl_close (&dianode, session);	//TODO//FIND_AS_LINK//
	} else if (opt_mechanism_list != NULL) {
		assert (der_isnull (&s->mechlist));
		assert (der_isnull (&s->s2c_token));
		s->mechlist.derptr = strdup (opt_mechanism_list);
		s->mechlist.derlen = strlen (opt_mechanism_list);
	} else if (s2c_token != NULL) {
		assert (dermem_alloc (s, s2c_token_len, (void **) &s->s2c_token.derptr));
		memcpy (s->s2c_token.derptr, s2c_token, s2c_token_len);
		s->s2c_token.derlen = s2c_token_len;
	}
}

static int der_client(char *server_domain, char *server_ip, char *server_port, char *chan_bind, char *mechanism)
{
	struct sockaddr_storage ss;
	
	memset (&ss, 0, sizeof (ss));
	if (!socket_parse (server_ip, server_port, &ss)) {
		fprintf (stderr, "Syntax error in host ip:port setting %s:%s\n", server_ip, server_port);
		exit (1);
	}
	int sox;
	if (!socket_client (&ss, SOCK_STREAM, &sox)) {
		perror ("Failed to connect");
		exit (1);
	}

	memset(&dianode, 0, sizeof(dianode));
	dianode.socket = sox;
	//
	// Start the SASL session as a client.
	kip_init ();
	kipservice_init (NULL, NULL);
	diasasl_node_open (&dianode);
	xsasl_init (NULL, "InternetWide_Identity");
	//
	// Be someone -- including step down to the ACL user
	char *str_login = getenv ("KIPSERVICE_CLIENTUSER_LOGIN");
	char *str_acl   = getenv ("KIPSERVICE_CLIENTUSER_ACL"  );
	char *str_realm = getenv ("KIPSERVICE_CLIENT_REALM"    );
	char *str_servr = getenv ("KIP_REALM"                  );
	char *service   =         "RealmCrossover"        ;
	assert(service != NULL);

	dercursor crs_login = { .derptr=str_login, .derlen=strlen (str_login) };
	dercursor crs_acl   = { .derptr=str_acl  , .derlen=strlen (str_acl  ) };
	dercursor crs_realm = { .derptr=str_realm, .derlen=strlen (str_realm) };
	dercursor crs_servr = { .derptr=str_servr, .derlen=strlen (str_servr) };
	printf("Creating new sasl\n");
	fflush(stderr);
	//
	// Start the SASL session as a client.
	struct diasasl_data *sasl_data = NULL;
	assert (dermem_open (sizeof (*sasl_data), (void **) &sasl_data));

	assert (xsasl_client (&sasl_data->sasl, service, NULL, NULL, 0));
	assert (xsasl_set_clientuser_login (sasl_data->sasl, crs2buf (crs_login)));
	assert (xsasl_set_clientuser_acl   (sasl_data->sasl, crs2buf (crs_acl  )));
	assert (xsasl_set_client_realm     (sasl_data->sasl, crs2buf (crs_realm)));
	assert (xsasl_set_server_realm     (sasl_data->sasl, crs2buf (crs_servr)));
	membuf mem_chanbind;
	if (chan_bind != NULL) {
		uint8_t *cb = NULL;
		assertxt (qsasl_alloc (sasl_data->sasl, strlen(QSA_X_MANUAL_SESSION_KEY) + strlen(chan_bind) + 2, &cb), "Failed to allocate channel binding value");
		sprintf (cb, "%s:%s", QSA_X_MANUAL_SESSION_KEY, chan_bind);
		mem_chanbind.bufptr = cb;
		mem_chanbind.buflen = strlen (cb);
		assert (xsasl_set_chanbind (sasl_data->sasl, false, mem_chanbind));
	} else {
		mem_chanbind.bufptr = NULL;
		mem_chanbind.buflen = 0;
	}
	
	bool success = false;
	bool failure = false;

	sasl_data->diasasl.callback = diasasl_cb;
	diasasl_open (&dianode, &sasl_data->diasasl, server_domain, NULL);
	assert (diasasl_process (&dianode, true) == 0);

	assert(sasl_data->mechlist.derptr != NULL);
	printf("after diasasl_open, mechlist: %.*s\n", (int) sasl_data->mechlist.derlen, sasl_data->mechlist.derptr);
	if (mechanism != NULL) {
		printf("Using supplied mechanism: %s\n", mechanism);
		dercursor crs_mechanism = { .derptr=mechanism, .derlen=strlen (mechanism) };
		assert(xsasl_set_mech(sasl_data->sasl, crs_mechanism));
	} else {
		assert(xsasl_set_mech(sasl_data->sasl, sasl_data->mechlist));
	}
	for (;;) {
		dercursor mech;
		dercursor c2s;

		assert(xsasl_step_client(sasl_data->sasl, sasl_data->s2c_token, &mech, &c2s));

		assert(xsasl_get_outcome(sasl_data->sasl, &success, &failure));
		printf("xsasl_get_outcome, success: %d, failure: %d\n", success, failure);
		if (success || failure) {
			break;
		}

		char *opt_mech_choice = NULL;
		sasl_data->diasasl.chanbind     = NULL;
		sasl_data->diasasl.chanbind_len = 0;
		if (mech.derptr != NULL) {
			dermem_strdup (sasl_data, mech, &opt_mech_choice);
			printf("chosen mechanism: %s\n", opt_mech_choice);

			if (mem_chanbind.bufptr != NULL) {
				printf("setting channel binding: %s\n", chan_bind);
				sasl_data->diasasl.chanbind     = mem_chanbind.bufptr;
				sasl_data->diasasl.chanbind_len = mem_chanbind.buflen;
			}
		}

		diasasl_send (&dianode, &sasl_data->diasasl, opt_mech_choice, c2s.derptr, c2s.derlen);
		assert (diasasl_process (&dianode, true) == 0);
	}
	diasasl_close (&dianode, &sasl_data->diasasl);
	xsasl_close(&sasl_data->sasl);
	xsasl_fini ();
	diasasl_node_close (&dianode);
	printf("DONE!\n");
	printf("--\n");
	kipservice_fini ();
	kip_fini ();
	dermem_close ((void**) &sasl_data);
	sleep(1);
	socket_close(sox);
	return failure;
}

int main(int argc, char *argv[])
{
	int rc = -1;
	socket_init ();
	//
	// Harvest commandline arguments
	program = argv [0];
	if (argc >= 3)
	{
		char *server_domain = argv[1];
		char *server_ip     = argv[2];
		char *server_port   = (argc >= 4) ? argv[3] : DEFAULT_PORT;
		char *chan_bind     = (argc >= 5) ? argv[4] : NULL;
		char *mechanism     = (argc >= 6) ? argv[5] : NULL;

		rc = der_client(server_domain, server_ip, server_port, chan_bind, mechanism);
	} else {
		fprintf(stderr, "Usage: %s <server domain> <IP> [port [chanbindvalue [mechanism]]]\n", argv[0]);
	}
	socket_fini ();
	return rc;
}
