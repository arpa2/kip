/* A SASL server-side that proxies requests over Diameter SASL.
 *
 * This is similar to the sasl-sample-server provided with Cyrus SASL2,
 * but we use different prompts and switched from BASE64 to HEX for
 * reasons of human readability (the term "human" is debatable, grinn).
 * When translating, also note the different mechanism lists; these are
 * not standardised as part of SASL, so each makes up their own.
 *
 * The server can listen to an IP and port combination, where a client
 * would be connecting.  When this is not requested, it will instead assume
 * traffic on stdin in a somewhat standard format.
 *
 * Any lines not recognised are quietly dropped.
 * Lines are prefixed with either "c2s> " or "s2c> " to indicate the kind
 * of information and the traffic passed.  Plus, initially there is a
 * "mech> " prefix from server to client with the mechanism, and one from
 * the client back to the server.  When returning data with success, the
 * prompt "extra> " might also be used.  Any lines not recognised are
 * quietly dropped, except for a message about misrecognised prompts.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include <unistd.h>
#include <fcntl.h>

#include <ev.h>

#include <arpa2/socket.h>
#include <arpa2/com_err.h>
#include <arpa2/except.h>

#include <arpa2/quick-der.h>
#include <arpa2/quick-dermem.h>
#include <arpa2/quick-diasasl.h>



#define DEFAULT_PORT "16832"


#include "sasl-common.c"


static char *program;
static char *chan_bind;


static struct ev_loop *loop;
static struct ev_signal ev_interrupt;

static struct diasasl_node dianode;
static struct ev_io ev_dianode;

static int saslsox = -1;
static struct ev_io ev_saslsox;

struct diasasl_data {
	struct diasasl_session diasasl;
	dercursor mech_used;
	char *mech_choice;
	FILE *c2s_stream;
	FILE *s2c_stream;
	ev_io ev_c2s;
	bool send_mech_choice;
	//TODO//HINT_OR_ACTION_FOR_CONSOLE_CLOSEDOWN//
};


/* Send an authentication token, plus optional mechanism choice.
 *
 * This is both called after choosing from the mechanism list and
 * to continue the authentication exchange.  It starts by reading
 * from the "c2s>" prompt and then sends it out, counting on a
 * later callback.
 *
 * Note: This is not completely correct for an event-driven design.
 *       The sasl-common.c functionality uses fgets() which is a
 *       blocking call.  So, although triggered by an event and
 *       only one element will be read, a protocol abuse could
 *       lock up this process.  Since this is test functionality,
 *       we have not wasted the time to be perfect on this.
 */
void relay_c2s_token (struct diasasl_session *session) {
	struct diasasl_data *sasl = (struct diasasl_data *) session;
	//
	// Receive a response from the client
	static const char *only_c2s []  = { "c2s" , NULL };
	const char *_lineprompt;
	dercursor c2s;
	prompted_hex_recv (session, sasl->c2s_stream, only_c2s, &_lineprompt, &c2s);
	//
	// Make a server step: c2s -> s2c
	diasasl_send (&dianode, session,
			sasl->send_mech_choice ? sasl->mech_choice : NULL,
			c2s.derptr, c2s.derlen);
	sasl->send_mech_choice = false;
}


/* Close the session.
 */
void close_diasasl_session (struct diasasl_session *session) {
	struct diasasl_data *sasl = (struct diasasl_data *) session;
	ev_io_stop (loop, &sasl->ev_c2s);
	diasasl_close (&dianode, session);	//TODO//FIND_AS_LINK//
	fclose (sasl->c2s_stream);
	fclose (sasl->s2c_stream);
	dermem_close ((void **) &sasl);
}


/* Callback responds to SASL authentication request.
 */
void diasasl_cb_auth (struct diasasl_session *session,
		const int32_t *com_errno,
		char *opt_mechanism_list,
		uint8_t *s2c_token, uint32_t s2c_token_len) {
	struct diasasl_data *sasl = (struct diasasl_data *) session;
	assert (opt_mechanism_list == NULL);
	dercursor s2c;
	s2c.derptr = s2c_token;
	s2c.derlen = s2c_token_len;
	if (com_errno == NULL) {
		//
		// Erase once-sent data for Channel Binding
		session->chanbind     = NULL;
		session->chanbind_len = 0;
		//
		// Send output to the client; initially "mech> " and later "s2c> "
		prompted_hex_send (sasl, sasl->s2c_stream, "s2c", s2c);
		//
		// Continue asking and relaying a "c2s>" prompted token
		// Rely on the event loop for this; it calls back relay_c2s_token()
	} else {
		if (*com_errno != 0) {
			//
			// Report an error
			com_err (program, *com_errno, "diasasl.authn failure");
		} else {
			//
			// Optionally send "extra>" with an addtional token upon success
			if (!der_isnull (&s2c)) {
				prompted_hex_send (sasl, sasl->s2c_stream, "extra", s2c);
			}
		}
		//
		// Determine the mechanism used, if it was one, and the clientid
		fprintf (sasl->s2c_stream, "Client identity (client_userid) is %s\n", session->client_userid);
		fprintf (sasl->s2c_stream, "Client identity (client_domain) is %s\n", session->client_domain);
		if (!der_isnull (&sasl->mech_used)) {
			printf ("Mechanism used was %.*s\n", (int) sasl->mech_used.derlen, sasl->mech_used.derptr);
		}
		//
		// Report whether the exchange was successful
		printf ("\n*\n* The SASL exchange was a %s.  Application %s the client.\n*\n\n",
				(*com_errno == 0) ? "SUCCESS" : "FAILURE",
				der_isnull (&s2c) ? "will tell" : "has told");
		//
		// Send an explicit EOF signal to be detected as failure
		dercursor eof_null = { .derptr = NULL, .derlen = 0 };
		prompted_hex_send (sasl, sasl->s2c_stream, "eof", eof_null);
		//
		// Cleanup the DiaSASL session
		close_diasasl_session (session);
	}
}


/* Callback responds to DiaSASL session open.
 */
void diasasl_cb_open (struct diasasl_session *session,
		const int32_t *com_errno,
		char *opt_mechanism_list,
		uint8_t *s2c_token, uint32_t s2c_token_len) {
	struct diasasl_data *sasl = (struct diasasl_data *) session;
	if (com_errno == NULL) {
		//
		// Ensure proper formats
		assert (opt_mechanism_list != NULL);
		assert (s2c_token == NULL);
		//
		// Setup channel binding information
		//TODO// Normally based on TLS and client type selection
		if (chan_bind != NULL) {
			uint8_t *cb = NULL;
			assertxt (mem_alloc (sasl, strlen(QSA_X_MANUAL_SESSION_KEY) + strlen(chan_bind) + 2, (void **) &cb), "Failed to allocate channel binding value");
			sprintf (cb, "%s:%s", QSA_X_MANUAL_SESSION_KEY, chan_bind);
			sasl->diasasl.chanbind     = cb;
			sasl->diasasl.chanbind_len = strlen ( cb );
		} else {
			sasl->diasasl.chanbind     = NULL;
			sasl->diasasl.chanbind_len = 0;
		}
		//
		// Store the mechanism list
		dercursor mechlist;
		mechlist.derptr =         opt_mechanism_list ;
		mechlist.derlen = strlen (opt_mechanism_list);
		//
		// Now provide a list of mechanisms for the client
		// Mechanism lists are not standardised but these demos pass them as
		// a string with spaces between the standardised mechanism names.
		prompted_hex_send (sasl, sasl->s2c_stream, "mech", mechlist);
		//
		// Await the response from the client, in the form of a "mech> " prompt
		static const char *only_mech [] = { "mech", NULL };
		const char *_lineprompt;
		dercursor mech_used;
		prompted_hex_recv (sasl, sasl->c2s_stream, only_mech, &_lineprompt, &mech_used);
		char *opt_mech_choice = NULL;
		assert (dermem_strdup (sasl, mech_used, &opt_mech_choice));
		//
		// Enter into authentication, redirecting future callbacks
		sasl->diasasl.callback = diasasl_cb_auth;
		sasl->mech_choice = opt_mech_choice;
		sasl->send_mech_choice = true;
		//
		// Do not rely on the event loop yet; it may not call back relay_c2s_token()
		// if it is already in the buffer of the c2s_stream and then block
		relay_c2s_token (session);
	} else {
		//
		// Report any error
		if (*com_errno != 0) {
			com_err (program, *com_errno, "diasasl.open failure");
		}
		//
		// Cleanup the DiaSASL session
		close_diasasl_session (session);
	}
}


/* Callback from event loop to read from a diasasl socket.
 *
 * This passes control to the diasasl_process() API call, which will
 * iterate on the non-blocking socket until nothing more can be read.
 * Any messages received will trigger the diasasl callbacks above.
 */
void event_dispatch_diasasl_answer (EV_P_ ev_io *evio, int revents) {
	assert (dianode.socket >= 0);
	int32_t com_errno = diasasl_process (&dianode, false);
	if (com_errno != 0) {
		com_err (program, com_errno, "diasasl_process() error");
		diasasl_break (&dianode, com_errno);
		close (dianode.socket);
		dianode.socket = -1;
	}
}


/* Callback from event loop to read from a sasl application socket.
 *
 * This retrieves information from the application-side socket,
 * to pass data from the client side of the SASL exchange.
 *
 * TODO: Use relay_c2s_token() or a variety.
 */
void event_c2s_input (EV_P_ ev_io *evio, int revents) {
	struct diasasl_data *sasl = (struct diasasl_data *) evio->data;
	relay_c2s_token (&sasl->diasasl);
}


/* Callback from event loop to accept a new incoming connection.
 */
void event_accept_connection (EV_P_ ev_io *evio, int revents) {
	//
	// Accept a new incoming connection (if it is still pending)
	struct sockaddr_storage sa;
	socklen_t salen = sizeof (sa);
	int saslcnx = accept (saslsox, (struct sockaddr *) &sa, &salen);
	if (saslcnx < 0) {
		return;
	}
	//
	// Start the SASL session as a client.
	struct diasasl_data *sasl = NULL;
	assert (dermem_open (sizeof (*sasl), (void **) &sasl));
	//
	// Fill out the bidirectional streams
	int fd_c2s = dup (saslcnx);
	int fd_s2c =      saslcnx ;
	sasl->c2s_stream = fdopen (fd_c2s, "r");
	sasl->s2c_stream = fdopen (fd_s2c, "w");
	//
	// Connect to the event loop
	ev_io_init (&sasl->ev_c2s, event_c2s_input, fd_c2s, EV_READ);
	sasl->ev_c2s.data = (void *) sasl;
	ev_io_start (loop, &sasl->ev_c2s);
	//
	// Setup callback and open the session with a diasasl send
	sasl->diasasl.callback = diasasl_cb_open;
	diasasl_open (&dianode, &sasl->diasasl, "unicorn.demo.arpa2.org", NULL);	/* Server domain */
}


/* Callback from event loop to process an interrupt signal.
 */
void event_interrupt_signal (EV_P_ ev_signal *evsig, int revents) {
	ev_break (EV_A_ EVBREAK_ALL);
}


/* Note quite a callback, but rather a console alternative to sockets.
 */
#ifdef TODO_CONSOLE_ALTERNATIVE
void fallback_accept_console (...) {
	//
	// Start the SASL session as a client
	struct diasasl_data *sasl = NULL;
	assert (dermem_open (sizeof (*sasl), (void **) &sasl));
	//
	// Use the console as the connection
	sasl->c2s_stream = fdopen (dup (0), "r");
	sasl->s2c_stream = fdopen (dup (1), "w");
	//
	//TODO//CONNECT_EVENTLOOP//
	//
	// Setup callback and open the session
	sasl->diasasl.callback = diasasl_cb_open;
	diasasl_open (&dianode, &sasl->diasasl, "unicorn.demo.arpa2.org");	/* Server domain */
}
#endif


/* Main program.
 */
int main (int argc, char *argv []) {
	//
	// Setup for DiaSASL
	diasasl_node_open (&dianode);
	//
	// Harvest commandline arguments
	program = argv [0];
	if ((argc < 4) || (argc > 7)) {
		fprintf (stderr, "Usage: %s signum diasaslIP diasaslPort [serverIP [serverPort [chanbindvalue]]]\n"
				"Run the TCP service for the Diameter SASL application at diasaslIP:diasaslPort\n"
				"Default serverPort is " DEFAULT_PORT " but without serverIP, the user is the network\n",
				program);
		exit (1);
	}
	socket_init();
	bool tty_user_networking = (argc < 5);
	int signum        = atoi         (argv [1]);
	char *diasasl_ip  =               argv [2];
	char *diasasl_port=               argv [3];
	char *server_ip   = (argc >= 5) ? argv [4] : NULL        ;
	char *server_port = (argc >= 6) ? argv [5] : DEFAULT_PORT;
	      chan_bind   = (argc >= 7) ? argv [6] : NULL        ;
	//
	// Have a TCP connection to the diasasl node's IP/Port
	struct sockaddr_storage diass;
	if (!socket_parse (diasasl_ip, diasasl_port, &diass)) {
		fprintf (stderr, "Syntax error in host ip:port setting %s:%s\n", diasasl_ip, diasasl_port);
		exit (1);
	}
	int diasox;
	if (!socket_client (&diass, SOCK_STREAM, &diasox)) {
		perror ("Failed to connect to Diameter SASL");
		exit (1);
	}
	int soxflags = fcntl (diasox, F_GETFL, 0);
	if (fcntl (diasox, F_SETFL, soxflags | O_NONBLOCK) != 0) {
		perror ("DiaSASL socket refuses non-blocking mode");
		exit (1);
	}
	//
	// Setup the diasasl node connection
	dianode.socket = diasox;
	//
	// Initialise the event loop
	loop = EV_DEFAULT;
	ev_signal_init (&ev_interrupt, event_interrupt_signal, signum);
	ev_signal_start (loop, &ev_interrupt);
	//
	// Add the diasasl node connection to the event loop
	ev_io_init (&ev_dianode, event_dispatch_diasasl_answer, diasox, EV_READ);
	ev_io_start (loop, &ev_dianode);
	//
	// Have streams for networking
	if (tty_user_networking) {
		printf ("\n*\n* You did not supply a network.  No problem.\n* Please copy/paste lines to the qsasl-client or xsasl-client.\n*\n\n");
	} else {
		struct sockaddr_storage ss;
		memset (&ss, 0, sizeof (ss));
		if (!socket_parse (server_ip, server_port, &ss)) {
			fprintf (stderr, "Syntax error in host ip:port setting %s:%s\n", server_ip, server_port);
			exit (1);
		}
		if (!socket_server (&ss, SOCK_STREAM, &saslsox)) {
			perror ("Failed to serve network");
			exit (1);
		}
		printf ("--\n");
		fflush (stdout);
		//
		// Connect the socket to the accept() callback
		ev_io_init (&ev_saslsox, event_accept_connection, saslsox, EV_READ);
		ev_io_start (loop, &ev_saslsox);
	}
	//
	// Run the event loop
	ev_run (loop, 0);
	//
	// Cleanup
	diasasl_node_close (&dianode);
	socket_fini ();
	//
	// Return success
	return 0;
}
