/* KIP client -- a simple commandline utility for "kip up" and "kip down".
 *
 * This is a simple command to protect a file with "kip up", and to remove
 * it with "kip down".  It can be expanded in a variety of ways, including
 * with modules for upload and download.  Its main function however, is to
 * add and remove encryption.
 *
 * The command has been updated to process KIP Document files, which have
 * a little more structure than the original simplistic file format that
 * was actually quite limited.  The extra structure brings about some CBOR
 * interactions, but even that means only slight complication of the demo.
 * It is however good to be aware that only certain KIP Document grammars
 * are processed well:
 *
 * Magic Header, Service Key Mud, repeat: Mud/Lit, Signature Mud.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include <errno.h>

#include <arpa2/com_err.h>
#include <arpa2/except.h>
#include <arpa2/socket.h>

#include <arpa2/quick-sasl.h>
#include <arpa2/kip.h>

#include <arpa2/except.h>

#include <krb5.h>

#include "../lib/kip-dbg.h"


/* CBOR, quick index; major type in 3 MSB; minor in 5 LSB, possibly indicating follow-up
 *
 * INT,   major type 0, minor 0..23 direct value,  24 1-byte follow, 25 2-byte follow
 * -INT,  major type 1, minor 0..23 direct value,  24 1-byte follow, 25 2-byte follow
 * BYTES, major type 2, minor 0..23 direct length, 24 1-byte follow, 25 2-byte follow
 * TEXT,  major type 3, minor 0..23 direct length, 24 1-byte follow, 25 2-byte follow
 * ARRAY, major type 4, minor 0..23 direct elems,  24 1-byte follow, 25 2-byte follow
 * MAP,   major type 5, minor 0..23 direct pairs,  24 1-byte follow, 25 2-byte follow
 * SEMAN, major type 6
 * PRMTV, major type 7, minor 0..23 direct value
 */

#define CBOR_INT   0x00
#define CBOR_MINT  0x20
#define CBOR_BYTES 0x40
#define CBOR_TEXT  0x60
#define CBOR_ARRAY 0x80
#define CBOR_MAP   0xa0
#define CBOR_SEMAN 0xc0
#define CBOR_PRMTV 0xe0


/* KIP magic header, version 202003 with no flags */

#define KIP_MAGIC_HEADER "ARPA2KIP202003====\n"


/* KIP tags */

#define KIP_TAG0_META      0
#define KIP_TAG0_LIT       1
#define KIP_TAG0_REF       2
#define KIP_TAG0_MUD       3
#define KIP_TAG0_KEYMUD    4
#define KIP_TAG0_SIGMUD    5
#define KIP_TAG0_TABKEY    6
#define KIP_TAG0_OFFKEY    7
#define KIP_TAG0_SVCKEYMUD 8
#define KIP_TAG0_SVCSIGMUD 9
#define KIP_TAG0_PUBKEY    10
#define KIP_TAG0_PUBDIG    11
#define KIP_TAG0_PUBSIG    12
#define KIP_TAG_MAX        12


/* Lengths per KIP tag */

int kip_tag2argminmax [KIP_TAG_MAX+1][2] = {
	{ 1,-1 },	/* Meta      */
	{ 1,1 },	/* Lit       */
	{ 1,1 },	/* Ref       */
	{ 2,2 },	/* Mud       */
	{ 4,4 },	/* KeyMud    */
	{ 2,2 },	/* SigMud    */
	{ 4,4 },	/* TabKey    */
	{ 0,-1 },	/* OffKey    */
	{ 1,1 },	/* SvcKeyMud */
	{ 1,1 },	/* SvcSigMud */
	{ 4,4 },	/* PubKey    */
	{ 2,2 },	/* PubDig    */
	{ 4,4 },	/* PubSig    */
};


/* Write a CBOR-level header in Canonical form (limited to the KIP application)
 */
bool cbor_put_head (uint8_t major, uint32_t minor_ext, FILE *outfile) {
	uint8_t hd [4];
	int hdlen;
	hd [0] = major;
	if (minor_ext < 24) {
		hd [0] |= minor_ext;
		hdlen = 1;
	} else if (minor_ext < 256) {
		hd [0] |= 24;
		hd [1] = minor_ext;
		hdlen = 2;
	} else if (minor_ext < 65536) {
		hd [0] |= 25;
		hd [1] = minor_ext >> 8;
		hd [2] = minor_ext & 0x00ff;
		hdlen = 3;
	} else {
		errno = ERANGE;
		return false;
	}
	if (fwrite (hd, 1, hdlen, outfile) != hdlen) {
		errno = EBADF;
		return false;
	}
	return true;
}


/* Read a CBOR-level header
 */
bool cbor_get_head (uint8_t *major, uint32_t *minor_ext, FILE *infile) {
	uint8_t byte;
	uint16_t len;
	if (fread (&byte, 1, 1, infile) != 1) {
		errno = 0;
		return false;
	}
	*major = byte & 0xe0;
	byte &= 0x1f;
	if (byte < 24) {
		*minor_ext = byte;
		//DEBUG// fprintf (stderr, "Major %02x len %02x\n", *major, byte);
	} else if (byte == 24) {
		uint8_t one;
		if (fread (&one, 1, 1, infile) != 1) {
			errno = EINVAL;
			return false;
		}
		//DEBUG// fprintf (stderr, "Major %02x len %02x\n", *major, one);
		*minor_ext = one;
	} else if (byte == 25) {
		uint8_t two [2];
		if (fread (two, 2, 1, infile) != 1) {
			errno = EINVAL;
			return false;
		}
		//DEBUG// fprintf (stderr, "Major %02x len %02x%02x\n", *major, two [0], two [1]);
		*minor_ext = (((uint32_t) two [0]) << 8) | two [1];
	} else {
		errno = ERANGE;
		return false;
	}
	return true;
}


/* Read a KIP-level array, that is an ARRAY and an INT with a KIP_TAGx_xxx
 */
bool cborray_get_head (uint32_t *kiptag, uint32_t *args, FILE *infile) {
	uint8_t major;
	uint32_t arraylen;
	if (!cbor_get_head (&major, &arraylen, infile)) {
		return false;
	}
	if (major != CBOR_ARRAY) {
		//DEBUG// fprintf (stderr, "CBOR_ARRAY expected, but got %02x\n", major);
		errno = EINVAL;
		return false;
	}
	*args = arraylen - 1;
	if (!cbor_get_head (&major, kiptag, infile)) {
		if (errno == 0) {
			errno = EINVAL;
		}
		return false;
	}
	if (*kiptag > KIP_TAG_MAX) {
		//DEBUG// fprintf (stderr, "KIP tag %d exceeds range up to %d\n", *kiptag, KIP_TAG_MAX);
		errno = EINVAL;
		return false;
	}
	int *minmax = kip_tag2argminmax [*kiptag];
	if ((*args < minmax [0]) || ((minmax [1] >= 0) && (*args > minmax [1]))) {
		//DEBUG// fprintf (stderr, "Range of args, %d not in [%d..%d]\n", *args, minmax [0], minmax [1]);
		errno = EINVAL;
		return false;
	}
	return true;
}


bool pump_up (char *progname, FILE *infile, FILE *outfile) {
	//
	// Determine the KIP realm to use
	char *kip_realm = getenv ("KIP_REALM");
	assertxt (kip_realm != NULL,
		"You must set your KIP_REALM as an environment variable");
	//
	// Open a context for KIP and start its use of KIP Service
	kipt_ctx kip;
	assertxt (kipctx_open (&kip),
		"Failed to open KIP Context");
	assertxt (kipservice_start (kip),
		"Failed to start KIP Service client");
	//
	// Be someone -- including step down to the ACL user
	char *str_login = getenv ("KIPSERVICE_CLIENTUSER_LOGIN");
	char *str_acl   = getenv ("KIPSERVICE_CLIENTUSER_ACL"  );
	char *str_realm = getenv ("KIPSERVICE_CLIENT_REALM"    );
	assert (kipservice_set_clientuser_login (kip, str_login));
	assert (kipservice_set_clientuser_acl   (kip, str_acl  ));
	assert (kipservice_set_client_realm     (kip, str_realm));
	//
	// Create a session key
	kipt_keyid seskeyid;
	assertxt (kipkey_generate (kip, ENCTYPE_AES128_CTS_HMAC_SHA256_128, 13, &seskeyid),
		"Failed to generate KIP key");
	kipt_keynr seskeynr = (kipt_keynr) seskeyid;
	//
	// Write out the magic header
	if (fwrite (KIP_MAGIC_HEADER, strlen (KIP_MAGIC_HEADER), 1, outfile) != 1) {
		return false;
	}
	//
	// Load the master key into the first context
	uint8_t *keymud;
	uint32_t keymudlen = 0;
	char *blacklist [] = { NULL };
	char *whitelist [] = { "@.", NULL };
	assertxt (kipservice_tomap (kip, kip_realm, seskeyid,
			blacklist, whitelist, 0, 0, &keymud, &keymudlen),
		"Failed to map KIP key to mud");
	assert (cbor_put_head (CBOR_ARRAY, 2, outfile));
	assert (cbor_put_head (CBOR_INT, KIP_TAG0_SVCKEYMUD, outfile));
	assert (cbor_put_head (CBOR_BYTES, keymudlen, outfile));
	assertxt (fwrite (keymud, 1, keymudlen, outfile) == keymudlen,
		"Failed to write keymud to output file");
	//
	// Loop over the blocks of input, encrypt them and send them out
	while (true) {
		//
		// Read as much as possible from infile
		uint8_t plain [4000];
		int32_t plainlen;
		plainlen = fread (plain, 1, sizeof (plain), infile);
		if (plainlen > 0) {
			/* continue */
			;
		} else if (ferror (infile)) {
			log_errno ("Failed to read");
			goto close_fail;
		} else if (feof (infile)) {
			break;
		}
		//
		// Encrypt what we've just read
		//TODO// Decide if Mud in a KIP Document contains binary or nests Lit, Ref, Meta
		uint8_t crypt [4200];
		uint32_t cryptlen;
		assertxt (kipdata_up (kip, seskeyid, plain, plainlen, crypt, sizeof (crypt), &cryptlen),
			"Failed to kip up a data block");
		assert (cbor_put_head (CBOR_ARRAY, 3, outfile));
		assert (cbor_put_head (CBOR_INT, KIP_TAG0_MUD, outfile));
		assert (cbor_put_head (CBOR_INT, seskeynr, outfile));
		assert (cbor_put_head (CBOR_BYTES, cryptlen, outfile));
		assertxt (fwrite (crypt, 1, cryptlen, outfile) == cryptlen,
			"Failed to write kipped up data block to the output file");
	}
	//
	// Write a differently tagged packet with the signature
	uint8_t sig [512];
	uint32_t siglen;
	if (kipsum_sign (kip, seskeyid, sizeof (sig), &siglen, sig)) {
		assert (cbor_put_head (CBOR_ARRAY, 3, outfile));
		assert (cbor_put_head (CBOR_INT, KIP_TAG0_SIGMUD, outfile));
		assert (cbor_put_head (CBOR_INT, seskeynr, outfile));
		assert (cbor_put_head (CBOR_BYTES, siglen, outfile));
		assertxt (fwrite (sig, 1, siglen, outfile) == siglen,
			"Failed to write final signature to the output file");
	}
	//
	// Close the KIP context
	kipctx_close (kip);
	//
	// Successful return
	return true;
	//
	// Return with error
close_fail:
	kipservice_stop (kip);
	kipctx_close (kip);
fail:
	return false;
}


bool pump_down (char *progname, FILE *infile, FILE *outfile) {
	//
	// Open a context for KIP and start its use of KIP Service
	kipt_ctx kip;
	assertxt (kipctx_open (&kip),
		"Failed to open the KIP Context");
	assertxt (kipservice_start (kip),
		"Failed to start the KIP Service client");
	//
	// Be someone -- but then step down to the "demo1" alias
	char *str_login = getenv ("KIPSERVICE_CLIENTUSER_LOGIN");
	char *str_acl   = getenv ("KIPSERVICE_CLIENTUSER_ACL"  );
	char *str_realm = getenv ("KIPSERVICE_CLIENT_REALM"    );
	assert (kipservice_set_clientuser_login (kip, str_login));
	assert (kipservice_set_clientuser_acl   (kip, str_acl  ));
	assert (kipservice_set_client_realm     (kip, str_realm));
	//
	// Use a shared buffer -- and load the mudkey into it
	uint8_t crypt [65536];
	uint32_t cryptlen;
	//
	// Recognise the magic header
	assertxt (fread (crypt, strlen (KIP_MAGIC_HEADER), 1, infile) == 1,
		"Failed to read the magic header from the input file");
	assertxt (memcmp (crypt, KIP_MAGIC_HEADER, strlen (KIP_MAGIC_HEADER)) == 0,
		"The input file does not look like a KIP Document");
	//
	// Load the master key into memory
	uint32_t kiptag;
	uint32_t args;
	assert (cborray_get_head (&kiptag, &args, infile));
	assertxt ((kiptag == KIP_TAG0_SVCKEYMUD) && (args == 1),
		"Frivolous input (expected SVCKEYMUD)");
	uint8_t subtag;
	assert (cbor_get_head (&subtag, &cryptlen, infile));
	assertxt ((subtag == CBOR_BYTES) && (cryptlen <= sizeof (crypt)),
		"Frivolous input (expected BYTES)");
	assertxt (fread (crypt, 1, cryptlen, infile) == cryptlen,
		"Failure reading SVCKEYMUD from the input file");
	//
	// Load the master key into the context
	kipt_keyid *seskeyarray;
	uint32_t oneseskey;
	assertxt (kipservice_frommap (kip, crypt, cryptlen, &seskeyarray, &oneseskey),
		"Failed to decode the session key for this KIP Document");
	assertxt (oneseskey == 1,
		"Frivolous input (expected one session key only)");
	kipt_keyid seskey = seskeyarray [0];
	//
	// Now loop to retrieve encrypted blocks, and decrypt those
	while (true) {
		if (!cborray_get_head (&kiptag, &args, infile)) {
			if ((errno == 0) && feof (infile)) {
				goto close_fail;
			}
			log_errno ("Data failed to read");
			goto close_fail;
		}
		if (kiptag != KIP_TAG0_MUD) {
			break;
		}
		assertxt ((kiptag == KIP_TAG0_MUD) && (args == 2),
			"Frivolous input (expected MUD)");
		kipt_keynr keynr;
		assert (cbor_get_head (&subtag, &keynr, infile));
		assertxt ((subtag == CBOR_INT) && (keynr == (kipt_keynr) seskey),
			"Frivolous input (expected MUD keynr");
		assert (cbor_get_head (&subtag, &cryptlen, infile));
		assertxt ((subtag == CBOR_BYTES) && (cryptlen <= sizeof (crypt)),
			"Frivolous input (expected MUD blob");
		assertxt (fread (crypt, 1, cryptlen, infile) == cryptlen,
			"Failed to read all MUD from the input file");
		uint8_t plain [sizeof (crypt)];
		uint32_t plainlen;
		assertxt (kipdata_down (kip, keynr, crypt, cryptlen, plain, sizeof (plain), &plainlen),
			"Failed to convert MUD to plaintext");
		if (fwrite (plain, 1, plainlen, outfile) != plainlen) {
			errno = EPIPE;
			goto close_fail;
		}
	}
	//
	// Expect to find the final kiptag for the signature
	assertxt ((kiptag == KIP_TAG0_SIGMUD) && (args == 2),
		"Frivolous input (expected SIGMUD)");
	kipt_keynr keynr;
	assert (cbor_get_head (&subtag, &keynr, infile));
	assertxt ((subtag == CBOR_INT) && (keynr == (kipt_keynr) seskey),
		"Frivolous input (expected SIGMUD subtag)");
	uint8_t sig [512];
	uint32_t siglen;
	assert (cbor_get_head (&subtag, &siglen, infile));
	assertxt ((subtag == CBOR_BYTES) && (siglen <= sizeof (sig)),
		"Frivolous input (expected SIGMUD blob");
	assertxt (fread (sig, 1, siglen, infile) == siglen,
		"Failed to read SIGMUD from the input file");
	if (!kipsum_verify (kip, seskey, siglen, sig)) {
		fprintf (stderr, "Integrity check failed\n");
		goto close_fail;
	}
	//SILENT// fprintf (stderr, "Integrity check successful\n");
	//
	// Successful end
	kipctx_close (kip);
	return true;
	//
	// Unsuccessful end
close_fail:
	kipservice_stop (kip);
	kipctx_close (kip);
fail:
	return false;
}


int main (int argc, char *argv []) {
	//
	// Commandline parsing
	char *progname = argv [0];
	fprintf (stderr, "%s is a demo for a basic KIP Document in a C program\n", progname);
	//
	// Options after "kip" go here, upping argv, downing argc
	//
	// Distinguish "up" and "down" variants
	if (argc < 4) {
		fprintf (stderr, "Usage: %s ... up|down <in> <out> [<recipient>...]\n", progname);
		exit (1);
	}
	bool up   = (strcmp (argv [1], "up"  ) == 0);
	bool down = (strcmp (argv [1], "down") == 0);
	if (! ( up || down ) ) {
		fprintf (stderr, "Usage: %s ... up|down ...\n", progname);
		exit (1);
	}
	argc -= 1;
	argv += 1;
	//
	// Initialise the KIP, KIP Service and QuickSASL modules
	kip_init ();
	kipservice_init (NULL, NULL);
	assertxt (qsasl_init (NULL, "Hosted_Identity"),
		"Failed to initialise Quick-SASL for Hosted_Identity");
	//
	// Open the input and output files
	char * input = argv [1];
	char *output = argv [2];
	argv += 2;
	argc -= 2;
	FILE *infile = fopen (input, "r");
	if (infile == NULL) {
		log_errno ("Failed to open input file");
		exit (1);
	}
	FILE *outfile = fopen (output, "w");
	if (outfile == NULL) {
		log_errno ("Failed to open output file");
		exit (1);
	}
	//
	// Start pumping up or down, as requested
	bool ok;
	if (up) {
		ok = pump_up   (progname, infile, outfile);
	} else {
		ok = pump_down (progname, infile, outfile);
	}
	//
	// Close off
	fclose (infile);
	fclose (outfile);
	qsasl_fini ();
	kipservice_fini ();
	kip_fini ();
	if (!ok) {
		com_err (progname, errno, "Failed during kip");
	}
	exit (ok ? 0 : 1);
}
