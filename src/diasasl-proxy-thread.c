/* A SASL server-side that proxies requests over Diameter SASL.
 *
 * This is similar to the sasl-sample-server provided with Cyrus SASL2,
 * but we use different prompts and switched from BASE64 to HEX for
 * reasons of human readability (the term "human" is debatable, grinn).
 * When translating, also note the different mechanism lists; these are
 * not standardised as part of SASL, so each makes up their own.
 *
 * The server can listen to an IP and port combination, where a client
 * would be connecting.  When this is not requested, it will instead assume
 * traffic on stdin in a somewhat standard format.
 *
 * Any lines not recognised are quietly dropped.
 * Lines are prefixed with either "c2s> " or "s2c> " to indicate the kind
 * of information and the traffic passed.  Plus, initially there is a
 * "mech> " prefix from server to client with the mechanism, and one from
 * the client back to the server.  When returning data with success, the
 * prompt "extra> " might also be used.  Any lines not recognised are
 * quietly dropped, except for a message about misrecognised prompts.
 *
 * This implementation demonstrates an integration with pthreads, using
 * a "master thread" that listens for events, and a mutex on the (extended)
 * session object to allow the master to re-enable the thread that initiated
 * a session request.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include <pthread.h>
#include <unistd.h>

#include <arpa2/com_err.h>
#include <arpa2/except.h>
#include <arpa2/socket.h>

#include <arpa2/quick-der.h>
#include <arpa2/quick-dermem.h>
#include <arpa2/quick-diasasl.h>


#define DEFAULT_PORT "16832"


#include "sasl-common.c"


static char *program;

static struct diasasl_node dianode;
struct diasasl_data {
	struct diasasl_session diasasl;
	pthread_mutex_t master_action;
	dercursor mechlist;
	dercursor s2c_token;
	bool success;
	bool failure;
};


/* Callback from diasasl.
 */
void diasasl_cb (struct diasasl_session *session,
		const int32_t *com_errno,
		char *opt_mechanism_list,
		uint8_t *s2c_token, uint32_t s2c_token_len) {
	struct diasasl_data *sasl = (struct diasasl_data *) session;
	log_debug ("diasasl_cb() -> sasl = %p", sasl);
	assert (!sasl->success);
	assert (!sasl->failure);
	sasl->s2c_token.derptr = NULL;
	sasl->s2c_token.derlen = 0;
	if (s2c_token != NULL) {
		log_debug ("Got S2C token #%d", s2c_token_len);
		assert (dermem_alloc (sasl, s2c_token_len, (void **) &sasl->s2c_token.derptr));
		memcpy (sasl->s2c_token.derptr, s2c_token, s2c_token_len);
		sasl->s2c_token.derlen = s2c_token_len;
	}
	if (com_errno == NULL) {
		if (opt_mechanism_list != NULL) {
			log_debug ("Got mechanism list %s", opt_mechanism_list);
			assert (der_isnull (&sasl->mechlist));
			assert (der_isnull (&sasl->s2c_token));
			sasl->mechlist.derlen = strlen (opt_mechanism_list);
			sasl->mechlist.derptr = NULL;
			assert (dermem_alloc (sasl, sasl->mechlist.derlen, (void **) &sasl->mechlist.derptr));
			memcpy (sasl->mechlist.derptr, opt_mechanism_list, sasl->mechlist.derlen);
		}
	} else if (*com_errno == 0) {
		sasl->success = true;
	} else {
		sasl->failure = true;
		com_err (program, *com_errno, "Diameter SASL failure");
		diasasl_close (&dianode, session);	//TODO//FIND_AS_LINK//
	}
	assert (pthread_mutex_unlock (&sasl->master_action) == 0);
}


/* The master thread for a diasasl node.  It arranges callbacks for all
 * sessions related to the node.  The node's socket should block-wait;
 * otherwise, the master thread would need to poll in a loop and become
 * highly inefficient.
 */
void *diasasl_master (void *node2master) {
	struct diasasl_node *mynode = node2master;
	int32_t com_errno;
	do {
		com_errno = diasasl_process (mynode, false);
		if (com_errno == 0) {
			log_debug ("Unexpected exit from diasasl_process() in the master thread");
		} else {
			com_err (program, com_errno, "Diameter SASL response error");
			diasasl_break (mynode, com_errno);
		}
	} while (com_errno == 0);
	return NULL;
}


/* Main program.
 */
int main (int argc, char *argv []) {
	//
	// Setup for DiaSASL
	diasasl_node_open (&dianode);
	//
	// Harvest commandline arguments
	program = argv [0];
	if ((argc < 3) || (argc > 6)) {
		fprintf (stderr, "Usage: %s diasaslIP diasaslPort [serverIP [serverPort [chanbindvalue]]]\n"
				"Run the TCP service for the Diameter SASL application at diasaslIP:diasaslPort\n"
				"Default serverPort is " DEFAULT_PORT " but without serverIP, the user is the network\n",
				program);
		exit (1);
	}
	socket_init();
	char *service = "demo";
	bool tty_user_networking = (argc < 4);
	char *diasasl_ip  =               argv [1];
	char *diasasl_port=               argv [2];
	char *server_ip   = (argc >= 4) ? argv [3] : NULL        ;
	char *server_port = (argc >= 5) ? argv [4] : DEFAULT_PORT;
	char *chan_bind   = (argc >= 6) ? argv [5] : NULL        ;
	//
	// Have a TCP connection to the diasasl IP/Port
	struct sockaddr_storage diass;
	if (!socket_parse (diasasl_ip, diasasl_port, &diass)) {
		fprintf (stderr, "Syntax error in host ip:port setting %s:%s\n", diasasl_ip, diasasl_port);
		exit (1);
	}
	if (!socket_client (&diass, SOCK_STREAM, &dianode.socket)) {
		perror ("Failed to connect to Diameter SASL");
		exit (1);
	}
	//
	// Have streams for networking
	FILE *c2s_stream = stdin;
	FILE *s2c_stream = stdout;
	if (tty_user_networking) {
		printf ("\n*\n* You did not supply a network.  No problem.\n* Please copy/paste lines to the qsasl-client or xsasl-client.\n*\n\n");
	} else {
		struct sockaddr_storage ss;
		memset (&ss, 0, sizeof (ss));
		if (!socket_parse (server_ip, server_port, &ss)) {
			fprintf (stderr, "Syntax error in host ip:port setting %s:%s\n", server_ip, server_port);
			exit (1);
		}
		int sox;
		if (!socket_server (&ss, SOCK_STREAM, &sox)) {
			perror ("Failed to serve network");
			exit (1);
		}
		printf ("--\n");
		fflush (stdout);
		int cnx = accept (sox, NULL, NULL);
		if (cnx < 0) {
			perror ("Failed to accept connection");
			exit (1);
		}
		// Experiment: Close down further connections
		close (sox);
		c2s_stream = fdopen (dup (cnx), "r");
		s2c_stream = fdopen (     cnx , "w");
	}
	//
	// Start a master process on the dianode before opening sessions on it.
	pthread_t diamaster;
	assert (pthread_create (&diamaster, NULL, diasasl_master, (void *) &dianode) == 0);
	//
	// Start the SASL session as a client.
	struct diasasl_data *sasl = NULL;
	assert (dermem_open (sizeof (*sasl), (void **) &sasl));
	log_debug ("dermem_open() -> sasl = %p", sasl);
	sasl->diasasl.callback = diasasl_cb;
	assert (pthread_mutex_init (&sasl->master_action, NULL) == 0);
	assert (pthread_mutex_lock (&sasl->master_action) == 0);
	diasasl_open (&dianode, &sasl->diasasl, "unicorn.demo.arpa2.org", NULL);	/* Server domain */
	assert (pthread_mutex_lock (&sasl->master_action) == 0);
	//
	// Setup channel binding information
	if (chan_bind != NULL) {
		uint8_t *cb = NULL;
		assertxt (mem_alloc (sasl, strlen(QSA_X_MANUAL_SESSION_KEY) + strlen(chan_bind) + 2, (void **) &cb), "Failed to allocate channel binding value");
		sprintf ((char*)cb, "%s:%s", QSA_X_MANUAL_SESSION_KEY, chan_bind);
		sasl->diasasl.chanbind     = cb;
		sasl->diasasl.chanbind_len = strlen ( (char*)cb );
	} else {
		sasl->diasasl.chanbind     = NULL;
		sasl->diasasl.chanbind_len = 0;
	}
	//
	// Now provide a list of mechanisms for the client
	// Mechanism lists are not standardised but these demos pass them as
	// a string with spaces between the standardised mechanism names.
	prompted_hex_send (sasl, s2c_stream, "mech", sasl->mechlist);
	//
	// Await the response from the client, in the form of a "mech> " prompt
	static const char *only_mech [] = { "mech", NULL };
	const char *_lineprompt;
	dercursor mech_used;
	prompted_hex_recv (sasl, c2s_stream, only_mech, &_lineprompt, &mech_used);
	char *opt_mech_choice = NULL;
	assert (dermem_strdup (sasl, mech_used, &opt_mech_choice));
	//
	// Start looping as a server
	while ((!sasl->success) && (!sasl->failure)) {
		//
		// Receive a response from the client
		static const char *only_c2s []  = { "c2s" , NULL };
		dercursor c2s;
		prompted_hex_recv (sasl, c2s_stream, only_c2s, &_lineprompt, &c2s);
		//
		// Make a server step: c2s -> s2c
		diasasl_send (&dianode, &sasl->diasasl, opt_mech_choice, c2s.derptr, c2s.derlen);
		opt_mech_choice = NULL;
		assert (pthread_mutex_lock (&sasl->master_action) == 0);
		//
		// Do not send a new challenge when the last response was final
		if (sasl->success || sasl->failure) {
			break;
		}
		//
		// Send output to the client; initially "mech> " and later "s2c> "
		prompted_hex_send (sasl, s2c_stream, "s2c", sasl->s2c_token);
	}
	//
	// Optionally send "extra>" with an addtional token upon success
	if (sasl->success && !der_isnull (&sasl->s2c_token)) {
		prompted_hex_send (sasl, s2c_stream, "extra", sasl->s2c_token);
	}
	//
	// Determine the mechanism used, if it was one, and the clientid
	printf ("Client identity (client_userid) is %s\n", sasl->diasasl.client_userid);
	printf ("Client identity (client_domain) is %s\n", sasl->diasasl.client_domain);
	if (mech_used.derptr != NULL) {
		printf ("Mechanism used was %.*s\n", (int) mech_used.derlen, mech_used.derptr);
	}
	//
	// Report whether the exchange was successful
	printf ("\n*\n* The SASL exchange was a %s.  Application %s the client.\n*\n\n",
			sasl->success ? "SUCCESS" : "FAILURE",
			der_isnull (&sasl->s2c_token) ? "will tell" : "has told");
	//
	// Send an explicit EOF signal to be detected as failure
	dercursor eof_null = { .derptr = NULL, .derlen = 0 };
	prompted_hex_send (sasl, s2c_stream, "eof", eof_null);
	//
	// Cleanup & Closedown
	int exitval = sasl->success ? 0 : 1;
	diasasl_close (&dianode, &sasl->diasasl);
	diasasl_node_close (&dianode);
	close (dianode.socket);
	dermem_close ((void **) &sasl);
	socket_fini ();
	pthread_cancel (diamaster);
	exit (exitval);
}
