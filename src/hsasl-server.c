/* A SASL server based on Quick SASL.
 *
 * This is similar to the sasl-sample-server provided with Cyrus SASL2,
 * but we use different prompts and switched from BASE64 to HEX for
 * reasons of human readability (the term "human" is debatable, grinn).
 * When translating, also note the different mechanism lists; these are
 * not standardised as part of SASL, so each makes up their own.
 *
 * The server can listen to an IP and port combination, where a client
 * would be connecting.  When this is not requested, it will instead assume
 * traffic on stdin in a somewhat standard format.
 *
 * Any lines not recognised are quietly dropped.
 * Lines are prefixed with either "c2s> " or "s2c> " to indicate the kind
 * of information and the traffic passed.  Plus, initially there is a
 * "mech> " prefix from server to client with the mechanism, and one from
 * the client back to the server.  When returning data with success, the
 * prompt "extra> " might also be used.  Any lines not recognised are
 * quietly dropped, except for a message about misrecognised prompts.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include <unistd.h>

#include <arpa2/socket.h>

#include <arpa2/quick-der.h>
#include <arpa2/quick-sasl.h>


#define DEFAULT_PORT "16832"


#include "sasl-common.c"



/* Main program.
 */
int main (int argc, char *argv []) {
	//
	// Harvest commandline arguments
	assert (argc > 0);
	if (argc > 3) {
		fprintf (stderr, "Usage: %s [serverIP [serverPort]]\n"
				"Default serverPort is " DEFAULT_PORT " but without serverIP, the user is the network\n",
				argv [0]);
		exit (1);
	}
	const char *service = "qsasl-xsasl-demo";
	bool tty_user_networking = (argc < 2);
	char *server_ip   = (argc >= 2) ? argv [1] : NULL        ;
	char *server_port = (argc >= 3) ? argv [2] : DEFAULT_PORT;
	//
	// Have streams for networking
	int sox = -1;
	FILE *c2s_stream = stdin;
	FILE *s2c_stream = stdout;
	socket_init ();
	if (tty_user_networking) {
		printf ("\n*\n* You did not supply a network.  No problem.\n* Please copy/paste lines to the hsasl-client or hsasl-makeid.\n*\n\n");
	} else {
		struct sockaddr_storage ss;
		memset (&ss, 0, sizeof (ss));
		if (!socket_parse (server_ip, server_port, &ss)) {
			fprintf (stderr, "Syntax error in host ip:port setting %s:%s\n", server_ip, server_port);
			exit (1);
		}
		if (!socket_server (&ss, SOCK_STREAM, &sox)) {
			perror ("Failed to serve network");
			exit (1);
		}
		printf ("--\n");
		fflush (stdout);
		int cnx = accept (sox, NULL, NULL);
		if (cnx < 0) {
			perror ("Failed to accept connection");
			exit (1);
		}
		c2s_stream = fdopen (dup (cnx), "r");
		s2c_stream = fdopen (     cnx , "w");
	}
	//
	// Start the SASL session as a client.
	qsasl_init (NULL, "HAAN");
	QuickSASL sasl = NULL;
	qsaslt_state curstate = QSA_UNDECIDED;
	assert (qsasl_server (&sasl, service, cb_statechange, &curstate, QSA_SERVERFLAG_ALLOW_FINAL_S2C));
	//
	// Be someone -- set the realms for client and server
	membuf crs_realm = { .bufptr=(uint8_t*)"unicorn.demo.arpa2.org", .buflen=22 };
	qsasl_set_client_realm     (sasl, crs_realm);
	qsasl_set_server_realm     (sasl, crs_realm);
	//
	// Now provide a list of mechanisms for the client
	// Mechanism lists are not standardised but these demos pass them as
	// a string with spaces between the standardised mechanism names.
	dercursor mechs;
	assert (qsasl_get_mech (sasl, crs2bufp (&mechs)));
	prompted_hex_send (sasl, s2c_stream, "mech", mechs);
	//
	// Await the response from the client, in the form of a "mech> " prompt
	static const char *only_mech [] = { "mech", NULL };
	const char *_lineprompt;
	dercursor mech_used;
	dercursor s2c;
	prompted_hex_recv (sasl, c2s_stream, only_mech, &_lineprompt, &mech_used);
	assert (qsasl_set_mech (sasl, crs2buf (mech_used)));
	//
	// Start looping as a server
	while (curstate == QSA_UNDECIDED) {
		//
		// Receive a response from the client
		static const char *only_c2s []  = { "c2s" , NULL };
		dercursor c2s;
		prompted_hex_recv (sasl, c2s_stream, only_c2s, &_lineprompt, &c2s);
		//
		// Make a server step: c2s -> s2c
		assert (qsasl_step_server (sasl, crs2buf (c2s), crs2bufp (&s2c)));
		//
		// Do not send a new challenge when the last response was final
		if (curstate != QSA_UNDECIDED) {
			break;
		}
		//
		// Send output to the client; initially "mech> " and later "s2c> "
		prompted_hex_send (sasl, s2c_stream, "s2c", s2c);
	}
	//
	// Optionally send "extra>" with an addtional token upon success
	if ((curstate == QSA_SUCCESS) && (s2c.derptr != NULL)) {
		prompted_hex_send (sasl, s2c_stream, "extra", s2c);
	}
	//
	// Determine the mechanism used, if it was one, and the clientid
	membuf clientuid;
	if (qsasl_get_client_userid (sasl, &clientuid)) {
		if (clientuid.bufptr == NULL) {
			printf ("No client userid\n");
		} else {
			printf ("Client userid is %.*s\n", (int)clientuid.buflen, clientuid.bufptr);
		}
	}
	membuf clientdom;
	if (qsasl_get_client_domain (sasl, &clientdom)) {
		if (clientdom.bufptr == NULL) {
			printf ("No client domain\n");
		} else {
			printf ("Client domain is %.*s\n", (int)clientdom.buflen, clientdom.bufptr);
		}
	}
	if (mech_used.derptr != NULL) {
		printf ("Mechanism used was %.*s\n", (int)mech_used.derlen, mech_used.derptr);
	}
	//
	// Report whether the exchange was successful
	printf ("\n*\n* The SASL exchange was a %s.  Application %s the client.\n*\n\n",
			(curstate == QSA_SUCCESS) ? "SUCCESS" : "FAILURE",
			(s2c.derptr == NULL) ? "should tell" : "has told");
	//
	// Wait for the client to close the connection
	fclose (s2c_stream);
	while (!feof (c2s_stream)) {
		char buf [100];
		fread (buf, 1, 100, c2s_stream);
	}
	fclose (c2s_stream);
	if (sox >= 0) {
		close (sox);
	}
	//
	// Cleanup & Closedown
	qsasl_close (&sasl);
	qsasl_fini ();
	socket_fini ();
	exit ((curstate == QSA_SUCCESS) ? 0 : 1);
}
