/* Username and password generator -- see also haanvfy.c
 *
 * This produces pairs of username and password, where the password is a
 * derivative of the username.  The derivation can later be repeated for
 * validation.
 *
 * The link between the username and password is made with a fixed key,
 * so other parties cannot make the same derivation.  Furthermore, the
 * username is generated randomly and cannot be externally provided, so
 * this service cannot be abused to reproduce the password.
 *
 * This is a locally running daemon, and a wrapper application should be
 * used to relay the username, validating realm/hostname and password in
 * (say) a PDF document.  This wrapper application should also constrain
 * the speed at which usernames and passwords are requested from this
 * daemon.
 *
 * The username and password must be relayed to the freshly born owner
 * of this empty, authentication-service-only account, presumably over
 * a transport like TLS.  As long as TLS is not quantum proof, there is
 * no value in using 256-bit strength, and 128 bit suffices.  But when
 * TLS is post quantum, it both makes sense and is required for the
 * overall security level to use 256 bit strenght.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <unistd.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/stat.h>

#include <krb5.h>

#include <arpa2/kip.h>


char BASE32 [] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";


int main (int argc, char *argv []) {
	//
	// Check command arguments
	if (argc != 2) {
		fprintf (stderr, "Usage: %s virtual.host.name\n", argv [0]);
		exit (1);
	}
	const char *svc = "haan";
	const char *vhost = argv [1];
	/* Assume that vhost == realm (no SRV redirection) */
	const char *realm = vhost;
	//
	// Initialise the KIP library and open a context
	kipt_ctx kip;
	assert (kip_init ());
	assert (kipctx_open (&kip));
	//
	// Load the virtual host keymap
	kipt_keyid vhostkey;
	if (!kipkey_fromvhosttab (kip, svc, vhost, &vhostkey)) {
		fprintf (stderr, "No virtual host key for service %s, vhost %s\n", svc, vhost);
		exit (1);
	}
	//
	// Generate the username with redundancy: 128-->160, 256-->320
	// Let's be lazy, ignore 3 bits out of 8 when mapping to BASE32
	uint8_t uid128 [32];
	assert (kipdata_random (kip, sizeof (uid128), uid128));
	//
	// Map username to a readable format
	char username [66];
	for (int i = 0; i < sizeof (uid128); i++) {
		username [i] = BASE32 [ uid128 [i] & 31 ];
	}
	username [sizeof(uid128)] = '\0';
	//
	// Produce a password from the readable username
	// We'll be lazy, ignoring 3 bits out of 8 when mapping to BASE32
	uint8_t pwd128 [32];
	assert (kipdata_usermud (kip, vhostkey, (uint8_t*)username, strlen (username), pwd128, sizeof (pwd128)));
	//
	// Map password to a readable format
	char password [66];
	for (int i = 0; i < sizeof (pwd128); i++) {
		password [i] = BASE32 [ pwd128 [i] & 31 ];
	}
	password [sizeof(pwd128)] = '\0';
	//
	// Print the account information
	printf ("Helm Key: %s@%s\n", username, realm);
	printf ("Password: %s\n", password);
	//
	// Cleanup the KIP environment
	kipctx_close (kip);
	kip_fini ();
	exit (0);
}
