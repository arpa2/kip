/* Cyrus-SASL2 auxprop plugin for HAAN
 *
 * This auxprop code is used to "lookup" the passphrase for
 * a given username.  In HAAN, this means that a derivation
 * is performed to compute the passphrase from the username.
 *
 * Given that the user cannot freely choose the username,
 * this is in fact safe.  Still, it is advised that the
 * password never be sent across in plain form, but instead
 * used through challenge/response interactions.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdio.h>
#include <string.h>

#include <sasl/sasl.h>
#include <sasl/saslplug.h>

#include <krb5.h>

#include <arpa2/kip.h>
#include <arpa2/except.h>


#ifdef WIN32
#define PLUG_API __declspec(dllexport)
#else
#define PLUG_API extern
#endif


char BASE32 [] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";


/* fill in fields of an auxiliary property context
*  last element in array has id of SASL_AUX_END
*  elements with non-0 len should be ignored.
*/
int haandb_lookup (void *glob_context,
			sasl_server_params_t *sparams,
			unsigned flags,
			const char *user, unsigned ulen) {
	//
	// We only lookup authentication information, not authorisation
	if (flags & SASL_AUXPROP_AUTHZID) {
printf ("No authorisation: SASL_NOMECH\n");
		return SASL_NOMECH;
	}
	//
	// If the @realm is added, find where it is
	bool failed = false;
	bool opened = false;
	const char *realm = NULL;
	unsigned rlen = 0;
	const char *uskip = memchr (user, '@', ulen);
	if (uskip != NULL) {
		realm = uskip + 1;
		rlen = ulen - 1;
		ulen = uskip - user;
		rlen -= ulen;
	} else {
		uskip = user + ulen;
	}
	//
	// Detect the realm, and collect if we "found" a reasonably looking login
	bool found = ((uskip - user) == 32) || ((uskip - user) == 64);
printf ("At haan_auxprop.c:%d, failed=%d, found=%d\n", __LINE__, failed, found);
	if (realm == NULL) {
		realm = sparams->user_realm;
	}
	if (realm == NULL) {
		realm = sparams->serverFQDN;
	}
	if (realm == NULL) {
		realm = getenv ("HAAN_REALM");
	}
	found = found && (realm != NULL);
printf ("At haan_auxprop.c:%d, failed=%d, found=%d\n", __LINE__, failed, found);
	while (found && (uskip-- > user)) {
		char ch = *uskip;
		found = ((ch >= 'A') && (ch <= 'Z')) || ((ch >= '2') && (ch <= '7'));
	}
printf ("At haan_auxprop.c:%d, failed=%d, found=%d\n", __LINE__, failed, found);
	//
	// Lookup the virtual-host key for the HAAN service
	// It may be more efficient to cache realm->vhostkey lookup
	kipt_ctx kip;
	kipt_keyid vhostkey;
	//TODO// kip_init() / kip_fini()
kip_init();
	opened = found && (!failed) && kipctx_open (&kip);
	failed = failed || (found && !opened);
printf ("At haan_auxprop.c:%d, failed=%d, found=%d\n", __LINE__, failed, found);
if (failed) { log_errno ("haan_auxprop.c: kipctx_open()"); }
kip_fini();
	bool loaded = false;
	if (!failed) {
		loaded = !kipkey_fromvhosttab (kip,
				"haan", realm,
				&vhostkey);
		found = found && loaded;
printf ("At haan_auxprop.c:%d, failed=%d, found=%d\n", __LINE__, failed, found);
	}
	//
	// If vhosttab loading failed, then leave this user to other auxprop modules
	if (opened && !loaded) {
printf ("Opened but not loaded: SASL_NOUSER\n");
		kipctx_close (kip);
		return SASL_NOUSER;
	}
	//
	// Note: No polishing here; client would not mirror this
	//       and things like hash-based mechanisms would fail.
	//       If you need to change spelling, provide a better
	//       identity as authorisation identifiy, and get free
	//       translation for lower/upper, for 1/I, for 0/O, but
	//       we need the formally correct username for login.
	//
	// Compute the binary form of the password
	// We're lazy, and shall only use 5 bits out of 8 for BASE32
	uint8_t exp128 [32];
	if (found) {
		failed = failed || !kipdata_usermud (kip,
					vhostkey,
					user, ulen,
					exp128, sizeof (exp128));
	}
printf ("At haan_auxprop.c:%d, failed=%d, found=%d\n", __LINE__, failed, found);
	//
	// Allocate a password string to return
	char *pwmem = NULL;
	unsigned pwlen = 0;
	if (found && !failed) {
		pwmem = sparams->utils->malloc (66);
		failed = (pwmem == NULL);
printf ("At haan_auxprop.c:%d, failed=%d, found=%d\n", __LINE__, failed, found);
	}
	if (pwmem != NULL) {
		pwlen = 32;
		for (int i = 0; i < pwlen; i++) {
			pwmem [i] = BASE32 [ exp128 [i] & 31 ];
		}
		pwmem [pwlen] = '\0';
	}
	//
	// Store the password in its auxprop slot
	if (found && !failed) {
		/* Ignore if SASL_OK, it does not seem to work as expected */
		sparams->utils->prop_set (sparams->propctx,
				"*" SASL_AUX_PASSWORD_PROP,
				pwmem, pwlen);
	}
	//
	// Close the KIP Context
	if (opened) {
		kipctx_close (kip);
	}
	//
	// Return success or failure
	if (found && !failed) {
printf ("Found and not failed: SASL_OK\n");
		return SASL_OK;
	} else {
		if (pwmem != NULL) {
			memset (pwmem, 0, 66);
			sparams->utils->free (pwmem);
		}
printf ("Failed or not found: %s\n", failed ? "SASL_FAIL" : "SASL_NOUSER");
		return failed ? SASL_FAIL : SASL_NOUSER;
	}
}


/* free global state for plugin (OPTIONAL) */
void haandb_free (void *glob_context,
			const sasl_utils_t *utils) {
	//TODO// Maybe erase
}


/* Registration structure for the auxprop plugin */
static sasl_auxprop_plug_t auxplug_haan = {
	.features = 0,
	.name = "haandb",
	.auxprop_free = haandb_free,
	.auxprop_lookup = haandb_lookup,
};


/* default name for auxprop plug-in entry point is "sasl_auxprop_init"
 *  similar to sasl_server_plug_init model, except only returns one
 *  sasl_auxprop_plug_t structure;
 */
PLUG_API int sasl_auxprop_plug_init (const sasl_utils_t *utils,
					int max_version,
					int *out_version,
					sasl_auxprop_plug_t **plug,
					const char *_plugname) {
	if (SASL_AUXPROP_PLUG_VERSION > max_version) {
		return SASL_BADVERS;
	} else {
		*out_version = SASL_AUXPROP_PLUG_VERSION;
		*plug = &auxplug_haan;
		return SASL_OK;
	}
}

