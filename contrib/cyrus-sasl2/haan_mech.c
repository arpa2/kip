/* Cyrus-SASL2 mechanism plugin for GS2-HAAN128-GENERATE
 *
 * This mechanism code is used to construct a username and
 * password.  In HAAN, this means that a derivation is
 * performed to compute the passphrase from the username.
 *
 * Given that the user cannot freely choose the username,
 * this is in fact safe.  Still, it is advised that the
 * password never be sent across in plain form, but instead
 * used through challenge/response interactions.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <unistd.h>
#include <fcntl.h>

#include <sasl/sasl.h>
#include <sasl/saslplug.h>
#include <sasl/saslutil.h>

#include <krb5.h>

#include <arpa2/kip.h>


#ifdef WIN32
#define PLUG_API __declspec(dllexport)
#else
#define PLUG_API extern
#endif


#define HAAN128GEN_NAME "GS2-HAAN128-GENERATE"


static const char BASE32 [] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";



/* Storage for the login username and password.  Kept per connection,
 * linked from oparams and removed during connection cleanup.
 */
union haangen_cli_state {
	struct sasl_interact realm_prompt [2];
	char generated_password [66];
};



/* Although Cyrus SASL needs its infrastructure to be setup, the function
 * of this mechanism is really elementary.  This is the protocol:
 *
 * C: n,,r=unicorn.demo.arpa2.org
 * S: <NUL>182a02be50ce603f4730c02816a6442b<NUL>c90def89284fe4e062fd88e28b89e811
 *
 * So, client asks sends a GS2 header without channel binding, and no
 * authorisation name.  In addition, there is a parameter r=<realm> to
 * designate the desired client realm name.
 *
 * The server responds with the format of PLAIN,
 *
 * [authzid] <NUL> authcid <NUL> passwd
 *
 * where we currently have no use for the [authzid] part so we never send it.
 * Note that this is a bit like PLAIN with the initiative reversed.  There
 * will actually only be ASCII codes in each of the strings, because UTF-8
 * adds complexity and still holds only 5 bits per char, as does BASE32.
 *
 * This is a client-first protocol with just one response, so the server
 * can report success immediately, along with additional data.  The glue
 * code may have to split the flow into another exchange if the protocol
 * does not supply additional data, but the mechanism does not care.
 *
 * At the same time, the authenticated identity, consisting of the realm
 * and the username, should be signalled to the server environment.  Funny
 * as it may seem, with nobody else that could claim the new account but
 * the client, and with the client in full control, we can consider the
 * authentication a success.
 */



/* client: create context for mechanism, using params supplied
 *  glob_context   -- from above
 *  params         -- params from sasl_client_new
 *  conn_context   -- context for one connection
 * returns:
 *  SASL_OK        -- success
 *  SASL_NOMEM     -- not enough memory
 *  SASL_WRONGMECH -- mech doesn't support security params
 */
int haangen_cli_new (void *glob_context,
				sasl_client_params_t *cparams,
				void **conn_context) {
	void *ctx = cparams->utils->malloc (sizeof (union haangen_cli_state));
	if (ctx == NULL) {
		return SASL_NOMEM;
	}
	memset (ctx, 0, sizeof (union haangen_cli_state));
	*conn_context = ctx;
	return SASL_OK;
}


/* server: create a new mechanism handler
 *  glob_context  -- global context
 *  sparams       -- server config params
 *  challenge     -- server challenge from previous instance or NULL
 *  challen       -- length of challenge from previous instance or 0
 * out:
 *  conn_context  -- connection context
 *  errinfo       -- error information
 *
 * returns:
 *  SASL_OK       -- successfully created mech instance
 *  SASL_*        -- any other server error code
 */
int haangen_srv_new (void *glob_context,
				sasl_server_params_t *sparams,
				const char *challenge,
				unsigned challen,
				void **conn_context) {
	if (!kip_init ()) {
		return SASL_FAIL;
	}
	*conn_context = "456";
	return SASL_OK;
}


/* server: perform one step in exchange
 *
 * returns:
 *  SASL_OK       -- success, all done
 *  SASL_CONTINUE -- success, one more round trip
 *  SASL_*        -- any other server error code
 */
int haangen_srv_step (void *conn_context,
				sasl_server_params_t *sparams,
				const char *clientin,
				unsigned clientinlen,
				const char **serverout,
				unsigned *serveroutlen,
				sasl_out_params_t *oparams) {
	//
	// Collect success
	bool ok = true;
	//
	// Collect the realm name (TODO: or use it from server_new)
	ok = ok && (clientinlen > 5);
	ok = ok && (strncmp (clientin, "n,,r=", 5) == 0);
	char realm [clientinlen];
	memcpy (realm, clientin + 5, clientinlen - 5);
	realm [clientinlen - 5] = '\0';
	ok = ok && (strchr (realm, '.') != NULL);
	if (!ok) {
		return SASL_BADAUTH;
	}
	//
	// Find the location of the keytab in configvar "haan_keytab"
	char *ktname = NULL;
	ok = ok && (sparams->utils->getopt (conn_context,
			HAAN128GEN_NAME,
			"haan_keytab",
			(const char **) &ktname, NULL) == SASL_OK);
	if (ktname == NULL) {
		ktname = "/etc/arpa2/haan/haan.keytab";
	}
	//
	// Open a KIP context and load the master key from its keytab
	kipt_ctx kip;
	kipt_keyid vhostkey;
	bool ctx = ok && kipctx_open (&kip);
	ok = ok && ctx;
	ok = ok && kipkey_fromvhosttab (kip,
				"haan", realm,
				&vhostkey);
	//
	// Generate the username with redundancy: 128-->160, 256-->320
	// Let's be lazy, ignore 3 bits out of 8 when mapping to BASE32
	uint8_t uid128 [32];
	ok = ok && kipdata_random (kip, sizeof (uid128), uid128);
	//
	// Map to a readable username
	char username [66];
	for (int i = 0; i < sizeof (uid128); i++) {
		username [i] = BASE32 [ uid128 [i] & 31 ];
	}
	username [sizeof(uid128)] = '\0';
	//
	// Produce a password from the readable username.
	// Note that the choice of perturbation is local!
	// We'll be lazy, ignoring 3 bits out of 8 when mapping to BASE32
	uint8_t pwd128 [32];
	ok = ok && kipdata_usermud (kip, vhostkey,
			username, strlen (username),
			pwd128, sizeof (pwd128));
	//
	// Map password to a typeable format
	char password [66];
	for (int i = 0; i < sizeof (pwd128); i++) {
		password [i] = BASE32 [ pwd128 [i] & 31 ];
	}
	password [sizeof(pwd128)] = '\0';
	//
	// Close the KIP context
	if (ctx) {
		kipctx_close (kip);
	}
	//
	// We can use fixed buffers, which then get copied
	char *s2c = sparams->utils->malloc (1024);
	ok = ok && (s2c != NULL);
	unsigned s2clen = 0;
	if (ok) {
		/* We have no use for authzid (yet) */
		s2c [s2clen++] = '\0';
		memcpy (s2c + s2clen, username, strlen (username));
		s2clen += strlen (username);
		s2c [s2clen++] = '\0';
		memcpy (s2c + s2clen, password, strlen (password));
		s2clen += strlen (password);
	}
	//
	// Register the login and acl user identity
	if (ok) {
		/* Ignore output: auxprop lookup is not useful */
		sparams->canon_user (sparams->utils->conn,
                                username, strlen (username),
                                SASL_CU_AUTHID | SASL_CU_AUTHZID,
                                oparams);
	}
	//
	// Hope to return success
	if (ok) {
		*serverout = s2c;
		*serveroutlen = s2clen;
		return SASL_OK;
	} else {
		if (s2c != NULL) {
			sparams->utils->free (s2c);
		}
		*serverout = NULL;
		*serveroutlen = 0;
		return SASL_FAIL;
	}
}


/* perform one step of exchange.  NULL is passed for serverin on
 * first step.
 * returns:
 *  SASL_OK        -- success
 *  SASL_INTERACT  -- user interaction needed to fill in prompts
 *  SASL_BADPROT   -- server protocol incorrect/cancelled
 *  SASL_BADSERV   -- server failed mutual auth
 */
int haangen_cli_step (void *conn_context,
				sasl_client_params_t *cparams,
				const char *serverin,
				unsigned serverinlen,
				sasl_interact_t **prompt_need,
				const char **clientout,
				unsigned *clientoutlen,
				sasl_out_params_t *oparams) {
	//
	// First time, we get no server-side input
	if (serverin == NULL) {
		//
		// Either construct a prompt or process the result
		struct sasl_interact *ia = conn_context;
		if (*prompt_need == NULL) {
			memset (conn_context, 0, sizeof (conn_context));  /* do not leak */
			ia [0].id = SASL_CB_GETREALM;
			ia [0].prompt = "Realm to host your identity: ";
			ia [0].defresult = getenv ("HAAN_REALM");
			ia [1].id = SASL_CB_LIST_END;
			//
			// Request the client realm
			*prompt_need = ia;
			return SASL_INTERACT;
		} else {
			const char *realm   = ia [0].result;
			unsigned rlen = ia [0].len;
			if ((realm == NULL) || (rlen == 0) || (rlen > 255)) {
				return SASL_BADPARAM;
			}
			char *c2s = conn_context;
			unsigned c2slen = 0;
			c2slen = sprintf (c2s, "n,,r=%.*s", rlen, realm);
			//
			// Return the first c2s message
			*clientout    = c2s;
			*clientoutlen = c2slen;
			return SASL_CONTINUE;
		}
	}
	//
	// Second time, we get [authzid] <NUL> loginid <NUL> passwd
	const char *user_acl = serverin;
	int userlen_acl = strnlen (user_acl, serverinlen-2);
	//NOTUSED// user_acl [userlen_acl] = '\0';
	const char *user_login = user_acl + userlen_acl + 1;
	int userlen_login = strnlen (user_login, serverinlen-2-userlen_acl);
	//NOTUSED// user_login [userlen_login] = '\0';
	const char *passwd = user_login + userlen_login + 1;
	int passwdlen = serverinlen-2-userlen_acl-userlen_login;
	//
	// Sanity checking on sizes
	assert (userlen_login <= 64);
	assert (passwdlen     <= 64);
	//
	//TODO// Debug statement leaks credential
	printf ("\n*\n* Freshly baked account arrival!\n*\n* User: %.*s\n* Pass: %.*s\n*\n\n", userlen_login, user_login, passwdlen, passwd);
	//
	// Copy the password into the connection context
	union haangen_cli_state *clistate = conn_context;
	memcpy (clistate->generated_password, passwd, passwdlen);
	clistate->generated_password [passwdlen] = '\0';
	//
	// Make available to sasl_getprop (...SASL_DELEGATEDCREDS...)
	oparams->client_creds = clistate->generated_password;
	printf ("Client credentials set at %p\n", oparams->client_creds);
	//
	// Register the login and acl user identity
	int res = cparams->canon_user (cparams->utils->conn,
                                user_login,
                                userlen_login,
                                SASL_CU_AUTHID | SASL_CU_AUTHZID,
                                oparams);
	printf ("After canon_user(), user=%s, authid=%s\n", oparams->user, oparams->authid);
	oparams->doneflag = 1;
	//
	// This is the final message -- return nothing to the server
	*clientout = NULL;
	*clientoutlen = 0;
	//
	// Hope to return SASL_OK
        return res;
}


/* client: dispose of connection context from mech_new
 */
void haangen_cli_dispose (void *conn_context,
				const sasl_utils_t *utils) {
	//
	// Clear the connection context state, then free memory
	memset (conn_context, 0, sizeof (union haangen_cli_state));  /* do not leak */
	utils->free (conn_context);
}


/* server: dispose of a connection state
 */
void haangen_srv_dispose (void *conn_context,
				const sasl_utils_t *utils) {
	;
}


/* sever: free global state for mechanism
 *  mech_dispose must be called on all mechanisms first
 */
void haangen_srv_free (void *glob_context,
				const sasl_utils_t *utils) {
	kip_fini ();
}


/* client: free all global space used by mechanism
 *  mech_dispose must be called on all mechanisms first
 */
void haangen_cli_free (void *glob_context,
				const sasl_utils_t *utils) {
	;
}



/* The structure with the internal client API for Cyrus SASL2.
 */
static struct sasl_client_plug haangen_cli_plug_init = {
	.mech_name = HAAN128GEN_NAME,
	.max_ssf = 0,
	.security_flags = SASL_SEC_NOANONYMOUS | SASL_SEC_PASS_CREDENTIALS,
	.features = SASL_FEAT_WANT_CLIENT_FIRST,
	.mech_new = haangen_cli_new,
	.mech_step = haangen_cli_step,
	.mech_dispose = haangen_cli_dispose,
	.mech_free = haangen_cli_free,
	// .user_query = haangen_cli_userquery,
	// .mech_avail = haangen_cli_mechavail,
};


/* The structure with the internal server API for Cyrus SASL2.
 */
static struct sasl_server_plug haangen_srv_plug_init = {
	.mech_name = HAAN128GEN_NAME,
	.max_ssf = 0,
	.security_flags = SASL_SEC_NOANONYMOUS | SASL_SEC_PASS_CREDENTIALS,
	.features = SASL_FEAT_WANT_CLIENT_FIRST,
	.mech_new = haangen_srv_new,
	.mech_step = haangen_srv_step,
	.mech_dispose = haangen_srv_dispose,
	.mech_free = haangen_srv_free,
};



/* plug-in entry point:
 *  utils       -- utility callback functions
 *  max_version -- highest client plug version supported
 * returns:
 *  out_version -- client plug version of result
 *  pluglist    -- list of mechanism plug-ins
 *  plugcount   -- number of mechanism plug-ins
 * results:
 *  SASL_OK       -- success
 *  SASL_NOMEM    -- failure
 *  SASL_BADVERS  -- max_version too small
 *  SASL_BADPARAM -- bad config string
 *  ...
 */
PLUG_API int sasl_client_plug_init (const sasl_utils_t *utils,
				    int max_version,
				    int *out_version,
				    sasl_client_plug_t **pluglist,
				    int *plugcount) {
	if (SASL_CLIENT_PLUG_VERSION > max_version) {
		return SASL_BADVERS;
	} else {
		*out_version = SASL_CLIENT_PLUG_VERSION;
		*pluglist = &haangen_cli_plug_init;
		*plugcount = 1;
		return SASL_OK;
	}
}


/* plug-in entry point:
 *  utils         -- utility callback functions
 *  plugname      -- name of plug-in (may be NULL)
 *  max_version   -- highest server plug version supported
 * returns:
 *  out_version   -- server plug-in version of result
 *  pluglist      -- list of mechanism plug-ins
 *  plugcount     -- number of mechanism plug-ins
 * results:
 *  SASL_OK       -- success
 *  SASL_NOMEM    -- failure
 *  SASL_BADVERS  -- max_version too small
 *  SASL_BADPARAM -- bad config string
 *  ...
 */
PLUG_API int sasl_server_plug_init (const sasl_utils_t *utils,
					int max_version,
					int *out_version,
					sasl_server_plug_t **pluglist,
					int *plugcount) {
	if (SASL_SERVER_PLUG_VERSION > max_version) {
		return SASL_BADVERS;
	} else {
		*out_version = SASL_SERVER_PLUG_VERSION;
		*pluglist = &haangen_srv_plug_init;
		*plugcount = 1;
		return SASL_OK;
	}
}

