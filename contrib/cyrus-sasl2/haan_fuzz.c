/* Fuzzy match to go from authentication/login user to authorisation/acl user.
 *
 * Authentication yields an identity which, given a HAAN identity form,
 * is known to represent a dignified level of entropy.  When used as a
 * Helm identity, this form may not have been tested before, and when
 * it was entered with typing errors this can be devestating.
 *
 * The idea of haan_fuzz is to allow modifications to the original form
 * by allowing corrections within a limited editing distance to come from
 * the login user to the acl user.  This sacrifices some of the security
 * level of the original form, but when this has extra bits there can
 * still be enough left for a desired 128 bit (or 256 bit) security level.
 *
 * This routine is about measuring the entropy lost during the edits that
 * change an identity from login user to acl user, and seeing how much of
 * the original entropy remains.  If this suffices, then the acl user
 * will be reported as a successfully establised identity, instead of
 * the login user that would normally be the same in HAAN.
 *
 * Edit distance is measured as Damerau-Levenshtein distance, with our
 * own cost for various operations, but generally allowing:
 *
 *  - adding      a char  --> position, code?
 *  - dropping    a char  --> position?
 *  - substitute  a char  --> position, code?
 *  - swap adjacent chars --> position?
 *
 * We do not actually encode these operations but we need to count the
 * number of bits taken to represent them compactly.  This is the loss
 * of entropy by such operations.  Considering that the login user and
 * its password must still be entered accurately, the only value in
 * this is to correct for Helm Identities that were registered with a
 * few typing errors in them.  This is a niche, but an important one.
 *
 * Sources used:
 *  - https://www.lemoda.net/text-fuzzy/damerau-levenshtein/
 *  - https://gist.github.com/badocelot/5331587
 *  - https://en.wikipedia.org/wiki/Damerau%E2%80%93Levenshtein_distance
 *    [the latter included in original form in badocelet_damlevdist.c]
 *
 * The code below started off as the implementation described here,
 * which was available in the public domain and hereby acknowledged.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <assert.h>

#include <sasl/sasl.h>

#ifdef DEBUG
#include <stdio.h>
#endif



/* Supported operations and their cost in bits.

   Short forms for substitution with a character on a key left/right

   The implementation does not just swap, but it can add or drop
   characters.  The computed cost is the sum of these values.

   With up to 64 positions and allowing at least 4 changes, the estimate
   for a position is that it spreads in the code and occupies 4 bits on
   average.

   If BASE32 encoding is assumed, codes would occupy 5 bits.

    - cmd_add           = 00 :2, position:4, code:5
    - cmd_sub           = 01 :2, position:4, code:5
    - cmd_sub_key_left  = 100:3, position:4
    - cmd_sub_key_right = 101:3, position:4
    - cmd_drop          = 110:3, position:4
    - cmd_swap          = 111:3, position:4

   With 11 bits for the longest commands and a minimum of 4 changes,
   the additional entropy should be at least 44 bits.  This is even
   possible when encoding 256 bits into 64 characters of BASE32, in
   which 64 bits are spare.

   Note that all commands are longer than the 5 bits for a BASE32
   character.  Even when everything is filled with character drops,
   then it is not possible to remove all of the added entropy.  This
   means that the power of the extra command bits is not so strong
   that it could replace a considerable part of the message with an
   arbitrary other code, or remove more of it than desired.  This is
   why the focus on character editing is a good choice here, and no
   commands should be permitted to range over many positions.

   The code has been compressed somewhat for longer commands, to
   reduce the asymmetry between adding and dropping characters.
   The cmd_sub_key_left and _right look at the US keyboard key on the
   left and right, respectively, and subtitutes that character in
   less bits than otherwise required.  This can help to capture more
   typing errors while retaining sufficient entropy.  Other keyboard
   layouts might be created for other servers.
 */

static const unsigned cost_add           = 2 + 4 + 5;
static const unsigned cost_sub           = 2 + 4 + 5;
static const unsigned cost_sub_key_left  = 3 + 4;
static const unsigned cost_sub_key_right = 3 + 4;
static const unsigned cost_drop          = 3 + 4;
static const unsigned cost_swap          = 3 + 4;

// static const unsigned cost_add           = 1;
// static const unsigned cost_sub           = 1;
// static const unsigned cost_sub_key_left  = 1;
// static const unsigned cost_sub_key_right = 1;
// static const unsigned cost_drop          = 1;
// static const unsigned cost_swap          = 1;



static unsigned cost_substitute (unsigned ch_l, unsigned ch_a) {
	/* The keys on a keyboard that are neighbours on the US keyboard
	 * are separated by 4 in the following string.  There is a prefix
	 * and trailer of 4 characters that cannot be found after the
	 * customary application of toupper() which is assumed to have
	 * been applied to both characters.
	 */
	static const unsigned char keylist_us [] = "xxxx1QAZ2WSX3EDC4RFV5TGB6YHN7UJM8IK,9OL.0P;/xxxx";

	//
	// Are the characters the same?
	if (ch_l == ch_a) {
		return 0;
	}
	//
	// Are the characters O/0 or I/1 equivalents?
	if ((ch_l == 'O') && (ch_a == '0')) {
		return 0;
	}
	if ((ch_l == 'I') && (ch_a == '1')) {
		return 0;
	}
	//
	// Are the characters keyboard neighbours?
	char *ptr_l = strchr ((char *)keylist_us, ch_l);  // keylist_us is 8-bit-clean
	if (ptr_l != NULL) {
		if (ptr_l [ 4] == ch_a) {
			return cost_sub_key_left;
		}
		if (ptr_l [-4] == ch_a) {
			return cost_sub_key_right;
		}
	}
	//
	// General substitution
	return cost_sub;
}

static unsigned cost_swap_reach_over (int rowdiff, int coldiff) {
	unsigned retval = cost_swap;
	retval += rowdiff * cost_drop;
	retval += coldiff * cost_add;
	return retval;
}


/* Damerau-Levenshtein edit distance code, based on code and explanations
 * by badocelot, see file header.
 *
 * The first ptr,len (or matrix row) represents the authenticated user_login
 * The first ptr,len (or matrix col) represents the requested     user_acl
 *
 * Incrementing the row but not the col constitutes deletion.
 * Incrementing the col but not the row constitutes addition.
 * The distinction matters, given that the weights are different.
 *
 * Changes for SASL and HAAN:
 *
 *  - Adapted for SASL, assuming non-NULL pointers, known string length.
 *  - Modern C version, with stack allocation for an array up to 66*66.
 *  - Added weighting with cost_xxx from above.
 *  - User names up to  64 chars, as pointer,length
 *  - Since I will need to maintain it, applied my spacing zeal
 */
int haanfuzz_distance (const char *user_login, unsigned userlen_login,
                              const char *user_acl  , unsigned userlen_acl) {
	//
	// Size of the strings and dimensions of the matrix
	assert (user_login != NULL);
	assert (user_acl   != NULL);
	assert ((userlen_login > 0) && (userlen_login <= 64));
	assert ((userlen_acl   > 0) && (userlen_acl   <= 64));
	//
	// If either is NULL or of zero length, there's no real work to be done: the
	// cost is just the length of the other.
	if (userlen_login == 0) {
		return userlen_acl;
	}
	if (userlen_acl == 0) {
		return userlen_login;
	}
	//
	// Set up the table
	int matrix [2+userlen_login] [2+userlen_acl];
	//
	// Initialize the table similar to Wagner-Fischer, with an extra
	// row and column with higher-than-possible costs to prevent
	// erroneous detection of swaps that would go outside the bounds
	// of the strings.
	const int INF = 48000;
	matrix [0] [0] = INF;
	for (int row = 0; row <= userlen_login; row++) {
		matrix [1+row] [0] = INF;
		matrix [1+row] [1] = row * cost_drop;
	}
	for (int col = 0; col <= userlen_acl; col++) {
		matrix [0] [1+col] = INF;
		matrix [1] [1+col] = col * cost_add;
	}
	//
	// This lets us store and look up the row where a given character last
	// appeared in `login_user` -- changing this to a map type might be
	// useful if GLib or a similar library is available to you.
	int last_row [256];
	//
	// Zero here denotes that the character has not be seen in `login_user`
	// yet, because zero is the "infinity" column
	for (int d = 0; d < 256; d++) {
		last_row [d] = 0;
	}
	//
	// Fill out the table
	for (int row = 1; row <= userlen_login; row++) {
		//
		// The current character in `user_login`; this and `ch_a` below
		// both have to to be unsigned so that they can be used as an
		// index to last_row no matter what their values
		unsigned char ch_l = toupper (user_login [row-1]);
		//
		// The last column this row where the character in `user_login`
		// matched the character in `user_acl`; as with last_row, zero
		// denotes no match yet
		int last_match_col = 0;
		//
		for (int col = 1; col <= userlen_acl; col++) {
			//
			// The current character in `user_acl`
			unsigned char ch_a = toupper (user_acl [col-1]);
			//
			// The last place in `user_login` where we can find
			// the current character in `user_acl`
			int last_matching_row = last_row [ch_a];
			//
			// When the characters are the same, set cost_base
			// to 0.  Otherwise try cost_sub_key_left/right and
			// fallback to cost_sub if none of these apply.
			int cost_base = cost_substitute (ch_l, ch_a);
			int dist_base = matrix [row  ] [col  ] + cost_base;
			//
			// Calculate the distances of all possible operations
			int dist_del  = matrix [row  ] [col+1] + cost_drop;
			int dist_add  = matrix [row+1] [col  ] + cost_add;
			//
			// This took me a while to figure out. What this
			// calculates is the cost of a swap between the current
			// character in `user_acl` and the last character found
			// in both strings.
			//
			// All characters between these two are treated as
			// either additions or deletions.
			//
			// NOTE: Damerau-Levenshtein allows for either additions
			// OR deletions between the swapped characters, NOT both.
			// This is not explicitly prevented, but if both
			// additions and deletions would be required between
			// swapped characters -- this is if neither
			//
			//     `(row - last_matching_row - 1)`
			//
			// nor
			//
			//     `(col - last_match_col - 1)`
			//
			// is zero -- then adding together both addition and
			// deletion costs will cause the total cost of a
			// swap to become higher than any other operation's
			// cost.
			int dist_trans = matrix [last_matching_row] [last_match_col]
						+ cost_swap_reach_over (
							(row - last_matching_row - 1),
							(col - last_match_col - 1));
			//
			// Find the minimum of the distances
			int min = dist_base;
			char *src = "base";
			if (dist_add < min) {
				min = dist_add;
				src = "add";
			}
			if (dist_del < min) {
				min = dist_del;
				src = "del";
			}
			if (dist_trans < min) {
				min = dist_trans;
				src = "swap";
			}
			//
			// Store the minimum as the cost for this cell
			#ifdef DEBUG
			printf ("%d/%s\t", min, src);
			#endif
			matrix [row+1] [col+1] = min;
			//
			// If there was a match, update the last matching
			// column
			if (cost_base == 0) {
				last_match_col = col;
			}
		}
		#ifdef DEBUG
		printf ("\n");
		#endif
		//
		// Update the entry for this row's character
		last_row [ch_l] = row;
	}
	//
	// Extract the bottom right-most cell -- that's the total distance
	int result = matrix [userlen_login+1] [userlen_acl+1];
	//
	// No cleanup, done.
	return result;
}


/* Measure the amount of extra entropy, assuming BASE32 encoding of an
 * identity.  This only works for 128 bit and 256 bit entropy bases,
 * which may not expand beyond 160 and 320 bits, respectively.
 */
int haanfuzz_entropy_extra (const char *username, unsigned ulen) {
	unsigned entropy = 0;
	//
	// Count the entropy in BASE32 characters
	for (int i=0; i < ulen; i++) {
		if (isalnum (username [i])) {
			entropy += 5;
		}
	}
	//
	// Return the entropy spare for 128 bits (up to 160 bits)
	// The maximum is a rounded figure in BASE32 encoding.
	if ((entropy >= 128) && (entropy <= 160 + 4)) {
		return entropy - 128;
	}
	//
	// Return the entropy spare for 256 bits (up to 320 bits)
	// The maximum is a rounded figure in BASE32 encoding.
	if ((entropy >= 256) && (entropy <= 320 + 4)) {
		return entropy - 256;
	}
	//
	// Other sizes are not expected, and may indicate abuse
	return -1;
}


/* A possible Cyrus SASL2 callback interface function for fuzzing the
 * mapping from an login user to an acl user.  In <sasl/sasl.h> we have:
 *
 * improved callback to verify authorization;
 *     canonicalization now handled elsewhere
 *  conn           -- connection context
 *  requested_user -- the identity/username to authorize (NUL terminated)
 *  rlen           -- length of requested_user
 *  auth_identity  -- the identity associated with the secret (NUL terminated)
 *  alen           -- length of auth_identity
 *  default_realm  -- default user realm, as passed to sasl_server_new if
 *  urlen          -- length of default realm
 *  propctx        -- auxiliary properties
 * returns SASL_OK on success,
 *         SASL_NOAUTHZ or other SASL response on failure
 *
   typedef int sasl_authorize_t (sasl_conn_t *conn,
                                 void *context,
                                 const char *requested_user, unsigned rlen,
                                 const char *auth_identity, unsigned alen,
                                 const char *def_realm, unsigned urlen,
                                 struct propctx *propctx);
   #define SASL_CB_PROXY_POLICY 0x8001
 */
int haanfuzz_authorize (sasl_conn_t *conn, void *context,
				const char *requested_user, unsigned rlen,
				const char *auth_identity,  unsigned alen,
				const char *def_realm,      unsigned urlen,
				struct propctx *propctr) {
	//
	// Remove the realm from the end of the requested_user, if present
	if ((alen > urlen) && (requested_user [rlen-urlen-1] == '@')) {
		if (memcmp (requested_user+rlen-urlen, def_realm, urlen) != 0) {
			return -1;
		}
		rlen -= 1 + urlen;
	}
	//
	// Remove the realm from the end of the auth_identity, if present
	if ((alen > urlen) && (auth_identity  [alen-urlen-1] == '@')) {
		if (memcmp (auth_identity +alen-urlen, def_realm, urlen) != 0) {
			return -1;
		}
		alen -= 1 + urlen;
	}
	//
	// Be sure that there is extra entropy in the login user (auth_id)
	int xtra = haanfuzz_entropy_extra (auth_identity,  alen);
	if (xtra < 0) {
		return SASL_BADPARAM;
	}
	//
	// Be sure that the acl user (requested_user) is not short on entropy
	int isok = haanfuzz_entropy_extra (requested_user, rlen);
	if (isok < 0) {
		return SASL_BADPARAM;
	}
	//
	// Compute the Damerau-Levenshtein distance and check it is ok
	int dist  = haanfuzz_distance (auth_identity, alen, requested_user, rlen);
	if (dist > xtra) {
		return SASL_NOAUTHZ;
	}
	//
	// None of our precautions failed -- so we succeed
	return SASL_OK;
}

