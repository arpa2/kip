/* haan_generate.c -- FastCGI program to generate HAAN credentials.
 *
 * This program outputs a simple web document that can be used in
 * surrounding code.  JSON output can be useful for JavaScript sites,
 * while plaintext can be used for inline information.
 *
 * The program does not repeat the hostname, as that would make it
 * a responsibility for the recipient to verify it.  The hostname is
 * best learnt from the same source that produces it for the URI,
 * where it is validated via HTTPS.  Do not use HAAN without HTTPS.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <fcgi_stdio.h>

#include <stdlib.h>
#include <stdbool.h>

#undef  LOG_STYLE
#define LOG_STYLE LOG_STYLE_SYSLOG

#include <arpa2/kip.h>
#include <arpa2/except.h>


/* The sequence of uppercase BASE32 characters.
 */
static const char BASE32 [] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";


/* Generate fresh HAAN credentials for the requested server_name.
 * Write to the username and password, which are 66 bytes long.
 * Upon successful/true return, these fields are filled with a
 * NUL termination character.
 *
 * TODO: channel_binding is not incorporated yet.
 * TODO: quantum_proof   is not incorporated yet.
 */
bool haan_generate (const char server_name [66], char username [66], char *password) {
	bool ok = true;
	//
	// Open a KIP context and load the master key from its keytab
	kipt_ctx kip;
	kipt_keyid vhostkey;
	bool ctx = ok && kipctx_open (&kip);
	ok = ok && ctx;
	ok = ok && kipkey_fromvhosttab (kip,
				"haan", server_name,
				&vhostkey);
	//
	// Generate the username with redundancy: 128-->160, 256-->320
	// Let's be lazy, ignore 3 bits out of 8 when mapping to BASE32
	uint8_t uid128 [32];
	ok = ok && kipdata_random (kip, sizeof (uid128), uid128);
	//
	// Map to a readable username
	for (int i = 0; i < sizeof (uid128); i++) {
		username [i] = BASE32 [ uid128 [i] & 31 ];
	}
	username [sizeof(uid128)] = '\0';
	//
	// Produce a password from the readable username.
	// Note that the choice of perturbation is local!
	// We'll be lazy, ignoring 3 bits out of 8 when mapping to BASE32
	uint8_t pwd128 [32];
	ok = ok && kipdata_usermud (kip, vhostkey,
			username, strlen (username),
			pwd128, sizeof (pwd128));
	//
	// Map password to a typeable format
	for (int i = 0; i < sizeof (pwd128); i++) {
		password [i] = BASE32 [ pwd128 [i] & 31 ];
	}
	password [sizeof(pwd128)] = '\0';
	//
	// Close the KIP context
	if (ctx) {
		kipctx_close (kip);
	}
	//
	// Return success or failure
	return ok;
}


/* Process a request.  Check the input and produce an error or
 * a user/pass combination for the requesting host.
 */
bool process_request (void) {
	bool ok = true;
	//
	// Retrieve envvars
	char *content_type    = getenv ("CONTENT_TYPE");
	char *server_name     = getenv ("SERVER_NAME");
	char *channel_binding = getenv ("CHANNEL_BINDING");
	char *quantum_proof   = getenv ("QUANTUM_PROOF");
	//
	// Parse the content type somewhat
	bool json = false;
	if (content_type == NULL) {
		log_error ("Missing CONTENT_TYPE variable");
		ok = false;
	} else if (strstr (content_type, "text/plain") != NULL) {
		json = false;
	} else if (strstr (content_type, "application/json") != NULL) {
		json = true;
	} else if (strstr (content_type, "text/*") != NULL) {
		json = false;
	} else {
		log_error ("Neither text/plain nor application/json in CONTENT_TYPE %s", content_type);
		ok = false;
	}
	if (!ok) {
		content_type = "text/plain";
	}
	//
	// Check the server_name
	if (server_name == NULL) {
		log_error ("Missing SERVER_NAME variable");
		ok = false;
	} else
	if (strchr (server_name, '.') == NULL) {
		log_error ("Invalid SERVER_NAME value \"%s\"", server_name);
		ok = false;
	}
	//
	// Check channel binding (pass for now)
	;
	//
	// Check Quantum Proof -- for now it must be 0
	if ((quantum_proof == NULL) || (quantum_proof [0] != '0') || (quantum_proof [1] != '\0')) {
		log_error ("For now, you must set QUANTUM_PROOF to \"0\"");
		ok = false;
	}
	//
	// Okay so far?  Then produce a username and password
	char username [66] = { 0 };
	char password [66] = { 0 };
	if (ok) {
		ok = ok && haan_generate (server_name, username, password);
	}
	if (!ok) {
		*username = *password = '\0';
	}
	//
	// Produce the output
	printf ("Content-type: %s; charset=UTF-8\n", content_type);
	printf ("\n");
	printf (json ? "{\"username\":\"%s\",\"password\":\"%s\"}\n" : "Username: %s\nPassword: %s\n", username, password);
	//
	// Return success or failure
	return ok;
}


/* Main routine.  Accept request(s) and process each.
 */
int main (int argc, char *argv []) {
	//
	// Initialise
	assertxt (kip_init (), "Failed to initialise KIP library");
	//
	// Fetch the/a request
	while (FCGI_Accept () >= 0) {
		if (!process_request ()) {
			FCGI_SetExitStatus (500);
		}
	}
	//
	// Cleanup and return
	kip_fini ();
	exit (0);
}
